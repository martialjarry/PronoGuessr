@ECHO OFF
SET /P confirm=*** ATTENTION *** Tu t'appretes a effacer le score de tous les Paniacos ! Tu veux vraiment continuer ? (oui/non) 
IF "%confirm%"=="oui" GOTO reset_scores
GOTO End
:reset_scores
echo Creation d'une backup puis reinitialisation des scores...
node js/scores_cleaner.js
pause
exit
:End
ECHO Les scores n'ont pas ete effaces
pause
exit