class PronoCounter {
	constructor(referenceRefreshRate) {
		this.currentCount = 0;
        this.maxCount = 0;
        
		this.currentTempCounts = [];
        this.maxTempCount = 0;
        
        this.tempCountResetTimes = [];
        
        this.referenceRefreshRate = referenceRefreshRate;
        var refreshPeriod = 1 / this.referenceRefreshRate;

        var currentDate = new Date();
        var currentTime = currentDate.getTime();

        for (let i = 0; i < this.referenceRefreshRate; i++) {
            this.currentTempCounts.push(0);
            this.tempCountResetTimes.push(currentTime + (refreshPeriod * 1000) * i);
        }
    }
    
    incrementCount() {
        this.currentCount++;

        if(this.currentCount > this.maxCount) {
            this.maxCount = this.currentCount;
        }
    }

	resetCount() {
		this.currentCount = 0;
    }
    
    incrementTempCounts() {
        for (var i = 0; i < this.currentTempCounts.length; i++) {
            this.currentTempCounts[i]++;

            var currentTempCount = this.currentTempCounts[i];

            if(currentTempCount > this.maxTempCount) {
                this.maxTempCount = currentTempCount;
            }
        }

    }

    resetTempCounts(forceResetAll) {
        var currentDate = new Date();
        var currentTime = currentDate.getTime();

        for(var i = 0; i < this.currentTempCounts.length; i++) {
            var lastCounterResetTime = this.tempCountResetTimes[i];

            if(forceResetAll) {
                this.currentTempCounts[i] = 0;
            }

            if(currentTime - lastCounterResetTime >= 1000) {
                this.currentTempCounts[i] = 0;
                this.tempCountResetTimes[i] += 1000;
            }
        }
    }

    toJson() {
        var currentRate = this.tempCountsToRate();
        var maxRate = this.maxTempCount;

        var jsonText = "{'current_count': " + this.currentCount + ", ";
        jsonText    +=  "'max_count': " + this.maxCount + ", ";
        jsonText    +=  "'current_rate': " + currentRate + ", ";
        jsonText    +=  "'max_rate': " + maxRate + "}";
        return jsonText;
    }

    tempCountsToRate() {
        var totalCount = 0;

        this.currentTempCounts.forEach(currentTempCount => {
            totalCount += currentTempCount;
        });

        return totalCount / this.currentTempCounts.length;
    }
}

exports.PronoCounter = PronoCounter;
