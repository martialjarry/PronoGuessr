var mongoose = require('mongoose');

// Structure d'un pays dans la base de données
var betDistrictSchema = new mongoose.Schema({
	district: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'District'
	},

	author_player: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Player'
	},

	session: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Session'
	},

	was_correct: {
		type: Boolean,
		default: 0
	},

	date: {
		type: Date,
		default: null
	}
});

var betDistrictModel = mongoose.model('Bet_district', betDistrictSchema);

module.exports = betDistrictModel;