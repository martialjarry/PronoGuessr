var mongoose = require('mongoose');

var discoveredDistrictSchema = new mongoose.Schema({
	district: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'District'
	},
	
	date: {
		type: Date,
		default: Date.now
	}
});

var discoveredDistrictModel = mongoose.model('Discovered_district', discoveredDistrictSchema);

module.exports = discoveredDistrictModel;