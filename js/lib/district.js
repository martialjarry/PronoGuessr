var mongoose = require('mongoose');

// Structure d'un pays dans la base de données
var districtSchema = new mongoose.Schema({
	name: {
		type: String,
		default: null
	},

	odds: {
		type: String,
		default: "0"
	},

	is_enabled: {
		type: Boolean,
		default: true
	}
});

var districtModel = mongoose.model('District', districtSchema);

module.exports = districtModel;