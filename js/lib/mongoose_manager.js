// Importation de la bibliothèque Mongoose permettant d'intéragir avec la base
// de données
const mongoose = require('mongoose');

class MongooseManager {
	constructor () {
		this.player = require("./player.js");

		this.district = require("./district.js");
		this.betDistrict = require("./bet_district.js");
		this.discoveredDistrict = require("./discovered_district.js");

		this.session = require("./session.js");
		
		this.mongooseClient = null;
	}

	openConnection(databasePath, callback) {
		// Connexion à la base de données mLab (ou MongoDB)
		this.mongooseClient = mongoose.connect(databasePath, { useNewUrlParser: true }, (error) => {
			if(error) {
				console.error("Impossible de se connecter à la base de données :\n" + error);
			} else {
				if(callback) {
					callback();
				}
			}
		});
	}
	
	closeConnection() {
		if(this.mongooseClient) {
			mongoose.disconnect();
			this.mongooseClient = null;
		}
	}

	/**
	 * Récupère les données de tous les joueurs depuis la base de données, classées
	 * par le score des joueurs.
	 * @param {Function} callback - Fonction à appeler une fois que la récupération
	 *                              récupération a été effectuée. Cette fonction un
	 *                              tableau de tous les joueurs trouvés.
	 */
	getAllPlayers(callback) {
		if(this.mongooseClient) {
			this.player.find({}, (error, playerDocs) => {
				if(error != null) {
					console.error("Erreur lors de la récupération de tous les joueurs : " + error);
				} else {
					if(callback) {
						callback(playerDocs);
					}
				}
			}).sort( { 'score': -1 } );
		}
	}
	
	getAllPlayingPlayers(callback) {
		if(this.mongooseClient) {
			this.player.find({ "prognosis.0": { "$exists": true } }, (error, playerDocs) => {
				if(error != null) {
					console.error("Erreur lors de la récupération de tous les joueurs : " + error);
				} else {
					if(callback) {
						callback(playerDocs);
					}
				}
			}).sort( { 'score': -1 } );
		}
	}
	
	/**
	 * Récupère les données d'un joueur à partir de son nom depuis la base de
	 * données MongoDB.
	 * @param {String}   playerName - Nom du joueur à récupérer.
	 * @param {Function} callback   - Fonction à appeler une fois que la
	 *                                récupération a été effectuée. Cette
	 *                                fonction contient le joueur trouvé.
	 */
	getPlayerByName(playerName, callback) {
		if(this.mongooseClient) {
			var formattedPlayerName = this.applyPlayerDefaultRegexp(playerName);
			
			var updateFilter = { 'name': formattedPlayerName };

			var updateValues = {
				name: playerName,
				$inc: { score: 0 }
			};

			var updateOptions = {
				upsert: true,
				etDefaultsOnInsert: true,
				useFindAndModify: false,
				new: true
			};
			
			this.player.findOneAndUpdate(updateFilter, updateValues,  updateOptions, (error, playerDoc) => {
				if (error) {
					console.error("Erreur lors de la récupération d'un joueur : ", error);
				} else {
					if(callback) {
						callback(playerDoc);
					}
				}
			});
		}
	}
	
		/**
		 * Met à jour le score d'un joueur et créé une nouvelle entrée si le joueur
		 * n'est pas dans la base de données. Déclenche la fermeture du script si
		 * tous les joueurs ont été traités.
		 * @param {JSON} 	 queryArguments - Arguments de la requête, définissant
		 *                                	  si le joueur doit être ajouté ou
		 *                                 	  simplement mis à jour.
		 * @param {Function} callback		- Fonction à appeler une fois que la
		 *                                    mise à jour a été effectuée. Cette
		 *                                    fonction contient un rapport sur la
		 *                                    mise à jour.
		 */
	updatePlayer(playerName, updateInstruction, enableUpsert, callback) {
		if(this.mongooseClient) {
			this.player.collection.findOneAndUpdate(playerName, updateInstruction, { upsert: enableUpsert }, (error, updateReport) => {
				if (error) {
					console.error("Erreur lors de la mise à jour des joueurs : \"" + updateReport + "\" : " + error);
				} else {
					if(callback) {
						callback(updateReport);
					}
				}
			});
		}
	}
	
	/* Récupère les données de tous les districts depuis la base de données.
	 * @param {Function} callback - Fonction à appeler une fois que la
	 *                              récupération a été effectuée. Cette
	 *                              fonction un tableau de tous les districts
	 *                              trouvés.
	 */
	getAllDistricts(callback) {
		if(this.mongooseClient) {
			this.district.find({}, (error, districtDocs) => {
				if(error != null) {
					console.error("Erreur lors de la récupération de tous les districts : " + error);
				} else {
					if(callback) {
						callback(districtDocs);
					}
				}
			});
		}
	}
	
	/**
	 * Récupère les données d'un district à partir de son ID depuis la base de
	 * données MongoDB.
	 * @param {String}   districtId - Identifiant unique du district à
	 *                                récupérer.
	 * @param {Function} callback   - Fonction à appeler une fois que la
	 *                                récupération a été effectuée. Cette
	 *                                fonction contient le district trouvé.
	 */
	getDistrictById(districtId, callback) {
		if(this.mongooseClient) {
			// Filtre permettant d'obtenir un district à partir de son ID
			var searchFilter = { '_id': districtId };
	
			// Recherche du district à partir de la requête indiquée et
			// déclenchement de la callback une fois l'action terminée
			this.district.findOne(searchFilter, (error, districtDoc) => {
				if (error) {
					console.error("Erreur lors de la recherche d'un district : " + error);
				} else {
					if(callback) {
						callback(districtDoc);
					}
				}
			});
		}
	}
	
	getDistrictsByIds(districtIds, callback) {
		if(this.mongooseClient) {
			if(districtIds != null) {
				var districtIdsJson = [];
				
				districtIds.forEach((districtId) => {
					var districtIdJson = new mongoose.ObjectId(districtId).path;
					districtIdsJson.push(districtIdJson);
				});
				
				// Requête permettant d'obtenir un district à partir de son nom
				var searchFilter = { _id: { $in: districtIdsJson } };
		
				// Recherche du district à partir de la requête indiquée et
				// déclenchement de la callback une fois l'action terminée
				this.district.find(searchFilter, (error, districtDocs) => {
					if (error) {
						console.error("Erreur lors de la recherche des districts : " + error);
					} else {
						if(callback) {
							callback(districtDocs);
						}
					}
				});
			} else {
				if(callback) {
					callback([]);
				}
			}
		}
	}
	
	/**
	 * Récupère les données d'un district à partir de son nom depuis la base de
	 * données MongoDB.
	 * @param {String}   districtName - Nom du district à récupérer.
	 * @param {Function} callback 	  - Fonction à appeler une fois que la
	 *                                  récupération a été effectuée. Cette
	 *                                  fonction contient le district trouvé.
	 */
	getDistrictByName(districtName, callback) {
		if(this.mongooseClient) {
			var formattedDistrictName = this.applyDistrictDefaultRegexp(districtName);
			
			if(formattedDistrictName) {
				// Requête permettant d'obtenir un district à partir de son nom
				var searchFilter = { 'name': { $regex: formattedDistrictName } };
		
				// Recherche du district à partir de la requête indiquée et
				// déclenchement de la callback une fois l'action terminée
				this.district.findOne(searchFilter, (error, districtDoc) => {
					if (error) {
						console.error("Erreur lors de la recherche d'un district : " + error);
					} else {
						if(callback) {
							callback(districtDoc);
						}
					}
				});
			} else {
				console.error("Ditrict " + districtName + " non reconnu.");
			}
		}
	}
	
	getDistrictsByNames(districtNames, callback) {
		if(this.mongooseClient) {
			var formattedDistrictNames = [];
			
			var currentInstance = this;
			districtNames.forEach((districtName) => {
				var formattedDistrictName = currentInstance.applyDistrictDefaultRegexp(districtName);
				
				if(formattedDistrictName) {
					formattedDistrictNames.push(formattedDistrictName);
				} else {
					console.error("Ditrict " + districtName + " non reconnu.");
				}
			});
						
			// Requête permettant d'obtenir un district à partir de son nom
			var searchFilter = { 'name': { '$in': formattedDistrictNames } };
	
			// Recherche du district à partir de la requête indiquée et
			// déclenchement de la callback une fois l'action terminée
			this.district.find(searchFilter, (error, districtDocs) => {
				if (error) {
					console.error("Erreur lors de la recherche des districts : " + error);
				} else {
					if(callback) {
						callback(districtDocs);
					}
				}
			});
		}
	}
	
	insertDistrict(newDistrict, callback) {
		if(this.mongooseClient) {
			this.district.collection.insertOne(newDistrict, (error, insertionResult) => {
				if (error) {
					console.error("Erreur lors de l'insertion du district : \"" + newDistrict.name + "\" : " + error);
				} else {
					if(callback) {
						callback();
					}
				}
			});
		}
	}
	
	/**
	 * Met à jour la cote d'un district et créé une nouvelle entrée si le district
	 * n'est pas dans la base de données. Déclenche la fermeture du script si tous
	 * les districts ont été traités.
	 * @param {JSON}	queryArguments - Arguments de la requête, définissant si
	 *                                 	 le district doit être ajouté ou
	 *                                 	 simplement mis à jour.
	 * @param {Integer} callback   	   - Fonction à appeler une fois que la
	 *                                   mise à jour a été effectuée. Cette
	 *                                   fonction contient le district trouvé.
	 */
	updateDistrict(districtName, updateInstruction, enableUpsert, callback) {
		if(this.mongooseClient) {
			this.district.collection.updateOne(districtName, updateInstruction, { upsert: enableUpsert }, (error, updateReport) => {
				if (error) {
					console.error("Erreur lors de la mise à jour des districts : \"" + updateReport + "\" : " + error);
				} else {
					if(callback(updateReport)) {
						callback(updateReport);
					}
				}
			});
		}
	}

	insertSession(callback) {
		var updateOptions = this.currentDateUpdateOption("start_date");
		
		if(this.mongooseClient) {
			this.session.collection.findOneAndUpdate({ "start_date": -1 }, updateOptions, { upsert: true }, (error, updateResult) => {
				if (error) {
					console.error("Erreur lors de l'insertion d'une session : " + error);
				} else {
					if(callback) {
						callback(updateResult);
					}
				}
			});
		}
	}

	findLastSession(callback) {
		if(this.mongooseClient) {
			this.session.find({}, (error, sessionDocs) => {
				if (error) {
					console.error("Erreur lors de la recherche de la dernière session : " + error);
				} else {
					if(callback) {
						callback(sessionDocs);
					}
				}
			}).sort( { 'start_date': -1 } ).limit(1);
		}
	}

	updateCurrentSession(discoveredDistrictId, winnerIds, callback) {
		this.findLastSession((sessionDocs) => {
			if(sessionDocs.length > 0) {
				var lastSessionDoc = sessionDocs[0];
				
				var updateOptions = this.currentDateUpdateOption("end_date");
				updateOptions.$set = {
					"discovered_district": discoveredDistrictId,
					"winners": winnerIds
				}

				if(this.mongooseClient) {
					this.session.collection.findOneAndUpdate({ "_id": lastSessionDoc._id }, updateOptions, (error, updateResult) => {
						if (error) {
							console.error("Erreur lors de la mise à jour de la dernière session : " + error);
						} else {
							if(callback) {
								callback(updateResult);
							}
						}
					})
				}
			} else {
				console.warn("Échec de la mise à jour de la dernière session : cette dernière n'a pas été trouvée dans la base de données.");
			}
		});
	}

	insertDiscoveredDistrict(districtId, callback) {
		var currentDateOption = this.currentDateUpdateOption("date");
		
		var updateOptions = {
			"$set": { "district": districtId },
			"$currentDate": currentDateOption.$currentDate
		};

		if(this.mongooseClient) {
			this.discoveredDistrict.collection.findOneAndUpdate({ "date": -1 }, updateOptions, { upsert: true }, (error, updateResult) => {
				if (error) {
					console.error("Erreur lors de l'insertion d'un pays décourvert : " + error);
				} else {
					if(callback) {
						callback(updateResult);
					}
				}
			});
		}
	}

	getDistrictsTop(top, districtCollection, callback) {
		var countPipeline = [
			{ "$sortByCount": "$district" },
			{ "$limit": top }
		];
	
		this.getAllDistricts((districtDocs) => {
			var districtNames = {};

			districtDocs.forEach(districtDoc => {
				var districtIdText = districtDoc._id.toString();
				districtNames[districtIdText] = districtDoc.name;
			});

			districtCollection.aggregate(countPipeline, (error1, districtsStatDocs) => {
				if (error1) {
					console.error("Erreur lors du comptage de chaque pays décourvert : " + error1);
				} else {
					districtCollection.count({}, (error2, districtCount) => {
						if (error2) {
							console.error("Erreur lors du comptage des pays décourverts : " + error2);
						} else {
							var augmentedStats = this.augmentDistrictsStats(districtCount, districtNames, districtsStatDocs);
							
							if(callback) {
								callback(augmentedStats);
							}
						}
					});
				}
			});
		});
	}

	augmentDistrictsStats(districtCount, districtNames, districtsStatDocs) {
		var augmentedStats = [];

		districtsStatDocs.forEach((districtsStatDoc) => {
			var districtIdText = districtsStatDoc._id.toString();

			var augmentedStat = {
				"name": districtNames[districtIdText],
				"count": districtsStatDoc.count,
				"ratio": districtsStatDoc.count / districtCount
			};

			augmentedStats.push(augmentedStat);
		});

		return augmentedStats;
	}

	insertBetDistrict(districtId, player, sessionId, isPronoCorrect, callback) {
		var currentDateOption = this.currentDateUpdateOption("date");
		
		var updateOptions = {
			"$set":
			{
				"district": districtId,
				"author_player": player._id,
				"session": sessionId,
				"was_correct": isPronoCorrect
			},
			"$currentDate": currentDateOption.$currentDate
		};

		if(this.mongooseClient) {
			this.betDistrict.collection.findOneAndUpdate({ "date": -1 }, updateOptions, { upsert: true }, (error, updateResult) => {
				if (error) {
					console.error("Erreur lors de l'insertion d'un pays décourvert : " + error);
				} else {
					if(callback) {
						callback(updateResult, player.name);
					}
				}
			});
		}
	}

	currentDateUpdateOption(fieldName) {
		return {
			"$currentDate": {
				[fieldName]: { "$type": "date" }
			}
		};
	}
	
	applyPlayerDefaultRegexp(playerTerm) {
		return new RegExp("^" + playerTerm.toLowerCase() + "$", "i");
	}
	
	applyDistrictDefaultRegexp(districtTerm) {
		if(districtTerm) {
			var formattedTerm = this.applyDistrictDefaultFormatting(districtTerm);
			return new RegExp("^" + formattedTerm + "$", "i");
		} else {
			return null;
		}
	}
	
	applyDistrictDefaultFormatting(districtTerm) {
		if(districtTerm) {
			var formattedTerm = districtTerm.toLowerCase().normalize('NFD');
			
			formattedTerm = formattedTerm.replace(/^[^a-zA-Z0-9]+$/g, "");
			
			var accentRemovingExp = new RegExp(/[\u0300-\u036f]/g);
			formattedTerm = formattedTerm.replace(accentRemovingExp, "");
			
			var dashRemovingExp = new RegExp(/-/g);
			
			return formattedTerm.replace(dashRemovingExp, "");
		} else {
			return null;
		}
	}
}

module.exports.MongooseManager = MongooseManager;