var mongoose = require('mongoose');

// Structure d'un joueur dans la base de données
var playerSchema = new mongoose.Schema({
	name: {
		type: String,
		default: ""
	},

	prognosis: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Bet_district'
	}],

	score: {
		type: Number,
		default: 0
	}
});

var playerModel = mongoose.model('Player', playerSchema);

module.exports = playerModel;