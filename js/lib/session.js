var mongoose = require('mongoose');

// Structure d'un joueur dans la base de données
var sessionSchema = new mongoose.Schema({
	start_date: {
		type: Date,
		default: Date.now
	},

	end_date: {
		type: Date,
		default: Date.now
	},

	discovered_district: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'District'
	},

	winners: [ {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Player'
	} ]
});

var sessionModel = mongoose.model('Session', sessionSchema);

module.exports = sessionModel;