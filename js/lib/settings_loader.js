// Importation de la bibliothèque File System permettant notamment de lire ou
// modifier des fichiers
const fs = require("fs");
const os = require("os");

const MESSAGES_FILE_PATH = "messages.json";

var loadConfiguration = function() {
	var configurationFilePath = getDataFolder() + "/configuration.json";
	
	if(fs.existsSync(configurationFilePath)) {
		try {
			// Lecture du fichier contenant la configuration du bot au format JSON
			var settingsContent = fs.readFileSync(configurationFilePath, 'UTF8');
		
			// Chargement de la configuration dans une variable JSON
			var configurationData = JSON.parse(settingsContent);
			
			if(isConfigurationFileComplete(configurationData)) {
				return configurationData;	
			} else {
				console.error("Il manque des informations au fichier de configuration \"" + configurationFilePath + "\", le programme va devoir se fermer.");
				return null; 
			}
		} catch(error) {
			console.error("Erreur lors de la lecture du fichier de configuration \"" + configurationFilePath + "\", sa syntaxe est-elle correcte ?");
			console.error(error);
			return null;
		}
	} else {
		console.error("Fichier de configuration \"" + configurationFilePath + "\" non trouvé. Lancez l'executable PronoGuessr.jar pour accéder au menu de configuration.");
		return null;
	}
};

function isConfigurationFileComplete(configurationData) {
	return configurationData.database_path != null
		&& configurationData.bot != null
		&& configurationData.bot.password != null
		&& configurationData.bot.username != null
		&& configurationData.bot.message_amount_restriction != null
		&& configurationData.bot.message_restriction_interval != null
		&& configurationData.bot.prono_amount_limit != null
		&& configurationData.target_channel != null
		&& configurationData.prono_tray != null
		&& configurationData.prono_tray.fullscreen != null
		&& configurationData.prono_tray.size != null
		&& configurationData.prono_tray.size.width != null
		&& configurationData.prono_tray.size.height != null
		&& configurationData.prono_tray.display_counter != null
		&& configurationData.prono_tray.cursed_paniac_status != null;
}

function saveConfiguration(configurationData) {
	var configurationFilePath = getDataFolder() + "/configuration.json";
	
	if(fs.existsSync(configurationFilePath)) {
		var stringifiedConfiguration = JSON.stringify(configurationData, null, '    ');
		fs.writeFileSync(configurationFilePath, stringifiedConfiguration);
	}
}

var loadMessages = function() {
	if(fs.existsSync(MESSAGES_FILE_PATH)) {
		try {
			// Lecture du fichier contenant les messages du bot au format JSON
			var messagesContent = fs.readFileSync(MESSAGES_FILE_PATH, 'UTF8');
		
			// Chargement des messages dans une variable JSON
			var messageData = JSON.parse(messagesContent);
			
			if(isMessagesFileComplete(messageData)) {
				return messageData;
			} else {
				console.error("Le fichier contenant les messages publiables semble ne pas être complet.");
				return null;
			}
		} catch(error) {
			console.error("Erreur lors de la lecture du fichier \"" + MESSAGES_FILE_PATH + "\" contenant les messages du bot, sa syntaxe est-elle correcte ?");
			console.error(error);
			return null;
		}
	} else {
		console.error("Fichier de messages \"" + MESSAGES_FILE_PATH + "\" non trouvé. Vous devriez réinstaller le logiciel.");
		return null;
	}
};

function isMessagesFileComplete(messageData) {
	return messageData.all_prognosis_reseted != null
		&& messageData.player_prognosis != null
		&& messageData.player_no_prognosis != null
		&& messageData.prognosis_help != null
		&& messageData.prognonsis_amount_overflow_one != null
		&& messageData.prognonsis_amount_overflow_multiple != null
		&& messageData.prognosis_double_one != null
		&& messageData.prognosis_double_multiple != null
		&& messageData.prognosis_limit_confirmation != null
		&& messageData.prognosis_limit_help != null
		&& messageData.nobody_made_prognosis != null
		&& messageData.prognosis_top_help != null
		&& messageData.prognosis_top_all != null
		&& messageData.prognosis_top_limited != null
		&& messageData.session_started != null
		&& messageData.session_stopped != null
		&& messageData.district_help != null
		&& messageData.district_add_help != null
		&& messageData.district_top_limited != null
		&& messageData.district_top_all != null
		&& messageData.district_top_help != null
		&& messageData.district_odds != null
		&& messageData.district_odds_help != null
		&& messageData.district_not_recognized_one != null
		&& messageData.district_not_recognized_one_special != null
		&& messageData.district_not_recognized_multiple != null
		&& messageData.district_not_available_one != null
		&& messageData.district_not_available_multiple != null
		&& messageData.district_added != null
		&& messageData.district_updated != null
		&& messageData.nobody_guessed_district != null
		&& messageData.one_player_guessed_district != null
		&& messageData.several_players_guessed_district != null
		&& messageData.player_score_after_request != null
		&& messageData.player_score_after_gain != null
		&& messageData.player_score_help != null
		&& messageData.player_score_unrecognized_value != null
		&& messageData.player_not_recognized != null
		&& messageData.no_player != null
		&& messageData.cannot_execute_command_in_context != null
		&& messageData.cannot_execute_command_in_context_precision != null
		&& messageData.cursed_paniac_spawn != null
		&& messageData.cursed_paniac_eaten_prono != null
		&& messageData.cursed_paniac_leave != null;
}

var getDataFolder = function() {
	var pronoGuessrDataFilePath = os.homedir() + "/PronoGuessr";
	
	if(fs.existsSync(pronoGuessrDataFilePath)) {
		return pronoGuessrDataFilePath
	} else {
		return ".";
	}
}

module.exports.loadConfiguration = loadConfiguration;
module.exports.saveConfiguration = saveConfiguration;

module.exports.loadMessages = loadMessages;
module.exports.getDataFolder = getDataFolder;