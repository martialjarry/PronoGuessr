// Importation de la bibliothèque tmi permettant de communiquer avec Twitch
const tmi = require('tmi.js');

class TmiManager {
	constructor() {
		this.tmiClient = null;
		
		this.publicationHistory = [];
		this.anticipatedPublications = [];
		this.publicationBuffer = [];

		var currentInstance = this;
		this.bufferedMessagePublisher = null;
	}
	
	setPublicationRestriction(messageAmountRestriction, messageRestrictionInterval) {
		if(messageAmountRestriction != null) {
			this.messageAmountRestriction = Math.max(1, messageAmountRestriction);
		} else {
			this.messageAmountRestriction = Number.POSITIVE_INFINITY;
		}
		
		if(messageRestrictionInterval != null) {
			this.messageRestrictionInterval = Math.max(0, messageRestrictionInterval);
		} else {
			this.messageRestrictionInterval = 0;
		}
	}
	
	openConnection(channelName, botUsername, botPassword, callback) {	
		// Configuration du bot
		const tmiConfig = {
			options: {
				debug: true
			},
			connection: {
				reconnect: true
			},
			identity: {
				// Nom de la chaîne du bot
				username: botUsername,
				
				// Oauth de la chaîne du bot
				password: botPassword
			},
			channels: [
				// Nom de la chaîne que le bot doit écouter et sur laquelle il doit
		   		// parler
				   channelName
				]
			};
			
			this.tmiClient = new tmi.client(tmiConfig);

			var currentInstance = this;
			this.bufferedMessagePublisher = setInterval(() => { currentInstance.publishBufferedMessages(currentInstance); }, 100);
			
			if(this.tmiClient != null) {
				this.tmiClient.on('connectfail', function () {
				console.error("Impossible de se connecter au serveur Twitch.");
			});
			
			// Connexion du client à la chaîne cible
			this.tmiClient.connect();
		}
	}
	
	closeConnection() {
		if(this.tmiClient != null) {
			this.tmiClient.disconnect();
		}

		if(this.bufferedMessagePublisher) {
			clearInterval(this.bufferedMessagePublisher);
		}
	}
	
	/**
	 * Récupère le nom de tous les joueurs du live de la chaine en entrée auprès de
	 * l'API TMI.
	 * @param {String} 	 channel  - Nom de la chaîne du diffuseur.
	 * @param {Function} callback - Fonction à appeler une fois que la
	 *                              récupération a été effectuée. Cette fonction
	 *                              contient un ensemble de joueurs mêlant les
	 *                              viewers, les modérateur et le diffuseur.
	 */
	getAllChatters(channel, callback) {
		if(this.tmiClient != null) {
			this.tmiClient.api({
				url: "http://tmi.twitch.tv/group/user/" + channel + "/chatters",
				method: "GET"
			}, (error, res, body) => {
				if(error) {
					console.error("Erreur lors de la récupération des \"chatters\" du live : " + error);
					if(callback) {
						callback([]);
					}
				} else {
					if(callback) {
						if(body.chatters) {
							var playerSet = new Set();
							
							for(var chatterSectionKey in body.chatters) {
								for(var i = 0; i < body.chatters[chatterSectionKey].length; i++) {
									var chatter = body.chatters[chatterSectionKey][i];
									playerSet.add(chatter);
								}
							}
							
							callback(Array.from(playerSet));
						} else {
							callback([]);
						}
					}
				}
			});
		} else {
			callback([]);
		}
	}
	
	publishForbiddenCommandMessage(channel, currentBotState, precision) {
		var message = "Je ne peux pas executer cette commande dans le contexte actuel";
		message += precision ? " (" + precision + ")." : ".";
		
		this.publishMessage(channel, message);
		console.warn("Commande interdite dans le contexte \"" + currentBotState + "\".");
	}

	publishBufferedMessages(currentInstance) {
		var publicationWhithinIntervalCount = this.publicationWhithinIntervalCount(this.messageRestrictionInterval);
		var publicationIntervalOverflow = publicationWhithinIntervalCount >= this.messageAmountRestriction; 
		
		if(!publicationIntervalOverflow) {
			var availablePublicationCount = this.messageAmountRestriction - publicationIntervalOverflow;
			
			for(var i = 0; i < this.publicationBuffer.length && i < Math.min(availablePublicationCount, 1); i++) {
				if(this.tmiClient && this.tmiClient.ws && this.tmiClient.ws._socket) {
					var publicationData = this.publicationBuffer.splice(0, 1);
					
					if(publicationData.length >= 1) {
						var publication = publicationData[0];
						
						if(publication.message) {
							this.tmiClient.say(publication.channel, publication.message);
							
							var currentDate = new Date();
							publication.publicationTime = currentDate.getTime();
							
							this.publicationHistory.push(publication);
						}
					}
				} else {
					console.log("Décrochage du bot lors de l'envoi du message : " + this.publicationBuffer[0].message);
				}
			}
		}
	}
	
	publishMessage(channel, messageData, messageArguments) {
		if(this.tmiClient != null && messageData != null) {
			var message = "";
			
			var priority = 50;
			
			if(typeof(messageData) != 'string') {
				var messageIndex = Math.round(Math.random() * (messageData.message.fr.length - 1));
				message = messageData.message.fr[messageIndex];
				priority = messageData.priority;
				
				if(messageArguments) {
					for(var i = 0; i < messageArguments.length; i++) {
						var parametersReplacingRegex = new RegExp("\\$" + i, "g") ;
						message = message.replace(parametersReplacingRegex, messageArguments[i]);
					}
				}
			} else {
				message = messageData;
			}
			
			var timeSinceLastPublication = this.timeSinceLastPublication();
			var publicationFrequencyTooHigh = !isNaN(timeSinceLastPublication) && timeSinceLastPublication < 100;
			var publicationIntervalOverflow = this.publicationWhithinIntervalCount(this.messageRestrictionInterval) >= this.messageAmountRestriction; 
			
			if(!publicationIntervalOverflow && !publicationFrequencyTooHigh) {
				if(this.tmiClient && this.tmiClient.ws && this.tmiClient.ws._socket) {
					this.tmiClient.say(channel, message);
					
					var newPublication = this.createPublication(channel, message, true, priority);
					this.publicationHistory.push(newPublication);
				}
			} else {
				var newPublication = this.createPublication(channel, message, false, priority);
				this.addPublicationToBuffer(newPublication);
			}
		} else {
			console.error("Impossible de trouver une référence pour le message demandé.");
		}
	}
	
	createPublication(channel, message, hasBeenPublished, priority) {
		var publicationTime = Number.POSITIVE_INFINITY;
		
		if(hasBeenPublished) {
			var currentDate = new Date();
			publicationTime = currentDate.getTime();
		}
		
		return {
			message: message,
			channel: channel,
			priority: priority,
			publicationTime: publicationTime
		};
	}
	
	publicationWhithinIntervalCount(timeInterval) {
		var publicationCount = 0;
		
		var currentDate = new Date();
		var currentTime = currentDate.getTime();
		
		var isWithinInterval = true;
		
		for(var i = this.publicationHistory.length - 1; i >= 0 && isWithinInterval; i--) {
			var publication = this.publicationHistory[i];
			var publicationTime = publication.publicationTime;
			
			if(publicationTime !== Number.POSITIVE_INFINITY) {
				var currentTimeInterval = currentTime - publicationTime; 
				
				if(currentTimeInterval < timeInterval * 1000) {
					publicationCount++;
				} else {
					isWithinInterval = false;
				}
			}
		}
		
		return publicationCount;
	}
	
	timeSinceLastPublication() {
		var lastPublication = this.lastPublication();
		
		if(lastPublication) {
			var currentDate = new Date();
			var currentTime = currentDate.getTime();
			
			return currentTime - lastPublication.publicationTime;
		} else {
			return Number.NaN;
		}
	}
	
	lastPublication() {
		var lastPublication = null;
		
		for(var i = this.publicationHistory.length - 1; i >= 0 && lastPublication == null; i--) {
			var publication = this.publicationHistory[i];
			
			if(publication.publicationTime !== Number.POSITIVE_INFINITY) {
				lastPublication = publication;
			}
		}
		
		return lastPublication;
	}
	
	addPublicationToBuffer(publication) {
		var newPublicationIndex = 0;
		
		for(var i = this.publicationBuffer.length - 1; i >= 0 && newPublicationIndex == 0; i--) {
			var bufferPublication = this.publicationBuffer[i];
			
			if(publication.priority <= bufferPublication.priority) {
				newPublicationIndex = i + 1;
			}
		}
		
		if(newPublicationIndex >= 0) {
			this.publicationBuffer.splice(newPublicationIndex, 0, publication);
		}
	}
}

module.exports.TmiManager = TmiManager;