// Importation de la bibliothèque Net permettant d'utiliser un socket
const net = require('net');

class UniguessSocketManager {
	constructor () {
		this.uniguessSocket = null;
		this.waitingForUniguess = true;
		this.receptionCallback = null;

		this.reconnectionInterval = null;
	}
	
	openConnection(uniguessPort) {
		this.uniguessSocket = net.connect({port: uniguessPort, timeout: 3000});
		
		this.uniguessSocket.setEncoding('utf8');
		
		this.reconnectionInterval = setInterval (() => {
			if(this.uniguessSocket != null) {
				if(this.uniguessSocket.destroyed && !this.uniguessSocket.connecting) {
					clearInterval(this.reconnectionInterval);
					this.openConnection(uniguessPort);
					this.setMessageReceptionCallback(this.receptionCallback);
				}
			}
		}, 3000);
		
		this.uniguessSocket.on('error', (message) => {
			if(!this.waitingForUniguess) {
				console.log("Déconnecté d'UniGuess, tentative de reconnection toutes les 3 secondes.");
				this.waitingForUniguess = true;
			}
		});
		
		this.uniguessSocket.on('ready', () => {
			console.log("Connecté à UniGuess.");
			this.waitingForUniguess = false;
		});
	}
	
	closeConnection() {
		this.uniguessSocket.destroy();

		if(this.reconnectionInterval) {
			clearInterval(this.reconnectionInterval);
		}
	}
	
	sendMessage(message) {
		if(this.uniguessSocket != null && !this.uniguessSocket.destroyed) {
			try {
				this.uniguessSocket.write(message);
			} catch(socketError) {
				console.error("Impossible de communiquer à UniGuess :", socketError);
			}
		}
	}
	
	setMessageReceptionCallback(callback) {
		this.receptionCallback = callback;
		
		if(callback != null) {
			this.uniguessSocket.on('data', this.receptionCallback);
		}
	}
}

module.exports = new UniguessSocketManager();