// Prefixe des commandes Twitch
const CSV_SEPARATOR = ";";


// Importation de la bibliothèque File System permettant notamment de lire ou
// modifier des fichiers
const fs = require('fs');

// Importation de la bibliothèque mkdirp qui permet de créer des dossiers
var mkdirp = require('mkdirp');

// Importation des réglages
const settingsLoader = require("./lib/settings_loader.js");

// Importation de la bibliothèque node-datetime permettant d'obtenir la date
// courante
const datetime = require('node-datetime');

// Importation de la bibliothèque csv-parser permettant de générer des fichiers
// CSV
const csvWriter = require('csv-writer');

// Importation du gestionnaire de la base de données mLab
const mongooseManagerBuilder = require("./lib/mongoose_manager.js"); 

var mongooseManager = new mongooseManagerBuilder.MongooseManager();

// Chargement des paramètres dans une variable JSON
var configurationData = settingsLoader.loadConfiguration();


if(configurationData) {
	// Connexion à la base de données mLab (ou MongoDB)
	mongooseManager.openConnection(configurationData.database_path, () => {
		// Chemin vers le fichier CSV de sortie, stockant les cotes
		var outputFilePath = newOddsSaveFilePath();
		var oddsFolderPath = settingsLoader.getDataFolder() + "/cotes";
		
		if(!fs.existsSync(oddsFolderPath)) {
			mkdirp(oddsFolderPath, function(error) {
				if(error) {
					console.error("Impossible de créer le dossier de sortie des cotes.");
				} else {
					var csvCreator = initializeCsvCreator(outputFilePath);
					saveOdds(csvCreator, outputFilePath);
				}
			});
		} else {
			var csvCreator = initializeCsvCreator(outputFilePath);
			saveOdds(csvCreator, outputFilePath);
		}
	});
} else {
	console.error("Impossible de sauvegarder les cotes sans avoir accès au fichier de configuration.");
}

/**
 * Génère un chemin vers un fichier de sauvegarde de cotes à partir de la date
 * courante.
 * @returns {String} - Nouveau chemin ver le fichier de sortie stockant la
 * 					   cote de tous les districts.
 */
function newOddsSaveFilePath() {
	var currentDateTime = datetime.create();
	var outputFileNameDateTime = currentDateTime.format('d-m-Y_H-M-S') + "_archive";
	return settingsLoader.getDataFolder() + '/cotes/cote_' + outputFileNameDateTime + '.csv';
}

/**
 * Initialise de l'écriveur du fichier CSV de sortie.
 */
function initializeCsvCreator(outputFilePath) {
	return csvWriter.createObjectCsvWriter({
		path: outputFilePath,
		header: [
			{ id: 'name', title: 'District' },
			{ id: 'odds', title: 'Cote' }
		],
		fieldDelimiter:CSV_SEPARATOR
	});
}

/**
 * Sauvegarde la cote de tous les districts en les récupérant depuis la base de
 * données puis en les enregistrant dans un fichier CSV.
 */
function saveOdds(csvCreator, outputFilePath) {
	mongooseManager.getAllDistricts((districtDocs) => {
		districtRows = [];

		// Pour chaque district, une ligne contenant le nom et la cote de ce
		// dernier doit être créée en vue d'être enregistrée dans le
		// fichier de sauvegarde
		districtDocs.forEach((districtDoc) => {
			if(districtDoc.is_enabled) {
				districtRows.push({name: districtDoc.name,  odds: districtDoc.odds});
			}
		});

		// Sauvegarde de l'état du district dans la base de données
		// distante
		csvCreator.writeRecords(districtRows).then(() => {
			console.log('Cotes enregistrées sous ' + outputFilePath);
		});

		// Fermeture du script
		mongooseManager.closeConnection();
	});
}