// Chemin vers le dossier contenant les fichiers de cotes
const ODDS_FOLDER_PATH = "cotes";

// Prefixe des commandes Twitch
const CSV_SEPARATOR = ";";


// Importation de la bibliothèque File System permettant notamment de lire ou
// modifier des fichiers
const fs = require('fs');

// Importation de la bibliothèque csv-parser permettant de décomposer des
// fichiers CSV
const csvParser = require('csv-parser');

// Importation des réglages
const settingsLoader = require("./lib/settings_loader.js");

//Importation du gestionnaire de la base de données mLab
const mongooseManagerBuilder = require("./lib/mongoose_manager.js"); 

var mongooseManager = new mongooseManagerBuilder.MongooseManager();

// Chargement des paramètres dans une variable JSON
var configurationData = settingsLoader.loadConfiguration();

// Nombre de districts mis à jour
var updatedDistrictCount = 0;

var enableTargetedDistricts = process.argv.includes("-ed") || process.argv.includes("--enable_districts");

if(configurationData) {
	// Chemin vers le fichier de cote
	var oddsFilePath = extractOddsFilePath();
	
	if (fs.existsSync(oddsFilePath)) {
		// Connexion à la base de données mLab (ou MongoDB)
		mongooseManager.openConnection(configurationData.database_path, () => {
			// Lecteur du flux du fichier de cotes
			var streamReader = fs.createReadStream(oddsFilePath);
		
			// Transmission du flux vers le parseur CSV
			var streamReaderPipe = streamReader.pipe(csvParser({ separator: CSV_SEPARATOR }, ['District', 'Cote']));
		
			var loadedDistricts = [];
		
			// Action à effectuer lorsqu'un lors de la lecteur du fichier (ici,
			// stockage des cotes dans un tableau temportaire)
			streamReaderPipe.on('data', (data) => {
				loadedDistricts.push(data)
			});
		
			// Action à effectuer lorsqu'un lorsque lecture du fichier est terminée
			// (ici, mise à jour en ligne des cotes)
			streamReaderPipe.on('end', () => {
				mongooseManager.getAllDistricts((districtDocs) => {
					var requiredUpdates = [];

					matchDatabaseWithFile(districtDocs, loadedDistricts, requiredUpdates);
					matchFileWithDatabase(loadedDistricts, requiredUpdates);

					uploadAllOdds(requiredUpdates);
				});
			});
		});
	} else {
		console.log("Désolé mais le fichier \"" + oddsFilePath + "\" n'existe pas.");
	}
} else {
	console.error("Impossible de mettre à jour les cotes sans avoir accès au fichier de configuration.");
}

function matchDatabaseWithFile(districtDocs, loadedDistricts, requiredUpdates) {
	districtDocs.forEach((districtDoc) => {
		var districtName = districtDoc.name;
		var districtOdds = districtDoc.odds;
		var enableDistrict = districtDoc.is_enabled;
		
		var matchingLoadedDistrict = loadedDistricts.find((loadedDistrict) => {
			if (loadedDistrict.District == districtName) {
				return loadedDistrict;
			} else {
				return null;
			}
		});

		if (matchingLoadedDistrict) {
			if (enableTargetedDistricts) {
				enableDistrict = true;
			}

			districtOdds = matchingLoadedDistrict.Cote;
		} else {
			enableDistrict = false;
		}

		var newRequiredUpdate = {
			name: districtName,
			odds: districtOdds,
			isEnabled: enableDistrict
		};

		requiredUpdates.push(newRequiredUpdate);
	});
}

function matchFileWithDatabase(loadedDistricts, requiredUpdates) {
	loadedDistricts.forEach((loadedDistrict) => {
		var districtName = loadedDistrict.District;

		var matchingRequiredUpdate = requiredUpdates.find((requiredUpdate) => {
			if (requiredUpdate.name == districtName) {
				return requiredUpdate;
			} else {
				return null;
			}
		});

		if (!matchingRequiredUpdate) {
			var newRequiredUpdate = {
				name: districtName,
				odds: loadedDistrict.Cote,
				isEnabled: true
			};

			requiredUpdates.push(newRequiredUpdate);
		}
	});
}

function extractOddsFilePath() {
	if(process.argv.length >= 3) {
		if(process.argv[2].startsWith("file:/")) {
			return decodeURI(process.argv[2].replace("file:/", ""));
		} else {
			return settingsLoader.getDataFolder() + "/cotes/" + process.argv[2];
		}
	} else {
		return null;
	}
}

	/**
	 * Créé ou met à jour la cote de chaque district présent dans le fichier de
	 * cotes.
	 * @param {String}  districts - Districts dont la cote doit être mise à jour.
	 */
function uploadAllOdds(requiredUpdates) {
	for(var i = 0; i < requiredUpdates.length; i++) {
		var requiredUpdate = requiredUpdates[i];

		// Nom du district courant
		var districtName = requiredUpdate.name;

		var updateCondition = {
			'name': { $eq: districtName.toLowerCase() }
		};
	
		var updateInstruction = {
			$set: {
				'name': districtName.toLowerCase(),
				'odds': requiredUpdate.odds,
				'is_enabled': requiredUpdate.isEnabled
			}
		};
	
		updateOdds(requiredUpdates.length, updateCondition, updateInstruction, true);
	}
}

function updateOdds(districtCount, updateCondition, updateInstruction, enableUpsert) {
	mongooseManager.updateDistrict(updateCondition, updateInstruction, enableUpsert, (updateReport) => {
		updatedDistrictCount++;

		if(updatedDistrictCount >= districtCount) {
			closeUpdater("J'ai mis à jour les cotes !");
		}
	});
}

/**
 * Déconnecte le script de la base de données, ce qui a pour effet de mettre
 * fin au script.
 * @param {String} message - Message affiché lors de la déconnection.
 */
function closeUpdater(message) {
	console.log(message);
	mongooseManager.closeConnection();
}