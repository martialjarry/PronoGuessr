

// Multiplicateur de cote
const ODDS_COMMON_MULTIPLICATOR = 100;

// Préfixe des commandes Twitch
const COMMAND_PREFIX = "!";

const PRONOGUESSR_PORT = 56250;

// Importation du gestionnaire de la base de données mLab
const mongooseManagerBuilder = require("./lib/mongoose_manager.js"); 

// Importation du gestionnaire TMI pour écouter le chat et y poster des
// messages ainsi que pour accéder à la liste des joueurs regardant le live
const tmiManagerBuilder = require("./lib/tmi_manager");

const uniguessSocketManager = require("./lib/uniguess_socket_manager.js");

// Importation des réglages
const settingsLoader = require("./lib/settings_loader.js");

const PronoCounterBuilder = require("./PronoCounter");

const Events = require('events');

// États possibles du bot durant son utilisation
var BotStates = {
	// En attente du lancement d'une session de pronostics
	"WAITING_PROGNOSIS": "WAITING_PROGNOSIS",
	
	// Session de pronostics en cours
	"IN_PROGNOSIS": "IN_PROGNOSIS",
	
	// En attente de l'annonce du district
	"WAITING_DISTRICT": "WAITING_DISTRICT"
};
Object.freeze(BotStates);

// État courant du bot durant son utilisation
var currentBotState = BotStates.WAITING_PROGNOSIS;

var WinnerStates = {
	"NEW": "NEW",
	"UPDATED": "UPDATED",
	"FAILED": "FAILED"
};

Object.freeze(WinnerStates);

// Chargement des paramètres dans une variable JSON
var configurationData = settingsLoader.loadConfiguration();
var messagesData = settingsLoader.loadMessages();

var oddsSpecialMultiplicator = 1;

var botStatePublisher = null;
var currentSessionId = null;

var tmiManager = new tmiManagerBuilder.TmiManager();
var mongooseManager = new mongooseManagerBuilder.MongooseManager();

var pronoCountRefreshDaemon = null;
var pronoCounter = new PronoCounterBuilder.PronoCounter(10);

var eventsEmitter = new Events.EventEmitter();

startPronoGuessr();

function startPronoGuessr() {
	if(configurationData) {
		var messageAmountRestriction = configurationData.bot.message_amount_restriction;
		var messageRestrictionInterval = configurationData.bot.message_restriction_interval;
		tmiManager.setPublicationRestriction(messageAmountRestriction, messageRestrictionInterval);
		
		if(messagesData && tmiManager) {
			// Multiplicateur de cotes, défini par le diffuseur via le launcher
			oddsSpecialMultiplicator = extractOddsSpecialMultiplicator();
			
			if(oddsSpecialMultiplicator > 0) {
				// Initialisation des connexion vers TMI et mLab
				initializeConnections();
			}
		} else {
			console.error("Impossible de lancer PronoGuessr sans avoir accès au fichier contenant les messages publiables.");
		}
	} else {
		console.error("Impossible de lancer PronoGuessr sans avoir accès au fichier de configuration.");
	}
}


/**
 * Si les paramètres de connexion sont corrects, initialise uen connexion vers
 * la base de donnée ainsi que vers le serveur Twitch afin de prendre en main
 * le bot.
 */
function initializeConnections() {
	mongooseManager.openConnection(configurationData.database_path, () => {
		var botData = configurationData.bot;
		
		var uniguessPort = extractUniguessPort();
		
		if(!isNaN(uniguessPort)) {
			uniguessSocketManager.openConnection(uniguessPort);
			uniguessSocketManager.setMessageReceptionCallback(processUniguessMessage);
		}
		
		botStatePublisher = setInterval(publishBotState, 1000);

		// Création du client à l'aide de la configuration créée
		tmiManager.openConnection(configurationData.target_channel, botData.username, botData.password);
		
		tmiManager.tmiClient.on("join", function (channel, username, self) {
			if(self) {
				// Action à effectuer lorsqu'un spectateur poste un message
				tmiManager.tmiClient.on('chat', processChatPost);
			} else {
				uniguessSocketManager.sendMessage("{'subject': 'PLAYER_JOIN', 'data': {'player_name': '" + username + "'}}\n");
			}
		});
	});
}

/**
 * Parse le 3ème argument de la commande de lancement du bot en tant que float.
 * Affecte ensuite la valeur reconnue au multiplicateur spécial.
 * @return {Integer} - La valeur du multiplicateur spécial s'il a été reconnu
 * 					   par le programme, 1 sinon. 
 */
function extractOddsSpecialMultiplicator() {
	if(process.argv.length >= 3) {
		var multiplicator = Math.floor(parseFloat(process.argv[2]) * 100) / 100;
		return isNaN(multiplicator) ? 1 : multiplicator;
	} else {
		return 1;
	}
}

function extractUniguessPort() {
	if(process.argv.length >= 4) {
		var port = parseInt(process.argv[3]);
		
		if(!isNaN(port)) {
			return port;
		} else {
			return PRONOGUESSR_PORT;
		}
	} else {
		return PRONOGUESSR_PORT;
	}
}

function publishBotState() {
	var publicationMessage = "{'subject': 'BOT_STATE', 'data': {'bot_state': '" + currentBotState + "'}}\n";
	uniguessSocketManager.sendMessage(publicationMessage);
}

function processUniguessMessage(message) {
	if(message) {
		try {
			var messageJson = JSON.parse(message);
			
			if(messageJson) {
				if(messageJson.action) {
					var targetChannel = configurationData.target_channel;
					var messageData = messageJson.data;

					switch(messageJson.action) {
					case "COMMAND":
						if(messageData && messageData.commandType && messageData.arguments) {
							var commandTypeJson = messageData.commandType;
							var commandArgumentsJson = messageData.arguments.join(" ");
							processCommand(targetChannel, null, commandTypeJson, commandArgumentsJson, true);
						}
						break;
					case "CURSED_PANIAC_READY":
						if(configurationData.prono_tray) {
							var cursedPaniacStatus = configurationData.prono_tray.cursed_paniac_status;

							if(messageData && messageData.invokator_name && cursedPaniacStatus) {
								if(cursedPaniacStatus == "ENABLED" || cursedPaniacStatus == "DISABLED_FOR_VIEWERS") {
									var invokatorName = messageData.invokator_name;
									tmiManager.publishMessage(targetChannel, messagesData.cursed_paniac_spawn, [invokatorName]);
								}
							}
						}
						break;
					case "CURSED_PANIAC_ATE_PRONO":
						var authorName = messageData.author_name;
						
						updatePrognosis(authorName, [], (retrievedDistrictNames) => {
							tmiManager.publishMessage(targetChannel, messagesData.cursed_paniac_eaten_prono, [authorName]);
							eventsEmitter.emit('onDatabaseUpdated');
						});
						break;
					case "CURSED_PANIAC_LEAVED":
						tmiManager.publishMessage(targetChannel, messagesData.cursed_paniac_leave);
						break;
					}
				}
			}
		} catch (e) {
			console.warn("Unable to parse received UniGuess message. Port could be used by another application.");
		}
	}
}

/**
 * Traite tous les messages envoyés par n'importe quel autre utilisateur que le
 * bot lui-même.
 * @param {String} 	channel - Nom de la chaîne du diffuseur.
 * @param {Object} 	user 	- Utilisateur ayant posté le message.
 * @param {String} 	message - Message posté par l'utilisateur.
 * @param {Boolean} isSelf 	- Le message a-t-il été envoyé par le bot
 *                            lui-même ?
 */
function processChatPost(channel, user, message, isSelf) {
	// Si le spectateur n'est pas le bot lui-même, alors le message est traité
	if (!isSelf) {
		// Commande et paramètres entrés par le joueur
		var fullCommand = parseCommand(message);

		// Si le format de la commande et de ses paramètres est valide, alors
		// ils doivent être traités
		if(fullCommand) {
			// Commande
			var command = fullCommand[1];

			// Arguments de la commande, sous forme d'une chaîne de caractères
			// (non décomposés)
			var rawArguments = fullCommand[2].trim();

			// Choix et lancement du traitement correspondant à la commande
			processCommand(channel, user, command, rawArguments, false);
		}
	}
}

function processCommand(channel, user, command, rawArguments, forceBroadcaster) {
	switch(command.toLowerCase()) {
	case "prono":
		processPronoCommand(channel, user, rawArguments, forceBroadcaster);
		break;
	case "monprono":
		processMyPronoCommand(channel, user);
		break;
	case "go_prono":
		processPronoGoCommand(channel, user, forceBroadcaster);
		break;
	case "stop_prono":
		processPronoStopCommand(channel, user, forceBroadcaster);
		break;
	case "pays":
		processDistrictCommand(channel, user, rawArguments, forceBroadcaster);
		break;
	case "cote":
		processOddsCommand(channel, user, rawArguments, forceBroadcaster);
		break;
	case "score":
		processScoreCommand(channel, user, rawArguments, forceBroadcaster);
		break;
	case "cursed_paniac":
		processCursedPaniacCommand(channel, user, rawArguments, forceBroadcaster);
		break;
	}
}

/**
 * Traite la commande !prono selon ses arguments :
 *   - Pour -reset-all : réinitialisation de tous les pronostics
 *   - Pour -pronos : affichage du pronostics de chaque joueur présent
 *   - Sinon : enregistrement du pronostic pour le joueur qui a posté le
 *			   message
 * @param {String}  channel 		 - Nom de la chaîne du diffuseur.
 * @param {Object}  user 			 - Utilisateur auteur de la commande.
 * @param {String}  rawArguments 	 - Arguments de la commande non décomposés.
 * @param {Boolean} forceBroadcaster - Si vrai, traite l'utilisateur en entrée
 * 									   comme le diffuseur.
 */
function processPronoCommand(channel, user, rawArguments, forceBroadcaster) {
	var commandArguments = rawArguments.split(" ");
	var username = extractUsername(user);
	
	switch(commandArguments[0].toLowerCase()) {
	case "-reset":
		if(currentBotState == BotStates.IN_PROGNOSIS
		|| currentBotState == BotStates.WAITING_DISTRICT) {
			if(forceBroadcaster || isBroadcaster(user)) {
				resetAllPrognosis(() => {
					// Post d'un message de confirmation une fois les pronostics
					// réinitialisés
					tmiManager.publishMessage(channel, messagesData.all_prognosis_reseted);

					eventsEmitter.emit('onDatabaseUpdated');
				});
			}
		} else {
			tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
		}
		break;
	case "-afficher_tous":
		if(currentBotState == BotStates.IN_PROGNOSIS
		|| currentBotState == BotStates.WAITING_DISTRICT) {
			if(forceBroadcaster || isModeratorOrBroadcaster(user)) {
				displayAllPrognosis(channel);
			}
		} else {
			tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
		}
		break;
	case "-max":
		if(currentBotState == BotStates.WAITING_PROGNOSIS
		|| currentBotState == BotStates.WAITING_DISTRICT) {
			if(forceBroadcaster || isBroadcaster(user)) {
				if(commandArguments.length >= 2) {
					var rawPronoAmountLimit = commandArguments[1];
					updatePronoAmoutLimit(channel, rawPronoAmountLimit, username);
				} else {
					tmiManager.publishMessage(channel, messagesData.prognosis_limit_help, [username]);
				}
			}
		} else {
			tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
		}
		break;
	case "-top":
		if(currentBotState == BotStates.WAITING_PROGNOSIS
		|| currentBotState == BotStates.IN_PROGNOSIS
		|| currentBotState == BotStates.WAITING_DISTRICT) {
			if(forceBroadcaster || isModeratorOrBroadcaster(user)) {
				if(commandArguments.length >= 2) {
					var topRaw = commandArguments[1];
					displayBetDistrictTop(channel, topRaw);
				} else if(commandArguments.length == 1) {
					displayBetDistrictTop(channel, null);
				} else {
					tmiManager.publishMessage(channel, messagesData.prognosis_top_help, [username]);
				}
			}
		} else {
			tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
		}
		break;
	default:
		if(currentBotState == BotStates.IN_PROGNOSIS) {
			//prono seulement pendant la phase de prono
			if(commandArguments.length > 0 && commandArguments[0]) {
				savePlayerPrognosis(channel, username, commandArguments, (retrievedDistrictNames) => {
					var predictionReportJson = "{'subject': 'PRONO_UPDATE', ";
					predictionReportJson += "'data': {'player_name': '" + username + "', ";
					predictionReportJson += "'district_names': " + JSON.stringify(retrievedDistrictNames) + " }}\n";
					uniguessSocketManager.sendMessage(predictionReportJson);
					
					pronoCounter.incrementCount();
					pronoCounter.incrementTempCounts();

					var pronoCountsJson = "{'subject': 'PRONO_COUNT', ";
					pronoCountsJson += "'data': " + pronoCounter.toJson() + "}\n";
					uniguessSocketManager.sendMessage(pronoCountsJson);

					eventsEmitter.emit('onDatabaseUpdated');
				});
			} else {
				tmiManager.publishMessage(channel, messagesData.prognosis_help, [username]);
			}
		}
		else {
			tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
		}
		break;
	}
}

/**
 * Traite la commande !monprono
 * @param {String} channel 
 * @param {Object} user 
 * @param {Boolean} forceBroadcaster 
 */
function processMyPronoCommand(channel, user) {
	var username = extractUsername(user);

	if(currentBotState == BotStates.WAITING_DISTRICT || currentBotState == BotStates.IN_PROGNOSIS) {
		displayPlayerPrognosis(channel, username)
	} else {
		tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
	}
}

/**
 * Traite la commande !go_prono en démarrant une nouvelle session de pronos.
 * @param {String}  channel 		 - Nom de la chaîne du diffuseur.
 * @param {Object}  user 			 - Utilisateur auteur de la commande.
 * @param {Boolean} forceBroadcaster - Si vrai, traite l'utilisateur en entrée
 * 									   comme le diffuseur.
 */
function processPronoGoCommand(channel, user, forceBroadcaster) {
	if(currentBotState == BotStates.WAITING_PROGNOSIS) { 
		if(forceBroadcaster || isBroadcaster(user)) {
			startSession(() => {
				currentBotState = BotStates.IN_PROGNOSIS;
				tmiManager.publishMessage(channel, messagesData.session_started);

				var pronoCountRefreshPeriod = 1 / pronoCounter.referenceRefreshRate * 500;
				pronoCountRefreshDaemon = setInterval(refreshPronoRate, pronoCountRefreshPeriod);

				eventsEmitter.emit('onDatabaseUpdated');
			});
		}
	} else {
		tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
	}
}

/**
 * Traite la commande !stop_prono en arrêtant la session de pronos en cours.
 * @param {String} channel 	- Nom de la chaîne du diffuseur.
 * @param {Object} user 	- Utilisateur auteur de la commande.
 * @param {Boolean} forceBroadcaster - Si vrai, traite l'utilisateur en entrée
 * 									   comme le diffuseur.
 */
function processPronoStopCommand(channel, user, forceBroadcaster) {
	if(currentBotState == BotStates.IN_PROGNOSIS) { 
		if(forceBroadcaster || isBroadcaster(user)) {
			stopSession(() => {
				currentBotState = BotStates.WAITING_DISTRICT;
				tmiManager.publishMessage(channel, messagesData.session_stopped);

				pronoCounter.resetCount();
				pronoCounter.resetTempCounts(true);
				
				var pronoCountsJson = "{'subject': 'PRONO_COUNT', ";
				pronoCountsJson += "'data': " + pronoCounter.toJson() + "}\n";
				uniguessSocketManager.sendMessage(pronoCountsJson);
				
				if(pronoCountRefreshDaemon != null) {
					clearInterval(pronoCountRefreshDaemon);
				}

				// eventsEmitter.emit('onDatabaseUpdated');
			});
		}
	} else {
		tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
	}
}

/**
 * Traite la commande !pays :
 *   - Pour -ajouter : ajout d'un district dans la base de données avec sa
 *   				   côte.
 *   - Sinon : Attribution des scores en fonction du district indiqué et
 *   		   réinitialisation des pronos.
 * @param {String}  channel 		 - Nom de la chaîne du diffuseur.
 * @param {Object}  user 			 - Utilisateur auteur de la commande.
 * @param {String}  rawArguments	 - Arguments de la commande non décomposés.
 * @param {Boolean} forceBroadcaster - Si vrai, traite l'utilisateur en entrée
 * 									   comme le diffuseur.
 */
function processDistrictCommand(channel, user, rawArguments, forceBroadcaster) {
	// Stockage des paramètres dans un tableau pour faciliter le traitement
	var commandArguments = rawArguments.split(" ");
	var username = extractUsername(user);
	
	switch(commandArguments[0].toLowerCase()) {
	case "-ajouter":
		if(currentBotState == BotStates.WAITING_PROGNOSIS
		|| currentBotState == BotStates.IN_PROGNOSIS
		|| currentBotState == BotStates.WAITING_DISTRICT) {
			if(forceBroadcaster || isBroadcaster(user)) {
				// S'il y a 3 arguments, alors le district doit être ajouté ou
				// bien sa cote mise à jour
				if(commandArguments.length >= 3) {
					var districtName = commandArguments[1];
					var districtOdds = commandArguments[2];
					
					if(districtName) {
						addDistrict(channel, user, districtName, districtOdds, () => {
							eventsEmitter.emit('onDatabaseUpdated');
						});
					} else {
						tmiManager.publishMessage(channel, messagesData.district_add_help, [username]);
					}
				} else {
					tmiManager.publishMessage(channel, messagesData.district_add_help, [username]);
				}
			}
		} else {
			tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
		}
		break;
	case "-top":
		if(currentBotState == BotStates.WAITING_PROGNOSIS
		|| currentBotState == BotStates.IN_PROGNOSIS
		|| currentBotState == BotStates.WAITING_DISTRICT) {
			if(forceBroadcaster || isModeratorOrBroadcaster(user)) {
				if(commandArguments.length >= 2) {
					var topRaw = commandArguments[1];
					displayDiscoveredDistrictTop(channel, topRaw);
				} else if(commandArguments.length == 1) {
					displayDiscoveredDistrictTop(channel, null);
				} else {
					tmiManager.publishMessage(channel, messagesData.district_top_help, [username]);
				}
			}
		} else {
			tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
		}
		break;
	default:
		if(currentBotState == BotStates.WAITING_DISTRICT) {
			if(forceBroadcaster || isBroadcaster(user)) {
				if(rawArguments) {
					attributeGains(channel, username, rawArguments, (discoveredDistrict, playingPlayers) => {
						recordSessionResult(discoveredDistrict, playingPlayers, () => {
							// Réinitialisation des pronostics une fois les points
							// attribués
							resetAllPrognosis(() => {
								currentBotState = BotStates.WAITING_PROGNOSIS;
								
								eventsEmitter.emit('onDatabaseUpdated');
							});
						});
					});
				} else {
					tmiManager.publishMessage(channel, messagesData.district_help, [username]);
				}
			}
		} else {
			tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
		}
		break;
	}
}

/**
 * Traite la commande !cote en affichant la côte du district indiqué.
 * @param {String}  channel 		 - Nom de la chaîne du diffuseur.
 * @param {Object}  user 			 - Utilisateur ayant posté le message.
 * @param {String}  rawArguments	 - Arguments de la commande non décomposés.
 * @param {Boolean} forceBroadcaster - Si vrai, traite l'utilisateur en entrée
 * 									   comme le diffuseur.
 */
function processOddsCommand(channel, user, rawArguments, forceBroadcaster) {
	var commandArguments = rawArguments.split(" ");
	var username = extractUsername(user);
	
	if(currentBotState == BotStates.WAITING_PROGNOSIS
	|| currentBotState == BotStates.IN_PROGNOSIS
	|| currentBotState == BotStates.WAITING_DISTRICT) {
		if(commandArguments.length > 0 && commandArguments[0].length > 0) {
			var districtName = mongooseManager.applyDistrictDefaultFormatting(commandArguments[0]);
			
			if(districtName) {
				// Récupération, depuis la base de données, des données d'un
				// district à partir du nom donné en argument puis affichage de sa
				// cote
				mongooseManager.getDistrictByName(districtName, (districtDoc) => {
					if(districtDoc) {
						// Post d'un message contenant la cote du district
						tmiManager.publishMessage(channel, messagesData.district_odds, [districtName, districtDoc.odds]);
					} else {
						// Post d'un message indiquant que le district demandé
						// n'est pas dans la base de données
						tmiManager.publishMessage(channel, messagesData.district_not_recognized_one, [username, districtName]);
					}
				});
			} else {
				tmiManager.publishMessage(channel, messagesData.district_not_recognized_one_special, [username]);
			}
		} else {
			tmiManager.publishMessage(channel, messagesData.district_odds_help, [username]);
		}
	} else {
		tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
	}
}

/**
 * Traite la commande !score :
 *   - Pour -afficher_tous : affichage du score de tous les viewers s'il est
 *     supérieur à 0. Affiche les 3 premiers joueurs dans tous les cas.
 *   - Pour -ajouter : ajout de points au joueur indiqué d'après une valeur
 *     d'incrément passée en paramètres.
 *   - Sinon : affichage du score de l'auteur de la commande.
 * @param {String}  channel 		 - Nom de la chaîne du diffuseur.
 * @param {Object}  user 		 	 - Utilisateur ayant posté la commande.
 * @param {String}  rawArguments 	 - Arguments non décomposés de la commande.
 * @param {Boolean} forceBroadcaster - Si vrai, traite l'utilisateur en entrée
 * 									   comme le diffuseur.
 */
function processScoreCommand(channel, user, rawArguments, forceBroadcaster) {
	var commandArguments = rawArguments.split(" ");
	var username = extractUsername(user);
	
	switch(commandArguments[0].toLowerCase()) {
	case "-afficher_tous":
		if(currentBotState == BotStates.WAITING_PROGNOSIS
		|| currentBotState == BotStates.IN_PROGNOSIS
		|| currentBotState == BotStates.WAITING_DISTRICT) {
			if(forceBroadcaster || isModeratorOrBroadcaster(user)) {
				displayAllScores(channel);
			}
		} else {
			tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
		}
		break;
	case "-ajouter":
		if(currentBotState == BotStates.WAITING_PROGNOSIS
		|| currentBotState == BotStates.IN_PROGNOSIS
		|| currentBotState == BotStates.WAITING_DISTRICT) {
			if(forceBroadcaster || isBroadcaster(user)) {
				// S'il y a 3 arguments, alors le score du joueur doit être
				// incrémenté après que l'incrément ait été corectement parsé
				if(commandArguments.length == 3) {
					var playerName = commandArguments[1];
					var scoreGain = parseFloat(commandArguments[2]);

					if(playerName && !isNaN(scoreGain)) {
						incrementScore(channel, playerName, scoreGain, () => {
							eventsEmitter.emit('onDatabaseUpdated');
						});
					} else if (playerName) {
						tmiManager.publishMessage(channel, messagesData.player_score_unrecognized_value, [commandArguments[2]]);
					} else {
						tmiManager.publishMessage(channel, messagesData.player_score_help, [username]);
					}
				} else {
					tmiManager.publishMessage(channel, messagesData.player_score_help, [username]);
				}
			}
		} else {
			tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
		}
		break;
	default:
		if(currentBotState == BotStates.WAITING_PROGNOSIS
		|| currentBotState == BotStates.IN_PROGNOSIS
		|| currentBotState == BotStates.WAITING_DISTRICT) {
			// Récupération, depuis la base de données, des données du joueur
			// auteur de la commande puis post de son score et de son nom
			mongooseManager.getPlayerByName(username, (playerDoc) => {
				if(playerDoc != null) {
					var playerScore = roundScore(playerDoc.score);
					tmiManager.publishMessage(channel, messagesData.player_score_after_request, [playerScore, playerDoc.name]);
				}
			});
		} else {
			tmiManager.publishForbiddenCommandMessage(channel, currentBotState);
		}
		break;
	}
}

function processCursedPaniacCommand(channel, user, rawArguments, forceBroadcaster) {
	if(configurationData.prono_tray) {
		var cursedPaniacStatus = configurationData.prono_tray.cursed_paniac_status;

		if(cursedPaniacStatus == "ENABLED"
		|| (cursedPaniacStatus == "DISABLED_FOR_VIEWERS" && (isModeratorOrBroadcaster(user) || forceBroadcaster))) {
			var username = extractUsername(user);
			uniguessSocketManager.sendMessage("{'subject': 'CURSED_PANIAC_SPAWNED', 'data': {'invokator_name': '" + username + "'}}\n");
		}
	}
}

/**
 * Traite le message posté en partant du principe qu'il s'agit d'une commande
 * et tente de le décomposer tout en renvoyant le résultat de la décomposition.
 * @param  {String} message - Message brut reçu par le bot.
 * @return {String[]}		  Tableau contenant la commande ainsi q'une chaîne
 *                            dans laquelle sont concaténés tous les arguments.
 */
function parseCommand(message) {
	// Échappement du préfixe (ici : "!")
	var escapedPrefix = COMMAND_PREFIX.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");

	// Création d'un outil d'extraction de la commande
	var regexp = new RegExp("^" + escapedPrefix + "([a-zA-Z_]+)\s?(.*)");

	// Extraction et retour de la commande
	return regexp.exec(message);
}

/**
 * Indique si l'utilisateur Twitch en entrée est un modérateur ou le
 * diffuseur du live en cours.
 * @param  {Object}  user - Joueur dont on veut s'assurer du statut.
 * @return {Boolean}        True si le joueur est aussi le diffuseur ou un
 *                          modérateur, false sinon.
 */
function isModeratorOrBroadcaster(user) {
	return user != null && (user.mod || (user.badges != null && user.badges.broadcaster == '1'));
}

/**
 * Indique si l'utilisateur Twitch en entrée est un modérateur du live en
 * cours.
 * @param  {Object}  user - Joueur dont on veut s'assurer du statut.
 * @return {Boolean}        True si le joueur est aussi un modérateur, false
 *                          sinon.
 */
function isModerator(user) {
	return user != null && (user.mod);
}

/**
 * Indique si l'utilisateur Twitch en entrée est le diffuseur du live en cours.
 * @param  {Object}  user - Joueur dont on veut s'assurer du statut.
 * @return {Boolean}        True si le joueur est aussi le diffuseur ou un
 *                          modérateur, false sinon.
 */
function isBroadcaster(user) {
	return user != null && (user.badges != null && user.badges.broadcaster == '1');
}

function extractUsername(userData) {
	if(userData != null) {
		return userData["username"];
	} else {
		return configurationData.target_channel;
	}
}


/* ===== Fonctions de traitement des commandes ===== */



function refreshPronoRate() {
	pronoCounter.resetTempCounts(false);

	var pronoCountsJson = "{'subject': 'PRONO_COUNT', ";
	pronoCountsJson += "'data': " + pronoCounter.toJson() + "}\n";
	uniguessSocketManager.sendMessage(pronoCountsJson);
}

/**
 * Sauvegarde le prono du joueur en entrée à l'aide du nom du district entré
 * par ce dernier.
 * @param {String} 	 channel 	   - Nom de la chaîne du diffuseur.
 * @param {String} 	 playerName    - Nom du joueur pour le lequel mettre à
 *                                   jour le prono.
 * @param {String[]} districtNames - Nom des districts pronostiqués.
 * @param {Function} callback      - Fonction à appeler une fois que tous les
 *                             	     pronos ont été sauvegardés.
 */
function savePlayerPrognosis(channel, playerName, districtNames, callback) {
	for(var i = 0; i < districtNames.length; i++) {
		districtNames[i] = mongooseManager.applyDistrictDefaultFormatting(districtNames[i]);
	}
	
	doubleRemovingData = removeDoubleArrayElements(districtNames);
	
	districtNames = doubleRemovingData.updatedArray;
	var doubleDistrictNames = doubleRemovingData.doubleElements;
	
	if(doubleDistrictNames.length > 0) {
		var doublePronoMessageFragment = humanReadableArrayExpression(doubleDistrictNames, true);
		
		if(doubleDistrictNames.length == 1) {
			tmiManager.publishMessage(channel, messagesData.prognosis_double_one, [playerName, doublePronoMessageFragment]);
		} else if(doubleDistrictNames.length > 1) {
			tmiManager.publishMessage(channel, messagesData.prognosis_double_multiple, [playerName, doublePronoMessageFragment]);
		}
	}
	
	var pronoAmountLimit = configurationData.bot.prono_amount_limit;

	if(districtNames.length > pronoAmountLimit) {
		if(pronoAmountLimit - districtNames.length == 1) {
			tmiManager.publishMessage(channel, messagesData.prognonsis_amount_overflow_one, [playerName, pronoAmountLimit]);
		} else {
			tmiManager.publishMessage(channel, messagesData.prognonsis_amount_overflow_multiple, [playerName, pronoAmountLimit]);
		}
		
		districtNames.splice(pronoAmountLimit, districtNames.length);
	}
	
	// Récupération, depuis la base de données, des données des districts et
	// mise à jour des pronostic du joueur à partir de ces dernières
	mongooseManager.getDistrictsByNames(districtNames, (districtDocs) => {
		if(districtDocs != null) {
			var unrecognizedDistrictNames = findMissingDistrictNames(districtNames, districtDocs);
			
			if(unrecognizedDistrictNames.length > 0) {
				if(unrecognizedDistrictNames.length == 1) {
					var unrecognizedDistrictsFragment = humanReadableArrayExpression(unrecognizedDistrictNames, false);
					if(unrecognizedDistrictNames[0]) {
						tmiManager.publishMessage(channel, messagesData.district_not_recognized_one, [playerName, unrecognizedDistrictsFragment]);
					} else {
						tmiManager.publishMessage(channel, messagesData.district_not_recognized_one_special, [playerName]);
					}
				} else if(unrecognizedDistrictNames.length > 1) {
					var unrecognizedDistrictsFragment = humanReadableArrayExpression(unrecognizedDistrictNames, true);
					tmiManager.publishMessage(channel, messagesData.district_not_recognized_multiple, [playerName, unrecognizedDistrictsFragment]);
				}
			}
			
			var existingDistrictNames = filterExistingDistrictNames(districtDocs);
			
			var enabledDistricts = filterEnabledDistricts(districtDocs);
			var unavailableDistrictNames = findMissingDistrictNames(existingDistrictNames, enabledDistricts);
			
			if(unavailableDistrictNames.length > 0) {
				var unavailableDistrictsFragment = humanReadableArrayExpression(unavailableDistrictNames, true);
				
				if(unavailableDistrictNames.length == 1) {
					if(unavailableDistrictNames[0]) {
						tmiManager.publishMessage(channel, messagesData.district_not_available_one, [playerName, unavailableDistrictsFragment]);
					}
				} else if(unavailableDistrictNames.length > 1) {
					tmiManager.publishMessage(channel, messagesData.district_not_available_multiple, [playerName, unavailableDistrictsFragment]);
				}
			}
			
			if(districtDocs.length > 0) {
				updatePrognosis(playerName, districtDocs, callback);
			}
		} else {
			tmiManager.publishMessage(channel, messagesData.district_not_recognized_one, [districtNames]);
		}
	});
}

function removeDoubleArrayElements(array) {
	var checkedElements = [];
	var doubleElements = [];
	
	for(var i = array.length - 1; i >= 0; i--) {
		var arrayElement = array[i];
		
		if(checkedElements.includes(arrayElement)) {
			doubleElements.push(arrayElement);
			array.splice(i, 1);
		} else {
			checkedElements.push(arrayElement);
		}
	}
	
	return { updatedArray: array, doubleElements: doubleElements };
}

function filterEnabledDistricts(districts) {
	var enabledDistricts = [];
	
	districts.forEach((district) => {
		if(district.is_enabled) {
			enabledDistricts.push(district);
		}
	});
	
	return enabledDistricts;
}

function filterExistingDistrictNames(districts) {
	var existingDistrictNames = [];

	for (let i = 0; i < districts.length; i++) {
		existingDistrictNames.push(districts[i].name);
	}

	return existingDistrictNames;
}

function findMissingDistrictNames(referenceDistrictNames, districts) {
	var missingDistricts = [];
	
	referenceDistrictNames.forEach((referenceDistrictName) => {
		var isIncludingDistrict = false;
		
		for(var i = 0; i < districts.length && !isIncludingDistrict; i++) {
			var newDistrictNameRegex = mongooseManager.applyDistrictDefaultRegexp(districts[i].name);
			
			if(referenceDistrictName.match(newDistrictNameRegex)) {
				isIncludingDistrict = true;
			}
		}
		
		if(!isIncludingDistrict) {
			missingDistricts.push(referenceDistrictName);
		}
	});
	
	return missingDistricts;
}

	/**
	 * Met à jour le pronostic du joueur en entrée à l'aide de l'ID du district en
	 * entrée.
	 * @param {String} 	 playerName  - Nom du joueur pour le lequel mettre à
	 *                                 jour le prono.
	 * @param {Document} districtDoc - District récupéré depuis la base de 
	 *                                 données.
	 */
function updatePrognosis(playerName, districtDocs, callback) {
	// Récupération, depuis la base de données, des données du joueur
	// correspondant au nom en entrée et mise à jour de son prono
	mongooseManager.getPlayerByName(playerName, (playerDoc) => {
		if(playerDoc != null) {
			var districtIds = [];
			var districtNames = [];

			districtDocs.forEach((districtDoc) => {
				if(districtDoc.is_enabled) {
					districtIds.push(districtDoc._id);
					districtNames.push(districtDoc.name);
				}
			});
				
			// Actualisation du pronosotic du joueur
			playerDoc.prognosis = districtIds;
			
			// Sauvegarde de l'état du joueur dans la base de données distante
			playerDoc.save((savingError) => {
				if (savingError) {
					console.error("Erreur lors de la mise à jour d'un prono : ", savingError);
				}

				if(callback != null) {
					callback(districtNames);
				}
			});
		} else {
			console.error("\"" + playerName + "\" n'est pas dans ma liste de joueurs");
		}
	});	
}

/**
 * Affiche le prono de tous les joueurs du live en cours.
 * @param {String} channel - Nom de la chaîne du diffuseur.
 */
function displayAllPrognosis(channel) {
	mongooseManager.getAllPlayingPlayers((playerDocs) => {
		// Message à afficher en fin de traitement
		var prognosisMessage = "";

		// Nombre de joueurs traités
		var processedPlayersCount = 0;

		// Nombre de pronostics effectués par les joueurs
		var prognosisCount = 0;

		if(playerDocs.length > 0) {
			playerDocs.forEach((playerDoc) => {
				if(playerDoc != null) {
					// Récupération, depuis la base de données, des données des
					// du district pronostiqués par le joueur courant, puis
					// construction du message des pronos
					mongooseManager.getDistrictsByIds(playerDoc.prognosis, (districtDocs) => {
						if(districtDocs.length > 0) {
							prognosisMessage += playerDoc.name + " => ";
							
							for(var i = 0; i < districtDocs.length; i++) {
								var districtDoc = districtDocs[i];
								
								prognosisMessage += districtDoc.name;
								if(i < districtDocs.length - 1) {
									prognosisMessage += ", ";
								} else {
									prognosisMessage += "; ";
								}
							}
							
							prognosisCount++;
						}

						// Si tous les joueurs ont été traités, alors le
						// message construit tout au long du traitement
						// construit doit être posté. Celui-ci indique le
						// pronostic donné par chaque joueur
						if(processedPlayersCount >= playerDocs.length - 1) {
							if(prognosisCount > 0) {
								tmiManager.publishMessage(channel, prognosisMessage);
							} else {
								tmiManager.publishMessage(channel, messagesData.nobody_made_prognosis);
							}
						}

						// Incrémentation du nombre de joueurs traités
						processedPlayersCount++;
					});
				}
			});
		} else {
			tmiManager.publishMessage(channel, messagesData.nobody_made_prognosis);
		}
	});
}

/**
 * Affiche le prono en cours d'un seul joueur
 * @param {String} channel  - Nom de la chaîne du diffuseur.
 * @param {String} username - Nom du joueur dont on veut afficher le prono
 */
function displayPlayerPrognosis(channel, username) {
	mongooseManager.getPlayerByName(username, (playerDoc) => {
		if(playerDoc != null) {
			mongooseManager.getDistrictsByIds(playerDoc.prognosis, (districtDocs) => {
				// Si le joueur a pronostiqué au moins un pays, ces derniers
				// sont affichés
				// Sinon, Un message inidquant au joueur qu'il n'a pas fait de
				// prono est affiché
				if(districtDocs.length > 0) {
					var districtsFragments = "";

					for(var i = 0; i < districtDocs.length; i++) {
						var districtDoc = districtDocs[i];
						
						districtsFragments += districtDoc.name;

						if(i < districtDocs.length - 1) {
							if(i == districtDocs.length - 2) {
								districtsFragments += " et ";
							} else {
								districtsFragments += ", ";
							}
						}
					}
					
					tmiManager.publishMessage(channel, messagesData.player_prognosis, [username, districtsFragments]);
				} else {
					tmiManager.publishMessage(channel, messagesData.player_no_prognosis, [username]);
				}
			});
		}
	});
}

function updatePronoAmoutLimit(channel, rawPronoAmoutLimit, username) {
	var pronoAmountLimit = parseInt(rawPronoAmoutLimit);
	
	if(!isNaN(pronoAmountLimit) && pronoAmountLimit > 0) {
		configurationData.bot.prono_amount_limit = pronoAmountLimit;
		
		settingsLoader.saveConfiguration(configurationData);
		var actualConfigurationData = settingsLoader.loadConfiguration();
		
		if(JSON.stringify(configurationData) == JSON.stringify(actualConfigurationData)) {
			var actualPronoAmountLimit = configurationData.bot.prono_amount_limit;
			tmiManager.publishMessage(channel, messagesData.prognosis_limit_confirmation, [username, actualPronoAmountLimit]);
		} else {
			console.error("La limite de prono n'a pas pu être enregistrée.");
		}
	} else {
		tmiManager.publishMessage(channel, messagesData.prognosis_limit_help, [username]);
	}
}

function startSession(callback) {
	resetAllPrognosis(() => {
		mongooseManager.insertSession((updateResult) => {
			if(updateResult.lastErrorObject) {
				currentSessionId = updateResult.lastErrorObject.upserted;
			}

			if(callback != null) {
				callback();
			}
		});			
	});
}

/**
 * Réinitialise tous les pronostics en mettant leur valeur à null pour chaque
 * joueur.
 * @param {Function} callback - Fonction à appeler une fois que tous les
 *                             	pronos ont été réinitialisés.
 */
function resetAllPrognosis(callback) {
	// Récupération, depuis la base de données, de tous les joueurs et
	// réinitialisation de leur prono
	mongooseManager.getAllPlayers((playerDocs) => {
		var resetPlayerCount = 0;

		if(playerDocs.length > 0) {
			// Pour chaque joueur, le prono doit être défini à null et les données
			// correspondantes doivent être mise à jour du dans la base de données
			playerDocs.forEach((playerDoc) => {
				playerDoc.prognosis = null;

				// Sauvegarde de l'état du joueur dans la base de données
				// distante
				playerDoc.save((savingError) => {
					if (savingError) {
						console.error("Erreur lors de la mise à jour d'un prono : ", savingError);
					}

					resetPlayerCount++;

					if(resetPlayerCount == playerDocs.length) {
						if(callback != null) {
							callback();
						}
					}
				});
			});
		} else {
			if(callback != null) {
				callback();
			}
		}
	});
}

function stopSession(callback) {
	mongooseManager.updateCurrentSession(null, [], (updateResult) => {
		if(callback != null) {
			callback();
		}
	});			
}

/**
 * Ajoute ou modifie un district s'il existe déjà en fonction du nom et de la
 * cote de ce nouveau district.
 * @param {String}	channel 	 - Nom de la chaîne du diffuseur.
 * @param {Object}  user 		 - Utilisateur ayant posté la commande.
 * @param {String}	districtName - Nom du district à ajouter.
 * @param {Integer} odds 		 - Côte du nouveau district.
 */
function addDistrict(channel, user, districtName, odds, callback) {	
	districtName = mongooseManager.applyDistrictDefaultFormatting(districtName);
	
	// Récupération, depuis la base de données, des données du district à
	// ajouter, puis mise à jour ou ajout de celui-ci dans les entrée selon
	// qu'il existe déjà ou non.
	mongooseManager.getDistrictByName(districtName, (districtDoc) => {
		// Mise en forme de la cote entrée par l'utlisateur et seuillage de
		// celle-ci à 0
		odds = odds.replace(",", ".");
		var oddsValue = parseFloat(odds);
		oddsValue = Math.max(0, oddsValue);

		// Si la cote entrée par l'utilisateur est valide, alors le district
		// doit être ajouté ou modifié
		if(!isNaN(oddsValue)) {
			odds = oddsValue.toString();

			// Si le district n'est pas présent dans la base de données, alors
			// ce dernier doit y être ajouté. Sinon, il doit être mis à jour
			if(districtDoc == null) {
				// Nouveau district à insérer dans la base de données
				var newDistrict = { name: districtName.toLowerCase(), odds: odds };

				// Insertion du nouveau district dans la base de données puis
				// post d'un message de confirmation
				mongooseManager.insertDistrict(newDistrict, () => {
					tmiManager.publishMessage(channel, messagesData.district_added, [districtName, odds]);

					if(callback != null) {
						callback();
					}
				});
			} else {
				// Mise à jour de la cote du district
				districtDoc.odds = odds;

				// Sauvegarde de l'état du district dans la base de données
				// distante
				districtDoc.save((savingError) => {
					if (savingError) {
						console.error("Erreur lors de la mise à jour d'un district : ", savingError);
					} else {
						// Post d'un message pour indiquer que le district
						// existait déjà mais a été mis à jour
						tmiManager.publishMessage(channel, messagesData.district_updated, [districtName, odds]);

						if(callback != null) {
							callback();
						}
					}
				});
			}
		} else {
			if(user != null) {
				tmiManager.publishMessage(channel, messagesData.district_add_help, [user['display-name']]);
			}
		}
	});
}

function displayBetDistrictTop(channel, topRaw) {
	var messages = {
		topAllMessage: messagesData.prognosis_top_all,
		topLimitedMessage: messagesData.prognosis_top_limited
	};

	displayDistrictTop(channel, topRaw, mongooseManager.betDistrict, messages);
}

function displayDiscoveredDistrictTop(channel, topRaw) {
	var messages = {
		topAllMessage: messagesData.district_top_all,
		topLimitedMessage: messagesData.district_top_limited
	};

	displayDistrictTop(channel, topRaw, mongooseManager.discoveredDistrict, messages);
}

function displayDistrictTop(channel, topRaw, districtCollection, messages) {
	var top = Number.MAX_SAFE_INTEGER;

	if(topRaw != null) {
		var topParsed = parseInt(topRaw);

		if(!isNaN(topParsed)) {
			top = Math.max(1, topParsed);
		}
	}

	mongooseManager.getDistrictsTop(top, districtCollection, (districtsStats) => {
		var readableStatMembers = [];

		districtsStats.forEach(districtsStat => {
			var districtName = districtsStat.name;

			var nameMember = districtName[0].toUpperCase() + districtName.slice(1).toLowerCase();
			var countMember = districtsStat.count + " fois";
			var rateMember = Math.trunc(districtsStat.ratio * 10000) / 100 + " %";

			var readableStat = nameMember + " (" + countMember + " - " + rateMember + ")";

			readableStatMembers.push(readableStat);
		});

		var statsMessage = humanReadableArrayExpression(readableStatMembers);

		if(topRaw != null) {
			tmiManager.publishMessage(channel, messages.topLimitedMessage, [top, statsMessage]);
		} else {
			tmiManager.publishMessage(channel, messages.topAllMessage, [statsMessage]);
		}
	});
}

/**
 * Attribue un gain de score à tous les joueurs présents en fonction du
 * district qu'ils ont précédement pronostiqué et du district actuel en entré.
 * Affiche ensuite le nombre de points gagnés par chaque gagnant.
 * @param {String}   channel 	  - Nom de la chaîne du diffuseur.
 * @param {String} 	 districtName - Nom du district correct.
 * @param {Function} callback 	  - Fonction à appeler lorsque l'attribution
 *                               	est terminée.
 */
function attributeGains(channel, username, districtName, callback) {
	districtName = mongooseManager.applyDistrictDefaultFormatting(districtName);
	
	// Récupération, depuis la base de données, des données du district
	// correspondant au nom du district courant en entrée, puis traitement
	// de chaque joueur
	mongooseManager.getDistrictByName(districtName, (districtDoc) => {
		if(districtDoc != null) {
			// Récupération de tous les joueurs du live puis attribution de leur score
			mongooseManager.getAllPlayingPlayers((playerDocs) => {
				// Gain calculé en fonction de la cote du district courant et
				// d'un multiplicateur constant
				districtDoc.odds = districtDoc.odds.replace(",", ".");
				var oddsValue = parseFloat(districtDoc.odds);
				var rawGain = oddsValue * ODDS_COMMON_MULTIPLICATOR * oddsSpecialMultiplicator;
				var playerCount = playerDocs.length;
				
				if(playerCount > 0) {
					// Pour chaque joueur, le prono doit être récupéré puis les
					// scores gérés en conséquence
					var winnerReports = findSessionWinners(playerDocs, districtDoc, rawGain);
					updateWinnersScore(channel, winnerReports, districtDoc, playerDocs, callback);
				} else {
					tmiManager.publishMessage(channel, messagesData.nobody_guessed_district);

					if(callback) {	
						callback(districtDoc, playerDocs);
					}
				}
			});
		} else {
			tmiManager.publishMessage(channel, messagesData.district_not_recognized_one, [username, districtName]);
		}
	});
}

/**
 * Met à jour le score d'un joueur dans la base de donées.
 * @param {String[]} playerDocs		- Joueurs ayant effectué un prono.
 * @param {Document} districtDoc   	- District récupéré depuis la base de
 * 									  donnée.
 * @param {Integer}  rawGain		- Gain à ajouter au score du joueur.
 */
function findSessionWinners(playerDocs, districtDoc, rawGain) {
	var winnerReports = [];
	
	playerDocs.forEach((playerDoc) => {
		// Si les données du joueur on été trouvées, alors la construction du
		// message du chat doit se poursuivre
		if(playerDoc != null) {
			if(playerDoc.prognosis != null && playerDoc.prognosis.length > 0) {
				var scoreGain = /*Math.ceil(*/rawGain / playerDoc.prognosis.length/*)*/;
				var isPronoCorrect = false;
				
				for(var i = 0; i < playerDoc.prognosis.length && !isPronoCorrect; i++) {
					var pronoDistrictId = playerDoc.prognosis[i];
					
					if(pronoDistrictId.equals(districtDoc._id)) {
						isPronoCorrect = true;
					}
				}
				
				if(isPronoCorrect) {
					// Incrémentation du score du joueur gagnant
					playerDoc.score += scoreGain;
					winnerReports.push({
						playerDoc: playerDoc,
						gain: scoreGain,
						state: WinnerStates.NEW
					});
				}
			}
		}
	});
	
	return winnerReports;
}

function updateWinnersScore(channel, winnerReports, districtDoc, playerDocs, callback) {
	var processedWinners = 0;
	
	if(winnerReports.length > 0) {
		winnerReports.forEach((winnerReport) => {
			// Sauvegarde de l'état du joueur dans la base de données
			// distante
			winnerReport.playerDoc.save((savingError) => {
				if (savingError) {
					winnerReport.state = WinnerStates.FAILED;
					console.error("Erreur lors de la mise à jour d'un score : ", savingError);
				} else {
					winnerReport.state = WinnerStates.UPDATED;
				}
				
				// Si tous les joueurs ont été traités, alors le message construit
				// tout au long du traitement doit être posté
				if(processedWinners >= winnerReports.length - 1) {
					publishGainsMessage(channel, winnerReports);
					
					if(callback) {
						callback(districtDoc, playerDocs);
					}
				}
				
				processedWinners++;
			});
		});
	} else {
		tmiManager.publishMessage(channel, messagesData.nobody_guessed_district);

		if(callback) {	
			callback(districtDoc, playerDocs);
		}
	}
}

function publishGainsMessage(channel, winnerReports) {
	// Si au moins un joueur a correstement pronostiqué le district
	// courant, alors le message de félicitation ainsi que les
	// points attribués doivent être postés. Sinon, un message
	// de déception doit être posté.
	if(winnerReports.length > 0) {
		var gainsMessage = newGainsMessage(winnerReports);
		
		if(winnerReports.length == 1) {
			tmiManager.publishMessage(channel, messagesData.one_player_guessed_district, [gainsMessage]);
		} else if(winnerReports.length > 1) {
			tmiManager.publishMessage(channel, messagesData.several_players_guessed_district, [gainsMessage]);
		}
	} else {
		tmiManager.publishMessage(channel, messagesData.nobody_guessed_district);
	}
}

function newGainsMessage(winnerReports) {
	var gainsMessage = "";
	
	for(var i = 0; i < winnerReports.length; i++) {
		var winnerReport = winnerReports[i];
		
		if(winnerReport.state == WinnerStates.UPDATED) {
			var formattedGain = roundScore(winnerReport.gain);

			var gainFragment = formattedGain + " point" + (formattedGain == 1 ? "" : "s");
			gainsMessage += winnerReport.playerDoc.name + " qui gagne " + gainFragment;
			
			if(i < winnerReports.length - 1) {
				if(i == winnerReports.length - 2) {
					gainsMessage += " et à ";
				} else {
					gainsMessage += ", à ";
				}
			}
		}
	}
	
	return gainsMessage;
}

/**
 * Incrémente le score d'un joueur d'un certain montant.
 * @param {String}  channel 	   - Nom de la chaîne du diffuseur.
 * @param {String}  playerName     - Nom du joueur bénéficiaire.
 * @param {Integer} scoreIncrement - Nombre de points à ajouter au joueur.
 */
function incrementScore(channel, playerName, scoreIncrement, callback) {
	// Récupération, depuis la base de données, des données du joueur à partir
	// de son nom, puis incrémentation de son score
	mongooseManager.getPlayerByName(playerName, (playerDoc) => {
		if(playerDoc != null) {
			// Incrémentation du score du joueur
			playerDoc.score += scoreIncrement;

			// Mise à jour du joueur dans la base de données
			playerDoc.save((savingError) => {
				if (savingError) {
					console.error("Erreur lors de l'incrément d'un score : ", savingError);
				}

				var playerScore = roundScore(playerDoc.score);

				// Post d'un message indiquant le nouveau score du joueur
				var messagePointPart = playerScore + " point" + (playerScore == 1 ? " !" : "s !");
				tmiManager.publishMessage(channel, messagesData.player_score_after_gain, [playerName, messagePointPart]);

				if(callback != null) {
					callback();
				}
			});
		} else {
			tmiManager.publishMessage(channel, messagesData.player_not_recognized, [playerName]);
		}
	});
}

/**
 * Affiche le score de tous les joueurs dans un unique message.
 * @param {String} channel - Nom de la chaîne du diffuseur.
 */
function displayAllScores(channel) {	
	// Récupération de tous les joueurs du live puis affichage de leur score
	tmiManager.getAllChatters(configurationData.target_channel, (players) => {
		// Récupèration, depuis la base de données, des données de tous les
		// joueurs, puis construction du message du classement en conséquence
		mongooseManager.getAllPlayers((playerDocs) => {
			// Message à poster lorsque tous les joueurs ont été passés en revue
			var scoreMessage = "";

			// Position du joueur courant
			var position = 1;

			if(playerDocs.length > 0) {
				// Pour chaque joueur récupéré depuis la base de données, son nom,
				// accompagné de sa position et de son score, doit être ajouté s'il
				// regarde le live, possède des points ou qu'il est sur le podium
				playerDocs.forEach((playerDoc) => {
					if(playerDoc && playerDoc.name) {
						//| Sur poduim ? |  |------------------ Regarde le live ? -------------------|    |-- Possède score ? --|
						if(position <= 3 || (position > 3 && players.includes(playerDoc.name.toLowerCase()) && playerDoc.score > 0)) {
							// Ajout de la portion concernant le nom du joueur
							// et sa position
							scoreMessage += (position == 1 ? "" : ", ") + playerDoc.name + " est " + (position <= 1 ? "1er·ère" : position + "ème");
							
							var playerScore = roundScore(playerDoc.score);

							// Ajout de la portion concernant le score du
							// joueur
							scoreMessage += " avec " + playerScore + " points";
						}

						// Incrémentation de la position courante même si le
						// joueur courant n'est pas en train de regarder le
						// live
						position++;
					}
				});

				// Si le message contenant le score de chaque joueur retenus pour
				// être affichés n'est pas vide ou null, alors il doit être posté
				if(scoreMessage) {
					tmiManager.publishMessage(channel, scoreMessage);
				}
			} else {
				tmiManager.publishMessage(channel, messagesData.no_player);
			}
		});
	});
}

function recordSessionResult(actualDistrict, playingPlayers, callback) {
	var pronoTotalCount = 0;

	var loggedPlayerCount = 0;

	var loggedPronoCounts = {};
	var playerTotalPronoCounts = {};

	var winnerIds = [];

	var actualDistrictId = actualDistrict._id;

	playingPlayers.forEach((playingPlayer) => {
		if(playingPlayer.prognosis) {
			var playerProno = playingPlayer.prognosis;
			var wasPronoCorrect = isPronoCorrect(playerProno, actualDistrict);
			
			if(wasPronoCorrect) {
				winnerIds.push(playingPlayer._id);
			}

			var playingPlayerName = playingPlayer.name;
			loggedPronoCounts[playingPlayerName] = 0;
			playerTotalPronoCounts[playingPlayerName] = playerProno.length;

			playerProno.forEach((pronoDistrictId) => {
				var wasDistrictCorrect = (pronoDistrictId.equals(actualDistrictId));
				
				mongooseManager.insertBetDistrict(pronoDistrictId, playingPlayer, currentSessionId, wasDistrictCorrect, (updateResult, playerName) => {
					loggedPronoCounts[playerName]++;
					var playerTotalPronoCount = playerTotalPronoCounts[playerName];

					if(loggedPronoCounts[playerName] == playerTotalPronoCount) {
						loggedPlayerCount++;
					}
					
					if(loggedPlayerCount == playingPlayers.length) {
						endCurrentSession(actualDistrictId, winnerIds, callback);
					}
				});

				pronoTotalCount++;
			});
		}
	});

	if(pronoTotalCount == 0) {
		endCurrentSession(actualDistrictId, winnerIds, callback);	
	}
}

function endCurrentSession(actualDistrictId, winnerIds, callback) {
	mongooseManager.insertDiscoveredDistrict(actualDistrictId, (updateResult) => {
		if(updateResult.lastErrorObject) {
			var lastErorObject = updateResult.lastErrorObject;

			if(lastErorObject.upserted) {
				var discoveredDistrictId = lastErorObject.upserted;
				
				mongooseManager.updateCurrentSession(discoveredDistrictId, winnerIds, (updateResult) => {
					if(callback) {
						callback();
					}
				});
			} else {
				if(callback) {
					callback();
				}
			}
		} else {
			if(callback) {
				callback();
			}
		}
	});
}

function isPronoCorrect(playerProno, discoveredDistrict) {
	var matchingPronoDistrict = playerProno.find((pronoDistrictId) => {
		return pronoDistrictId.equals(discoveredDistrict._id);
	});

	return matchingPronoDistrict != null;
}

function humanReadableArrayExpression(array, surroundWithQuotes) {
	var message = "";
	
	for(var i = 0; i < array.length; i++) {
		var arrayElement = array[i];
		
		if(surroundWithQuotes) {
			message += "\"" + arrayElement + "\"";
		} else {
			message += arrayElement;
		}

		if(i < array.length - 2) {
			message += ", ";
		} else if(i == array.length - 2) {
			message += " et ";
		}
	}
	
	return message;
}

function roundScore(scoreRaw) {
	return Math.round(scoreRaw);
}

function exit() {
	mongooseManager.closeConnection();
	tmiManager.closeConnection();
	uniguessSocketManager.closeConnection();

	if(botStatePublisher) {
		clearInterval(botStatePublisher);
	}
}

function restart() {
	currentBotState = BotStates.WAITING_PROGNOSIS;

	configurationData = settingsLoader.loadConfiguration();
	messagesData = settingsLoader.loadMessages();
	
	startPronoGuessr();
}

module.exports.tmiManager = tmiManager;
module.exports.mongooseManager = mongooseManager;

module.exports.eventsEmitter = eventsEmitter;

module.exports.exit = exit;
module.exports.restart = restart;