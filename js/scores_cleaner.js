// Prefixe des commandes Twitch
const CSV_SEPARATOR = ";";


// Importation de la bibliothèque File System permettant notamment de lire ou
// modifier des fichiers
const fs = require('fs');

// Importation de la bibliothèque mkdirp qui permet de créer des dossiers
var mkdirp = require('mkdirp');

// Importation des réglages
const settingsLoader = require("./lib/settings_loader.js");

// Importation de la bibliothèque node-datetime permettant d'obtenir la date
// courante
const datetime = require('node-datetime');

// Importation de la bibliothèque csv-parser permettant de générer des fichiers
// CSV
const csvWriter = require('csv-writer');

// Importation du gestionnaire de la base de données mLab
const mongooseManagerBuilder = require("./lib/mongoose_manager.js"); 

var mongooseManager = new mongooseManagerBuilder.MongooseManager();

// Chargement des paramètres dans une variable JSON
var configurationData = settingsLoader.loadConfiguration();


if(configurationData) {
	// Connexion à la base de données mLab (ou MongoDB)
	mongooseManager.openConnection(configurationData.database_path, () => {
		// Chemin vers le fichier CSV de sortie, stockant les scores
		var outputFilePath = newScoreSaveFilePath();
		var oddsFolderPath = settingsLoader.getDataFolder() + "/scores";
		
		if(!fs.existsSync(oddsFolderPath)) {
			mkdirp(oddsFolderPath, function(error) {
				if(error) {
					console.error("Impossible de créer le dossier de sortie des scores.");
				} else {
					var csvCreator = initializeCsvCreator(outputFilePath);
					saveScores(outputFilePath, resetScores());
				}
			});
		} else {
			var csvCreator = initializeCsvCreator(outputFilePath);
			saveScores(outputFilePath, resetScores());
		}
	});
} else {
	console.error("Impossible de réinitialiser les scores sans avoir accès au fichier de configuration.");
}

/**
 * Génère un chemin vers un fichier de sauvegarde de scores à partir de la date
 * courante.
 * @returns {String} - Nouveau chemin ver le fichier de sortie stockant le
 * 					   score de tous les joueurs.
 */
function newScoreSaveFilePath() {
	var currentDateTime = datetime.create();
	var outputFileNameDateTime = currentDateTime.format('d-m-Y_H-M-S') + "_backup";
	return settingsLoader.getDataFolder() + '/scores/scores_' + outputFileNameDateTime + '.csv';
}

/**
 * Initialise de l'écriveur du fichier CSV de sortie.
 */
function initializeCsvCreator(outputFilePath) {
	csvCreator = csvWriter.createObjectCsvWriter({
		path: outputFilePath,
		header: [
			{id: 'name', title: 'Joueur'},
			{id: 'score', title: 'Score'}
		],
		fieldDelimiter:CSV_SEPARATOR
	});
}

/**
 * Sauvegarde le score de tous les joueurs en les récupérant depuis la base de
 * données puis en les enregistrant dans un fichier CSV.
 * @param {Function} callback - Fonction à appeler une fois que les scores ont
 * 	                          	été sauvegardés.
 */
function saveScores(outputFilePath, callback) {
	mongooseManager.getAllPlayers((playerDocs) => {
		playerRows = [];

		// Pour chaque joueur, une ligne contenant le nom et le score de ce
		// dernier doit être créée en vue d'être enregistrée dans le
		// fichier de sauvegarde
		playerDocs.forEach((playerDoc) => {
			playerRows.push({name: playerDoc.name, score: playerDoc.score});
		});

		// Enregistrement des scores dans le nouveau fichier CSV de sortie
		csvCreator.writeRecords(playerRows).then(() => {
			console.log('Scores enregistrés dans ' + outputFilePath);
			if(callback) {
				callback();
			}
		});
	});
}

/**
 * Réinitialisation du score de tous les joueurs au niveau de la base de
 * données. Attention, cette action est irréversible.
 */
function resetScores() {
	// Récupération des données de tous les joueurs depuis la base données et
	// réinitialisation de leur score
	mongooseManager.getAllPlayers((playerDocs) => {
		// Nombre de joueurs traités
		var processedPlayerCount = 0;

		// Pour chaque joueur, le score doit être remis à zéro
		playerDocs.forEach((playerDoc) => {
			playerDoc.score = 0;

			// Sauvegarde de l'état du joueur dans la base de données
			// distante
			playerDoc.save((savingError) => {
				if (savingError) {
					console.log("Erreur lors de la réinialisation d'un score : " , savingError);
				}

				// Si tous les joueurs ont été traité, le script doit se
				// fermer
				if(processedPlayerCount >= playerDocs.length - 1) {
					console.log('Scores réinitialisés');
					mongooseManager.closeConnection();
				}

				processedPlayerCount++;
			});
		});
	});
}