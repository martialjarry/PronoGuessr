// Prefixe des commandes Twitch
const CSV_SEPARATOR = ";";


//Importation des réglages
const settingsLoader = require("./lib/settings_loader.js");

// Importation de la bibliothèque File System permettant notamment de lire ou
// modifier des fichiers
const fs = require('fs');

// Importation de la bibliothèque csv-parser permettant de décomposer des
// fichiers CSV
const csvParser = require('csv-parser');

//Importation du gestionnaire de la base de données mLab
const mongooseManagerBuilder = require("./lib/mongoose_manager.js"); 

var mongooseManager = new mongooseManagerBuilder.MongooseManager();

// Lecture puis décomposition du fichier de réglages
var configurationData = settingsLoader.loadConfiguration();

// Number of updated players
var updatedPlayerCount = 0;

if(configurationData) {
	// Path to the scores file, given by the user via the command line
	var scoreFilePath = extractScoresFilePath();
	
	// Si le fichier de score existe, alors la mise à jour peut se poursuivre
	if (fs.existsSync(scoreFilePath)) {
		
		// Connexion à la base de données mLab (ou MongoDB)
		mongooseManager.openConnection(configurationData.database_path, () => {
			// Lecture et transmission du flux du fichier de scores vers le parseur CSV
			var streamReader = fs.createReadStream(scoreFilePath);
			var streamReaderPipe = streamReader.pipe(csvParser({ separator: CSV_SEPARATOR }, ['Joueur', 'Score']));
		
			// Nombre de joueurs extraits du fichier de scores
			var players = [];
		
			// Action à effectuer lorsqu'un lors de la lecture du fichier (ici, stockage
			// des données de joueurs dans un tableau temportaire)
			streamReaderPipe.on('data', (data) => {
				players.push(data)
			});
		
			// Action à effectuer lorsque la lecture du fichier est terminée (ici, mise
			// à jour en ligne des scores)
			streamReaderPipe.on('end', () => {
				uploadAllScores(players);
			});
		});
	} else {
		console.log("Désolé mais le fichier \"" + scoreFilePath + "\" n'existe pas.");
	}
} else {
	console.error("Impossible de mettre à jour les scores sans avoir accès au fichier de configuration.");
}


function extractScoresFilePath() {
	if(process.argv.length >= 3) {
		if(process.argv[2].startsWith("file:/")) {
			return decodeURI(process.argv[2].replace("file:/", ""));
		} else {
			return settingsLoader.getDataFolder() + "/scores/" + process.argv[2];
		}
	} else {
		return null;
	}
}

/**
 * Créé ou met à jour le score de chaque joueur présent dans le fichier de
 * scores.
 */
function uploadAllScores(players) {
	for(var i = 0; i < players.length; i++) {
		var playerData = players[i];

		// Nom du joueur courant
		var playerName = playerData['Joueur'];

		// Score courant
		var playerScore = parseFloat(playerData['Score']);

		uploadScore(playerName, playerScore, players.length);
	}
}

function uploadScore(playerName, playerScore, playerCount) {
	var updateCondition = {
		'name': { $eq: playerName.toLowerCase() }
	};

	var updateInstruction = {
		$set: {
			'name': playerName.toLowerCase(),
			'score': playerScore,
		}
	};

	updateScore(playerCount, updateCondition, updateInstruction, true);
}

function updateScore(playerCount, updateCondition, updateInstruction, enableUpsert) {
	mongooseManager.updatePlayer(updateCondition, updateInstruction, enableUpsert, (updateReport) => {
		updatedPlayerCount++;

		if(updatedPlayerCount >= playerCount) {
			closeUpdater("J'ai restauré les scores !");
		}
	});
}

/**
 * Déconnecte le script de la base de données, ce qui a pour effet de mettre
 * fin au script.
 * @param {String} message - Message affiché lors de la déconnection.
 */
function closeUpdater(message) {
	console.log(message);
	mongooseManager.closeConnection();
}