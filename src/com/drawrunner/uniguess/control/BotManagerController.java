package com.drawrunner.uniguess.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTextField;

import com.drawrunner.uniguess.model.communication.nodejs.BotStateListener;
import com.drawrunner.uniguess.model.communication.nodejs.BotStates;
import com.drawrunner.uniguess.model.communication.nodejs.CursedPaniacListener;
import com.drawrunner.uniguess.model.communication.nodejs.NodeJsConnectionStateListener;
import com.drawrunner.uniguess.model.communication.nodejs.NodeJsLinkManager;
import com.drawrunner.uniguess.model.data.CommandTypes;
import com.drawrunner.uniguess.model.data.Configuration;
import com.drawrunner.uniguess.model.data.CursedPaniacStatus;
import com.drawrunner.uniguess.view.display.swing.BotManagerFrame;
import com.drawrunner.uniguess.view.display.swing.CommandArgumentsFrame;
import com.drawrunner.uniguess.view.display.swing.CursedPaniacDisablingFrame;

public class BotManagerController implements ActionListener, BotStateListener, NodeJsConnectionStateListener, CursedPaniacListener {
	private BotManagerFrame botManagerFrame;
	private CommandArgumentsFrame commandArgumentsFrame;

	private CursedPaniacDisablingFrame cursedPaniacDisablingFrame;
	
	private NodeJsLinkManager nodeJsLinkManager;
	
	private BotStates botState;
	
	public BotManagerController(PronoTrayController pronoTrayController) {
		botManagerFrame = new BotManagerFrame(this, pronoTrayController);
		commandArgumentsFrame = null;
		
		cursedPaniacDisablingFrame = new CursedPaniacDisablingFrame(this);
		
		Configuration configuration = Configuration.getInstance();
		
		botManagerFrame.disableAllButtons();
		botManagerFrame.togglePronoCounterCheckbox(configuration.isDisplayCounter());
		
		try {
			nodeJsLinkManager = new NodeJsLinkManager();
			nodeJsLinkManager.addConnectionStateListener(this);
			nodeJsLinkManager.addBotStateListener(this);
			
			Thread serverLinkThread = new Thread(nodeJsLinkManager);
			serverLinkThread.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		botState = BotStates.WAITING_PROGNOSIS;
	}
	
	@Override
	public void nodeJsConnectionStateChanged(boolean isConnected) {
		botManagerFrame.toggleLaunchPronoguessrButton(!isConnected);
		
		if(!isConnected) {
			botManagerFrame.disableAllButtons();
		}
	}
	
	@Override
	public void botStateRefreshed(BotStates botState) {
		this.botState = botState;
		
		switch (botState) {
		case WAITING_PROGNOSIS:
			botManagerFrame.displayAsWaitingPronosis();
			break;
		case IN_PROGNOSIS:
			botManagerFrame.displayAsInPronosis();
			break;
		case WAITING_DISTRICT:
			botManagerFrame.displayAsWaitingDistrict();
			break;
		default:
			break;
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		if(event.getSource() instanceof JButton) {
			JButton sourceButton = (JButton) event.getSource();

			Configuration configuration = Configuration.getInstance();
			
			switch (sourceButton.getName()) {
			case "Launch PronoGuessr":
				this.launchPronoGuessr();
				break;
			case "Disable Cursed Paniac":
				if(configuration.isCursedPaniacEnabled()) {
					cursedPaniacDisablingFrame.setVisible(true);
				}
				break;
			case "Disable once":
				configuration.setCursedPaniacStatus(CursedPaniacStatus.DISABLED_ONCE);
				configuration.saveConfiguration();
				
				cursedPaniacDisablingFrame.setVisible(false);
				botManagerFrame.toggleCursedPaniacDisablingButton(false);
				break;
			case "Disable for viewers":
				configuration.setCursedPaniacStatus(CursedPaniacStatus.DISABLED_FOR_VIEWERS);
				configuration.saveConfiguration();
				
				cursedPaniacDisablingFrame.setVisible(false);
				botManagerFrame.toggleCursedPaniacDisablingButton(false);
				break;
			case "Disable forever":
				configuration.setCursedPaniacStatus(CursedPaniacStatus.DISABLED_FOREVER);
				configuration.saveConfiguration();
				
				cursedPaniacDisablingFrame.setVisible(false);
				botManagerFrame.toggleCursedPaniacDisablingButton(false);
				break;
			case "Go Prono":
				nodeJsLinkManager.sendMessage("{\"action\": \"COMMAND\", \"data\": {\"commandType\": \"go_prono\", \"arguments\": []}}");
				break;
			case "Stop Prono":
				nodeJsLinkManager.sendMessage("{\"action\": \"COMMAND\", \"data\": {\"commandType\": \"stop_prono\", \"arguments\": []}}");
				break;
			case "Reset pronos":
				nodeJsLinkManager.sendMessage("{\"action\": \"COMMAND\", \"data\": {\"commandType\": \"prono\", \"arguments\": [\"-reset\"]}}");
				break;
			case "Display all pronos":
				nodeJsLinkManager.sendMessage("{\"action\": \"COMMAND\", \"data\": {\"commandType\": \"prono\", \"arguments\": [\"-afficher_tous\"]}}");
				break;
			case "District":
				this.requestDistrictArguments();
				break;
			case "Add district":
				this.requestDsitrictAddArguments();
				break;
			case "Display district odds":
				this.requestOddsArguments();
				break;
			case "Display all scores":
				nodeJsLinkManager.sendMessage("{\"action\": \"COMMAND\", \"data\": {\"commandType\": \"score\", \"arguments\": [\"-afficher_tous\"]}}");
				break;
			case "Add score":
				this.requestScoreAddArguments();
				break;
			case "Send command":
				if(commandArgumentsFrame != null && commandArgumentsFrame.isReadyToSend()) {
					this.sendParametricCommand();
				}
				break;
			}
		} else if(event.getSource() instanceof JCheckBox) {
			JCheckBox sourceCheckbox = (JCheckBox) event.getSource();
			boolean isCheckboxSelected = sourceCheckbox.isSelected();
			
			Configuration configuration = Configuration.getInstance();
			
			switch (sourceCheckbox.getName()) {
			case "Enable world selection":
				botManagerFrame.toggleWorldCheckbox(isCheckboxSelected);
				break;
			case "Toggle Prono Counter":
				configuration.setDisplayCounter(isCheckboxSelected);
				configuration.saveConfiguration();
				break;
			}
		}
	}
	
	@Override
	public void cursedPaniacSpawned(String invokatorName) {
		Configuration configuration = Configuration.getInstance();
		
		if(configuration.isCursedPaniacEnabled()) {
			botManagerFrame.toggleCursedPaniacDisablingButton(true);
		}
	}

	@Override
	public void cursedPaniacReady(String invokatorName) { }

	@Override
	public void cursedPaniacAteProno(String pronoAuthorName) { }

	@Override
	public void cursedPaniacEndedEating() { }

	@Override
	public void cursedPaniacLeaved() { }
	
	private void launchPronoGuessr() {
		try {
			// R�cup�ration du chemin vers un fichier de cotes � partir
			// du menu d�roulant
			String oddsFilePath = botManagerFrame.chosenWorldPath();

			// R�cup�ration de la valeur du multiplicateur de score � partir du
			// champ de saisie pr�vu � cet effet
			String specialMultiplicator = botManagerFrame.multiplicator();

			int communicationPort = nodeJsLinkManager.communicationPort();

			// Lancement de PronoGuessr � partir du multiplicateur et du chemin
			// vers le fichier de cotes
			String[] pronoGuessrArgs = new String[] { "cmd", "/c", "start", "\"\"", "PronoGuessr.bat", oddsFilePath, specialMultiplicator, communicationPort + "" };
//			String[] pronoGuessrArgs = new String[] { "cmd", "/c", "start", "\"\"", "node", "js/prono_guessr.js", specialMultiplicator, communicationPort + "" };
			
			Runtime.getRuntime().exec(pronoGuessrArgs);

			// Fermeture du gestionnaire apr�s le lancement de PronoGuessr
			// BotManagerController.this.dispatchEvent(new WindowEvent(BotManagerController.this, WindowEvent.WINDOW_CLOSING));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	private void requestDistrictArguments() {
		Map<String, JTextField> argumentInputs = new LinkedHashMap<String, JTextField>();
		argumentInputs.put("Pays", new JTextField());
		commandArgumentsFrame = new CommandArgumentsFrame(this, botManagerFrame, CommandTypes.DISTRICT, argumentInputs);
	}
	
	private String[] extractDistrictCommandArguments() {
		String districtName = commandArgumentsFrame.getArgument("Pays");
		
		if(districtName != null) {
			return new String[] {
				districtName
			};
		} else {
			return null;
		}
	}
	
	private void requestDsitrictAddArguments() {
		Map<String, JTextField> argumentInputs = new LinkedHashMap<String, JTextField>();
		argumentInputs.put("Pays", new JTextField());
		argumentInputs.put("Cote", new JTextField());
		commandArgumentsFrame = new CommandArgumentsFrame(this, botManagerFrame, CommandTypes.DISTRICT_ADD, argumentInputs);
	}
	
	private String[] extractDistrictAddCommandArguments() {
		String districtName = commandArgumentsFrame.getArgument("Pays");
		String districtOdds = commandArgumentsFrame.getArgument("Cote");
		
		if(districtName != null && districtOdds != null) {
			return new String[] {
				"-ajouter",
				districtName,
				districtOdds
			};
		} else {
			return null;
		}
	}
	
	private void requestOddsArguments() {
		Map<String, JTextField> argumentInputs = new LinkedHashMap<String, JTextField>();
		argumentInputs.put("Pays", new JTextField());
		commandArgumentsFrame = new CommandArgumentsFrame(this, botManagerFrame, CommandTypes.ODDS, argumentInputs);
	}
	
	private String[] extractOddsCommandArguments() {
		String districtName = commandArgumentsFrame.getArgument("Pays");
		
		if(districtName != null) {
			return new String[] {
				districtName
			};
		} else {
			return null;
		}
	}
	
	private void requestScoreAddArguments() {
		Map<String, JTextField> argumentInputs = new LinkedHashMap<String, JTextField>();
		argumentInputs.put("Joueur", new JTextField());
		argumentInputs.put("Gain", new JTextField());
		commandArgumentsFrame = new CommandArgumentsFrame(this, botManagerFrame, CommandTypes.SCORE_ADD, argumentInputs);
	}
	
	private String[] extractScoreAddCommandArguments() {
		String playerName = commandArgumentsFrame.getArgument("Joueur");
		String scoreGain = commandArgumentsFrame.getArgument("Gain");
		
		if(playerName != null && scoreGain != null) {
			return new String[] {
				"-ajouter",
				playerName,
				scoreGain
			};
		} else {
			return null;
		}
	}
	
	private void sendParametricCommand() {
		String userCommandType = "";
		String[] commandArguments = null;

		switch (commandArgumentsFrame.getCommandType()) {
		case DISTRICT:
			userCommandType = "pays";
			commandArguments = this.extractDistrictCommandArguments();
			break;
		case DISTRICT_ADD:
			userCommandType = "pays";
			commandArguments = this.extractDistrictAddCommandArguments();
			break;
		case ODDS:
			userCommandType = "cote";
			commandArguments = this.extractOddsCommandArguments();
			break;
		case SCORE_ADD:
			userCommandType = "score";
			commandArguments = this.extractScoreAddCommandArguments();
			break;
		}
		
		String command = this.buildJsonCommand(userCommandType, commandArguments);
		
		nodeJsLinkManager.sendMessage("{\"action\": \"COMMAND\", \"data\": {" + command + "}}");
		commandArgumentsFrame.close();
	}
	
	private String buildJsonCommand(String commandType, String[] arguments) {
		String commandJson = "\"commandType\": \"" + commandType + "\", \"arguments\": [";
		
		if(arguments != null) {
			for (int i = 0; i < arguments.length; i++) {
				commandJson += "\"" + arguments[i] + "\"";
				
				if(i < arguments.length - 1) {
					commandJson += ", ";
				}
			}
		}
		
		commandJson += "]";
		
		return commandJson;
	}
	
	public void beginDisplay() {
		botManagerFrame.setVisible(true);
	}

	public NodeJsLinkManager getNodeJsLinkManager() {
		return nodeJsLinkManager;
	}
}