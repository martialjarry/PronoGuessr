package com.drawrunner.uniguess.control;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;

import com.drawrunner.uniguess.model.communication.mongo.MongoTools;
import com.drawrunner.uniguess.model.communication.twitch.TwitchTools;
import com.drawrunner.uniguess.model.data.Configuration;
import com.drawrunner.uniguess.view.display.swing.ConnectionCredentialsFrame;
import com.drawrunner.uniguess.view.display.swing.MongoUserCreationFrame;

public class ConfigurationController implements ActionListener {
	private enum Menues { NONE, MONGO_USER_CREATION, CONNECTION_CREDENTIALS }
	private Menues currentMenu;

	private MongoUserCreationFrame mongoUserCreationFrame;
	private ConnectionCredentialsFrame connectionCredentialsFrame;

	private Configuration configuration;
	
	public ConfigurationController(UniguessController pronoGuessrController) {
		mongoUserCreationFrame = new MongoUserCreationFrame(pronoGuessrController, this);
		connectionCredentialsFrame = new ConnectionCredentialsFrame(pronoGuessrController, this);
		
		currentMenu = Menues.NONE;
	}
	
	public void loadConfiguration() {
		configuration = Configuration.getInstance();
		configuration.addObserver(connectionCredentialsFrame);

		configuration.loadConfiguration();
		TwitchTools.initializeInstance(configuration);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		if(event.getSource() instanceof JButton) {
			JButton sourceButton = (JButton) event.getSource();
			
			switch (currentMenu) {
			case MONGO_USER_CREATION:
				if(sourceButton.getName().equals("Validate mongo user creation")) {
					this.createMongoUser();
				}
				break;
			case CONNECTION_CREDENTIALS:
				if(sourceButton.getName().equals("Validate connection credentials")) {
					this.updateConnectionConfiguration();
				}
				break;
			}
		} else if(event.getSource() instanceof JToggleButton) {
			JToggleButton sourceRadioButton = (JToggleButton) event.getSource();

			switch (currentMenu) {
			case MONGO_USER_CREATION:
				if(sourceRadioButton.getName().equals("Online database")) {
					mongoUserCreationFrame.toggleUserCreationForm(false);
				} else if(sourceRadioButton.getName().equals("Offline database")) {
					mongoUserCreationFrame.toggleUserCreationForm(true);
				}
				break;
			}
		}
	}

	private void createMongoUser() {
		if(mongoUserCreationFrame.isFormReady()) {
			if(mongoUserCreationFrame.isInOfflineMode()) {
				String userName = mongoUserCreationFrame.userNameValue();
				String password = mongoUserCreationFrame.passwordValue();
				String databasePath = "mongodb://" + userName + ":" + password + "@localhost:27017/prono_guessr_db";
				
				configuration.setDatabasePath(databasePath);
				configuration.saveConfiguration();
				
				MongoTools mongoTools = MongoTools.getInstance();
				mongoTools.createUser(userName, password);
			}
		}
	}
	
	private void updateConnectionConfiguration() {
		String targetChannel = connectionCredentialsFrame.targetChannelValue();
		String databasePath = connectionCredentialsFrame.databasePathValue();
		
		String botUsername = connectionCredentialsFrame.botUsernameValue();
		String botPassword = connectionCredentialsFrame.botPasswordValue();
		
		configuration.setTargetChannel(targetChannel);
		configuration.setDatabasePath(databasePath);
		
		configuration.setBotUsername(botUsername);
		configuration.setBotPassword(botPassword);
		
		configuration.saveConfiguration();

		if(connectionCredentialsFrame.isFormReady()) {
			TwitchTools.initializeInstance(configuration);
		} else {
			JOptionPane.showMessageDialog(null, "Tous les champs doivent �tre remplis.", "Attention", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	public boolean needsMongoUserCreationInput() {
		return (currentMenu == Menues.NONE && (configuration.getDatabasePath() == null || configuration.getDatabasePath().isEmpty()))
			|| (currentMenu == Menues.MONGO_USER_CREATION && mongoUserCreationFrame.isInOfflineMode() && (configuration.getDatabasePath() == null || configuration.getDatabasePath().isEmpty()));
	}
	
	public boolean needsConnectionCredentialsInput() {
		return !configuration.isValid();
	}
	
	public void openMongoUserCreationFrame() {
		mongoUserCreationFrame.setVisible(true);
		connectionCredentialsFrame.setVisible(false);
		
		currentMenu = Menues.MONGO_USER_CREATION;
	}
	
	public void openConnectionCredentialsFrame() {
		mongoUserCreationFrame.setVisible(false);
		connectionCredentialsFrame.setVisible(true);
		
		currentMenu = Menues.CONNECTION_CREDENTIALS;
	}
	
	public void closeAllConfigurationFrames() {
		mongoUserCreationFrame.setVisible(false);
		connectionCredentialsFrame.setVisible(false);
	}
}
