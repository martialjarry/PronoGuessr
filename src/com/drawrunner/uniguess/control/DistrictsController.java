package com.drawrunner.uniguess.control;

import com.drawrunner.uniguess.model.communication.DistrictLoadingListener;
import com.drawrunner.uniguess.model.data.DistrictBase;

public class DistrictsController {
	public void launchDistrictsLoading(DistrictLoadingListener loadingListener) {
		DistrictBase districtBase = DistrictBase.getInstance();
		districtBase.LaunchLoadingFromMongoDb(loadingListener);
	}
}
