package com.drawrunner.uniguess.control;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.drawrunner.uniguess.model.data.OutputManager;

public class Main {
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		OutputManager outputManager = OutputManager.getInstance();
		outputManager.setupDataFolder();
		outputManager.redirectErrorsToLogFile();
		
		@SuppressWarnings("unused")
		UniguessController uniguessController = new UniguessController();
	}
}