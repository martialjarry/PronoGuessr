package com.drawrunner.uniguess.control;

import com.drawrunner.uniguess.model.communication.PlayerLoadingListener;
import com.drawrunner.uniguess.model.communication.nodejs.PronoListener;
import com.drawrunner.uniguess.model.data.Configuration;
import com.drawrunner.uniguess.model.data.PlayerBase;

public class PlayersController implements PronoListener {
	private PlayerBase playerBase;
	
	public PlayersController() {
		playerBase = PlayerBase.getInstance();
	}
	
	public void launchPlayersLoading(PlayerLoadingListener loadingListener, Configuration configuration) {
		playerBase.LaunchLoadingFromTwitch(configuration.getTargetChannel(), loadingListener);
		playerBase.LaunchLoadingFromMongoDb(loadingListener);
	}

	@Override
	public void pronoUpdated(String playerName, String prono[]) {
		playerBase.updatePlayerProno(playerName, prono);
	}
	
	@Override
	public void pronoCounted(int currentCount, int maxCount, float currentRate, float maxRate) {
		playerBase.publishPronoCount(currentCount, maxCount, currentRate, maxRate);
	}
}
