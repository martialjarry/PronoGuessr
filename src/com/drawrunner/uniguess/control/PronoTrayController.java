package com.drawrunner.uniguess.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;

import com.drawrunner.uniguess.model.communication.DistrictLoadingListener;
import com.drawrunner.uniguess.model.communication.PlayerLoadingListener;
import com.drawrunner.uniguess.model.communication.nodejs.BotStateListener;
import com.drawrunner.uniguess.model.communication.nodejs.BotStates;
import com.drawrunner.uniguess.model.communication.nodejs.CursedPaniacListener;
import com.drawrunner.uniguess.model.data.District;
import com.drawrunner.uniguess.model.data.DistrictBase;
import com.drawrunner.uniguess.model.data.Player;
import com.drawrunner.uniguess.model.data.PlayerBase;
import com.drawrunner.uniguess.view.display.processing.FrameUpdateListener;
import com.drawrunner.uniguess.view.display.processing.LoadingScreen;
import com.drawrunner.uniguess.view.display.processing.PronoTrayVisualizer;

public class PronoTrayController implements FrameUpdateListener, ActionListener, ItemListener,
	PlayerLoadingListener, DistrictLoadingListener, BotStateListener, CursedPaniacListener {
	private enum PronoTrayStates {
		LOADING,
		DISTRICTS,
		DISTRICTS_TO_PRONOS,
		PRONOS,
		PRONOS_TO_DISTRICTS
	}
	
	private PronoTrayVisualizer pronoTrayVisualizer;
	private PronoTrayStates currentTrayState;
	
	public PronoTrayController(PronoTrayVisualizer pronoTrayVisualizer) {
		this.pronoTrayVisualizer = pronoTrayVisualizer;
		
		this.pronoTrayVisualizer.assignLoadingListener(this);
		this.pronoTrayVisualizer.setFrameUpdateListener(this);
		
		currentTrayState = PronoTrayStates.LOADING;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		Object eventSource = event.getSource();
		
		if(eventSource instanceof JButton) {
			JButton sourceButton = (JButton) event.getSource();
			
			switch(sourceButton.getName()) {
			case "Toggle Prono Tray visibility":
				this.togglePronoTray(sourceButton);
				break;
			case "Disable Cursed Paniac":
				pronoTrayVisualizer.retireCursedPaniac();
				break;
			}
		} else if(eventSource instanceof LoadingScreen) {
			switch (currentTrayState) {
			case LOADING:
				currentTrayState = PronoTrayStates.PRONOS;
				break;
			}
		}
	}
	
	@Override
	public void itemStateChanged(ItemEvent event) {
		Object eventSource = event.getSource();
		
		if(eventSource instanceof JCheckBox) {
			JCheckBox sourceCheckbox = (JCheckBox) event.getSource();
			
			switch(sourceCheckbox.getName()) {
			case "Toggle Prono Counter":
				pronoTrayVisualizer.togglePronoCounter(sourceCheckbox.isSelected());
				break;
			}
		}
	}
	
	@Override
	public void onFrameUpdated() {
		switch (currentTrayState) {
		case LOADING:
			pronoTrayVisualizer.updateLoadingScreen();
			
			if(pronoTrayVisualizer.hasLoadedAllData()) {
				currentTrayState = PronoTrayStates.PRONOS;
			}
			
			break;
		case PRONOS_TO_DISTRICTS:
			pronoTrayVisualizer.updateRegularScreen();
			break;
		case DISTRICTS:
			pronoTrayVisualizer.updateRegularScreen();
			break;
		case DISTRICTS_TO_PRONOS:
			pronoTrayVisualizer.updateRegularScreen();
			break;
		case PRONOS:
			pronoTrayVisualizer.updateRegularScreen();
			break;
		}
	}
	
	public void togglePronoTray(JButton visibilityButton) {
		if(pronoTrayVisualizer.isVisible()) {
			visibilityButton.setText("Activer la Table des Pronos");
			pronoTrayVisualizer.toggle(false);
		} else {
			visibilityButton.setText("Désactiver la Table des Pronos");
			pronoTrayVisualizer.toggle(true);
		}
	}
	
	@Override
	public void botStateRefreshed(BotStates botState) {
		switch (currentTrayState) {
		case PRONOS:
			if(botState == BotStates.WAITING_PROGNOSIS) {
				pronoTrayVisualizer.exposeGridDistrictCards();
				currentTrayState = PronoTrayStates.DISTRICTS;
			}
			break;
		case DISTRICTS:
			if(botState == BotStates.IN_PROGNOSIS) {
				pronoTrayVisualizer.disposeGridDistrictCards();
				currentTrayState = PronoTrayStates.PRONOS;
			}
		}
	}
	
	@Override
	public void playerLoaded(Player loadedPlayer) {
		PlayerBase playerBase = PlayerBase.getInstance();
		playerBase.addOrUpdatePlayer(loadedPlayer);
	}
	
	@Override
	public void districtLoaded(District loadedDistrict) {
		DistrictBase districtBase = DistrictBase.getInstance();
		districtBase.addOrUpdateDistrict(loadedDistrict);
	}

	@Override
	public void cursedPaniacSpawned(String invokatorName) {
		pronoTrayVisualizer.spawnCursedPaniac(invokatorName);
	}
	
	@Override
	public void cursedPaniacReady(String invokatorName) { }
	
	@Override
	public void cursedPaniacAteProno(String pronoAuthorName) { }
	
	@Override
	public void cursedPaniacEndedEating() { }
	
	@Override
	public void cursedPaniacLeaved() { }
	
	public void assignCursedPaniacListener(CursedPaniacListener cursedPaniacListener) {
		pronoTrayVisualizer.assignCursedPaniacListener(cursedPaniacListener);
	}
	
	public void retireCursedPaniacFromPronoTray() {
		pronoTrayVisualizer.retireCursedPaniac();
	}
	
	public PronoTrayVisualizer getPronoTrayFrame() {
		return pronoTrayVisualizer;
	}
}
