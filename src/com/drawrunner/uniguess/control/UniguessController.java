package com.drawrunner.uniguess.control;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.drawrunner.uniguess.model.communication.nodejs.CursedPaniacListener;
import com.drawrunner.uniguess.model.communication.nodejs.NodeJsLinkManager;
import com.drawrunner.uniguess.model.data.Configuration;
import com.drawrunner.uniguess.model.data.DistrictBase;
import com.drawrunner.uniguess.model.data.PlayerBase;
import com.drawrunner.uniguess.view.display.processing.PronoTrayVisualizer;

import processing.core.PApplet;

public class UniguessController implements ActionListener, CursedPaniacListener {
	private enum Menues { CONFIGURATION, MANAGER }
	private Menues currentMenu;
	
	private PronoTrayVisualizer pronoTrayVisualizer;

	private ConfigurationController configurationController;
	private BotManagerController botManagerController;
	private PronoTrayController pronoTrayController;
	
	private PlayersController playersController;
	private DistrictsController districtsController;
	
	public UniguessController() {
		configurationController = new ConfigurationController(this);
		configurationController.loadConfiguration();
		
		if(configurationController.needsMongoUserCreationInput()) {
			configurationController.openMongoUserCreationFrame();
			currentMenu = Menues.CONFIGURATION;
		} else {
			if(configurationController.needsConnectionCredentialsInput()) {
				configurationController.openConnectionCredentialsFrame();
				currentMenu = Menues.CONFIGURATION;
			} else {
				this.startUniguess();
				currentMenu = Menues.MANAGER;
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		if(event.getSource() instanceof JButton) {
			JButton sourceButton = (JButton) event.getSource();
			
			switch (currentMenu) {
			case CONFIGURATION:
				if(sourceButton.getName().equals("Validate mongo user creation")) {
					if(!configurationController.needsMongoUserCreationInput()) {
						if(!configurationController.needsConnectionCredentialsInput()) {
							this.startUniguess();
							currentMenu = Menues.MANAGER;
						} else {
							configurationController.openConnectionCredentialsFrame();
						}
					}
				} else if(sourceButton.getName().equals("Validate connection credentials")) {
					if(!configurationController.needsConnectionCredentialsInput()) {
						this.startUniguess();
						currentMenu = Menues.MANAGER;
					}
				} else if(sourceButton.getName().equals("Close PronoGuessr")) {
					System.exit(0);
				}
				break;
			default:
				break;
			}
		}
	}
	
	public void startUniguess() {
		Configuration configuration = Configuration.getInstance();
		
		configurationController.closeAllConfigurationFrames();
		
		this.startPronoTray(configuration);
		this.waitForPronoTrayStartup();

		this.startEntityControllersAndModels(configuration);
		
		this.startBotManager();
	}

	private void startBotManager() {
		botManagerController = new BotManagerController(pronoTrayController);
		
		NodeJsLinkManager nodeJsLinkManager = botManagerController.getNodeJsLinkManager(); 
		nodeJsLinkManager.addPronoListener(playersController);
		
		nodeJsLinkManager.addBotStateListener(pronoTrayController);
		
		nodeJsLinkManager.addCursedPaniacSpawnListener(pronoTrayController);
		nodeJsLinkManager.addCursedPaniacSpawnListener(botManagerController);
		
		botManagerController.beginDisplay();
	}

	public void startPronoTray(Configuration configuration) {
		int pronoTrayFullscreen = configuration.getPronoTrayFullscreen();
		Point pronoTraySize = configuration.pronoTraySize();
		
		pronoTrayVisualizer = new PronoTrayVisualizer(pronoTrayFullscreen, pronoTraySize);
		pronoTrayController = new PronoTrayController(pronoTrayVisualizer);
		pronoTrayController.assignCursedPaniacListener(this);
		
		String visualizerAdress = "com.paniac.pronoguessr.view.display.processing.PronoTrayVisualizer";
		PApplet.runSketch(new String[] {visualizerAdress}, pronoTrayVisualizer);
	}
	
	private void waitForPronoTrayStartup() {
		while(!pronoTrayVisualizer.isReady()) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void startEntityControllersAndModels(Configuration configuration) {
		playersController = new PlayersController();
		districtsController = new DistrictsController();
		
		PlayerBase playerBase = PlayerBase.getInstance();
		playerBase.addObserver(pronoTrayController.getPronoTrayFrame());
		playerBase.addObserver(pronoTrayController.getPronoTrayFrame().getPronoCounter());

		DistrictBase districtBase = DistrictBase.getInstance();
		districtBase.addObserver(pronoTrayController.getPronoTrayFrame());
		
		playersController.launchPlayersLoading(pronoTrayController, configuration);
		districtsController.launchDistrictsLoading(pronoTrayController);
	}
	
	@Override
	public void cursedPaniacSpawned(String invokatorName) { }

	@Override
	public void cursedPaniacReady(String invokatorName) {
		NodeJsLinkManager nodeJsLinkManager = botManagerController.getNodeJsLinkManager(); 
		nodeJsLinkManager.sendMessage("{\"action\": \"CURSED_PANIAC_READY\", \"data\": {\"invokator_name\": \"" + invokatorName + "\"}}");
	}
	
	@Override
	public void cursedPaniacAteProno(String pronoAuthorName) {
		NodeJsLinkManager nodeJsLinkManager = botManagerController.getNodeJsLinkManager(); 
		nodeJsLinkManager.sendMessage("{\"action\": \"CURSED_PANIAC_ATE_PRONO\", \"data\": {\"author_name\": \"" + pronoAuthorName + "\"}}");
	}
	
	public void cursedPaniacEndedEating() {
		pronoTrayController.retireCursedPaniacFromPronoTray();
	}
	
	@Override
	public void cursedPaniacLeaved() {
		NodeJsLinkManager nodeJsLinkManager = botManagerController.getNodeJsLinkManager(); 
		nodeJsLinkManager.sendMessage("{\"action\": \"CURSED_PANIAC_LEAVED\", \"data\": null}");
	}
}