package com.drawrunner.uniguess.model.communication;

import com.drawrunner.uniguess.model.data.District;

public interface DistrictLoadingListener extends EntityLoadingListener {
	public void districtLoaded(District loadedDistrict);
}
