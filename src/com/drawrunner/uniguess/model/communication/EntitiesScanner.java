package com.drawrunner.uniguess.model.communication;

public abstract class EntitiesScanner implements Runnable {
	protected static final float LOADING_TIME_INTERVAL = 1000;
	
	protected enum LoadingStates {IDLE, LOADING};
	protected LoadingStates loadingState;
	
	protected int loadedEntitiesCount;
	protected int totalEntitiesCount;
	
	public EntitiesScanner() {
		loadingState = LoadingStates.IDLE;
		
		loadedEntitiesCount = 0;
		totalEntitiesCount = 0;
	}
	
	public boolean isLoading() {
		return loadingState == LoadingStates.LOADING;
	}
	
	protected void awaitNextReadingTime(long timeStamp) {
		long sleepingTime = (long) Math.max(LOADING_TIME_INTERVAL - ((System.nanoTime() - timeStamp) / 1000L), 0);
		
		if(sleepingTime > 0) {
			try {
				Thread.sleep(sleepingTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}