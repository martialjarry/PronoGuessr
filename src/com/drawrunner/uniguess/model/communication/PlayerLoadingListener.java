package com.drawrunner.uniguess.model.communication;

import com.drawrunner.uniguess.model.data.Player;

public interface PlayerLoadingListener extends EntityLoadingListener {
	public void playerLoaded(Player player);
}
