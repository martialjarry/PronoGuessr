package com.drawrunner.uniguess.model.communication.mongo;

import java.io.File;
import java.util.function.Consumer;

import org.bson.Document;
import org.json.JSONObject;

import com.drawrunner.uniguess.model.communication.DistrictLoadingListener;
import com.drawrunner.uniguess.model.communication.EntitiesScanner;
import com.drawrunner.uniguess.model.data.District;
import com.drawrunner.uniguess.model.data.DistrictBase;

public class MongoDistrictsScanner extends EntitiesScanner {
	private DistrictLoadingListener loadingListener;
	
	public MongoDistrictsScanner(DistrictLoadingListener loadingListener) {
		this.loadingListener = loadingListener;
	}
	
	@Override
	public void run() {
		Thread.currentThread().setName("Mongo Districts Scanner");
		
		long timeStamp = System.nanoTime();
		
		while(true) {
			if(System.nanoTime() - timeStamp >= LOADING_TIME_INTERVAL * 1000000L || totalEntitiesCount == 0) {
				timeStamp = System.nanoTime();
				
				loadingState = LoadingStates.LOADING;
				this.loadMongoPlayers();
				
				loadedEntitiesCount = 0;
				totalEntitiesCount = 0;
				
				loadingState = LoadingStates.IDLE;
				
				this.awaitNextReadingTime(timeStamp);
			}
		}
	}

	private void loadMongoPlayers() {
		MongoTools mongoTools = MongoTools.getInstance();
		
		if(mongoTools != null) {
			Iterable<Document> districtDocs = mongoTools.getAllDistricts();
			
			districtDocs.forEach(new Consumer<Document>() {
				@Override
				public void accept(Document playerDoc) {
					totalEntitiesCount++;
				}
			});
			
			districtDocs.forEach(new ReceivedDistrictExtractor());
		}
	}
	
	private class ReceivedDistrictExtractor implements Consumer<Document> {
		@Override
		public void accept(Document districtDoc) {
			JSONObject districtJson = new JSONObject(districtDoc.toJson());
			
			if(districtJson.has("_id") && districtJson.has("name")) {
				JSONObject identifierJson = districtJson.getJSONObject("_id");
				String name = districtJson.getString("name");
				
				if(identifierJson.has("$oid")) {
					String identifier = identifierJson.getString("$oid");

					District district = new District(identifier, name);
					
					boolean isEnabled = false;
					
					if(districtJson.has("is_enabled")) {
						isEnabled = districtJson.getBoolean("is_enabled");
					
						if(isEnabled) {
							if(districtJson.has("odds")) {
								int odds = districtJson.getInt("odds");
								district.setOdds(odds);
							}
							
							String logoPath = "media/flags/" + name.toLowerCase() + "_flag.png";
							File logoFile = new File(logoPath);
							
							if(logoFile.exists()) {
								district.setFlagUrl(logoPath);
							}
							
							if(loadingListener != null) {
								loadingListener.districtLoaded(district);
							}
						}
					}
				}
			}
			
			loadedEntitiesCount++;
			
			DistrictBase districtBase = DistrictBase.getInstance();
			districtBase.publishMongoDistrictLoadingProgress(loadedEntitiesCount, totalEntitiesCount);
		}
	}
}