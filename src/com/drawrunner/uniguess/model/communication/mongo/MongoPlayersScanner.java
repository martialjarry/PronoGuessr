package com.drawrunner.uniguess.model.communication.mongo;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import com.drawrunner.uniguess.model.communication.EntitiesScanner;
import com.drawrunner.uniguess.model.communication.PlayerLoadingListener;
import com.drawrunner.uniguess.model.data.District;
import com.drawrunner.uniguess.model.data.DistrictBase;
import com.drawrunner.uniguess.model.data.Player;
import com.drawrunner.uniguess.model.data.PlayerBase;

public class MongoPlayersScanner extends EntitiesScanner {
	private PlayerLoadingListener loadingListener;
	
	public MongoPlayersScanner(PlayerLoadingListener loadingListener) {
		this.loadingListener = loadingListener;
	}
	
	@Override
	public void run() {
		Thread.currentThread().setName("Mongo Players Scanner");
		
		long timeStamp = System.nanoTime();
		
		while(true) {
			if(System.nanoTime() - timeStamp >= LOADING_TIME_INTERVAL * 1000000L || totalEntitiesCount == 0) {
				timeStamp = System.nanoTime();
				
				loadingState = LoadingStates.LOADING;
				this.loadMongoPlayers();
				
				loadedEntitiesCount = 0;
				totalEntitiesCount = 0;
				
				loadingState = LoadingStates.IDLE;

				this.awaitNextReadingTime(timeStamp);
			}
		}
	}
	
	private void loadMongoPlayers() {
		MongoTools mongoTools = MongoTools.getInstance();
		
		if(mongoTools != null) {
			Iterable<Document> playerDocs = mongoTools.getAllPlayers();
			
			playerDocs.forEach(new Consumer<Document>() {
				@Override
				public void accept(Document playerDoc) {
					totalEntitiesCount++;
				}
			});
			
			playerDocs.forEach(new ReceivedPlayerExtractor());
		}
	}
	
	private class ReceivedPlayerExtractor implements Consumer<Document> {
		@Override
		public void accept(Document playerDoc) {
			JSONObject playerJson = new JSONObject(playerDoc.toJson());
			
			if(playerJson.has("_id") && playerJson.has("name")) {
				JSONObject identifierJson = playerJson.getJSONObject("_id");
				String name = playerJson.getString("name");
				
				if(identifierJson.has("$oid")) {
					String playerId = identifierJson.getString("$oid");

					Player player = new Player(playerId, null, name);
					
					if(playerJson.has("score")) {
						int score = playerJson.getInt("score");
						player.setScore(score);
					}
					
					List<String> predictedCountryIds = new ArrayList<String>();
					
					if(playerJson.has("prognosis")) {
						if(!playerJson.isNull("prognosis")) {
							JSONArray predictedDistrictIdsJson = playerJson.getJSONArray("prognosis");

							for(int i = 0; i < predictedDistrictIdsJson.length(); i++) {
								JSONObject predictedDistrictIdJson = predictedDistrictIdsJson.getJSONObject(i);
								if(predictedDistrictIdJson.has("$oid")) {
									String predictedCountryId = predictedDistrictIdJson.getString("$oid");
									predictedCountryIds.add(predictedCountryId);
								}
							}
						}
						
						DistrictBase districtBase = DistrictBase.getInstance();
						
						District[] predictedDistricts = new District[predictedCountryIds.size()];
						
						for(int i = 0; i < predictedCountryIds.size(); i++) {
							String predictedCountryId = predictedCountryIds.get(i);
							District predictedCountry = districtBase.getDistrict(predictedCountryId);
							predictedDistricts[i] = predictedCountry;
							
						}
						
						player.setProno(predictedDistricts);
					}
					
					if(loadingListener != null) {
						loadingListener.playerLoaded(player);
					}
				}
			}
			
			loadedEntitiesCount++;
			
			PlayerBase playerBase = PlayerBase.getInstance();
			playerBase.publishMongoPlayerLoadingProgress(loadedEntitiesCount, totalEntitiesCount);
		}
	}
}
