package com.drawrunner.uniguess.model.communication.mongo;

import java.util.Collections;

import org.bson.Document;

import com.drawrunner.uniguess.model.data.Configuration;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCommandException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoTools {
	private MongoClient mongoClient;
	private MongoDatabase database;
	
	private MongoTools() {
		Configuration configuration = Configuration.getInstance();
		
		MongoClientURI databaseUri = new MongoClientURI(configuration.getDatabasePath());
		mongoClient = new MongoClient(databaseUri);
		database = mongoClient.getDatabase(databaseUri.getDatabase());
	}
	
	public static MongoTools getInstance() {
		return MongoToolsHolder.instance;
	}
	
	public void createUser(String userName, String password) {
        MongoClient defaultMongoClient = new MongoClient();
		MongoDatabase pronoGuessrDatatabase = defaultMongoClient.getDatabase("prono_guessr_db");
		
        Document userCreationCommand = new Document("createUser", userName);
        userCreationCommand.append("pwd", password);
        userCreationCommand.append("roles", Collections.singletonList("readWrite"));
        
        try {
        	pronoGuessrDatatabase.runCommand(userCreationCommand);
        } catch(MongoCommandException e) {
        	if(e.getErrorCode() == 11000) {
        		Document userDroppingCommand = new Document("dropUser", userName);
            	pronoGuessrDatatabase.runCommand(userDroppingCommand);
            	pronoGuessrDatatabase.runCommand(userCreationCommand);
        	}
        	
        } finally {
        	defaultMongoClient.close();
        }
	}
	
	public Iterable<Document> getAllPlayers() {
		MongoCollection<Document> collection = database.getCollection("players");
		return collection.find();
	}
	
	public Iterable<Document> getAllDistricts() {
		MongoCollection<Document> collection = database.getCollection("districts");
		return collection.find();
	}
	
	private final static class MongoToolsHolder {
		private static final MongoTools instance = new MongoTools();
	}
}
