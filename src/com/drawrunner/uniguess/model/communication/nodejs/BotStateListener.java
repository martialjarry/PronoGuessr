package com.drawrunner.uniguess.model.communication.nodejs;

public interface BotStateListener {
	public void botStateRefreshed(BotStates botState);
}
