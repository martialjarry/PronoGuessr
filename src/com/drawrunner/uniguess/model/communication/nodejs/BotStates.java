package com.drawrunner.uniguess.model.communication.nodejs;

public enum BotStates {
	WAITING_PROGNOSIS, IN_PROGNOSIS, WAITING_DISTRICT
}
