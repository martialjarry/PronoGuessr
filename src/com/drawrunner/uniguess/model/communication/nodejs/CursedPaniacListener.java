package com.drawrunner.uniguess.model.communication.nodejs;

public interface CursedPaniacListener {
	public void cursedPaniacSpawned(String invokatorName);
	public void cursedPaniacReady(String invokatorName);
	public void cursedPaniacAteProno(String pronoAuthorName);
	public void cursedPaniacEndedEating();
	public void cursedPaniacLeaved();
}
