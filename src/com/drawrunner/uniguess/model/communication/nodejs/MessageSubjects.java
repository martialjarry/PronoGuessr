package com.drawrunner.uniguess.model.communication.nodejs;

public enum MessageSubjects {
	PLAYER_JOIN,
	PRONO_UPDATE,
	PRONO_COUNT,
	BOT_STATE,
	CURSED_PANIAC_SPAWNED;
}
