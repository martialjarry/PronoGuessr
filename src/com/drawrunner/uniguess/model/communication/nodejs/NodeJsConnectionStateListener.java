package com.drawrunner.uniguess.model.communication.nodejs;

import com.drawrunner.uniguess.model.communication.EntityLoadingListener;

public interface NodeJsConnectionStateListener extends EntityLoadingListener {
	public void nodeJsConnectionStateChanged(boolean isConnected);
}
