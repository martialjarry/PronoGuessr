package com.drawrunner.uniguess.model.communication.nodejs;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NodeJsLinkManager implements Runnable {
	private final static int PRONOGUESSR_PORT = 56250;
	
	private enum ConnectionStates {AWAITING_CONNECTION, CONNECTED}
	private ConnectionStates connectionState; 
	
	private ServerSocket mainSocket;
	private BufferedWriter linkWriter;
	
	private List<NodeJsConnectionStateListener> connectionStateListeners;
	private List<BotStateListener> botStateListeners;
	private List<PronoListener> pronoListeners;
	private List<CursedPaniacListener> cursedPaniacListeners;

	private long timeStamp;
	private int pronoCount;
	
	private List<String[]> newPronos;
	
	public NodeJsLinkManager() throws IOException {
		connectionState = ConnectionStates.AWAITING_CONNECTION;
		
		mainSocket = new ServerSocket(PRONOGUESSR_PORT);
		linkWriter = null;
		
		connectionStateListeners = new ArrayList<NodeJsConnectionStateListener>();
		pronoListeners = new ArrayList<PronoListener>();
		botStateListeners = new ArrayList<BotStateListener>();
		cursedPaniacListeners = new ArrayList<CursedPaniacListener>();
		
		timeStamp = 0;
		pronoCount = 0;
		
		newPronos = new ArrayList<String[]>();
	}
	
	@Override
	public void run() {
		Thread.currentThread().setName("Node JS Link Manager");
		
		try {
			Socket linkSocket = null;
			BufferedReader linkReader = null;
			
			while(true) {
				if(System.nanoTime() > timeStamp) {
					timeStamp = System.nanoTime() + 1000000000L;
					pronoCount = 0;
				}
				
				switch (connectionState) {
				case AWAITING_CONNECTION:
					linkSocket = mainSocket.accept();

					linkReader = new BufferedReader(new InputStreamReader(linkSocket.getInputStream()));
					linkWriter = new BufferedWriter(new OutputStreamWriter(linkSocket.getOutputStream()));
					
					System.out.println("Connect� � PronoGuessr.");
					
					this.invokeConnectionStateChangedCallbackAsync(true);
					connectionState = ConnectionStates.CONNECTED;
					break;
				case CONNECTED:
					try {
						if (linkSocket != null) {
							if(linkReader != null && linkWriter != null) {
								String receivedMessage = linkReader.readLine();
								this.processReceivedMessage(receivedMessage);
							} else {
								linkReader = new BufferedReader(new InputStreamReader(linkSocket.getInputStream()));
								linkWriter = new BufferedWriter(new OutputStreamWriter(linkSocket.getOutputStream()));
							}
						}
					} catch (SocketException e) {
						System.out.println("D�connect� de PronoGuessr.");
						this.invokeConnectionStateChangedCallbackAsync(false);
						connectionState = ConnectionStates.AWAITING_CONNECTION;
					}
					break;
				default:
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendMessage(String message) {
		if(linkWriter != null) {
			try {
				linkWriter.write(message + "\n");
				linkWriter.flush();
			} catch (IOException e) {
				System.err.println("Impossible d'envoyer un message � PronoGuessr.");
				e.printStackTrace();
			}
		}
	}
	
	public int communicationPort() {
		return mainSocket.getLocalPort();
	}
	
	private void processReceivedMessage(String receivedMessage) {
//		System.out.println(receivedMessage);
		
		if (receivedMessage != null) {
			try {
				JSONObject receivedJson = new JSONObject(receivedMessage);
	
				if (receivedJson.has("subject") && receivedJson.has("data") && !receivedJson.isNull("subject")) {
					String subjectRaw = receivedJson.getString("subject");
					
					JSONObject dataJson = null;
					
					if(!receivedJson.isNull("data")) {
						dataJson = receivedJson.getJSONObject("data");
					}
					
					try {
						MessageSubjects subject = MessageSubjects.valueOf(subjectRaw.toUpperCase());
						
						switch (subject) {
						case PLAYER_JOIN:
							break;
						case PRONO_UPDATE:
							pronoCount++;
							this.processPronoUpdate(dataJson);
							break;
						case PRONO_COUNT:
							this.processPronoCount(dataJson);
							break;
						case BOT_STATE:
							this.processBotStateRefresh(dataJson);
							break;
						case CURSED_PANIAC_SPAWNED:
							this.processCursedPaniacSpawn(dataJson);
							break;
						}
					} catch (IllegalArgumentException e) {
						System.out.println("No matching enum for subject: " + subjectRaw.toUpperCase());
					}
				}
			} catch(JSONException e) {
				System.err.println("Impossible d'interpr�ter le message re�u :\n" + receivedMessage);
				e.printStackTrace();
			}
		} else {
			System.out.println("D�connect� de PronoGuessr.");
			connectionState = ConnectionStates.AWAITING_CONNECTION;
		}
	}
	
	private void processPronoUpdate(JSONObject dataJson) {
		if(dataJson.has("player_name") && dataJson.has("district_names")) {
			String playerName = dataJson.getString("player_name");
			JSONArray districtNames = dataJson.getJSONArray("district_names");
			
			String[] newProno = new String[districtNames.length()];
			
			for(int i = 0; i < districtNames.length(); i++) {
				newProno[i] = districtNames.getString(i);
			}
			
			newPronos.add(newProno);
			this.invokePronoUpdateCallbackAsync(playerName, newProno);
		}
	}
	
	private void processPronoCount(JSONObject dataJson) {
		if(dataJson.has("current_count") && dataJson.has("max_count")
		&& dataJson.has("current_rate") && dataJson.has("max_rate")) {
			int currentCount = dataJson.getInt("current_count");
			int maxCount= dataJson.getInt("max_count");
			
			float currentRate = dataJson.getFloat("current_rate");
			float maxRate = dataJson.getFloat("max_rate");
			
			this.invokePronoCountCallbackAsync(currentCount, maxCount, currentRate, maxRate);
		}
	}
	
	private void processBotStateRefresh(JSONObject dataJson) {
		if(dataJson.has("bot_state")) {
			BotStates botState = BotStates.valueOf(dataJson.getString("bot_state"));
			this.invokeBotStateRefreshCallbackAsync(botState);
		}
	}
	
	private void processCursedPaniacSpawn(JSONObject dataJson) {
		if(!dataJson.isNull("invokator_name")) {
			String invokatorName = dataJson.getString("invokator_name");
			this.invokeCursedPaniacInvokationCallback(invokatorName);
		}
	}
	
	public void addConnectionStateListener(NodeJsConnectionStateListener connectionStateListener) {
		connectionStateListeners.add(connectionStateListener);
	}
	
	public void addPronoListener(PronoListener pronoListener) {
		pronoListeners.add(pronoListener);
	}
	
	public void addBotStateListener(BotStateListener botStateListener) {
		botStateListeners.add(botStateListener);
	}
	
	public void addCursedPaniacSpawnListener(CursedPaniacListener cursedPaniacListener) {
		cursedPaniacListeners.add(cursedPaniacListener);
	}
	
	public void invokeConnectionStateChangedCallbackAsync(boolean isConnected) {
		Runnable listenerInvokator = new Runnable() {
			public void run() {
				for (NodeJsConnectionStateListener connectionStateListener : connectionStateListeners) {
					connectionStateListener.nodeJsConnectionStateChanged(isConnected);
				}
			}
		};
		
		listenerInvokator.run();
	}
	
	public void invokePronoUpdateCallbackAsync(String playerName, String[] prono) {
		for (PronoListener pronoListener : pronoListeners) {
			pronoListener.pronoUpdated(playerName, prono);
		}
	}
	
	public void invokePronoCountCallbackAsync(int currentCount, int maxCount, float currentRate, float maxRate) {
		for (PronoListener pronoListener : pronoListeners) {
			pronoListener.pronoCounted(currentCount, maxCount, currentRate, maxRate);
		}
	}
	
	public void invokeBotStateRefreshCallbackAsync(BotStates botState) {
		Runnable listenerInvokator = new Runnable() {
			public void run() {
				for (BotStateListener botStateListener : botStateListeners) {
					botStateListener.botStateRefreshed(botState);
				}
			}
		};
		
		listenerInvokator.run();
	}
	
	public void invokeCursedPaniacInvokationCallback(String invokatorName) {
		for (CursedPaniacListener cursedPaniacListener : cursedPaniacListeners) {
			cursedPaniacListener.cursedPaniacSpawned(invokatorName);
		}
	}
}
