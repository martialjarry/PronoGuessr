package com.drawrunner.uniguess.model.communication.nodejs;

public interface PronoListener {
	public void pronoUpdated(String playerName, String[] districtNames);
	public void pronoCounted(int currentCount, int maxCount, float currentRate, float maxRate);
}