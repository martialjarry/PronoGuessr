package com.drawrunner.uniguess.model.communication.twitch;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.json.JSONArray;
import org.json.JSONObject;

import com.drawrunner.uniguess.model.communication.PlayerLoadingListener;
import com.drawrunner.uniguess.model.data.Player;

public class PlayerPoolDownloader implements Callable<Player[]> {
	public static final int POOL_SIZE = 25;
	
	private List<String> chatterNamePool;
	private List<PlayerLoadingListener> loadingListeners;
	
	public PlayerPoolDownloader(List<String> chatterNamePool) {
		this.chatterNamePool = chatterNamePool;
		this.loadingListeners = new ArrayList<PlayerLoadingListener>();
	}
	
	@Override
	public Player[] call() {
		Thread.currentThread().setName("Player Pool Downloader");
		
		Player[] players = new Player[chatterNamePool.size()];
		
		TwitchTools twitchTools = TwitchTools.getInstance();
		
		if(twitchTools != null) {
			for (String chatterName : chatterNamePool) {
				StringBuffer userData = twitchTools.getUser(chatterName);
				
				if(userData != null) {
					JSONObject userDataJson = new JSONObject(userData.toString());
					
					if(userDataJson.has("users")) {
						JSONArray usersJson = userDataJson.getJSONArray("users");
						
						if(!usersJson.isEmpty()) {
							for(int i = 0; i < usersJson.length(); i++) {
								JSONObject userJson = usersJson.getJSONObject(i);
								
								if(this.isUserJsonComplete(userJson)) {
									String playerId = userJson.getString("_id");
									String name = userJson.getString("name");
									String displayName = userJson.getString("display_name");
	
									String logoUrl = userJson.getString("logo");
									
									Player player = new Player(null, playerId, name, displayName, logoUrl);
									
									this.invokeLoadingListeners(player);
									
									players[i] = player;
								}
							}
						}
					}
				}
			}
		}
		
		return players;
	}
	
	public void addLoadingListener(PlayerLoadingListener loadingListener) {
		if(loadingListener != null) {
			loadingListeners.add(loadingListener);
		}
	}
	
	private void invokeLoadingListeners(Player player) {
		if(player != null) {
			for (PlayerLoadingListener loadingListener : loadingListeners) {
				loadingListener.playerLoaded(player);
			}
		}
	}
	
	private boolean isUserJsonComplete(JSONObject userJson) {
		return userJson.has("_id")
			&& userJson.has("name")
			&& userJson.has("display_name")
			&& userJson.has("logo");
	}
}
