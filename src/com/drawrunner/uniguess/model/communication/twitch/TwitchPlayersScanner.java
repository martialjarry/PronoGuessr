package com.drawrunner.uniguess.model.communication.twitch;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONArray;
import org.json.JSONObject;

import com.drawrunner.uniguess.model.communication.EntitiesScanner;
import com.drawrunner.uniguess.model.communication.PlayerLoadingListener;
import com.drawrunner.uniguess.model.data.Player;
import com.drawrunner.uniguess.model.data.PlayerBase;

public class TwitchPlayersScanner extends EntitiesScanner implements PlayerLoadingListener {
	private String channel;
	private PlayerLoadingListener loadingListener;
	
	private int scanCount;
	
	public TwitchPlayersScanner(String channel, PlayerLoadingListener loadingListener) {
		this.loadingListener = loadingListener;
		this.channel = channel;
		
		scanCount = 1;
	}
	
	@Override
	public void run() {
		Thread.currentThread().setName("Twitch Players Scanner");
		
		long timeStamp = System.nanoTime();
		
		while(true) {
			if(System.nanoTime() - timeStamp >= LOADING_TIME_INTERVAL * 1000000L || totalEntitiesCount == 0) {
				timeStamp = System.nanoTime();
				
				loadingState = LoadingStates.LOADING;
				this.loadTwitchPlayers();
				
				scanCount++;
				loadedEntitiesCount = 0;
				totalEntitiesCount = 0;
				
				loadingState = LoadingStates.IDLE;
				
				this.awaitNextReadingTime(timeStamp);
			}
		}
	}
	
	public void loadTwitchPlayers() {
		TwitchTools twitchTools = TwitchTools.getInstance();
		
		if(twitchTools != null) {
			StringBuffer channelChattersData = twitchTools.getChatters(channel);
			
			if(channelChattersData != null) {
				JSONObject channelChattersJson = new JSONObject(channelChattersData.toString());
				
				Set<String> chatterNames = this.extractChatterNames(channelChattersJson);
				Set<Callable<Player[]>> downloaders = this.buildDownloaders(chatterNames);
				
				this.launchDownloads(downloaders);
			}
		}
	}

	private Set<String> extractChatterNames(JSONObject channelChattersJson) {
		Set<String> chatterNames = new HashSet<String>();
		
		if(channelChattersJson.has("chatters")) {
			JSONObject chattersJson = channelChattersJson.getJSONObject("chatters");
			
			for (Iterator<String> keyIterator = chattersJson.keys(); keyIterator.hasNext();) {
				String chatterSectionKey = (String) keyIterator.next();
				JSONArray chatterSection = chattersJson.getJSONArray(chatterSectionKey);
				
				for(int i = 0; i < chatterSection.length(); i++) {
					String newChatterName = chatterSection.getString(i);
					chatterNames.add(newChatterName);
				}
			}
		}
		
		return chatterNames;
	}
	
	private Set<Callable<Player[]>> buildDownloaders(Set<String> chatterNames) {
		List<List<String>> chatterNamePools = new ArrayList<List<String>>();
		
		PlayerBase playerBase = PlayerBase.getInstance();
		
		totalEntitiesCount = 0;
		
		for(Iterator<String> iterator = chatterNames.iterator(); iterator.hasNext();) {
			String chatterName = (String) iterator.next();
			
			if(chatterNamePools.isEmpty()) {
				chatterNamePools.add(new ArrayList<String>());
			}
			
			List<String> chatterNamePool = chatterNamePools.get(chatterNamePools.size() - 1);
			
			if(chatterNamePool.size() >= PlayerPoolDownloader.POOL_SIZE) {
				chatterNamePool = new ArrayList<String>();
				chatterNamePools.add(chatterNamePool);
			}
			
			if(!playerBase.containsPlayerReadyToDisplay(chatterName) || scanCount % 30 == 0) {
				totalEntitiesCount++;
				chatterNamePool.add(chatterName);
			}
		}
		
		Set<Callable<Player[]>> downloaders = new HashSet<Callable<Player[]>>();
		for (List<String> playerNamePool : chatterNamePools) {
			PlayerPoolDownloader PlayerPoolDownloader = new PlayerPoolDownloader(playerNamePool);
			PlayerPoolDownloader.addLoadingListener(loadingListener);
			PlayerPoolDownloader.addLoadingListener(this);
			
			downloaders.add(PlayerPoolDownloader);
		}
		
		return downloaders;
	}
	
	private void launchDownloads(Set<Callable<Player[]>> downloaders) {
		int coreCount = Runtime.getRuntime().availableProcessors();
		ExecutorService executorService = Executors.newFixedThreadPool(coreCount * 4);
		
		try {
			executorService.invokeAll(downloaders);
			executorService.shutdownNow();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public synchronized void playerLoaded(Player player) {
		loadedEntitiesCount++;
		
		PlayerBase playerBase = PlayerBase.getInstance();
		playerBase.publishTwitchPlayerLoadingProgress(loadedEntitiesCount, totalEntitiesCount);
	}
	
	public boolean isLoading() {
		return loadingState == LoadingStates.LOADING;
	}
}