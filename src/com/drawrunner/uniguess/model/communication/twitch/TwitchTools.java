package com.drawrunner.uniguess.model.communication.twitch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.drawrunner.uniguess.model.data.Configuration;

public class TwitchTools {
	private Configuration configuration;
	
	private TwitchTools() {}
	
	public static TwitchTools initializeInstance(Configuration configuration) {
		TwitchToolsHolder.instance.configuration = configuration;
		return TwitchToolsHolder.instance;
	}
	
	public static TwitchTools getInstance() {
		if(TwitchToolsHolder.instance.configuration != null) {
			return TwitchToolsHolder.instance;
		} else {
			return null;
		}
	}
	
	public StringBuffer getUserId(String userName) {
		String apiUrl = "https://api.twitch.tv/kraken/users?login=" + userName;
		return this.getTwitchData(apiUrl);
	}
	
	public StringBuffer getChatters(String channel) {
		String apiUrl = "http://tmi.twitch.tv/group/user/" + channel + "/chatters";
		return this.getTwitchData(apiUrl);
	}
	
	public StringBuffer getUser(String userName) {
		String apiUrl = "https://api.twitch.tv/kraken/users?login=" + userName;
		return this.getTwitchData(apiUrl);
	}
	
	public StringBuffer getTwitchData(String stringUrl) {
		try {
			URL url = new URL(stringUrl);
			
			HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
			httpConnection.setRequestProperty("Accept", "application/vnd.twitchtv.v5+json");
			httpConnection.setRequestProperty("Authorization", "OAuth " + configuration.getBotPassword());
			
			httpConnection.setRequestMethod("GET");
			
			InputStream connectionInputStream = httpConnection.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(connectionInputStream);
			BufferedReader responseInput = new BufferedReader(inputStreamReader);
			
			StringBuffer responseContent = new StringBuffer();
			String responseLine = new String();
			while (responseLine != null) {
				responseContent.append(responseLine);
				responseLine = responseInput.readLine();
			}
			
			responseInput.close();
			
			return responseContent;
		} catch (MalformedURLException e) {
			System.err.println("URL mal form�e : " + stringUrl);
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Impossible de lire les donn�es depuis Twitch.");
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
	
	private static class TwitchToolsHolder {
		private static final TwitchTools instance = new TwitchTools();
	}
}
