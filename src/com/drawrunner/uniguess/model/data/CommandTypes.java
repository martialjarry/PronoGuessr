package com.drawrunner.uniguess.model.data;

public enum CommandTypes {
	GO_PRONO, STOP_PRONO,
	PRONO, PRONO_RESET, PRONO_DISPLAY_ALL,
	DISTRICT, DISTRICT_ADD,
	ODDS,
	SCORE, SCORE_DISPLAY_ALL, SCORE_ADD;
}
