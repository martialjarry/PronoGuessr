package com.drawrunner.uniguess.model.data;

import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Observable;
import java.util.Scanner;

import javax.swing.JOptionPane;

import org.json.JSONException;
import org.json.JSONObject;

public class Configuration extends Observable {
	private String configurationFilePath;
	
	private String targetChannel;
	
	private String databasePath;
	
	private String botUsername;
	private String botPassword;
	private int botPronoAmoutLimit;
	private int botMessagesAmountRestriction;
	private int botMessagesRestrictionInterval;
	
	private int pronoTrayFullscreen;
	
	private int pronoTrayWidth;
	private int pronoTrayHeight;
	
	private boolean displayCounter;
	
	private CursedPaniacStatus cursedPaniacStatus;
	
	private Configuration() {
		OutputManager outputManager = OutputManager.getInstance();
		configurationFilePath = outputManager.getDataFolderPath() + File.separator + "configuration.json";
		
		targetChannel = null;
		
		databasePath = null;
		
		botUsername = null;
		botPassword = null;
		botPronoAmoutLimit = 1;
		botMessagesAmountRestriction = 100;
		botMessagesRestrictionInterval = 30;
		
		pronoTrayFullscreen = 0;
		
		pronoTrayWidth = 960;
		pronoTrayHeight = 540;
		
		displayCounter = true;
		
		cursedPaniacStatus = CursedPaniacStatus.ENABLED;
	}
	
	public static Configuration getInstance() {
		return ConfigurationHolder.instance;
	}
	
	public void loadConfiguration() {
		File configurationFile = new File(configurationFilePath);
		
		if(configurationFile.exists()) {
			this.parseAndPublishConfiguration(configurationFile);
		}
	}
	
	private void parseAndPublishConfiguration(File configurationFile) {
		try {
			Scanner configurationReader = new Scanner(configurationFile);
			String configurationContent = new String();
			
			while (configurationReader.hasNext()) {
				configurationContent += configurationReader.nextLine();
			}
			
			configurationReader.close();
			
			JSONObject configurationJson = new JSONObject(configurationContent);
			
			boolean isFileComplete = true;
			
			if(configurationJson.has("target_channel") && isFileComplete) {
				targetChannel = configurationJson.getString("target_channel").toLowerCase();
			} else {
				isFileComplete = false;
			}
			
			if(configurationJson.has("database_path") && isFileComplete) {
				databasePath = configurationJson.getString("database_path");
			} else {
				isFileComplete = false;
			}
			
			if(isFileComplete) {
				isFileComplete = this.parseBotData(configurationJson);
			}
			
			if(isFileComplete) {
				isFileComplete = this.parsePronoTrayData(configurationJson);
			}
			
			if(!isFileComplete) {
				String incompleteFileWarningMessage = "Le fichier de configuration semble incomplet.";
				JOptionPane.showMessageDialog(null, incompleteFileWarningMessage, "Attention", JOptionPane.WARNING_MESSAGE);
			}
			
			this.publishConfiguration();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Je n'ai pas trouv� le fichier de configuration", "Erreur", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (JSONException e) {
			String unreadableFileErrorMessage = "Je n'ai pas pu lire le fichier de configuration.";
			unreadableFileErrorMessage += "\nIl sera �cras� lors de la validation de la configuration.";
			
			JOptionPane.showMessageDialog(null, unreadableFileErrorMessage, "Erreur", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	private boolean parseBotData(JSONObject configurationJson) {
		if(configurationJson.has("bot")) {
			JSONObject botJson = configurationJson.getJSONObject("bot");
			
			if(botJson.has("username")) {
				botUsername = botJson.getString("username");
			} else {
				return false;
			}
			
			if(botJson.has("password")) {
				botPassword = botJson.getString("password");
			} else {
				return false;
			}
			
			if(botJson.has("prono_amount_limit")) {
				botPronoAmoutLimit = botJson.getInt("prono_amount_limit");
			} else {
				return false;
			}
			
			if(botJson.has("message_amount_restriction")) {
				botMessagesAmountRestriction = botJson.getInt("message_amount_restriction");
			} else {
				return false;
			}
			
			if(botJson.has("message_restriction_interval")) {
				botMessagesRestrictionInterval = botJson.getInt("message_restriction_interval");
			} else {
				return false;
			}
		} else {
			return false;
		}
		
		return true;
	}
	
	private boolean parsePronoTrayData(JSONObject configurationJson) {
		if(configurationJson.has("prono_tray")) {
			JSONObject pronoTrayJson = configurationJson.getJSONObject("prono_tray");
			
			if(pronoTrayJson.has("fullscreen")) {
				pronoTrayFullscreen = pronoTrayJson.getInt("fullscreen");
			} else {
				return false;
			}
			
			if(pronoTrayJson.has("size")) {
				JSONObject sizeJson = pronoTrayJson.getJSONObject("size");
				
				if(sizeJson.has("width")) {
					pronoTrayWidth = Math.max(sizeJson.getInt("width"), 200);
				} else {
					return false;
				}
				
				if(sizeJson.has("height")) {
					pronoTrayHeight = Math.max(sizeJson.getInt("height"), 200);
				} else {
					return false;
				}
			} else {
				return false;
			}
			
			if(pronoTrayJson.has("display_counter")) {
				displayCounter = pronoTrayJson.getBoolean("display_counter");
			} else {
				return false;
			}
			
			if(pronoTrayJson.has("cursed_paniac_status")) {
				String cursedPaniacStatusRaw = pronoTrayJson.getString("cursed_paniac_status");
				
				if(cursedPaniacStatusRaw != null) {
					try {
						cursedPaniacStatus = CursedPaniacStatus.valueOf(CursedPaniacStatus.class, cursedPaniacStatusRaw);
					} catch (IllegalArgumentException e) {
						cursedPaniacStatus = CursedPaniacStatus.DISABLED_FOR_VIEWERS;
						e.printStackTrace();
					}
					
					if(cursedPaniacStatus == CursedPaniacStatus.DISABLED_ONCE) {
						cursedPaniacStatus = CursedPaniacStatus.ENABLED;
						this.saveConfiguration();
					}
				}
			}
		} else {
			return false;
		}
		
		return true;
	}
	
	public void saveConfiguration() {
		File configurationFile = new File(configurationFilePath);
		
		if(!configurationFile.exists()) {
			this.createBlankConfigurationFile(configurationFile);
		}
		
		if(configurationFile.exists()) {
			try {
				FileWriter configurationWriter = new FileWriter(configurationFile);
				
				JSONObject configuration = new JSONObject();
				configuration.put("target_channel", (targetChannel != null ? targetChannel.toLowerCase() : null));
				configuration.put("database_path", databasePath);
				
				JSONObject botJson = new JSONObject();
				botJson.put("username", botUsername);
				botJson.put("password", botPassword);
				botJson.put("prono_amount_limit", botPronoAmoutLimit);
				botJson.put("message_amount_restriction", botMessagesAmountRestriction);
				botJson.put("message_restriction_interval", botMessagesRestrictionInterval);
				configuration.put("bot", botJson);
				
				JSONObject pronoTrayJson = new JSONObject();
				pronoTrayJson.put("fullscreen", pronoTrayFullscreen);
				
				JSONObject pronoTraySize = new JSONObject();
				pronoTraySize.put("width", pronoTrayWidth);
				pronoTraySize.put("height", pronoTrayHeight);
				pronoTrayJson.put("size", pronoTraySize);
				
				pronoTrayJson.put("display_counter", displayCounter);
				pronoTrayJson.put("cursed_paniac_status", cursedPaniacStatus);
				
				configuration.put("prono_tray", pronoTrayJson);

				configurationWriter.write(configuration.toString(4));
				configurationWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void createBlankConfigurationFile(File configurationFile) {
		try {
			configurationFile.createNewFile();
			
			FileWriter configurationWriter = new FileWriter(configurationFile);

			JSONObject blankConfiguration = new JSONObject();
			blankConfiguration.put("target_channel", new String());
			blankConfiguration.put("database_path", new String());
			
			JSONObject botJson = new JSONObject();
			blankConfiguration.put("bot", botJson);
			
			botJson.put("username", new String());
			botJson.put("password", new String());
			botJson.put("prono_amount_limit", 1);
			botJson.put("message_amount_restriction", 100);
			botJson.put("message_restriction_interval", 30);
			
			JSONObject pronoTrayJson = new JSONObject();
			pronoTrayJson.put("fullscreen", 0);
			
			JSONObject pronoTraySize = new JSONObject();
			pronoTraySize.put("width", 960);
			pronoTraySize.put("height", 540);
			pronoTrayJson.put("size", pronoTraySize);
			
			pronoTrayJson.put("display_counter", true);
			pronoTrayJson.put("cursed_paniac_status", CursedPaniacStatus.ENABLED);
			
			configurationWriter.write(blankConfiguration.toString(4));
			configurationWriter.close();
		} catch (IOException e) {
			String iOExceptionMessage = "Je n'ai pas pu cr�er un nouveau fichier de configuration vierge.";
			iOExceptionMessage += "\nEssayez de relancer le programme en tant qu'administrateur.";
			JOptionPane.showMessageDialog(null, iOExceptionMessage, "Erreur", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	public boolean isValid() {
		return targetChannel != null && !targetChannel.isEmpty()
			&& databasePath != null && !databasePath.isEmpty()
			&& botUsername != null && !botUsername.isEmpty()
			&& botPassword != null && !botPassword.isEmpty()
			&& botPronoAmoutLimit > 0
			&& botMessagesAmountRestriction > 1
			&& botMessagesRestrictionInterval > 0
			&& pronoTrayFullscreen >= 0
			&& pronoTrayWidth > 0
			&& pronoTrayHeight > 0;
	}
	
	public Point pronoTraySize() {
		return new Point(pronoTrayWidth, pronoTrayHeight);
	}
	
	private void publishConfiguration() {
		String[] configurationData = new String[] {
				targetChannel,
				databasePath,
				botUsername,
				botPassword,
				botPronoAmoutLimit + "",
				botMessagesAmountRestriction + "",
				botMessagesRestrictionInterval + "",
				pronoTrayFullscreen + "",
				pronoTrayWidth + "",
				pronoTrayHeight + "",
				cursedPaniacStatus + "",
				displayCounter + ""
		};
		
		this.setChanged();
		this.notifyObservers(configurationData);
	}
	
	public boolean isCursedPaniacEnabled() {
		return cursedPaniacStatus == CursedPaniacStatus.ENABLED
			|| cursedPaniacStatus == CursedPaniacStatus.DISABLED_FOR_VIEWERS;
	}
	
	public String getTargetChannel() {
		return targetChannel;
	}

	public void setTargetChannel(String targetChannel) {
		this.targetChannel = targetChannel;
		this.publishConfiguration();
	}

	public String getDatabasePath() {
		return databasePath;
	}

	public void setDatabasePath(String databasePath) {
		this.databasePath = databasePath;
		this.publishConfiguration();
	}

	public String getBotUsername() {
		return botUsername;
	}

	public void setBotUsername(String botUsername) {
		this.botUsername = botUsername;
		this.publishConfiguration();
	}

	public String getBotPassword() {
		return botPassword;
	}

	public void setBotPassword(String botPassword) {
		this.botPassword = botPassword;
		this.publishConfiguration();
	}

	public int getBotPronoAmoutLimit() {
		return botPronoAmoutLimit;
	}

	public void setBotPronoAmoutLimit(int botPronoAmoutLimit) {
		this.botPronoAmoutLimit = botPronoAmoutLimit;
		this.publishConfiguration();
	}
	
	public int getBotMessagesAmountRestriction() {
		return botMessagesAmountRestriction;
	}

	public void setBotMessagesAmountRestriction(int botMessagesAmountRestriction) {
		this.botMessagesAmountRestriction = botMessagesAmountRestriction;
		this.publishConfiguration();
	}

	public int getBotMessagesRestrictionInterval() {
		return botMessagesRestrictionInterval;
	}

	public void setBotMessagesRestrictionInterval(int botMessagesRestrictionInterval) {
		this.botMessagesRestrictionInterval = botMessagesRestrictionInterval;
		this.publishConfiguration();
	}
	
	public int getPronoTrayFullscreen() {
		return pronoTrayFullscreen;
	}

	public void setPronoTrayFullscreen(int pronoTrayFullscreen) {
		this.pronoTrayFullscreen = pronoTrayFullscreen;
		this.publishConfiguration();
	}

	public int getPronoTrayWidth() {
		return pronoTrayWidth;
	}

	public void setPronoTrayWidth(int pronoTrayWidth) {
		this.pronoTrayWidth = pronoTrayWidth;
		this.publishConfiguration();
	}

	public int getPronoTrayHeight() {
		return pronoTrayHeight;
	}

	public void setPronoTrayHeight(int pronoTrayHeight) {
		this.pronoTrayHeight = pronoTrayHeight;
		this.publishConfiguration();
	}
	
	public boolean isDisplayCounter() {
		return displayCounter;
	}

	public void setDisplayCounter(boolean displayCounter) {
		this.displayCounter = displayCounter;
		this.publishConfiguration();
	}
	
	public CursedPaniacStatus getCursedPaniacStatus() {
		return cursedPaniacStatus;
	}

	public void setCursedPaniacStatus(CursedPaniacStatus cursedPaniacStatus) {
		this.cursedPaniacStatus = cursedPaniacStatus;
	}

	private final static class ConfigurationHolder {
		private final static Configuration instance = new Configuration();
	}
}