package com.drawrunner.uniguess.model.data;

public enum CursedPaniacStatus {
	DISABLED_FOREVER, DISABLED_FOR_VIEWERS, DISABLED_ONCE, ENABLED
}
