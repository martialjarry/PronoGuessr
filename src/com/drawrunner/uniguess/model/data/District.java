package com.drawrunner.uniguess.model.data;

public class District {
	private String identifier;
	
	private String name;
	private String flagUrl;
	private float odds;
	
	public District(String identifier, String name) {
		this.identifier = identifier;
		this.name = name;
		
		this.flagUrl = null;
	}
	
	public District(String identifier, String name, String flagUrl, float odds) {
		this.identifier = identifier;
		this.name = name;
		
		this.flagUrl = flagUrl;
		this.odds = odds;
	}

	public boolean isReadyForDisplay() {
		return name != null && flagUrl != null;
	}
	
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFlagUrl() {
		return flagUrl;
	}

	public void setFlagUrl(String flagUrl) {
		this.flagUrl = flagUrl;
	}

	public float getOdds() {
		return odds;
	}

	public void setOdds(float odds) {
		this.odds = odds;
	}
}
