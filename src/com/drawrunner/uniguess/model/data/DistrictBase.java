package com.drawrunner.uniguess.model.data;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.drawrunner.uniguess.model.communication.DistrictLoadingListener;
import com.drawrunner.uniguess.model.communication.mongo.MongoDistrictsScanner;

public class DistrictBase extends Observable {
	private Map<String, District> districts;
	
	private Lock storageLock;
	
	private DistrictBase() {
		districts = new HashMap<String, District>();
		
		storageLock = new ReentrantLock();
	}
	
	public static DistrictBase getInstance() {
		return PlayerBaseHolder.instance;
	}
	
	public void LaunchLoadingFromMongoDb(DistrictLoadingListener loadingListener) {
		MongoDistrictsScanner districtsScanner = new MongoDistrictsScanner(loadingListener);
		
		Thread districtsScannerTread = new Thread(districtsScanner);
		districtsScannerTread.start();
	}
	
	public void addOrUpdateDistrict(District district) {
		try {
			storageLock.lock();
			
			if(district != null) {
				if(!districts.containsKey(district.getIdentifier())) {
					districts.put(district.getIdentifier(), district);
				} else {
					District existingDistrict = districts.get(district.getIdentifier());
					existingDistrict.setName(district.getName());
					existingDistrict.setOdds(district.getOdds());
				}
				
				if(district.isReadyForDisplay()) {
					this.publishDistrict(district);
				}
			}
		} finally {
			storageLock.unlock();
		}
	}
	
	public void publishDistrict(District district) {
		String[] districtData = new String[5];
		districtData[0] = "DISTRICT";
		districtData[1] = district.getIdentifier();
		districtData[2] = district.getName();
		districtData[3] = district.getOdds() + "";
		districtData[4] = district.getFlagUrl();

		this.setChanged();
		this.notifyObservers(districtData);
	}
	
	public void publishMongoDistrictLoadingProgress(int loadedDistrictsCount, int totalDistrictsCount) {
		String[] districtData = {
			"MONGO_DISTRICTS_LOADING_PROGRESS",
			loadedDistrictsCount + "",
			totalDistrictsCount + ""
		};
		
		this.setChanged();
		this.notifyObservers(districtData);
	}
	
	public District removeDistrict(String identifier) {
		District removedDistrict = null;
		
		try {
			storageLock.lock();
			removedDistrict = districts.remove(identifier);
		} finally {
			storageLock.unlock();
		}
		
		return removedDistrict; 
	}

	public District getDistrict(String identifier) {
		District district = null;
		
		try {
			storageLock.lock();
			district = districts.get(identifier);
		} finally {
			storageLock.unlock();
		}
		
		return district;
	}
	
	public District getDistrictByName(String name) {
		District currentDistrict = null;
		boolean districtFound = false;
		
		try {
			storageLock.lock();
			String lowerName = name.toLowerCase();
			
			for (Iterator<District> districtIterator = districts.values().iterator(); districtIterator.hasNext() && !districtFound;) {
				currentDistrict = districtIterator.next();
				
				if(currentDistrict.getName().equals(lowerName)) {
					districtFound = true;
				}
			}
		} finally {
			storageLock.unlock();
		}
		
		if(districtFound) {
			return currentDistrict;
		} else {
			return null;
		}
	}
	
	public boolean isEmpty() {
		return this.districtCount() == 0;
	}
	
	public int districtCount() {
		int count = 0;
		
		try {
			storageLock.lock();
			count = districts.size();
		} finally {
			storageLock.unlock();
		}
		
		return count;
	}
	
	private static final class PlayerBaseHolder {
		private final static DistrictBase instance = new DistrictBase();
	}
}
