package com.drawrunner.uniguess.model.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OutputManager {
	private String dataFolderPath;
	
	private OutputManager() {
		dataFolderPath = ".";
	}
	
	public static OutputManager getInstance() {
		return OutputManagerHolder.instance;
	}
	
	public void setupDataFolder() {
		String userHomePath = System.getProperty("user.home");
		
		if(userHomePath != null) {
			File userFolder = new File(userHomePath);
			
			if(userFolder.exists()) {
				dataFolderPath = userFolder.getAbsolutePath() + File.separator + "PronoGuessr";
			}
			
			File pronoguessrDataFolder = new File(dataFolderPath);
			
			if(!pronoguessrDataFolder.exists()) {
				pronoguessrDataFolder.mkdirs();
			}
		}
	}
	
	public void redirectErrorsToLogFile() {
		try {
			File logFile = new File(dataFolderPath + File.separator + "log.txt");
			
			FileOutputStream logFileOutputStream = new FileOutputStream(logFile, true);
			System.setErr(new PrintStream(logFileOutputStream));
			
			this.printLogFileHeader(logFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private void printLogFileHeader(File logFile) {
		boolean isEmpty = true;
		
		if(logFile.exists()) {
			try {
				BufferedReader logFileReader = new BufferedReader(new FileReader(logFile));     
				isEmpty = (logFileReader.readLine() == null);
				logFileReader.close();
			} catch (IOException e) {
				System.err.println("Impossible de lire le fichier de logs.");
				e.printStackTrace();
			}
		}
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		String headerMesage = "==== [" + dateFormat.format(date) + "] ======================================================================================================================================================================";
		if(!isEmpty) {
			headerMesage = "\n" + headerMesage;
		}
		
		System.err.println(headerMesage);
	}
	
	public String getDataFolderPath() {
		return dataFolderPath;
	}
	
	private static final class OutputManagerHolder {
		private final static OutputManager instance = new OutputManager();
	}
}
