package com.drawrunner.uniguess.model.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Player extends Observable {
	private String mongoId;
	private String twitchId;
	
	private String name;
	private String displayName;

	private String logoUrl;
	
	private int score;
	private District[] prono;

	public Player(String mongoId, String twitchId, String name) {
		this.mongoId = mongoId;
		this.twitchId = twitchId;

		this.name = name;
		displayName = null;
		
		logoUrl = null;
		
		score = 0;
		prono = new District[0];
	}
	
	public Player(String mongoId, String twitchId, String name, String displayName, String logoUrl) {
		this.mongoId = mongoId;
		this.twitchId = twitchId;
		
		this.name = name;
		this.displayName = displayName;
		
		this.logoUrl = logoUrl;
		
		score = 0;
		prono = new District[0];
	}
	
	public boolean isReadyForDisplay() {
		return displayName != null;
	}
	
	public String getMongoId() {
		return mongoId;
	}

	public void setMongoId(String mongoId) {
		this.mongoId = mongoId;
	}

	public String getTwitchId() {
		return twitchId;
	}

	public void setTwitchId(String twitchId) {
		this.twitchId = twitchId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public District[] getProno() {
		return prono;
	}
	
	public void setProno(District[] prono) {
		List<District> validDistricts = new ArrayList<District>();
		
		for(District pronoDistrict : prono) {
			if(pronoDistrict != null) {
				validDistricts.add(pronoDistrict);
			}
		}
		
		District[] clearedProno = new District[validDistricts.size()];
		for(int i=  0; i < validDistricts.size(); i++) {
			clearedProno[i] = validDistricts.get(i);
		}
		
		this.prono = clearedProno;
	}
}
