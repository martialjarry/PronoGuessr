package com.drawrunner.uniguess.model.data;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import com.drawrunner.uniguess.model.communication.PlayerLoadingListener;
import com.drawrunner.uniguess.model.communication.mongo.MongoPlayersScanner;
import com.drawrunner.uniguess.model.communication.twitch.TwitchPlayersScanner;

public class PlayerBase extends Observable {
	private Map<String, Player> players;
	
	private PlayerBase() {
		players = new HashMap<String, Player>();
	}
	
	public static PlayerBase getInstance() {
		return PlayerBaseHolder.instance;
	}
	
	public void LaunchLoadingFromTwitch(String channel, PlayerLoadingListener loadingListener) {
		TwitchPlayersScanner playersScanner = new TwitchPlayersScanner(channel, loadingListener);
		
		Thread playersScannerThread = new Thread(playersScanner);
		playersScannerThread.start();
	}
	
	public void LaunchLoadingFromMongoDb(PlayerLoadingListener loadingListener) {
		MongoPlayersScanner playersScanner = new MongoPlayersScanner(loadingListener);
		
		Thread playersScannerThread = new Thread(playersScanner);
		playersScannerThread.start();
	}
	
	public void addOrUpdatePlayer(Player player) {
		if(player != null) {
			if(!players.containsKey(player.getName().toLowerCase())) {
				players.put(player.getName().toLowerCase(), player);
			} else {
				Player existingPlayer = players.get(player.getName().toLowerCase());
				
				if(player.getMongoId() != null) {
					existingPlayer.setMongoId(player.getMongoId());
				}
				
				if(player.getTwitchId() != null) {
					existingPlayer.setTwitchId(player.getTwitchId());
				}
				
				existingPlayer.setName(player.getName());
				existingPlayer.setDisplayName(player.getDisplayName());
				existingPlayer.setProno(player.getProno());
				existingPlayer.setScore(player.getScore());
			}
		}
		
//		if(player.isReadyForDisplay()) {
			this.publishPlayer(player);
//		}
	}
	
	public void publishPlayer(Player player) {
		String[] playerData = new String[4];
		playerData[0] = "PLAYER";
		playerData[1] = player.getName().toLowerCase();
		playerData[2] = player.getDisplayName();
		playerData[3] = player.getLogoUrl();
		
		this.setChanged();
		this.notifyObservers(playerData);
	}
	
	public void updatePlayerProno(String playerName, String[] districtNames) {
		Player sourcePlayer = this.getPlayer(playerName);
		
		if(sourcePlayer != null) {
			DistrictBase districtBase = DistrictBase.getInstance();
			
			District[] prono = new District[districtNames.length];
			
			for(int i = 0; i < districtNames.length; i++) {
				String districtName = districtNames[i];
				prono[i] = districtBase.getDistrictByName(districtName);
			}
			
			sourcePlayer.setProno(prono);
			
			this.publishPlayerProno(sourcePlayer, prono);
		}
	}
	
	public void publishPlayerProno(Player player, District[] prono) {
		String[] playerData = new String[3];
		playerData[0] = "PLAYER_PRONO";
		playerData[1] = player.getName().toLowerCase();
		
		String pronoData = "";
		for (int i = 0; i < prono.length; i++) {
			District district = prono[i];
			
			String pronoId = district.getIdentifier();
			String pronoOdds = String.valueOf(district.getOdds());
			
			pronoData += pronoId + ";" + pronoOdds;
			
			if(i < prono.length - 1) {
				pronoData += ";";
			}
		}
		
		playerData[2] = pronoData;
		
		this.setChanged();
		this.notifyObservers(playerData);
	}
	
	public void publishPronoCount(int currentCount, int maxCount, float currentRate, float maxRate) {
		String[] pronoCountData = new String[5];
		
		pronoCountData[0] = "PRONO_COUNT";
		
		pronoCountData[1] = currentCount + "";
		pronoCountData[2] = maxCount + "";
	
		pronoCountData[3] = currentRate + "";
		pronoCountData[4] = maxRate + "";
		
		this.setChanged();
		this.notifyObservers(pronoCountData);
	}
	
	public void publishTwitchPlayerLoadingProgress(int loadedPlayersCount, int totalPlayersCount) {
		String[] playerData = {
			"TWITCH_PLAYERS_LOADING_PROGRESS",
			loadedPlayersCount + "",
			totalPlayersCount + ""
		};
		
		this.setChanged();
		this.notifyObservers(playerData);
	}
	
	public void publishMongoPlayerLoadingProgress(int loadedPlayersCount, int totalPlayersCount) {
		String[] playerData = {
			"MONGO_PLAYERS_LOADING_PROGRESS",
			loadedPlayersCount + "",
			totalPlayersCount + ""
		};
		
		this.setChanged();
		this.notifyObservers(playerData);
	}
	
	public Player removePlayer(String playerName) {
		return players.remove(playerName.toLowerCase());
	}

	public Player getPlayer(String playerName) {
		return players.get(playerName.toLowerCase());
	}
	
	public boolean containsPlayerWithName(String playerName) {
		return this.getPlayer(playerName.toLowerCase()) != null;
	}
	
	public boolean containsPlayerReadyToDisplay(String playerName) {
		Player player = players.get(playerName.toLowerCase());
		
		if(player != null) {
			return player.isReadyForDisplay();
		} else {
			return false;
		}
	}
	
	public boolean isEmpty() {
		return players.isEmpty();
	}
	
	public int playersCount() {
		return players.size();
	}
	
	private static final class PlayerBaseHolder {
		private final static PlayerBase instance = new PlayerBase();
	}
}
