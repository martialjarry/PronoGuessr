package com.drawrunner.uniguess.view.animation;

import java.util.concurrent.TimeUnit;

/**
 * Permet d'animer une variable d'une valeur de d�but � une valeur
 * de fin et d'un temps de d�but � un temps de fin
 * @author Dorian
 */
public abstract class Animation {
	
	/** Dur�e de l'animation */
	protected long duration;
	
	/** Unit� du temps sp�cifi� par l'utilisateur */
	protected TimeUnit timeUnit;
	
	/** Statut de l'animation */
	protected AnimationStatus status;
	
	/** Temps de d�but de l'animation */
	protected long startTime;
	
	/** Temps de fin de l'animation */
	protected long endTime;

	public Animation() {
		this.duration = 0;
		this.timeUnit = TimeUnit.SECONDS;
		this.status = AnimationStatus.INITIALISED;
	}
	
	public Animation(long duree, TimeUnit uniteeTemps) {
		this.duration = duree;
		this.timeUnit = uniteeTemps;
		this.status = AnimationStatus.INITIALISED;
	}
	
	public Animation(long duration, TimeUnit timeUnit, boolean demarrer) {
		this.duration = duration;
		this.timeUnit = timeUnit;
		
		if(demarrer) {
			this.start();
		} else {
			status = AnimationStatus.INITIALISED;
		}
	}
	
	/**
	 * D�marre l'animation
	 */
	public void start() {
		this.startTime = System.nanoTime();
		this.refreshEndTime();
		this.status = AnimationStatus.ANIMATING;
	}
	
	/**
	 * Arrête l'animation
	 */
	public void stop() {
		this.endTime = System.nanoTime();
		this.status = AnimationStatus.FINISHED;
	}
	
	/**
	 * Progression de l'animation
	 * @return : valeur entre 0 et 1, repr�sentant la progression de l'animation
	 */
	public float nextFraction() {
		long currentTime = System.nanoTime();
		
		float nextFraction = (currentTime - startTime) / ((endTime - startTime) * 1F);
		
		if(nextFraction >= 1) {
			nextFraction = 1;
			status = AnimationStatus.FINISHED;
		}
		
		return nextFraction;
	}
	
	/**
	 * Actualise le temps de fin � partir de l'unit� de emps sp�cifi�e
	 */
	public void refreshEndTime() {
		switch (this.timeUnit) {
		case NANOSECONDS:
			endTime = startTime + duration;
			break;
		case MICROSECONDS:
			endTime = startTime + duration * 1000L;
			break;
		case MILLISECONDS:
			endTime = startTime + duration * 1000000L;
			break;
		case SECONDS:
			endTime = startTime + duration * 1000000000L;
			break;
		default:
			endTime = startTime + duration;
			break;
		}
	}
	
	/**
	 * Indique si l'aniation est en cours
	 * @return : true si l'animation et en cours, faux sinon
	 */
	public boolean isAnimating() {
		return status == AnimationStatus.ANIMATING;
	}
	
	/**
	 * Indique si l'aniation est termin�e
	 * @return : true si l'animation et termin�e, faux sinon
	 */
	public boolean isFinished() {
		return status == AnimationStatus.FINISHED;
	}
	
	public AnimationStatus getStatus() {
		return status;
	}
	
	public void setStatus(AnimationStatus status) {
		this.status = status;
	}

	public long getDuration() {
		return duration;
	}
	
	public void setDuration(long duration) {
		this.duration = duration;
	}

	public TimeUnit getTimeUnit() {
		return timeUnit;
	}
	
	public void setTimeUnit(TimeUnit timeUnit) {
		this.timeUnit = timeUnit;
	}

	public long getStartTime() {
		return startTime;
	}
	
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}
	
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
}
