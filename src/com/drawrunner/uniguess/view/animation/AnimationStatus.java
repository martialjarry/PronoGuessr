package com.drawrunner.uniguess.view.animation;

/**
 * Statuts des aniations
 * @author Dorian
 */
public enum AnimationStatus {
	INITIALISED, ANIMATING, FINISHED
}
