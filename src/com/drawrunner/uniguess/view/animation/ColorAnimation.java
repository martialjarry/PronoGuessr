package com.drawrunner.uniguess.view.animation;

import java.awt.Color;
import java.util.concurrent.TimeUnit;

import processing.core.PApplet;

/**
 * Permet d'animer une couleur d'une valeur de d�but � une valeur
 * de fin et d'un temps de d�but � un temps de fin
 * @author Dorian
 */
public class ColorAnimation extends Animation {
	/** Valeur de rouge de d�but */
	int startRedValue;
	
	/** Valeur de vert de d�but */
	int startGreenValue;

	/** Valeur de bleu de d�but */
	int startBlueValue;

	/** Valeur de rouge de fin */
	int endRedvalue;
	
	/** Valeur de vert de fin */
	int endGreenValue;

	/** Valeur de bleu de fin */
	int endBlueValue;
	
	public ColorAnimation() {
		super();
		
		endRedvalue = 0;
		startGreenValue = 0;
		startBlueValue = 0;
		
		endRedvalue = 0;
		endGreenValue = 0;
		endBlueValue = 0;
	}

	public ColorAnimation(long duration, TimeUnit timeUnit) {
		super(duration, timeUnit);

		endRedvalue = 0;
		startGreenValue = 0;
		startBlueValue = 0;
		
		endRedvalue = 0;
		endGreenValue = 0;
		endBlueValue = 0;
	}

	public ColorAnimation(long duration, TimeUnit timeUnit, boolean startNow) {
		super(duration, timeUnit, startNow);

		endRedvalue = 0;
		startGreenValue = 0;
		startBlueValue = 0;
		
		endRedvalue = 0;
		endGreenValue = 0;
		endBlueValue = 0;
	}

	public ColorAnimation(long duration, TimeUnit timeUnit, Color startColor, Color endColor) {
		super(duration, timeUnit);

		startRedValue = startColor.getRed();
		startGreenValue = startColor.getGreen();
		startBlueValue = startColor.getBlue();;
		
		endRedvalue = endColor.getRed();
		endGreenValue = endColor.getGreen();
		endBlueValue = endColor.getBlue();
	}

	public ColorAnimation(long duration, TimeUnit timeUnit, boolean startNow, Color startColor, Color endColor) {
		super(duration, timeUnit, startNow);

		startRedValue = startColor.getRed();
		startGreenValue = startColor.getGreen();
		startBlueValue = startColor.getBlue();;
		
		endRedvalue = endColor.getRed();
		endGreenValue = endColor.getGreen();
		endBlueValue = endColor.getBlue();
	}

	@Override
	public void stop() {
		super.stop();
		
		Color nextColor = this.nextColor();
		
		endRedvalue = nextColor.getRed();
		endGreenValue = nextColor.getGreen();
		endBlueValue = nextColor.getBlue();
	}
	
	/**
	 * Rtourne la couleur suivante dans l'animation
	 * @return : couleur suivante
	 */
	public Color nextColor() {
		float nextFraction = this.nextFraction();
		
		if(this.status == AnimationStatus.ANIMATING) {
			int newRedValue = (int) (startRedValue + (endRedvalue - startRedValue) * PApplet.sin(nextFraction * PApplet.HALF_PI));
			int newGreenValue = (int) (startGreenValue + (endGreenValue - startGreenValue) * PApplet.sin(nextFraction * PApplet.HALF_PI));
			int newBlueValue = (int) (startBlueValue + (endBlueValue - startBlueValue) * PApplet.sin(nextFraction * PApplet.HALF_PI));
			
			return new Color(newRedValue, newGreenValue, newBlueValue);
		} else if(this.status == AnimationStatus.INITIALISED) {
			return new Color(startRedValue, startGreenValue, startBlueValue);
		} else {
			return new Color(endRedvalue, endGreenValue, endBlueValue);
		}
	}
}
