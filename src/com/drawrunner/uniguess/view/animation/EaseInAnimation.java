package com.drawrunner.uniguess.view.animation;

import java.util.concurrent.TimeUnit;

import processing.core.PApplet;

public class EaseInAnimation extends ValueAnimation {
	public EaseInAnimation(long duration, TimeUnit timeUnit) {
		super(duration, timeUnit);
	}

	public EaseInAnimation(long duration, TimeUnit timeUnit, boolean startNow) {
		super(duration, timeUnit, startNow);
	}

	public EaseInAnimation(long duration, TimeUnit timeUnit, double startValue, double endValue) {
		super(duration, timeUnit, startValue, endValue);
	}

	public EaseInAnimation(long duration, TimeUnit timeUnit, boolean startNow, double startValue, double endValue) {
		super(duration, timeUnit, startNow, startValue, endValue);
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		EaseInAnimation cloneAnimation = new EaseInAnimation(duration, timeUnit, startValue, endValue);
		cloneAnimation.setStartTime(startTime);
		cloneAnimation.setEndTime(endTime);
		cloneAnimation.setDuration(duration);
		cloneAnimation.setStatus(status);
		
		return cloneAnimation;
	}
	
	@Override
	public double nextValue() {
		if(status == AnimationStatus.ANIMATING) {
			return startValue + (endValue - startValue) * (1 + PApplet.sin(this.nextFraction() * PApplet.HALF_PI - PApplet.HALF_PI));
		} else if(status == AnimationStatus.INITIALISED) {
			return startValue;
		} else {
			return endValue;
		}
	}
}
