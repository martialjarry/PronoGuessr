package com.drawrunner.uniguess.view.animation;

import java.util.concurrent.TimeUnit;

import processing.core.PApplet;

public class EaseInOutAnimation extends ValueAnimation {
	public EaseInOutAnimation(long duration, TimeUnit timeUnit) {
		super(duration, timeUnit);
	}

	public EaseInOutAnimation(long duration, TimeUnit timeUnit, boolean startNow) {
		super(duration, timeUnit, startNow);
	}

	public EaseInOutAnimation(long duration, TimeUnit timeUnit, double startValue, double endValue) {
		super(duration, timeUnit, startValue, endValue);
	}

	public EaseInOutAnimation(long duration, TimeUnit timeUnit, boolean startNow, double startValue, double endValue) {
		super(duration, timeUnit, startNow, startValue, endValue);
	}

	public Object clone() throws CloneNotSupportedException {
		EaseInOutAnimation cloneAnimation = new EaseInOutAnimation(duration, timeUnit, startValue, endValue);
		cloneAnimation.setStartTime(startTime);
		cloneAnimation.setEndTime(endTime);
		cloneAnimation.setDuration(duration);
		cloneAnimation.setStatus(status);
		
		return cloneAnimation;
	}
	
	@Override
	public double nextValue() {
		if(status == AnimationStatus.ANIMATING) {
			return startValue + (endValue - startValue) * ((1 + PApplet.sin(this.nextFraction() * PApplet.PI - PApplet.HALF_PI))/2);
		} else if(status == AnimationStatus.INITIALISED) {
			return this.startValue;
		} else {
			return this.endValue;
		}
	}
}