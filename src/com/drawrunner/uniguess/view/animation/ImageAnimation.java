package com.drawrunner.uniguess.view.animation;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import processing.core.PApplet;
import processing.core.PImage;

public class ImageAnimation extends Animation {
	private List<PImage> imageSequence;
	
	public ImageAnimation() {
		super();
		imageSequence = new LinkedList<PImage>();
	}

	public ImageAnimation(long duration, TimeUnit timeUnit) {
		super(duration, timeUnit);
		imageSequence = new LinkedList<PImage>();
	}

	public ImageAnimation(long duration, TimeUnit timeUnit, boolean startNow) {
		super(duration, timeUnit, startNow);
		imageSequence = new LinkedList<PImage>();
	}

	public ImageAnimation(long duration, TimeUnit timeUnit, LinkedList<PImage> imageSequence) {
		super(duration, timeUnit);
		
		this.imageSequence = new LinkedList<PImage>();
		for (PImage image : imageSequence) {
			this.imageSequence.add(image.copy());
		}
	}

	public ImageAnimation(long duration, TimeUnit timeUnit, boolean startNow, LinkedList<PImage> imageSequence) {
		super(duration, timeUnit, startNow);
		
		this.imageSequence = new LinkedList<PImage>();
		for (PImage image : imageSequence) {
			this.imageSequence.add(image.copy());
		}
	}
	
	public void stop() {
		super.stop();
	}
	
	public PImage nextImage() {
		if(!imageSequence.isEmpty()) {
			float nextFraction = this.nextFraction();
			int nextImageIndex = PApplet.floor(imageSequence.size() * nextFraction);
			
			if(this.status == AnimationStatus.ANIMATING) {
				return imageSequence.get(nextImageIndex);
			} else if(this.status == AnimationStatus.INITIALISED) {
				return imageSequence.get(0);
			} else {
				return imageSequence.get(imageSequence.size() - 1);
			}
		} else {
			return null;
		}
	}
}
