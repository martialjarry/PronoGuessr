package com.drawrunner.uniguess.view.animation;

import java.util.concurrent.TimeUnit;

public class LinearAnimation extends ValueAnimation {
	public LinearAnimation(long duration, TimeUnit timeUnit, boolean startNow, double startValue, double endValue) {
		super(duration, timeUnit, startNow, startValue, endValue);
	}

	public LinearAnimation(long duration, TimeUnit timeUnit, boolean startNow) {
		super(duration, timeUnit, startNow);
	}

	public LinearAnimation(long duration, TimeUnit timeUnit, double startValue, double endValue) {
		super(duration, timeUnit, startValue, endValue);
	}

	public LinearAnimation(long duration, TimeUnit timeUnit) {
		super(duration, timeUnit);
	}

	public Object clone() throws CloneNotSupportedException {
		LinearAnimation cloneAnimation = new LinearAnimation(duration, timeUnit, startValue, endValue);
		cloneAnimation.setStartTime(startTime);
		cloneAnimation.setEndTime(endTime);
		cloneAnimation.setDuration(duration);
		cloneAnimation.setStatus(status);
		
		return cloneAnimation;
	}
	
	@Override
	public double nextValue() {
		if(this.status == AnimationStatus.ANIMATING) {
			return startValue + (endValue - startValue) * this.nextFraction();
		} else if(status == AnimationStatus.INITIALISED) {
			return startValue;
		} else {
			return endValue;
		}
	}
}
