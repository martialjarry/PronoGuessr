package com.drawrunner.uniguess.view.animation;

import java.util.concurrent.TimeUnit;

import processing.core.PApplet;

public class SquareInAnimation extends ValueAnimation {
	public SquareInAnimation(long duration, TimeUnit timeUnit) {
		super(duration, timeUnit);
	}

	public SquareInAnimation(long duration, TimeUnit timeUnit, boolean startNow) {
		super(duration, timeUnit, startNow);
	}

	public SquareInAnimation(long duration, TimeUnit timeUnit, double startValue, double endValue) {
		super(duration, timeUnit, startValue, endValue);
	}

	public SquareInAnimation(long duration, TimeUnit timeUnit, boolean startNow, double startValue, double endValue) {
		super(duration, timeUnit, startNow, startValue, endValue);
	}

	public Object clone() throws CloneNotSupportedException {
		SquareInAnimation cloneAnimation = new SquareInAnimation(duration, timeUnit, startValue, endValue);
		cloneAnimation.setStartTime(startTime);
		cloneAnimation.setEndTime(endTime);
		cloneAnimation.setDuration(duration);
		cloneAnimation.setStatus(status);
		
		return cloneAnimation;
	}
	
	@Override
	public double nextValue() {
		if(status == AnimationStatus.ANIMATING) {
			return startValue + (endValue - startValue) * (-PApplet.sq(this.nextFraction() - 1) + 1);
		} else if(status == AnimationStatus.INITIALISED) {
			return startValue;
		} else {
			return endValue;
		}
	}
}
