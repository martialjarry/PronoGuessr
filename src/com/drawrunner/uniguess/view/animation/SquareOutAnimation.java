package com.drawrunner.uniguess.view.animation;

import java.util.concurrent.TimeUnit;

import processing.core.PApplet;

public class SquareOutAnimation extends ValueAnimation {
	public SquareOutAnimation(long duration, TimeUnit timeUnit) {
		super(duration, timeUnit);
	}

	public SquareOutAnimation(long duration, TimeUnit timeUnit, boolean startNow) {
		super(duration, timeUnit, startNow);
	}

	public SquareOutAnimation(long duration, TimeUnit timeUnit, double startValue, double endValue) {
		super(duration, timeUnit, startValue, endValue);
	}

	public SquareOutAnimation(long duration, TimeUnit timeUnit, boolean startNow, double startValue, double endValue) {
		super(duration, timeUnit, startNow, startValue, endValue);
	}

	public Object clone() throws CloneNotSupportedException {
		SquareOutAnimation cloneAnimation = new SquareOutAnimation(duration, timeUnit, startValue, endValue);
		cloneAnimation.setStartTime(startTime);
		cloneAnimation.setEndTime(endTime);
		cloneAnimation.setDuration(duration);
		cloneAnimation.setStatus(status);
		
		return cloneAnimation;
	}
	
	@Override
	public double nextValue() {
		if(status == AnimationStatus.ANIMATING) {
			return startValue + (endValue - startValue) * PApplet.sq(this.nextFraction());
		} else if(status == AnimationStatus.INITIALISED) {
			return startValue;
		} else {
			return endValue;
		}
	}
}
