package com.drawrunner.uniguess.view.animation;

import java.util.concurrent.TimeUnit;

public abstract class ValueAnimation extends Animation implements Cloneable {
	/** Valeur de d�but */
	protected double startValue;
	
	/** Valeur de fin */
	protected double endValue;
	
	public ValueAnimation() {
		super();
		
		startValue = 0;
		endValue = 0;
	}
	
	public ValueAnimation(long duration, TimeUnit timeUnit) {
		super(duration, timeUnit);
		
		startValue = 0;
		endValue = 0;
	}
	
	public ValueAnimation(long duration, TimeUnit timeUnit, boolean startNow) {
		super(duration, timeUnit, startNow);
		
		startValue = 0;
		endValue = 0;
	}
	
	public ValueAnimation(long duration, TimeUnit timeUnit, double startValue, double endValue) {
		super(duration, timeUnit);
		
		this.startValue = startValue;
		this.endValue = endValue;
	}
	
	public ValueAnimation(long duration, TimeUnit timeUnit, boolean startNow, double startValue, double endValue) {
		super(duration, timeUnit, startNow);
		
		this.startValue = startValue;
		this.endValue = endValue;
	}

	@Override
	public abstract Object clone() throws CloneNotSupportedException;
	
	public void start() {
		this.startTime = System.nanoTime();
		this.refreshEndTime();
		this.status = AnimationStatus.ANIMATING;
	}

	@Override
	public void stop() {
		super.stop();
		this.endValue = this.nextValue();
	}
	
	public float nextFraction() {
		long currentTime = System.nanoTime();
		
		float nextFraction = (currentTime - startTime) / ((endTime - startTime) * 1F);
		
		if(nextFraction > 1) {
			nextFraction = 1;
			status = AnimationStatus.FINISHED;
		}
		
		return nextFraction;
	}
	
	public void refreshEndTime() {
		switch (timeUnit) {
		case NANOSECONDS:
			endTime = startTime + duration;
			break;
		case MICROSECONDS:
			endTime = startTime + duration * 1000L;
			break;
		case MILLISECONDS:
			endTime = startTime + duration * 1000000L;
			break;
		case SECONDS:
			endTime = startTime + duration * 1000000000L;
			break;
		default:
			endTime = startTime + duration;
			break;
		}
	}
	
	/**
	 * Rtourne la valeur suivante dans l'animation
	 * @return : valeur suivante
	 */
	public abstract double nextValue();
	
	public double getStartValue() {
		return startValue;
	}
	
	public void setStartValue(double startValue) {
		this.startValue = startValue;
	}

	public double getEndValue() {
		return endValue;
	}
	
	public void setEndValue(double endValue) {
		this.endValue = endValue;
	}
}
