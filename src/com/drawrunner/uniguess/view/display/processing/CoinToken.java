package com.drawrunner.uniguess.view.display.processing;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.drawrunner.uniguess.view.animation.EaseInOutAnimation;
import com.drawrunner.uniguess.view.animation.EaseOutAnimation;
import com.drawrunner.uniguess.view.animation.LinearAnimation;
import com.drawrunner.uniguess.view.animation.ValueAnimation;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;

public class CoinToken extends TrayEntity {
	public static final int DAFAULT_SIZE = PlayerToken.DEFAULT_SIZE / 2;
	
	public static final long CARD_TRANSIT_ANIMATION_DURATION = 800;
	public static final long TARGET_TRANSITION_ANIMATION_DURATION = 600;
	public static final long CURSED_PANIAC_TRANSIT_ANIMATION_DURATION = 700;

	private enum CoinTokenStates {
		WAITING,
		TRANSITING_TO_CARD, ARRIVED_AT_CARD,
		TRANSITING_TO_CURSED_PANIAC, ARRIVED_AT_CURSED_PANIAC
	}
	
	private CoinTokenStates coinTokenState;
	
	private int pronoIndex;
	
	private PlayerToken originPlayerToken;
	private DistrictCard targetDistrictCard;
	private CursedPaniac targetCursedPaniac;
	
	private ValueAnimation posXToCardAnimation;
	private ValueAnimation posYToCardAnimation;
	
	private ValueAnimation targetRatioAnimation;
	
	private ValueAnimation posXToCursedPaniacAnimation;
	private ValueAnimation posYToCursedPaniacAnimation;
	
	private float opacity;
	private ValueAnimation opacityAnimation;
	
	private PVector previousPosition;
	private PVector speed;
	
	private List<CoinTriangle> coinTriangles;

	public CoinToken(PGraphics coinBackground, PImage shadow) {
		coinTokenState = CoinTokenStates.WAITING;
		
		pronoIndex = 0;
		
		isVisible = false;

		this.background = coinBackground;
		this.shadow = shadow;
		
		position = new PVector(-DAFAULT_SIZE, -DAFAULT_SIZE);
		dimension = new PVector(DAFAULT_SIZE, DAFAULT_SIZE);
		
		posXToCardAnimation = new EaseInOutAnimation(CARD_TRANSIT_ANIMATION_DURATION, TimeUnit.MILLISECONDS);
		posYToCardAnimation = new EaseInOutAnimation(CARD_TRANSIT_ANIMATION_DURATION, TimeUnit.MILLISECONDS);
		
		targetRatioAnimation = new EaseOutAnimation(TARGET_TRANSITION_ANIMATION_DURATION, TimeUnit.MILLISECONDS, 0, 1);
		
		posXToCursedPaniacAnimation = new EaseOutAnimation(CURSED_PANIAC_TRANSIT_ANIMATION_DURATION, TimeUnit.MILLISECONDS);
		posYToCursedPaniacAnimation = new EaseOutAnimation(CURSED_PANIAC_TRANSIT_ANIMATION_DURATION, TimeUnit.MILLISECONDS);
		
		originPlayerToken = null;
		targetDistrictCard = null;
		targetCursedPaniac = null;
		
		opacity = 255;
		opacityAnimation = new LinearAnimation(30, TimeUnit.MILLISECONDS, 255, 0);
		
		previousPosition = new PVector(position.x, position.y);
		speed = new PVector(0, 0);
		
		coinTriangles = new ArrayList<CoinTriangle>();
	}
	
	public void transitToDistrict(PlayerToken originPlayerToken, DistrictCard targetDistrictCard, float startDelayMillis) {
		long startTime = (long) (System.nanoTime() + startDelayMillis * 1000000L);
		
		float startPosX = originPlayerToken.getPosition().x + originPlayerToken.getDimension().x * 0.5f - DAFAULT_SIZE * 0.5f;
		float startPosY = originPlayerToken.getPosition().y + originPlayerToken.getDimension().y * 0.5f - DAFAULT_SIZE * 0.5f;
		
		float endPosX = targetDistrictCard.getPosition().x + targetDistrictCard.getDimension().x * 0.5f - DAFAULT_SIZE * 0.5f;
		float endPosY = targetDistrictCard.getPosition().y + targetDistrictCard.getOverlapShift() + targetDistrictCard.getDimension().y * 0.5f - DAFAULT_SIZE * 0.5f;
		
		posXToCardAnimation.setStartValue(startPosX);
		posYToCardAnimation.setStartValue(startPosY);
		
		posXToCardAnimation.setEndValue(endPosX);
		posYToCardAnimation.setEndValue(endPosY);
		
		posXToCardAnimation.setStartTime(startTime);
		posYToCardAnimation.setStartTime(startTime);
		
		this.originPlayerToken = originPlayerToken;
		this.targetDistrictCard = targetDistrictCard;
		
		previousPosition = position.copy();
		
		coinTokenState = CoinTokenStates.WAITING;
	}
	
	public void feedCursedPaniac(PApplet p, CursedPaniac targetCursedPaniac) {
		this.splitInTriangles();
		
		posXToCursedPaniacAnimation.setStartValue(position.x);
		posYToCursedPaniacAnimation.setStartValue(position.y);
		
		PVector cursedPaniacMouthAnchor = targetCursedPaniac.mouthAnchor();
		posXToCursedPaniacAnimation.setEndValue(cursedPaniacMouthAnchor.x);
		posYToCursedPaniacAnimation.setEndValue(cursedPaniacMouthAnchor.y);
		
		for (CoinTriangle triangle : coinTriangles) {
			triangle.fadeIn(p);
			
			triangle.setPosXToCardAnimation(posXToCardAnimation);
			triangle.setPosYToCardAnimation(posYToCardAnimation);
			
			triangle.setTargetRatioAnimation(targetRatioAnimation);
			
			triangle.setPosXToCursedPaniacAnimation(posXToCursedPaniacAnimation);
			triangle.setPosYToCursedPaniacAnimation(posYToCursedPaniacAnimation);
		}
		
		targetRatioAnimation.start();
		
		posXToCursedPaniacAnimation.start();
		posYToCursedPaniacAnimation.start();
		
		opacityAnimation.start();
		
		this.targetCursedPaniac = targetCursedPaniac;
		
		this.addObserver(targetCursedPaniac);
		this.notifySwitchingToCursedPaniac();
		
		coinTokenState = CoinTokenStates.TRANSITING_TO_CURSED_PANIAC;
	}
	
	private void splitInTriangles() {
		coinTriangles.clear();
		
		PVector vertexA = new PVector(position.x, position.y);
		PVector vertexB = new PVector(position.x + dimension.x, position.y);
		PVector vertexC = new PVector(position.x + dimension.x, position.y + dimension.y);
		PVector vertexD = new PVector(position.x, position.y + dimension.y);
		
		CoinTriangle triangle1 = new CoinTriangle(this, vertexA, vertexB, vertexC);
		triangle1.setBackground(background);
		
		CoinTriangle triangle2 = new CoinTriangle(this, vertexA, vertexC, vertexD);
		triangle2.setBackground(background);
		
		coinTriangles.add(triangle1);
		coinTriangles.add(triangle2);
		
		for(int i = 0; i < 3; i++) {
			List<CoinTriangle> trianglesToAdd = new ArrayList<CoinTriangle>();
			List<CoinTriangle> trianglesToRemove = new ArrayList<CoinTriangle>();

			for (CoinTriangle currentTriangle : coinTriangles) {
				CoinTriangle[] subTriangles = currentTriangle.split();
				
				if(subTriangles.length >= 2 && subTriangles[0] != null && subTriangles[1] != null) {
					trianglesToAdd.add(subTriangles[0]);
					trianglesToAdd.add(subTriangles[1]);
					
					trianglesToRemove.add(currentTriangle);
				}
			}
			
			coinTriangles.addAll(trianglesToAdd);
			coinTriangles.removeAll(trianglesToRemove);
		}
	}
	
	@Override
	public void animate(PApplet p) {	
		float currentPosX = position.x;
		float currentPosY = position.y;
		
		speed = PVector.sub(position, previousPosition);
		previousPosition = position.copy();
		
		switch (coinTokenState) {
		case WAITING:
			if(System.nanoTime() >= posXToCardAnimation.getStartTime() && System.nanoTime() >= posYToCardAnimation.getStartTime()) {
				PVector playerTokenPosition = originPlayerToken.getPosition();
				
				posXToCardAnimation.setStartValue(playerTokenPosition.x);
				posYToCardAnimation.setStartValue(playerTokenPosition.y);
				
				posXToCardAnimation.start();
				posYToCardAnimation.start();
				
				currentPosX = (float) posXToCardAnimation.nextValue();
				currentPosY = (float) posYToCardAnimation.nextValue();
				
				previousPosition = new PVector(currentPosX, currentPosY);
				position = new PVector(currentPosX, currentPosY);

				isVisible = true;
				coinTokenState = CoinTokenStates.TRANSITING_TO_CARD;
			}
			break;
		case TRANSITING_TO_CARD:
			posXToCardAnimation.setEndValue(targetDistrictCard.getPosition().x + targetDistrictCard.getDimension().x * 0.5f - DAFAULT_SIZE * 0.5f);
			posYToCardAnimation.setEndValue(targetDistrictCard.getPosition().y + targetDistrictCard.getOverlapShift() + targetDistrictCard.getDimension().y * 0.5f - DAFAULT_SIZE * 0.5f);
			
			currentPosX = (float) posXToCardAnimation.nextValue();
			currentPosY = (float) posYToCardAnimation.nextValue();
			
			position = new PVector(currentPosX, currentPosY);
			
			if(posYToCardAnimation.isFinished() && posYToCardAnimation.isFinished()) {
				this.notifyReachedDistrict();
				isVisible = false;
				coinTokenState = CoinTokenStates.ARRIVED_AT_CARD;
			}
			break;
		case TRANSITING_TO_CURSED_PANIAC:
			opacity = (float) opacityAnimation.nextValue();
			
			float toCardPosX = (float) posXToCardAnimation.nextValue();
			float toCardPosY = (float) posYToCardAnimation.nextValue();
			
			float targetRatio = (float) targetRatioAnimation.nextValue();
			
			if(targetRatio < 1) {
				posXToCursedPaniacAnimation.setStartValue(toCardPosX);
				posYToCursedPaniacAnimation.setStartValue(toCardPosY);
			}
			
			PVector cursedPaniacMouthAnchor = targetCursedPaniac.mouthAnchor();
			posXToCursedPaniacAnimation.setEndValue(cursedPaniacMouthAnchor.x);
			posYToCursedPaniacAnimation.setEndValue(cursedPaniacMouthAnchor.y);
			
			float toCursedPaniacPosX = (float) posXToCursedPaniacAnimation.nextValue();
			float toCursedPaniacPosY = (float) posYToCursedPaniacAnimation.nextValue();
			
			currentPosX = toCardPosX + (toCursedPaniacPosX - toCardPosX) * targetRatio;
			currentPosY = toCardPosY + (toCursedPaniacPosY - toCardPosY) * targetRatio;
			
			position = new PVector(currentPosX, currentPosY);
			
			PVector direction = speed.copy().normalize();
			PVector center = PVector.add(position, PVector.mult(dimension, 0.5f));
			float radius = PVector.mult(dimension, 0.5f).mag(); 
					
			PVector forwardPosition = PVector.add(center, PVector.mult(direction, radius));
			
			boolean areTrianglesDissolved = true;
			
			for (CoinTriangle triangle : coinTriangles) {
				PVector trianglePosition = triangle.getPosition();
				PVector triangleDimension = triangle.getDimension();
				
				PVector triangleCenter = PVector.add(trianglePosition, PVector.mult(triangleDimension, 0.5f));
				
				float triangleForwardDistance = PVector.dist(triangleCenter, forwardPosition);
				PVector triangleSpeedShift = PVector.mult(direction, (radius - triangleForwardDistance) * 0.08f);
				
				triangle.setSpeedShift(PVector.add(triangle.getSpeedShift(), triangleSpeedShift));

				triangle.animate(p);
				
				if(areTrianglesDissolved && !triangle.isDissolved()) {
					areTrianglesDissolved = false;
				}
			}
			
			if(areTrianglesDissolved) {
				isVisible = false;
				this.notifyReachedCursedPaniac();
				coinTokenState = CoinTokenStates.ARRIVED_AT_CURSED_PANIAC;
			}
			
			break;
		}
	}

	public PVector center() {
		return PVector.add(position, PVector.mult(dimension, 0.5f));
	}
	
	private void notifyReachedDistrict() {
		this.setChanged();
		
		Object[] coinContextData = this.buildContextData(CoinTokenActions.REACHED_DISTRICT);
		this.notifyObservers(coinContextData);
	}
	
	private void notifySwitchingToCursedPaniac() {
		this.setChanged();
		
		Object[] coinContextData = this.buildContextData(CoinTokenActions.SWITCHING_TO_CURSED_PANIAC);
		this.notifyObservers(coinContextData);
	}
	
	private void notifyReachedCursedPaniac() {
		this.setChanged();
		
		Object[] coinContextData = this.buildContextData(CoinTokenActions.REACHED_CURSED_PANIAC);
		this.notifyObservers(coinContextData);
	}
	
	private Object[] buildContextData(CoinTokenActions coinTokenAction) {
		return  new Object[] {
			coinTokenAction,
			originPlayerToken.getPlayerName(),
			targetDistrictCard.getDistrictName(),
			pronoIndex
		};
	}
	
	@Override
	public void draw(PApplet p) {
		if(isVisible) {
			this.drawShadow(p, opacity);
			
			float deformationFactor = PApplet.min(PApplet.max(1, speed.mag() * 0.065f), 1.8f);
			
			p.imageMode(PApplet.CORNER);
			p.tint(255, opacity);
			
			p.translate(position.x + dimension.x * 0.5f, position.y + dimension.y * 0.5f);
			p.rotate(PApplet.atan2(speed.y, speed.x));
			p.scale(deformationFactor, 1);
			p.rotate(-PApplet.atan2(speed.y, speed.x));
			
			p.image(background, -dimension.x * 0.5f, -dimension.y * 0.5f, dimension.x, dimension.y);
			p.noTint();
			
			for (CoinTriangle triangle : coinTriangles) {
				triangle.draw(p);
			}
			
			p.resetMatrix();
		}
	}
	
	public boolean isArrivedAtTarget() {
		return coinTokenState == CoinTokenStates.ARRIVED_AT_CARD
			|| coinTokenState == CoinTokenStates.ARRIVED_AT_CURSED_PANIAC;
	}
	
	public boolean isFeedingCursedPaniac() {
		return coinTokenState == CoinTokenStates.TRANSITING_TO_CURSED_PANIAC
			|| coinTokenState == CoinTokenStates.ARRIVED_AT_CURSED_PANIAC; 
	}

	public int getPronoIndex() {
		return pronoIndex;
	}

	public void setPronoIndex(int pronoIndex) {
		this.pronoIndex = pronoIndex;
	}
	
	public PVector getSpeed() {
		return speed;
	}
}
