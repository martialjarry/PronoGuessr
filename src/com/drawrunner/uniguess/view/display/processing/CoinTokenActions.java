package com.drawrunner.uniguess.view.display.processing;

public enum CoinTokenActions {
	REACHED_DISTRICT,
	SWITCHING_TO_CURSED_PANIAC,
	REACHED_CURSED_PANIAC
}
