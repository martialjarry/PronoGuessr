package com.drawrunner.uniguess.view.display.processing;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;

public class CoinTokenFactory {
	private PGraphics coinToken1Background;
	private PGraphics coinToken3Background;
	private PGraphics coinToken5Background;
	private PGraphics coinToken10Background;
	private PGraphics coinToken15Background;
	private PGraphics coinToken25Background;
	
	private PImage coinTokenShadow;
	
	private CoinTokenFactory() {
		coinToken1Background = null;
		coinToken3Background = null;
		coinToken5Background = null;
		coinToken10Background = null;
		coinToken15Background = null;
		coinToken25Background = null;
		
		coinTokenShadow = null;
	}
	
	public static CoinTokenFactory getInstance() {
		return CoinTokenFactoryHolder.instance;
	}
	
	public CoinToken createCoinToken1(PApplet p) {
		if(coinToken1Background == null) {
			PImage coinTokenImage = p.loadImage("media/coins/coin_1.png");
			coinToken1Background = p.createGraphics(coinTokenImage.width, coinTokenImage.height);
			this.fillBackground(p, coinTokenImage, coinToken1Background);
		}
		
		return this.createCoinToken(p, coinToken1Background, 0.4f);
	}
	
	public CoinToken createCoinToken3(PApplet p) {
		if(coinToken3Background == null) {
			PImage coinTokenImage = p.loadImage("media/coins/coin_3.png");
			coinToken3Background = p.createGraphics(coinTokenImage.width, coinTokenImage.height);
			this.fillBackground(p, coinTokenImage, coinToken3Background);
		}
		
		return this.createCoinToken(p, coinToken3Background, 0.43f);
	}
	
	public CoinToken createCoinToken5(PApplet p) {
		if(coinToken5Background == null) {
			PImage coinTokenImage = p.loadImage("media/coins/coin_5.png");
			coinToken5Background = p.createGraphics(coinTokenImage.width, coinTokenImage.height);
			this.fillBackground(p, coinTokenImage, coinToken5Background);
		}
		
		return this.createCoinToken(p, coinToken5Background, 0.45f);
	}
	
	public CoinToken createCoinToken10(PApplet p) {
		if(coinToken10Background == null) {
			PImage coinTokenImage = p.loadImage("media/coins/coin_10.png");
			coinToken10Background = p.createGraphics(coinTokenImage.width, coinTokenImage.height);
			this.fillBackground(p, coinTokenImage, coinToken10Background);
		}
		
		return this.createCoinToken(p, coinToken10Background, 0.5f);
	}
	
	public CoinToken createCoinToken15(PApplet p) {
		if(coinToken15Background == null) {
			PImage coinTokenImage = p.loadImage("media/coins/coin_15.png");
			coinToken15Background = p.createGraphics(coinTokenImage.width, coinTokenImage.height);
			this.fillBackground(p, coinTokenImage, coinToken15Background);
		}
		
		return this.createCoinToken(p, coinToken15Background, 0.55f);
	}
	
	public CoinToken createCoinToken25(PApplet p) {
		if(coinToken25Background == null) {
			PImage coinTokenImage = p.loadImage("media/coins/coin_25.png");
			coinToken25Background = p.createGraphics(coinTokenImage.width, coinTokenImage.height);
			this.fillBackground(p, coinTokenImage, coinToken25Background);
		}
		
		return this.createCoinToken(p, coinToken25Background, 0.65f);
	}
	
	private CoinToken createCoinToken(PApplet p, PGraphics background, float tokeSize) {
		this.loadShadowIfNeeded(p);
		
		float tokenSize = PlayerToken.DEFAULT_SIZE * tokeSize;
		
		CoinToken coinToken = new CoinToken(background, coinTokenShadow);
		coinToken.setDimension(new PVector(tokenSize, tokenSize));
		
		return coinToken;
	}
	
	private void fillBackground(PApplet p, PImage coinTokenImage, PGraphics background) {
		background.beginDraw();
		background.image(coinTokenImage, 0, 0, background.width, background.height);
		background.endDraw();
	}
	
	private void loadShadowIfNeeded(PApplet p) {
		if(coinTokenShadow == null) {
			coinTokenShadow = p.loadImage("media/shadows/player_token_shadow.png");
		}
	}
	
	private static class CoinTokenFactoryHolder {
		private final static CoinTokenFactory instance = new CoinTokenFactory(); 
	}
}
