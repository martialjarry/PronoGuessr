package com.drawrunner.uniguess.view.display.processing;

import java.awt.Color;
import java.util.concurrent.TimeUnit;

import com.drawrunner.uniguess.view.animation.EaseInAnimation;
import com.drawrunner.uniguess.view.animation.EaseInOutAnimation;
import com.drawrunner.uniguess.view.animation.EaseOutAnimation;
import com.drawrunner.uniguess.view.animation.LinearAnimation;
import com.drawrunner.uniguess.view.animation.ValueAnimation;

import processing.core.PApplet;
import processing.core.PShape;
import processing.core.PVector;

public class CoinTriangle extends TrayEntity {
	private enum FeedingStates { APPEARING, FRAGMENTING_EXPANSING, FRAGMENTING_SHRINKING, DISSOLVING, DISSOLVED }

	private static final float DISSOLVING_DURATION = 160; 
	
	private CoinToken parentCoin;
	
	private PVector vertexA;
	private PVector vertexB;
	private PVector vertexC;
	
//	private PVector vertexADelta;
//	private PVector vertexBDelta;
//	private PVector vertexCDelta;
	
	private FeedingStates feedingState;
	
	private float opacity;
	private ValueAnimation opacityAnimation;
	
	private PVector localPosition;
	private PVector localCenterPosition;
	
	private PVector speedShift;
	
	private ValueAnimation rotationAnimation;
	private ValueAnimation scaleAnimation;
	
	private ValueAnimation expansionFactorAnimation;
	
//	private ValueAnimation ejectionXAnimation;
//	private ValueAnimation ejectionYAnimation;
	
	private ValueAnimation posXToCardAnimation;
	private ValueAnimation posYToCardAnimation;
	
	private ValueAnimation targetRatioAnimation;
	
	private ValueAnimation posXToCursedPaniacAnimation;
	private ValueAnimation posYToCursedPaniacAnimation;
	
	private PShape fragmentShape;
	
	public CoinTriangle(CoinToken parentCoin, PVector vertexA, PVector vertexB, PVector vertexC) {
		this.parentCoin = parentCoin;
		
		this.vertexA = vertexA;
		this.vertexB = vertexB;
		this.vertexC = vertexC;
		
		position = this.topLeftMinPoint();
		dimension = PVector.sub(this.bottomRightMinPoint(), position);
		
		PVector parentCenter = parentCoin.center();
		PVector center = PVector.add(position, PVector.mult(dimension, 0.5f));
		
//		vertexADelta = PVector.sub(vertexA, centerPoint);
//		vertexBDelta = PVector.sub(vertexB, centerPoint);
//		vertexCDelta = PVector.sub(vertexC, centerPoint);
		
		feedingState = FeedingStates.APPEARING;
		
		opacity = 0;
		opacityAnimation = new LinearAnimation(30, TimeUnit.MILLISECONDS, 0, 255);
		
		localPosition = PVector.sub(position, parentCoin.getPosition());
		localCenterPosition = PVector.sub(center, parentCenter);
		
		speedShift = new PVector(0, 0);

//		ejectionXAnimation = new EaseOutAnimation(500, TimeUnit.MILLISECONDS);
//		ejectionYAnimation = new EaseOutAnimation(500, TimeUnit.MILLISECONDS);
		
		posXToCardAnimation = new EaseInOutAnimation(CoinToken.CARD_TRANSIT_ANIMATION_DURATION, TimeUnit.MILLISECONDS);
		posYToCardAnimation = new EaseInOutAnimation(CoinToken.CARD_TRANSIT_ANIMATION_DURATION, TimeUnit.MILLISECONDS);
		
		targetRatioAnimation = new EaseOutAnimation(CoinToken.TARGET_TRANSITION_ANIMATION_DURATION, TimeUnit.MILLISECONDS, 0, 1);
		
		posXToCursedPaniacAnimation = new EaseOutAnimation(CoinToken.CURSED_PANIAC_TRANSIT_ANIMATION_DURATION, TimeUnit.MILLISECONDS);
		posYToCursedPaniacAnimation = new EaseOutAnimation(CoinToken.CURSED_PANIAC_TRANSIT_ANIMATION_DURATION, TimeUnit.MILLISECONDS);
		
		expansionFactorAnimation = new EaseInAnimation((long) (posXToCursedPaniacAnimation.getDuration() * 0.4f), TimeUnit.MILLISECONDS, 0, 1);
		
		double finalRotation = Math.random() * 50 - 25;
		rotationAnimation = new EaseInAnimation(posXToCursedPaniacAnimation.getDuration(), TimeUnit.MILLISECONDS, 0, finalRotation);
		
		double finalScale = 1 - (0.2f + Math.random() * 0.3f);
		scaleAnimation = new EaseInAnimation(posXToCursedPaniacAnimation.getDuration(), TimeUnit.MILLISECONDS, 1, finalScale);
	}

	public void fadeIn(PApplet p) {
		opacityAnimation.start();
		
		PVector parentPosition = parentCoin.getPosition();
		PVector parentDimension = parentCoin.getDimension();
		
		PVector parentBasedVertexAPosition = PVector.sub(vertexA, parentPosition);
		PVector parentBasedVertexBPosition = PVector.sub(vertexB, parentPosition);
		PVector parentBasedVertexCPosition = PVector.sub(vertexC, parentPosition);
		
		PVector localVertexAPosition = PVector.sub(vertexA, position);
		PVector localVertexBPosition = PVector.sub(vertexB, position);
		PVector localVertexCPosition = PVector.sub(vertexC, position);
		
		float vertexATexturePosX = parentBasedVertexAPosition.x / parentDimension.x * background.width;
		float vertexATexturePosY = parentBasedVertexAPosition.y / parentDimension.y * background.height;
		
		float vertexBTexturePosX = parentBasedVertexBPosition.x / parentDimension.x * background.width;
		float vertexBTexturePosY = parentBasedVertexBPosition.y / parentDimension.y * background.height;
		
		float vertexCTexturePosX = parentBasedVertexCPosition.x / parentDimension.x * background.width;
		float vertexCTexturePosY = parentBasedVertexCPosition.y / parentDimension.y * background.height;
		
		fragmentShape = p.createShape();
		fragmentShape.beginShape();
		
		fragmentShape.noStroke();
		fragmentShape.texture(background);
		
		fragmentShape.vertex(localVertexAPosition.x, localVertexAPosition.y, vertexATexturePosX, vertexATexturePosY);
		fragmentShape.vertex(localVertexBPosition.x, localVertexBPosition.y, vertexBTexturePosX, vertexBTexturePosY);
		fragmentShape.vertex(localVertexCPosition.x, localVertexCPosition.y, vertexCTexturePosX, vertexCTexturePosY);
		
		fragmentShape.endShape();
		
		feedingState = FeedingStates.APPEARING;
	}
	
	public void fragmente() {
//		float ejectionDistance = (float) (20 + Math.random() * 10);
//		long ejectionDuration = (long) (400 + Math.random() * 200);
		
//		PVector startEjctionPosition = this.centerPoint();
//		PVector finalEjectionPosition = PVector.add(startEjctionPosition, PVector.mult(ejectionDirection, ejectionDistance));
		
//		ejectionXAnimation.setStartValue(startEjctionPosition.x);
//		ejectionYAnimation.setStartValue(startEjctionPosition.y);
//		
//		ejectionXAnimation.setEndValue(finalEjectionPosition.x);
//		ejectionYAnimation.setEndValue(finalEjectionPosition.y);
//		
//		ejectionXAnimation.setDuration(ejectionDuration);
//		ejectionYAnimation.setDuration(ejectionDuration);
//		
//		ejectionXAnimation.start();
//		ejectionYAnimation.start();
		
		expansionFactorAnimation.start();
		
		rotationAnimation.start();
		scaleAnimation.start();
		
		feedingState = FeedingStates.FRAGMENTING_EXPANSING;
	}
	
	public void dissolve() {
		opacityAnimation.setStartValue(opacity);
		opacityAnimation.setEndValue(0);
		opacityAnimation.setDuration((long) DISSOLVING_DURATION);
		
		opacityAnimation.start();
		feedingState = FeedingStates.DISSOLVING;
	}
	
	@Override
	public void animate(PApplet p) {
		long remainingDuration = 0;
		
		switch (feedingState) {
		case APPEARING:
			opacity = (float) opacityAnimation.nextValue();
			
			this.updatePosition();
			
			if(opacityAnimation.isFinished()) {
				opacity = 255;
				this.fragmente();
			}
			break;
		case FRAGMENTING_EXPANSING:
			this.updatePosition();
			
			remainingDuration = (posXToCursedPaniacAnimation.getEndTime() - System.nanoTime()) / 1000000L;
			
			if(remainingDuration <= DISSOLVING_DURATION + 100) {
				this.dissolve();
			}
			
			if(expansionFactorAnimation.isFinished()) {
				expansionFactorAnimation = new EaseOutAnimation((long) (posXToCursedPaniacAnimation.getDuration() * 0.6f), TimeUnit.MILLISECONDS, 1, 0);
				expansionFactorAnimation.start();
				feedingState = FeedingStates.FRAGMENTING_SHRINKING;
			}
			break;
		case FRAGMENTING_SHRINKING:
			this.updatePosition();
			
			remainingDuration = (posXToCursedPaniacAnimation.getEndTime() - System.nanoTime()) / 1000000L;
			
			if(remainingDuration <= DISSOLVING_DURATION + 100) {
				this.dissolve();
			}
			break;
		case DISSOLVING:
			this.updatePosition();
			opacity = (float) opacityAnimation.nextValue();
			
			if(opacityAnimation.isFinished()) {
				isVisible = false;
				feedingState = FeedingStates.DISSOLVED;
			}
			
			break;
		}
	}

	private void updatePosition() {
		try {
			ValueAnimation clonePosXToCardAnimation = (ValueAnimation) posXToCardAnimation.clone();
			ValueAnimation clonePosYToCardAnimation = (ValueAnimation) posYToCardAnimation.clone();
			
			clonePosXToCardAnimation.setStartValue(posXToCardAnimation.getStartValue() + localPosition.x);
			clonePosYToCardAnimation.setStartValue(posYToCardAnimation.getStartValue() + localPosition.y);
			
			clonePosXToCardAnimation.setEndValue(posXToCardAnimation.getEndValue() + localPosition.x);
			clonePosYToCardAnimation.setEndValue(posYToCardAnimation.getEndValue() + localPosition.y);
			
			ValueAnimation clonePosXToCursedPaniacAnimation = (ValueAnimation) posXToCursedPaniacAnimation.clone();
			ValueAnimation clonePosYToCursedPaniacAnimation = (ValueAnimation) posYToCursedPaniacAnimation.clone();
		
			clonePosXToCursedPaniacAnimation.setStartValue(posXToCursedPaniacAnimation.getStartValue() + localPosition.x);
			clonePosYToCursedPaniacAnimation.setStartValue(posYToCursedPaniacAnimation.getStartValue() + localPosition.y);
			
			PVector expansionShift = new PVector(0, 0);
			
			if(feedingState == FeedingStates.FRAGMENTING_EXPANSING
			|| feedingState == FeedingStates.FRAGMENTING_SHRINKING
			|| feedingState == FeedingStates.DISSOLVING) {
				float speedShiftLength = speedShift.mag();
				long newDuration = (long) PApplet.max(0, posXToCursedPaniacAnimation.getDuration() - (100 * (speedShiftLength * 0.1f)));
				
				clonePosXToCursedPaniacAnimation.setDuration(newDuration);
				clonePosXToCursedPaniacAnimation.refreshEndTime();
				
				clonePosYToCursedPaniacAnimation.setDuration(newDuration);
				clonePosYToCursedPaniacAnimation.refreshEndTime();
				
				PVector expansionDirection = localCenterPosition.copy().normalize();
				float expansionFactor = (float) expansionFactorAnimation.nextValue();
				
				expansionShift = PVector.add(PVector.mult(expansionDirection, expansionFactor * 10), PVector.mult(speedShift, 1));
			}
			
			PVector parentPosition = parentCoin.getPosition();
			PVector parentDimension = parentCoin.getDimension();
			
			PVector parentCenter = PVector.add(parentPosition, PVector.mult(parentDimension, 0.5f));
			
			clonePosXToCursedPaniacAnimation.setEndValue(parentCenter.x);
			clonePosYToCursedPaniacAnimation.setEndValue(parentCenter.y);
			
			float toCardPosX = (float) clonePosXToCardAnimation.nextValue();
			float toCardPosY = (float) clonePosYToCardAnimation.nextValue();
			
			float targetRatio = (float) targetRatioAnimation.nextValue();
			
			float toCursedPaniacPosX = (float) clonePosXToCursedPaniacAnimation.nextValue();
			float toCursedPaniacPosY = (float) clonePosYToCursedPaniacAnimation.nextValue();
			
			float currentPosX = toCardPosX + (toCursedPaniacPosX - toCardPosX) * targetRatio + expansionShift.x/* + speedShift.x*/;
			float currentPosY = toCardPosY + (toCursedPaniacPosY - toCardPosY) * targetRatio + expansionShift.y/* + speedShift.y*/;
			
			position = new PVector(currentPosX, currentPosY);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void draw(PApplet p) {
		PVector parentPosition = parentCoin.getPosition();
		PVector parentDimension = parentCoin.getDimension();
		
//		PVector centerPoint = this.centerPoint();
		
//		PVector vertexANormal = PVector.sub(vertexA, centerPoint).normalize();
//		PVector vertexBNormal = PVector.sub(vertexB, centerPoint).normalize();
//		PVector vertexCNormal = PVector.sub(vertexC, centerPoint).normalize();
		
//		p.fill(0, 32);
//		p.noStroke();
//		for (int i = 0; i < 6; i++) {
//			PVector shadowVertexAPosition = PVector.add(vertexA, PVector.mult(vertexANormal, i));
//			PVector shadowVertexBPosition = PVector.add(vertexB, PVector.mult(vertexBNormal, i));
//			PVector shadowVertexCPosition = PVector.add(vertexC, PVector.mult(vertexCNormal, i));
//			
//			p.triangle(shadowVertexAPosition.x, shadowVertexAPosition.y,
//					shadowVertexBPosition.x, shadowVertexBPosition.y,
//					shadowVertexCPosition.x, shadowVertexCPosition.y);
//		}
		
//		p.fill(255);
//		p.noStroke();
//		p.rect(position.x, position.y, 15, 15);
		
		float compensatedPosX = position.x - parentPosition.x - parentDimension.x * 0.5f;
		float compensatedPosY = position.y - parentPosition.y - parentDimension.y * 0.5f;
		
		fragmentShape.setTint(new Color(1, 1, 1, opacity / 256f).getRGB());
		
		float currentAngle = PApplet.radians((float) rotationAnimation.nextValue());
		float currentScale = (float) scaleAnimation.nextValue();
		
		p.pushMatrix();
		p.translate(compensatedPosX + dimension.x * 0.5f, compensatedPosY + dimension.y * 0.5f);
		p.rotate(currentAngle);
		p.scale(currentScale);
		p.shape(fragmentShape, -dimension.x * 0.5f, -dimension.y * 0.5f);
		p.popMatrix();
	}
	
//	public PVector centerPoint() {
//		return PVector.div(PVector.add(vertexA, PVector.add(vertexB, vertexC)), 3);
//	}

	public PVector topLeftMinPoint() {
		float minTopValue = Float.POSITIVE_INFINITY;
		float minLeftValue = Float.POSITIVE_INFINITY;
		
		PVector[] vertices = new PVector[] { vertexA, vertexB, vertexC };
		
		for (PVector vertex : vertices) {
			if(vertex.y < minTopValue) {
				minTopValue = vertex.y;
			}
			
			if(vertex.x < minLeftValue) {
				minLeftValue = vertex.x;
			}
		}
		
		return new PVector(minLeftValue, minTopValue);
	}
	
	public PVector bottomRightMinPoint() {
		float maxBottomValue = Float.NEGATIVE_INFINITY;
		float maxRightValue = Float.NEGATIVE_INFINITY;
		
		PVector[] vertices = new PVector[] { vertexA, vertexB, vertexC };
		
		for (PVector vertex : vertices) {
			if(vertex.y > maxBottomValue) {
				maxBottomValue = vertex.y;
			}
			
			if(vertex.x > maxRightValue) {
				maxRightValue = vertex.x;
			}
		}
		
		return new PVector(maxRightValue, maxBottomValue);
	}
	
	public CoinTriangle[] split() {
		float locationRatio = (float) (0.25 + (Math.random() * 0.5));
		
		float lengthAB = PVector.dist(vertexA, vertexB);
		float lengthBC = PVector.dist(vertexB, vertexC);
		float lengthCA = PVector.dist(vertexC, vertexA);
		
		CoinTriangle triangle1 = null;
		CoinTriangle triangle2 = null;
		
		if(lengthAB > lengthBC && lengthAB > lengthCA) {
			PVector medianPoint = PVector.add(vertexA, PVector.mult(PVector.sub(vertexB, vertexA), locationRatio));
			
			triangle1 = new CoinTriangle(parentCoin, vertexA, medianPoint, vertexC);
			triangle2 = new CoinTriangle(parentCoin, vertexB, vertexC, medianPoint);
		} else if(lengthBC > lengthAB && lengthBC > lengthCA) {
			PVector medianPoint = PVector.add(vertexB, PVector.mult(PVector.sub(vertexC, vertexB), locationRatio));
			
			triangle1 = new CoinTriangle(parentCoin, vertexA, vertexB, medianPoint);
			triangle2 = new CoinTriangle(parentCoin, vertexA, medianPoint, vertexC);
		} else if(lengthCA > lengthAB && lengthCA > lengthBC) {
			PVector medianPoint = PVector.add(vertexC, PVector.mult(PVector.sub(vertexA, vertexC), locationRatio));
			
			triangle1 = new CoinTriangle(parentCoin, vertexA, vertexB, medianPoint);
			triangle2 = new CoinTriangle(parentCoin, vertexB, vertexC, medianPoint);
		}
		
		triangle1.setBackground(background);
		triangle2.setBackground(background);
		
		return new CoinTriangle[] { triangle1, triangle2 };
	}
	
//	public CoinTriangle[] split() {
//		double randomNumber = Math.random();
//		
//		float locationRatio = (float) (0.25 + (Math.random() * 0.5));
//		
////		float angleCAB = PVector.angleBetween(PVector.sub(vertexB, vertexA), PVector.sub(vertexC, vertexA));
////		float angleABC = PVector.angleBetween(PVector.sub(vertexA, vertexB), PVector.sub(vertexC, vertexB));
////		float angleBCA = PVector.angleBetween(PVector.sub(vertexB, vertexC), PVector.sub(vertexA, vertexC));
//		
////		if((randomNumber < 0.5 && angleCAB > angleABC && angleCAB > angleBCA)
////		|| (randomNumber > 0.5 && ((angleCAB > angleABC && angleCAB < angleBCA) || (angleCAB < angleABC && angleCAB > angleBCA)))) {
////			PVector medianPoint = PVector.add(vertexA, PVector.mult(PVector.sub(vertexB, vertexA), locationRatio));
////			
////			CoinTriangle triangle1 = new CoinTriangle(vertexA, medianPoint, vertexC);
////			CoinTriangle triangle2 = new CoinTriangle(vertexB, vertexC, medianPoint);
////			
////			return new CoinTriangle[] { triangle1, triangle2 };
////		} else if((randomNumber < 0.5 && angleABC > angleCAB && angleABC > angleBCA)
////			   || (randomNumber > 0.5 && ((angleABC > angleCAB && angleABC < angleBCA) || (angleABC < angleCAB && angleABC > angleBCA)))) {
////			PVector medianPoint = PVector.add(vertexB, PVector.mult(PVector.sub(vertexC, vertexB), locationRatio));
////			
////			CoinTriangle triangle1 = new CoinTriangle(vertexA, vertexB, medianPoint);
////			CoinTriangle triangle2 = new CoinTriangle(vertexA, medianPoint, vertexC);
////			
////			return new CoinTriangle[] { triangle1, triangle2 };
////		} else {
////			PVector medianPoint = PVector.add(vertexC, PVector.mult(PVector.sub(vertexA, vertexC), locationRatio));
////			
////			CoinTriangle triangle1 = new CoinTriangle(vertexA, vertexB, medianPoint);
////			CoinTriangle triangle2 = new CoinTriangle(vertexB, vertexC, medianPoint);
////			
////			return new CoinTriangle[] { triangle1, triangle2 };
////		}
//	}
	
	public boolean hasPointInside(PVector point) {
		float areaPAB = this.signedArea(point, vertexA, vertexB);
		float areaPBC = this.signedArea(point, vertexB, vertexC);
		float areaPCA = this.signedArea(point, vertexC, vertexA);
			
		boolean hasPositiveAreas = areaPAB > 0 || areaPBC > 0 || areaPCA > 0;
		boolean hasNegativeAreas = areaPAB < 0 || areaPBC < 0 || areaPCA < 0;
				   
		return !(hasPositiveAreas && hasNegativeAreas);
	}	

	public float signedArea(PVector vertex1, PVector vertex2, PVector vertex3) {
		return (vertex1.x - vertex3.x) * (vertex2.y - vertex3.y)
			 - (vertex2.x - vertex3.x) * (vertex1.y - vertex3.y);
	}

	public boolean isDissolved() {
		return feedingState == FeedingStates.DISSOLVED;
	}
	
	public PVector getVertexA() {
		return vertexA;
	}

	public PVector getVertexB() {
		return vertexB;
	}

	public PVector getVertexC() {
		return vertexC;
	}

	public PVector getSpeedShift() {
		return speedShift;
	}

	public void setSpeedShift(PVector speedShift) {
		this.speedShift = speedShift;
	}

	public void setPosXToCardAnimation(ValueAnimation posXToCardAnimation) {
		this.posXToCardAnimation = posXToCardAnimation;
	}

	public void setPosYToCardAnimation(ValueAnimation posYToCardAnimation) {
		this.posYToCardAnimation = posYToCardAnimation;
	}

	public void setTargetRatioAnimation(ValueAnimation targetRatioAnimation) {
		this.targetRatioAnimation = targetRatioAnimation;
	}

	public void setPosXToCursedPaniacAnimation(ValueAnimation posXToCursedPaniacAnimation) {
		this.posXToCursedPaniacAnimation = posXToCursedPaniacAnimation;
	}

	public void setPosYToCursedPaniacAnimation(ValueAnimation posYToCursedPaniacAnimation) {
		this.posYToCursedPaniacAnimation = posYToCursedPaniacAnimation;
	}
}
