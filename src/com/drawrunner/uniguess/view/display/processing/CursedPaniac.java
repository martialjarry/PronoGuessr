package com.drawrunner.uniguess.view.display.processing;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.TimeUnit;

import com.drawrunner.uniguess.model.communication.nodejs.CursedPaniacListener;
import com.drawrunner.uniguess.view.animation.EaseInAnimation;
import com.drawrunner.uniguess.view.animation.EaseInOutAnimation;
import com.drawrunner.uniguess.view.animation.EaseOutAnimation;
import com.drawrunner.uniguess.view.animation.ImageAnimation;
import com.drawrunner.uniguess.view.animation.LinearAnimation;
import com.drawrunner.uniguess.view.animation.SquareInAnimation;
import com.drawrunner.uniguess.view.animation.SquareOutAnimation;
import com.drawrunner.uniguess.view.animation.ValueAnimation;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;

public class CursedPaniac extends TrayEntity implements Observer {
	public final static float SPEED = 10;
	public final static long CHEWING_DURATION = 250;
	public final static int CHEWING_ANIMATIONS_LENGTH = 22;
	public final static int MIN_COINS_AMOUNT_BEFORE_LEAVING = 50;
	
	private enum CursedPaniacStates { HIDDEN, SPAWNING, MOVING_AROUND, LEAVING }
	
	private enum JumingAnimationStates { RAISING, FALLING }
	private enum ZigZagAnimationStates { ZIGING, ZAGING }

	private enum EastingAnimationStates { NOT_EATING, STARTING_TO_EAT, EATING, FINISHING_TO_EAT }
	
	private PVector localMouthAnchor;
	
	private ValueAnimation posXAnimation;
	private ValueAnimation posYAnimation;
	
	private CursedPaniacStates cursedPaniacState;
	
	private JumingAnimationStates jumingAnimationState;
	private ZigZagAnimationStates zigZagAnimationState;
	
	private EastingAnimationStates eatingAnimationState;
	
	private List<ImageAnimation> chewingAnimations; 
	private int eatingAnimationIndex;
	
	private int cointsToEatCount;
	private int eatenCoinsCount;
	
	private String invokatorName;
	private CursedPaniacListener cursedPaniacListener;
	
	private List<Integer> capturednPronoIndices;
	
	public CursedPaniac() {
		dimension = new PVector(180, 180);
		
		this.background = (PGraphics) background;
		
		chewingAnimations = new LinkedList<ImageAnimation>();
		
		localMouthAnchor = new PVector(dimension.x * 0.46f, dimension.y * 0.42f);
		
		posXAnimation = new EaseOutAnimation(2, TimeUnit.SECONDS);
		posYAnimation = new SquareOutAnimation(120, TimeUnit.MILLISECONDS);
		
		cursedPaniacState = CursedPaniacStates.HIDDEN;
		
		jumingAnimationState = JumingAnimationStates.RAISING;
		zigZagAnimationState = ZigZagAnimationStates.ZIGING;
		
		eatingAnimationState = EastingAnimationStates.NOT_EATING;
		
		eatingAnimationIndex = -1;
		
		cointsToEatCount = 0;
		eatenCoinsCount = 0;
		
		isVisible = false;
		
		invokatorName = null;
		cursedPaniacListener = null;
		
		capturednPronoIndices = new ArrayList<Integer>();
	}

	public void buildEatingSequence(List<PImage> chewingSprites) {
		int chewingAnimationsCount = PApplet.floor(chewingSprites.size() / (CHEWING_ANIMATIONS_LENGTH * 1f));
		for (int i = 0; i < chewingAnimationsCount; i++) {
			LinkedList<PImage> nextChewingAnimationImageSequence = new LinkedList<PImage>();
			
			for (int j = 0; j < CHEWING_ANIMATIONS_LENGTH; j++) {
				PImage chewingAnimationImage = chewingSprites.get(i * CHEWING_ANIMATIONS_LENGTH + j);
				nextChewingAnimationImageSequence.add(chewingAnimationImage);
			}
			
			ImageAnimation newChewingAnimation = new ImageAnimation(CHEWING_DURATION, TimeUnit.MILLISECONDS, nextChewingAnimationImageSequence);
			chewingAnimations.add(newChewingAnimation);
		}
	}
	
	public void spawn(String invokatorName, PVector startPosision, PVector endPosition) {
		if(cursedPaniacState == CursedPaniacStates.HIDDEN) {
			this.invokatorName = invokatorName;
			
			isVisible = true;
			
			posXAnimation = new EaseOutAnimation(2, TimeUnit.SECONDS, startPosision.x, endPosition.x);
			posYAnimation = new SquareInAnimation(140, TimeUnit.MILLISECONDS, startPosision.y, endPosition.y);
			
			posXAnimation.start();
			posYAnimation.start();
			
			jumingAnimationState = JumingAnimationStates.RAISING;
			cursedPaniacState = CursedPaniacStates.SPAWNING;
		}
	}
	
	public void startEating() {
		if(eatingAnimationState != EastingAnimationStates.EATING) {
			eatingAnimationIndex = PApplet.max(eatingAnimationIndex, 0);
			eatingAnimationState = EastingAnimationStates.STARTING_TO_EAT;
		}
	}
	
	public void finishEating() {
		if(eatingAnimationState !=  EastingAnimationStates.NOT_EATING) {
			eatingAnimationState = EastingAnimationStates.FINISHING_TO_EAT;
		}
	}
	
	public void leave(float screenBottomPosY) {
		if(cursedPaniacState == CursedPaniacStates.MOVING_AROUND) {
			long leavinAnimationDuration = (long) (PApplet.abs(screenBottomPosY - position.y) * 16);
			
			posXAnimation = new EaseInAnimation(250, TimeUnit.MILLISECONDS, position.x, position.x + 30);
			posYAnimation = new LinearAnimation(leavinAnimationDuration, TimeUnit.MILLISECONDS, position.y, screenBottomPosY);
			
			posXAnimation.start();
			posYAnimation.start();
			
			zigZagAnimationState = ZigZagAnimationStates.ZIGING;
			cursedPaniacState = CursedPaniacStates.LEAVING;
		}
	}
	
	@Override
	public void animate(PApplet p) {
		this.animateSpawn();
		this.animateEating(p);
	}

	private void animateSpawn() {
		switch(cursedPaniacState) {
		case SPAWNING:
			this.animateJumping();
			
			if(posXAnimation.isFinished() && posYAnimation.isFinished()) {
				if(cursedPaniacListener != null) {
					cursedPaniacListener.cursedPaniacReady(invokatorName);
				}
				
				cursedPaniacState = CursedPaniacStates.MOVING_AROUND;
			}
			
			break;
		case LEAVING:
			this.animateZigZaging();
			
			if(posXAnimation.isFinished() && posYAnimation.isFinished()) {
				if(cursedPaniacListener != null) {
					cursedPaniacListener.cursedPaniacLeaved();
				}
				
				cursedPaniacState = CursedPaniacStates.HIDDEN;
			}
			
			break;
		}
	}

	private void animateJumping() {
		float nextPosX = (float) posXAnimation.nextValue();
		float nextPosY = (float) posYAnimation.nextValue();
		
		switch (jumingAnimationState) {
		case RAISING:
			if(posYAnimation.isFinished()) {
				long jumpHalfDuration = (long) (40 + (100 * (1 - posXAnimation.nextFraction())));
				
				posYAnimation = new SquareOutAnimation(jumpHalfDuration, TimeUnit.MILLISECONDS, posYAnimation.getEndValue(), posYAnimation.getStartValue());
				posYAnimation.start();
				
				jumingAnimationState = JumingAnimationStates.FALLING;
			}
			
			break;
		case FALLING:
			if(posYAnimation.isFinished() && !posXAnimation.isFinished()) {
				long jumpHalfDuration = (long) (40 + (100 * (1 - posXAnimation.nextFraction())));
				float jumpHeight = (float) (30 * (1 - posXAnimation.nextFraction())); 
				
				posYAnimation = new SquareInAnimation(jumpHalfDuration, TimeUnit.MILLISECONDS, posYAnimation.getEndValue(), posYAnimation.getEndValue() - jumpHeight);
				posYAnimation.start();
				
				jumingAnimationState = JumingAnimationStates.RAISING;
			}
			
			break;
		}
		
		position = new PVector(nextPosX, nextPosY);
	}

	private void animateEating(PApplet p) {
		float nextPosX = position.x;
		float nextPosY = position.y;
		
		if(eatingAnimationIndex >= 0 && eatingAnimationIndex < chewingAnimations.size()) {
			ImageAnimation currentChewingAnimation = chewingAnimations.get(eatingAnimationIndex);
			
			switch (eatingAnimationState) {
			case STARTING_TO_EAT:
				if(currentChewingAnimation.isFinished()) {
					eatingAnimationIndex = PApplet.min(eatingAnimationIndex + 1, chewingAnimations.size() - 1);
					
					if(eatingAnimationIndex == chewingAnimations.size() - 1) {
						eatingAnimationState = EastingAnimationStates.EATING;
					} else {
						currentChewingAnimation = chewingAnimations.get(eatingAnimationIndex);
						
						this.stopAllChewingAnimations();
						currentChewingAnimation.start();
					}
				}
				break;
			case EATING:
				if(currentChewingAnimation.isFinished()) {
					if(eatingAnimationIndex == chewingAnimations.size() - 1) {
						this.stopAllChewingAnimations();
						currentChewingAnimation.start();
					} else {
						eatingAnimationState = EastingAnimationStates.FINISHING_TO_EAT;
					}
				}
				
				break;
			case FINISHING_TO_EAT:
				if(currentChewingAnimation.isFinished()) {
					eatingAnimationIndex = PApplet.max(eatingAnimationIndex - 1, -1);
					
					if(eatingAnimationIndex == -1) {
						eatingAnimationState = EastingAnimationStates.NOT_EATING;
					} else {
						currentChewingAnimation = chewingAnimations.get(eatingAnimationIndex);
						
						this.stopAllChewingAnimations();
						currentChewingAnimation.start();
					}
				}
				
				break;
			}
			
			PImage nextCursedPaniacSprite = currentChewingAnimation.nextImage();
			background = p.createGraphics(nextCursedPaniacSprite.width, nextCursedPaniacSprite.height);
			
			background.beginDraw();
			background.imageMode(PApplet.CORNER);
			background.background(nextCursedPaniacSprite);
			background.endDraw();
		}
		
		position = new PVector(nextPosX, nextPosY);
	}
	
	private void stopAllChewingAnimations() {
		for(ImageAnimation chewingAnimation : chewingAnimations) {
			chewingAnimation.stop();
		}
	}
	
	private void animateZigZaging() {
		float nextPosX = (float) posXAnimation.nextValue();
		float nextPosY = (float) posYAnimation.nextValue();
		
		switch (zigZagAnimationState) {
		case ZIGING:
			if(posXAnimation.isFinished()) {
				posXAnimation = new EaseInOutAnimation(500, TimeUnit.MILLISECONDS, posXAnimation.getEndValue(), posXAnimation.getEndValue() - 60);
				posXAnimation.start();
				
				zigZagAnimationState = ZigZagAnimationStates.ZAGING;
			}
			
			break;
		case ZAGING:
			if(posXAnimation.isFinished() && !posYAnimation.isFinished()) {
				posXAnimation = new EaseInOutAnimation(500, TimeUnit.MILLISECONDS, posXAnimation.getEndValue(), posXAnimation.getEndValue() + 60);
				posXAnimation.start();
				
				zigZagAnimationState = ZigZagAnimationStates.ZIGING;
			}
			
			break;
		}
		
		position = new PVector(nextPosX, nextPosY);
	}
	
	@Override
	public void draw(PApplet p) {
		if(isVisible) {
			float backgroundRatio = background.height / (background.width * 1f); 
			
			if(backgroundRatio > 1) {
				p.image(background, position.x, position.y, dimension.x / backgroundRatio, dimension.y);
			} else {
				p.image(background, position.x, position.y, dimension.x, dimension.y * backgroundRatio);
			}
		}
	}
	
	public void move(PVector translation) {
		if(cursedPaniacState == CursedPaniacStates.MOVING_AROUND) {
			position = PVector.add(position, translation);
		}
	}
	
	public PVector mouthAnchor() {
		return PVector.add(position, localMouthAnchor);
	}

	@Override
	public synchronized void update(Observable observable, Object data) {
		if(observable instanceof CoinToken && data instanceof Object[]) {
			Object[] dataObjects = (Object[]) data;
			
			if (dataObjects.length >= 3
			 && dataObjects[0] instanceof CoinTokenActions
			 && dataObjects[1] instanceof String
			 && dataObjects[2] instanceof String
			 && dataObjects[3] instanceof Integer) {
				CoinTokenActions coinAction = (CoinTokenActions) dataObjects[0];
				String sourcePlayerName = (String) dataObjects[1];
				String targetDictrictName = (String) dataObjects[2];
				int pronoIndex = (Integer) dataObjects[3];
				
				switch(coinAction) {
				case SWITCHING_TO_CURSED_PANIAC:
					cointsToEatCount++;
					this.startEating();
					if(cursedPaniacListener != null && !capturednPronoIndices.contains(pronoIndex)) {
						cursedPaniacListener.cursedPaniacAteProno(sourcePlayerName);
						capturednPronoIndices.add(pronoIndex);
					}
					break;
				case REACHED_CURSED_PANIAC:
					cointsToEatCount--;
					eatenCoinsCount++;
					
					if(cointsToEatCount == 0) {
						this.finishEating();
					}
					
					if(cursedPaniacListener != null && eatenCoinsCount > MIN_COINS_AMOUNT_BEFORE_LEAVING
					&& Math.random() < 0.1f && cursedPaniacState == CursedPaniacStates.MOVING_AROUND) {
						eatenCoinsCount = 0;
						cursedPaniacListener.cursedPaniacEndedEating();
					}
					
					break;
				}
			}
		}
	}

	public boolean isMovingAround() {
		return (cursedPaniacState == CursedPaniacStates.MOVING_AROUND);
	}
	
	public void setCursedPaniacListener(CursedPaniacListener cursedPaniacListener) {
		this.cursedPaniacListener = cursedPaniacListener;
	}
}
