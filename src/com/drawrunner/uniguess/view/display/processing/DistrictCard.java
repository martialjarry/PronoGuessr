package com.drawrunner.uniguess.view.display.processing;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import com.drawrunner.uniguess.view.animation.EaseOutAnimation;
import com.drawrunner.uniguess.view.animation.ValueAnimation;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;

public class DistrictCard extends TrayEntity implements Observer {
	public final static int DEFAULT_HEIGHT = 64; 
	public final static float DEFAULT_RATIO = 3 / 2f; 
	public final static int DEFAULT_WIDTH = (int) (DEFAULT_HEIGHT * DEFAULT_RATIO);
	public final static float DEFAULT_OVERLAP_FACTOR = 0.5f;
	public final static PVector LABEL_MARGINS = new PVector(5, 3);

	private enum OverlayStates { HIDDEN, SHOWING, HIDDING }
	private OverlayStates overlayState;
	
	private enum States {
		OVERLAPPED_LOW, OVERLAPPED_INCREASING,
		OVERLAPPED_DECREASING, OVERLAPPED_HIGH,
		AWAITING_EXPOSING, EXPOSING, EXPOSED,
		AWAITING_DISPOSING, DISPOSING
	}
	private States currentState;
	
	private String districtIdentifier;
	private String districtName;
	private float districtOdds;
	
	private List<CoinToken> coinTokens;

	private float scale;
	
	private float oddsScale;
	private int oddsTint;
	
	private int overlayOpacity;
	
	private float overlapFactor;
	private float overlapShift;
	
	private int labelOpacity;
	private float labelScrolling;
	private PGraphics nameLabelGraphic;
	
	private PImage oddsShadow;
	
	private ValueAnimation posXAnimation;
	private ValueAnimation posYAnimation;
	
	private ValueAnimation scaleAnimation;
	private ValueAnimation oddsScaleAnimation;
	
	private ValueAnimation overlayOpacityAnimation;

	private ValueAnimation overlapPositionAnimation;
	private ValueAnimation labelOpacityAnimation;
	
	public DistrictCard(PGraphics districtFlag, PImage cardShadow, String districtIdentifier, String districtName, float districtOdds, PFont districtLabelFont, PImage oddsShadow) {
		super(districtFlag, cardShadow, districtLabelFont);
		
		overlayState = OverlayStates.HIDDEN;
		currentState = States.OVERLAPPED_LOW;
		
		this.districtIdentifier = districtIdentifier;
		this.districtName = districtName;
		this.districtOdds = districtOdds;
		
		this.oddsShadow = oddsShadow;
		
		coinTokens = new CopyOnWriteArrayList<CoinToken>();
		
		scale = 1;
		
		oddsScale = 0;
		oddsTint = 0;
		
		overlayOpacity = 0;
		
		labelOpacity = 0;
		labelScrolling = 0;
		nameLabelGraphic = null;
		
		overlapShift = 0;
		overlapShift = 0;
		
		posXAnimation = new EaseOutAnimation(400, TimeUnit.MILLISECONDS);
		posYAnimation = new EaseOutAnimation(400, TimeUnit.MILLISECONDS);
		
		scaleAnimation = new EaseOutAnimation(400, TimeUnit.MILLISECONDS);
		oddsScaleAnimation = new EaseOutAnimation(750, TimeUnit.MILLISECONDS);
		
		overlayOpacityAnimation = new EaseOutAnimation(120, TimeUnit.MILLISECONDS, 0, 127);
		
		overlapPositionAnimation = new EaseOutAnimation(400, TimeUnit.MILLISECONDS, 0, 127);
		labelOpacityAnimation = new EaseOutAnimation(overlapPositionAnimation.getDuration(), overlapPositionAnimation.getTimeUnit(), 0, 255);
	}

	public void fadeInOverlay() {
		overlayOpacityAnimation.stop();
		
		overlayOpacityAnimation.setStartValue(overlayOpacity);
		overlayOpacityAnimation.setEndValue(127);
		overlayOpacityAnimation.start();
		
		overlayState = OverlayStates.SHOWING;
	}
	
	public void deploy() {
		if(!this.isExposingOrDisposing()) {
			overlapPositionAnimation.stop();
			labelOpacityAnimation.stop();
			
			overlapPositionAnimation.setStartValue(overlapFactor);
			overlapPositionAnimation.setEndValue(1 - DEFAULT_OVERLAP_FACTOR - 0.1f);
			
			labelOpacityAnimation.setStartValue(labelOpacity);
			labelOpacityAnimation.setEndValue(255);
			
			overlapPositionAnimation.start();
			labelOpacityAnimation.start();
			
			currentState = States.OVERLAPPED_INCREASING;
		}
	}

	public void retract() {
		if(!this.isExposingOrDisposing()) {
			overlapPositionAnimation.stop();
			labelOpacityAnimation.stop();
			
			overlapPositionAnimation.setStartValue(overlapFactor);
			overlapPositionAnimation.setEndValue(0);
			
			labelOpacityAnimation.setStartValue(labelOpacity);
			labelOpacityAnimation.setEndValue(0);
			
			overlapPositionAnimation.start();
			labelOpacityAnimation.start();
		}
		
		currentState = States.OVERLAPPED_DECREASING;
	}
	
	public void expose(float targetPosX, float targetPosY, int startDelayMillis) {
		if(currentState == States.AWAITING_DISPOSING
		|| currentState == States.OVERLAPPED_LOW
		|| currentState == States.DISPOSING) {
			this.animateExposition(targetPosX, targetPosY, 0.85f, 1, startDelayMillis, 255);
			currentState = States.AWAITING_EXPOSING;
		}
	}
	
	public void dispose(float targetPosX, float targetPosY, int startDelayMillis) {
		if(currentState == States.AWAITING_EXPOSING
		|| currentState == States.EXPOSING
		|| currentState == States.EXPOSED) {
			this.animateExposition(targetPosX, targetPosY, 1f, 0, startDelayMillis, 0);
			currentState = States.AWAITING_DISPOSING;
		}
	}
	
	private void animateExposition(float targetPosX, float targetPosY, float targetScale, float targetOddsScale, int startDelayMillis, int targetLabelOpacity) {
		posXAnimation.setStartValue(position.x);
		posYAnimation.setStartValue(position.y);
		
		scaleAnimation.setStartValue(scale);
		oddsScaleAnimation.setStartValue(oddsScale);
		
		posXAnimation.setEndValue(targetPosX);
		posYAnimation.setEndValue(targetPosY);
		
		scaleAnimation.setEndValue(targetScale);
		oddsScaleAnimation.setEndValue(targetOddsScale);
		
		labelOpacityAnimation.setEndValue(targetLabelOpacity);
		
		long startTime = (long) (System.nanoTime() + ((long) startDelayMillis) * 1000000l);
		
		posXAnimation.setStartTime(startTime);
		posYAnimation.setStartTime(startTime);
		
		scaleAnimation.setStartTime(startTime);
		oddsScaleAnimation.setStartTime(startTime);
		
		labelOpacityAnimation.setStartTime(startTime);
	}
	
	@Override
	public void animate(PApplet p) {
		this.animateTransform();
		this.animateOverlay();
		
		labelScrolling += (1 / (p.frameRate * 1f)) * 50;
	}

	private void animateOverlay() {
		switch (overlayState) {
		case HIDDEN:
			break;
		case SHOWING:
			overlayOpacity = (int) overlayOpacityAnimation.nextValue();
			
			if(overlayOpacity >= overlayOpacityAnimation.getEndValue() || overlayOpacityAnimation.isFinished()) {
				overlayOpacityAnimation.stop();
				
				overlayOpacityAnimation.setStartValue(overlayOpacity);
				overlayOpacityAnimation.setEndValue(0);
				overlayOpacityAnimation.start();
				
				overlayState = OverlayStates.HIDDING;
			}
			break;
		case HIDDING:
			overlayOpacity = (int) overlayOpacityAnimation.nextValue();
			
			if(overlayOpacityAnimation.isFinished()) {
				overlayState = OverlayStates.HIDDEN;
			}
			break;
		default:
			break;
		}
	}
	
	private void animateTransform() {
		float currentPosX = position.x;
		float currentPosY = position.y;
		
		float currentTime = System.nanoTime();

		switch (currentState) {
		case OVERLAPPED_INCREASING:
			overlapFactor = (float) overlapPositionAnimation.nextValue();
			labelOpacity = (int) labelOpacityAnimation.nextValue();
			
			if(overlapPositionAnimation.isFinished()) {
				overlapPositionAnimation.stop();
				labelOpacityAnimation.stop();
				
				currentState = States.OVERLAPPED_HIGH;
			}
			break;
		case OVERLAPPED_DECREASING:
			overlapFactor = (float) overlapPositionAnimation.nextValue();
			labelOpacity = (int) labelOpacityAnimation.nextValue();
			
			if(overlapPositionAnimation.isFinished()) {
				overlapPositionAnimation.stop();
				labelOpacityAnimation.stop();
				
				currentState = States.OVERLAPPED_LOW;
			}
			break;
		case AWAITING_EXPOSING:
			if(currentTime >= posXAnimation.getStartTime() && currentTime >= posYAnimation.getStartTime()
			&& currentTime >= scaleAnimation.getStartTime() && currentTime >= scaleAnimation.getStartTime()
			&& currentTime >= labelOpacityAnimation.getStartTime()) {
				posXAnimation.start();
				posYAnimation.start();
				
				scaleAnimation.start();
				oddsScaleAnimation.start();
				
				labelOpacityAnimation.start();
				
				currentState = States.EXPOSING;
			}
			break;
		case EXPOSING:
			currentPosX = (float) posXAnimation.nextValue();
			currentPosY = (float) posYAnimation.nextValue();
			
			position = new PVector(currentPosX, currentPosY);
			scale = (float) scaleAnimation.nextValue();
			oddsScale = (float) oddsScaleAnimation.nextValue();
			labelOpacity = (int) labelOpacityAnimation.nextValue();
			
			if(posXAnimation.isFinished() && posYAnimation.isFinished()
			&& scaleAnimation.isFinished() && oddsScaleAnimation.isFinished()
			&& labelOpacityAnimation.isFinished()) {
				currentState = States.EXPOSED;
			}
			
			break;
		case AWAITING_DISPOSING:
			if(currentTime >= posXAnimation.getStartTime() && currentTime >= posYAnimation.getStartTime()
			&& currentTime >= scaleAnimation.getStartTime() && currentTime >= scaleAnimation.getStartTime()
			&& currentTime >= labelOpacityAnimation.getStartTime()) {
				posXAnimation.start();
				posYAnimation.start();
				
				scaleAnimation.start();
				oddsScaleAnimation.start();
				
				labelOpacityAnimation.start();
				
				currentState = States.DISPOSING;
			}
			break;
		case DISPOSING:
			currentPosX = (float) posXAnimation.nextValue();
			currentPosY = (float) posYAnimation.nextValue();
			
			position = new PVector(currentPosX, currentPosY);
			scale = (float) scaleAnimation.nextValue();
			oddsScale = (float) oddsScaleAnimation.nextValue();
			labelOpacity = (int) labelOpacityAnimation.nextValue();
			
			if(posXAnimation.isFinished() && posYAnimation.isFinished()
			&& scaleAnimation.isFinished() && oddsScaleAnimation.isFinished()
			&& labelOpacityAnimation.isFinished()) {
				currentState = States.OVERLAPPED_LOW;
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void draw(PApplet p) {
//		isVisible = false;
		
		if(isVisible) {
			float halfWidth = dimension.x * 0.5f;
			float halfHeight = dimension.y * 0.5f;
			
			float centerX = position.x + halfWidth;
			float centerY = position.y + halfHeight;
			
			p.pushMatrix();
			p.translate(centerX, centerY);
			p.scale(scale);
			
			position = new PVector(position.x - centerX, position.y + overlapShift - centerY);
			this.drawShadow(p);
			position = new PVector(position.x + centerX, position.y - overlapShift + centerY);
			
			p.imageMode(PApplet.CORNER);
			p.image(background, -halfWidth, -halfHeight + overlapShift, dimension.x, dimension.y);
			
			if (overlayOpacity > 0) {
				p.rectMode(PApplet.CORNER);
				p.stroke(255, overlayOpacity * 2);
				p.strokeWeight(3);
				
				p.fill(255, overlayOpacity);
				p.rect(-halfWidth, -halfHeight + overlapShift, dimension.x, dimension.y, dimension.y * 0.1f);
			}
			
			float labelTextSize = dimension.y * 0.22f;
			
			p.textAlign(PApplet.CENTER, PApplet.CENTER);
			p.textFont(labelFont, labelTextSize);
			
			float labelWidth = p.textWidth(districtName);
			float labelHeight = labelTextSize * 2f + LABEL_MARGINS.y * 2;

			float labelPosY = overlapShift;
			
			if(this.isExposingOrDisposing()) {
				labelPosY = halfHeight + labelHeight * 0.5f + labelHeight * 0.16f;
			}
			
			float labelTextCorrectionY = -(1 / scale) * dimension.y * 0.038f;
			
			if(labelOpacity > 0) {
				if(labelWidth > dimension.x * 0.7f) {
					if(nameLabelGraphic == null) {
						nameLabelGraphic = p.createGraphics((int) (dimension.x * 0.7f + LABEL_MARGINS.x * 2), (int) labelHeight, PApplet.P2D);
					} else {
						nameLabelGraphic.resize((int) (dimension.x * 0.7f + LABEL_MARGINS.x * 2), (int) labelHeight);
					}
					
					nameLabelGraphic.beginDraw();
					nameLabelGraphic.clear();
					
					nameLabelGraphic.rectMode(PApplet.CORNER);
					nameLabelGraphic.fill(0, labelOpacity * 0.7f);
					nameLabelGraphic.noStroke();
					nameLabelGraphic.rect(0, 0, nameLabelGraphic.width, nameLabelGraphic.height, labelHeight);
					
					final float TEXTS_MARGIN = dimension.y * 0.16f;
					
					float textWidth = p.textWidth(districtName);
					float scrollingCursor = labelScrolling % (textWidth + TEXTS_MARGIN);
					
					float textPosX = nameLabelGraphic.width * 0.5f - scrollingCursor;
					float textPosY = nameLabelGraphic.height * 0.5f + labelTextCorrectionY;
					
					nameLabelGraphic.imageMode(PApplet.CENTER);
					nameLabelGraphic.textFont(labelFont, labelTextSize);
					nameLabelGraphic.textAlign(PApplet.CENTER, PApplet.CENTER);
					
					nameLabelGraphic.fill(255, labelOpacity);
					nameLabelGraphic.text(districtName, textPosX, textPosY);
					nameLabelGraphic.text(districtName, textPosX + textWidth + TEXTS_MARGIN, textPosY);
					
					nameLabelGraphic.endDraw();
					
					p.fill(255, 0, 0);
					p.imageMode(PApplet.CENTER);
					p.image(nameLabelGraphic, 0, labelPosY);
				} else {
					p.imageMode(PApplet.CENTER);
					p.textFont(labelFont, labelTextSize);
					
					p.rectMode(PApplet.CENTER);
					p.fill(0, labelOpacity * 0.7f);
					p.noStroke();
					p.rect(0, labelPosY, labelWidth + LABEL_MARGINS.x * 2, labelHeight, labelHeight);
					
					p.fill(255, labelOpacity);
					p.text(districtName, 0,  labelPosY + labelTextCorrectionY);
				}
			}
			
			if(this.isExposingOrDisposing()) {
				this.drawOddsIndicator(p);
			}
			
			p.popMatrix();
		}
		
		for (CoinToken coinToken : coinTokens) {
			coinToken.animate(p);
			coinToken.draw(p);
		}
	}

	private boolean isExposingOrDisposing() {
		return currentState == States.AWAITING_EXPOSING || currentState == States.EXPOSING || currentState == States.EXPOSED
		|| currentState == States.AWAITING_DISPOSING || currentState == States.DISPOSING;
	}

	private void drawOddsIndicator(PApplet p) {
		p.translate(dimension.x * 0.32f, -dimension.y * 0.35f);
		
		if(oddsScale < 1) {
			p.scale(oddsScale);
		}
		
		float oddsTintNorm = oddsTint / 360f;
		
		p.ellipseMode(PApplet.CENTER);
		
		p.imageMode(PApplet.CENTER);
		p.image(oddsShadow, 0, 0, dimension.y * 0.95f, dimension.y * 0.95f);
		
		p.fill(Color.HSBtoRGB(oddsTintNorm , 1f, 0.74f * (1 - (oddsTintNorm * 1f))));
		p.ellipse(0, 0, dimension.y * 0.68f, dimension.y * 0.68f);
		
//		p.noFill();
//		p.ellipse(0, 0, dimension.y * 0.68f, dimension.y * 0.68f);
		
		int fontColor = Color.HSBtoRGB(oddsTintNorm , 0.00f, 1f);
		String indicatorString = PApplet.floor(districtOdds) + "";
		
		p.fill(fontColor, 255);
		
		if(districtOdds < 100) {
			p.textFont(labelFont, dimension.y * 0.2f);
			p.text(indicatorString, -dimension.y * 0.012f, -dimension.y * 0.046f);
			
			p.fill(fontColor, 164);
			p.text(indicatorString, -dimension.y * 0.012f, -dimension.y * 0.046f);
		} else {
			p.textFont(labelFont, dimension.y * 0.16f);
			p.text(indicatorString, -dimension.y * 0.02f, -dimension.y * 0.03f);
			
			p.fill(fontColor, 164);
			p.text(indicatorString, -dimension.y * 0.02f, -dimension.y * 0.03f);
		}
		
		p.translate(-dimension.x * 0.32f, dimension.y * 0.35f);
		
		if(oddsScale < 1) {
			p.scale(1 / oddsScale);
		}
	}
	
	@Override
	public void update(Observable observable, Object data) {
		if(observable instanceof CoinToken && data instanceof Object[]) {
			Object[] dataObjects = (Object[]) data;
			
			if (dataObjects.length >= 1
			 && dataObjects[0] instanceof CoinTokenActions) {
				CoinTokenActions coinAction = (CoinTokenActions) dataObjects[0];
				
				if(coinAction == CoinTokenActions.REACHED_DISTRICT) {
					this.fadeInOverlay();
				}
			}
		}
	}
	
	public void addCoinToken(CoinToken coinToken) {
		coinToken.addObserver(this);
		coinTokens.add(coinToken);
	}
	
	public int coinTokenCount() {
		return coinTokens.size();
	}
	
	public CoinToken getCoinToken(int coinIndex) {
		return coinTokens.get(coinIndex);
	}
	
	public synchronized void removeUselessCoinTokens() {
		List<CoinToken> tokensToDelete = new ArrayList<CoinToken>();
		
		for (CoinToken coinToken : coinTokens) {
			if(coinToken.isArrivedAtTarget()) {
				tokensToDelete.add(coinToken);
			}
		}
		
		for (CoinToken tokenToDelete : tokensToDelete) {
			coinTokens.remove(tokenToDelete);
		}
	}
	
	public boolean equals(Object object) {
		if(object != null && object instanceof DistrictCard) {
			DistrictCard otherDistrictCard = (DistrictCard) object;
			if(districtName != null && otherDistrictCard.getDistrictIdentifier() != null) {
				return districtName.equals(otherDistrictCard.getDistrictIdentifier());
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public boolean isDeployed() {
		return currentState == States.OVERLAPPED_INCREASING || currentState == States.OVERLAPPED_HIGH;
	}
	
	public boolean isRetracted() {
		return currentState == States.OVERLAPPED_DECREASING || currentState == States.OVERLAPPED_LOW;
	}
	
	public String getDistrictIdentifier() {
		return districtIdentifier;
	}

	public void setDistrictIdentifier(String districtIdentifier) {
		this.districtIdentifier = districtIdentifier;
	}
	
	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public float getDistrictOdds() {
		return districtOdds;
	}

	public void setDistrictOdds(float districtOdds) {
		this.districtOdds = districtOdds;
	}
	
	public float getOverlapFactor() {
		return overlapFactor;
	}

	public void setOverlapFactor(float overlapFactor) {
		this.overlapFactor = overlapFactor;
	}
	
	public float getOverlapShift() {
		return overlapShift;
	}

	public void setOverlapShift(float overlapShift) {
		this.overlapShift = overlapShift;
	}
	
	public int getOddsTint() {
		return oddsTint;
	}

	public void setOddsTint(int oddsTint) {
		this.oddsTint = oddsTint;
	}
}
