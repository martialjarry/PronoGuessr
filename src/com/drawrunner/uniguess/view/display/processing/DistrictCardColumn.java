package com.drawrunner.uniguess.view.display.processing;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import processing.core.PApplet;

public class DistrictCardColumn extends TrayEntityContainer {
	public void deployCard(DistrictCard districtCard, int cardIndex) {
		int currentRow = 0;
		for (Iterator<TrayEntity> trayEntityIterator = entities.iterator(); trayEntityIterator.hasNext() && currentRow <= cardIndex;) {
			DistrictCard currentDistrictCard = (DistrictCard) trayEntityIterator.next();
			currentDistrictCard.deploy();
			currentRow++;
		}
	}
	
	@Override
	public void animate(PApplet p) {
		List<TrayEntity> inverseCardColumn = new LinkedList<TrayEntity>(entities);
		Collections.reverse(inverseCardColumn);
		
		float cumulatedShift = 0;
		for (TrayEntity trayEntity : inverseCardColumn) {
			if (trayEntity instanceof DistrictCard) {
				DistrictCard districtCard = (DistrictCard) trayEntity;
				
				cumulatedShift += districtCard.dimension.y * districtCard.getOverlapFactor();
				districtCard.setOverlapShift(-cumulatedShift);
			}
		}
	}

	@Override
	public void draw(PApplet p) {
		// Non utilis� car les cartes sont trac�es ligne par ligne
		// et non colonne par colonne
	}

	@Override
	public void addOrUpdateTrayEntity(PApplet p, TrayEntity playerToken) {
		entities.add(playerToken);
	}

	@Override
	public TrayEntity getTrayEntity(String entityId) {
		DistrictCard districtCard = null;

		for (Iterator<TrayEntity> trayEntityIterator = entities.iterator(); trayEntityIterator.hasNext() && districtCard == null;) {
			TrayEntity trayEntity = trayEntityIterator.next();
			
			if(trayEntity instanceof DistrictCard) {
				DistrictCard currentDistrictCard = (DistrictCard) trayEntity;
				
				if(currentDistrictCard.getDistrictIdentifier().equals(entityId)) {
					districtCard = currentDistrictCard;
				}
			}
		}
		
		return districtCard;
	}
	
	public int getCardIndex(DistrictCard districtCard) {
		int currentRow = 0;
		
		boolean isFound = false;
		for (Iterator<TrayEntity> trayEntityIterator = entities.iterator(); trayEntityIterator.hasNext() && !isFound;) {
			TrayEntity trayEntity = trayEntityIterator.next();
			
			if(trayEntity == districtCard) {
				isFound = true;
			}
			
			currentRow++;
		}
		
		currentRow = isFound ? currentRow : -1;
		
		return currentRow;
	}
	
	public DistrictCard getRandomDistrictCard() {
		if(!entities.isEmpty()) {
			TrayEntity trayEntity = entities.get((int) (Math.random() * entities.size()));
			
			if (trayEntity instanceof DistrictCard) {
				return (DistrictCard) trayEntity;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public void removeUselessCoinTokens() {
		for (TrayEntity trayEntity : entities) {
			if (trayEntity instanceof DistrictCard) {
				DistrictCard districtCard = (DistrictCard) trayEntity;
				districtCard.removeUselessCoinTokens();
			}
		}
	}
}
