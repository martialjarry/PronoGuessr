package com.drawrunner.uniguess.view.display.processing;

import java.awt.Point;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PVector;

public class DistrictCardGrid extends TrayEntityContainer {
	private final static int DISTRICT_CARD_GRID_HEIGHT = 100;
	private final static Point ODDS_TINT_RANGE = new Point(100, 0);
	
	private int rowCount;
	private int columnCount;
	
	public DistrictCardGrid(PApplet p) {
		position = new PVector(0, p.height - DISTRICT_CARD_GRID_HEIGHT);
		dimension = new PVector(p.width, DISTRICT_CARD_GRID_HEIGHT);
		
		entities.add(new DistrictCardColumn());
		
		rowCount = 1;
		columnCount = 1; 
	}

	@Override
	public void addOrUpdateTrayEntity(PApplet p, TrayEntity trayEntity) {
		if(trayEntity instanceof DistrictCard) {
			DistrictCard districtCard = (DistrictCard) trayEntity;
			TrayEntity existingEntity = this.getTrayEntity(districtCard.getDistrictIdentifier());
			
			if(existingEntity == null) {
				PGraphics districtFlag = districtCard.getBackground();
				if (districtFlag != null) {
					this.updateGridSize(this.totalCardCount() + 1);
					this.buildGrid(p, districtCard);
				} 
			} else {
				if (existingEntity instanceof DistrictCard) {
					DistrictCard existingDistrictCard = (DistrictCard) existingEntity;
					
					existingDistrictCard.setDistrictIdentifier(districtCard.getDistrictIdentifier());
					existingDistrictCard.setDistrictName(districtCard.getDistrictName());
					existingDistrictCard.setDistrictOdds(districtCard.getDistrictOdds());
					
					if(districtCard.getBackground() != null) {
						existingDistrictCard.setBackground(districtCard.getBackground());
					}
				}
			}
		}
	}
	
	private void updateGridSize(int cardCount) {
		float ratio = DistrictCard.DEFAULT_RATIO * (1 / DistrictCard.DEFAULT_OVERLAP_FACTOR) * (dimension.y / dimension.x);
		
		int newRowCount = 0;
		int newColumnCount = 0;
		
		while(newRowCount * newColumnCount < cardCount) {
			newColumnCount++;
			newRowCount = (int) Math.ceil(newColumnCount * ratio);
		}
		
		while(newRowCount * newColumnCount - newRowCount >= cardCount) {
			newColumnCount--;
		}
		
		rowCount = Math.max(newRowCount, 1);
		columnCount = Math.max(newColumnCount, 1);
	}
	
	private void buildGrid(PApplet p, DistrictCard newDistrictCard) {
		float columnWidth = dimension.x / (columnCount * 1f);
		float rowHeight = columnWidth / DistrictCard.DEFAULT_RATIO * DistrictCard.DEFAULT_OVERLAP_FACTOR;
		
		List<DistrictCard> allDistrictCards = this.getAllDistrictCards();
		allDistrictCards.add(newDistrictCard);
		
		int totalGap = rowCount * columnCount - allDistrictCards.size();
		int leftGap = (int) (totalGap / 2f);
		int rightGap = totalGap - leftGap;
		
		entities.clear();
		
		for (int column = 0; column < columnCount && !allDistrictCards.isEmpty(); column++) {
			DistrictCardColumn districtCardColumn = new DistrictCardColumn();
			entities.add(districtCardColumn);
			
			int startRow = 0;
			
			if(column == 0) {
				startRow = leftGap;
			} else if(column == columnCount - 1) {
				startRow = rightGap;
			}
			
			this.buildColumn(p, columnWidth, rowHeight, allDistrictCards, column, districtCardColumn, startRow);
		}
	}

	private void buildColumn(PApplet p, float columnWidth, float rowHeight, List<DistrictCard> allDistrictCards,
			int column, DistrictCardColumn districtCardColumn, int startRow) {
		for (int row = startRow; row < rowCount && !allDistrictCards.isEmpty(); row++) {
			float cardHeight = rowHeight * (1 / DistrictCard.DEFAULT_OVERLAP_FACTOR);
			float cardWidth = cardHeight * DistrictCard.DEFAULT_RATIO;
			
			float cardPosX = position.x + column * columnWidth;
			float cardPosY = position.y + (row - 1) * rowHeight;
			
			DistrictCard districtCard = allDistrictCards.remove(0);
			
			if(districtCard != null) {
				districtCard.setPosition(new PVector(cardPosX, cardPosY));
				districtCard.setDimension(new PVector(cardWidth, cardHeight));
				
				districtCardColumn.addOrUpdateTrayEntity(p, districtCard);
			}
		}
	}
	
	public void exposeDistrictCards(PApplet p) {
		this.toggleDistrictCardsExposition(p, true);
	}
	
	public void disposeDistrictCards(PApplet p) {
		this.toggleDistrictCardsExposition(p, false);
	}
	
	public void toggleDistrictCardsExposition(PApplet p, boolean expose) {
		float columnsWidth = p.width / (columnCount * 1f);
		float rowsHeight = p.height / (rowCount * 1f);
		
		float maxOdds = this.maxDistrictsOdds();
		
		for (int column = 0; column < columnCount; column++) {
			TrayEntity containerTrayEntity = entities.get(column);
			
			if (containerTrayEntity instanceof DistrictCardColumn) {
				DistrictCardColumn cardColumn = (DistrictCardColumn) containerTrayEntity;
				int startRow = rowCount - cardColumn.trayEntityCount();
				
				for (int row = 0; row < cardColumn.trayEntityCount(); row++) {
					TrayEntity objectTrayEntity = cardColumn.getTrayEntity(row);
					
					float realRow = startRow + row;
					
					if(objectTrayEntity instanceof DistrictCard) {
						DistrictCard districtCard = (DistrictCard) objectTrayEntity;
					
						float targetCardPosX = 0;
						float targetCardPosY = 0;
						
						int animationOrder = 0;
						
						int districtOddsTint = this.districtOddsToTint(districtCard.getDistrictOdds(), maxOdds);
						
						if(expose) {
							targetCardPosX = column * columnsWidth;
							targetCardPosY = (realRow + 0.12f) * rowsHeight;
							
							animationOrder = (int) ((realRow * columnCount) * 0.5f + column * 1.5f);
							int animationDelay = animationOrder * 14;

							districtCard.expose(targetCardPosX, targetCardPosY, animationDelay);
							districtCard.setOddsTint(districtOddsTint);
						} else {
							PVector targetCardPosition = this.overlappedDistrictCardPosition(column, realRow);
							
							targetCardPosX = targetCardPosition.x;
							targetCardPosY = targetCardPosition.y;
							
							animationOrder = (int) (((rowCount - realRow) * columnCount) * 0.5f + (columnCount - column) * 1.5f);
							int animationDelay = animationOrder * 14;

							districtCard.dispose(targetCardPosX, targetCardPosY, animationDelay);
							districtCard.setOddsTint(districtOddsTint);
						}
					}
				}
			}
		}
	}
	
	private PVector overlappedDistrictCardPosition(int column, float row) {
		float columnWidth = dimension.x / (columnCount * 1f);
		float rowHeight = columnWidth / DistrictCard.DEFAULT_RATIO * DistrictCard.DEFAULT_OVERLAP_FACTOR;
		
		float cardPosX = position.x + column * columnWidth;
		float cardPosY = position.y + (row - 0.5f) * rowHeight;
		
		return new PVector(cardPosX, cardPosY);
	}
	
	private float maxDistrictsOdds() {
		float maxOdds = 0;
		
		for (DistrictCard districtCard : this.getAllDistrictCards()) {
			float districtOdds = districtCard.getDistrictOdds(); 
			
			if(districtOdds > maxOdds) {
				maxOdds = districtOdds;
			}
		}
		
		return maxOdds;
	}
	
	private int districtOddsToTint(float odds, float maxOdds) {
		return PApplet.floor(PApplet.map(odds, 0, maxOdds, ODDS_TINT_RANGE.x, ODDS_TINT_RANGE.y));
	}
	
	@Override
	public synchronized void animate(PApplet p) {
		this.removeUselessCoinTokens();
		Iterator<TrayEntity> trayEntityIterator = entities.iterator();
		
		for (; trayEntityIterator.hasNext();) {
			TrayEntity trayEntity = trayEntityIterator.next();
			trayEntity.animate(p);
		}
	}
	
	@Override
	public synchronized void draw(PApplet p) {
		List<DistrictCard> cardRow = new LinkedList<DistrictCard>(); 
		
		int rowNumber = 0;
		
		do {
			cardRow = this.getRow(rowNumber);
			
			for (int i = 0; i < cardRow.size(); i++) {
				DistrictCard districtCard = cardRow.get(i);
				
				districtCard.animate(p);
				districtCard.draw(p);
				
				if(districtCard.coinTokenCount() == 0 && districtCard.isDeployed()) {
					districtCard.retract();
				}
			}
			
			rowNumber++;
		} while(!cardRow.isEmpty());
	}

	@Override
	public TrayEntity getTrayEntity(String districtIdentifier) {
		TrayEntity districtCard = null;
		for(Iterator<TrayEntity> cardColumnIterator = entities.iterator(); cardColumnIterator.hasNext() && districtCard == null;) {
			TrayEntity trayEntity = cardColumnIterator.next();
			
			if(trayEntity instanceof DistrictCardColumn) {
				DistrictCardColumn cardColumn = (DistrictCardColumn) trayEntity;
				districtCard = cardColumn.getTrayEntity(districtIdentifier);
			}
		}
		
		return districtCard;
	}
	
	public List<DistrictCard> getAllDistrictCards() {
		List<DistrictCard> districtCards = new LinkedList<DistrictCard>();
		
		for (TrayEntity trayEntity : entities) {
			if (trayEntity instanceof DistrictCardColumn) {
				DistrictCardColumn cardColumn = (DistrictCardColumn) trayEntity;
				
				for (int i = 0; i < cardColumn.trayEntityCount(); i++) {
					TrayEntity childTrayEntity = cardColumn.getTrayEntity(i);
					
					if (childTrayEntity instanceof DistrictCard) {
						DistrictCard districtCard = (DistrictCard) childTrayEntity;
						districtCards.add(districtCard);
					}
				}
			}
		}
		
		return districtCards;
	}
	
	public DistrictCard getRandomDistrictCard() {
		if(!entities.isEmpty()) {
			TrayEntity trayEntity = entities.get((int) (Math.random() * columnCount));
			if (trayEntity instanceof DistrictCardColumn) {
				DistrictCardColumn cardColumn = (DistrictCardColumn) trayEntity;
				return cardColumn.getRandomDistrictCard();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public List<DistrictCard> getRow(int rowNumber) {
		List<DistrictCard> rowEntities = new LinkedList<>();
		
		for (TrayEntity trayEntity : entities) {
			if (trayEntity instanceof DistrictCardColumn) {
				DistrictCardColumn cardColumn = (DistrictCardColumn) trayEntity;
				
				if(rowNumber < cardColumn.trayEntityCount()) {
					TrayEntity childTrayEntity = cardColumn.getTrayEntity(rowNumber);
					
					if(childTrayEntity instanceof DistrictCard) {
						rowEntities.add((DistrictCard) childTrayEntity);
					}
				}
			}
		}
		
		return rowEntities;
	}
	
	public int totalCardCount() {
		int districtCardCount = 0;
		
		for (TrayEntity trayEntity : entities) {
			if (trayEntity instanceof DistrictCardColumn) {
				DistrictCardColumn cardColumn = (DistrictCardColumn) trayEntity;
				districtCardCount += cardColumn.trayEntityCount();
			}
		}
		
		return districtCardCount;
	}

	public void deployCard(DistrictCard districtCard) {
		Point cardCoordinate = this.getCardCoordinates(districtCard);
		
		if(cardCoordinate != null) {
			TrayEntity trayEntity = entities.get(cardCoordinate.y);
			
			if(trayEntity instanceof DistrictCardColumn) {
				DistrictCardColumn cardColumn = (DistrictCardColumn) trayEntity;
				cardColumn.deployCard(districtCard, cardCoordinate.x);
			}
		}
	}
	
	public void retractCard(DistrictCard districtCard) {
		districtCard.retract();
	}
	
	public Point getCardCoordinates(DistrictCard districtCard) {
		Point cardCorrdinate = null;
		
		int currentColumn = 0;
		for (Iterator<TrayEntity> trayEntityIterator = entities.iterator(); trayEntityIterator.hasNext() && cardCorrdinate == null;) {
			TrayEntity trayEntity = (TrayEntity) trayEntityIterator.next();
		
			if (trayEntity instanceof DistrictCardColumn) {
				DistrictCardColumn cardColumn = (DistrictCardColumn) trayEntity;
				int rowIndex = cardColumn.getCardIndex(districtCard);
				
				if(rowIndex >= 0) {
					cardCorrdinate = new Point(rowIndex, currentColumn);
				}
			}
			
			currentColumn++;
		}
		
		return cardCorrdinate;
	}
	
	private void removeUselessCoinTokens() {
		for (TrayEntity trayEntity : entities) {
			if (trayEntity instanceof DistrictCardColumn) {
				DistrictCardColumn cardColumn = (DistrictCardColumn) trayEntity;
				cardColumn.removeUselessCoinTokens();
			}
		}
	}
}
