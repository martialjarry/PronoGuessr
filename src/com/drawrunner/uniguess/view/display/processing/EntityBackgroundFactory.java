package com.drawrunner.uniguess.view.display.processing;

import java.awt.Color;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;
import processing.core.PImage;

public class EntityBackgroundFactory {
	private EntityBackgroundFactory() {}
	
	public static EntityBackgroundFactory getInstance() {
		return BackgroundFactoryHolder.instance;
	}
	
	public PGraphics createPlayerTokenGeneratedBackground(PApplet p, PFont ppFont, String playerName) {
		int backgroundWidth = PlayerToken.DEFAULT_SIZE;
		int backgroundHeight = PlayerToken.DEFAULT_SIZE;
		
		int tint = (int) (Math.random() * 255f);
		
		PGraphics tokenBackground = p.createGraphics(backgroundWidth, backgroundHeight);
		
		tokenBackground.beginDraw();
		
		tokenBackground.noStroke();
		
		tokenBackground.ellipseMode(PApplet.CORNER);
		tokenBackground.fill(Color.HSBtoRGB(tint / 255f, 0.7f, 0.85f));
		tokenBackground.ellipse(0, 0, tokenBackground.width, tokenBackground.height);
		
		tokenBackground.fill(255);
		tokenBackground.textFont(ppFont, 48);
		tokenBackground.textAlign(PApplet.CENTER, PApplet.CENTER);
		tokenBackground.text(playerName.toUpperCase().charAt(0) + "", 0, 0, backgroundWidth, backgroundHeight);
		
		tokenBackground.endDraw();
		
		return tokenBackground;
	}
	
	public PGraphics createPlayerTokenCustomBackground(PApplet p, PImage playerLogo) {
		int backgroundWidth = PlayerToken.DEFAULT_SIZE;
		int backgroundHeight = PlayerToken.DEFAULT_SIZE;
		
		PGraphics tokenBackground = p.createGraphics(backgroundWidth, backgroundHeight);
		PGraphics backgroundTexture = this.generateBackgroundTexture(p, playerLogo, backgroundWidth, backgroundHeight);
		
		tokenBackground.beginDraw();
		
		tokenBackground.noStroke();
		tokenBackground.background(0);
		
		tokenBackground.ellipseMode(PApplet.CORNER);
		tokenBackground.fill(255);
		tokenBackground.ellipse(0, 0, tokenBackground.width, tokenBackground.height);
		
		backgroundTexture.loadPixels();
		tokenBackground.loadPixels();
		
		for(int i = 0; i < tokenBackground.pixels.length; i++) {
			Color backgroundPixelColor = new Color(tokenBackground.pixels[i]);
			int backgroundPixelLevel = backgroundPixelColor.getRed();
			
			if(backgroundPixelLevel > 0) {
				Color logoPixelColor = new Color(backgroundTexture.pixels[i]);
				
				Color newPixelColor = new Color(
						logoPixelColor.getRed() / 255f,
						logoPixelColor.getGreen() / 255f,
						logoPixelColor.getBlue() / 255f,
						backgroundPixelLevel / 255f);
				
				tokenBackground.pixels[i] = newPixelColor.getRGB();
			} else {
				Color invisibleColor = new Color(0, 0, 0, 0); 
				tokenBackground.pixels[i] = invisibleColor.getRGB();
			}
		}
		
		tokenBackground.updatePixels();
		tokenBackground.endDraw();
		
		return tokenBackground;
	}

	public PGraphics createDistrictCardBackground(PApplet p, PImage districtFlag) {
		int backgroundWidth = DistrictCard.DEFAULT_WIDTH;
		int backgroundHeight = DistrictCard.DEFAULT_HEIGHT;
		
		PGraphics flagBackground = p.createGraphics(backgroundWidth, backgroundHeight);
		PGraphics backgroundTexture = this.generateBackgroundTexture(p, districtFlag, backgroundWidth, backgroundHeight);
		
		flagBackground.beginDraw();
		
		flagBackground.noStroke();
		flagBackground.background(0);
		flagBackground.fill(255);
		flagBackground.rectMode(PApplet.CORNER);
		flagBackground.rect(0, 0, flagBackground.width, flagBackground.height, flagBackground.height * 0.1f);
		
		backgroundTexture.loadPixels();
		flagBackground.loadPixels();
		
		for(int i = 0; i < flagBackground.pixels.length; i++) {
			Color backgroundPixelColor = new Color(flagBackground.pixels[i]);
			int backgroundPixelLevel = backgroundPixelColor.getRed();
			
			if(backgroundPixelLevel > 0) {
				Color logoPixelColor = new Color(backgroundTexture.pixels[i]);
				
				Color newPixelColor = new Color(
						logoPixelColor.getRed() / 255f,
						logoPixelColor.getGreen() / 255f,
						logoPixelColor.getBlue() / 255f,
						backgroundPixelLevel / 255f);
				
				flagBackground.pixels[i] = newPixelColor.getRGB();
			} else {
				Color invisibleColor = new Color(0, 0, 0, 0); 
				flagBackground.pixels[i] = invisibleColor.getRGB();
			}
		}
		
		flagBackground.updatePixels();
		flagBackground.endDraw();
		
		return flagBackground;
	}
	
	private PGraphics generateBackgroundTexture(PApplet p, PImage image, int width, int height) {
		PGraphics backgroundTexture = p.createGraphics(width, height);
		
		backgroundTexture.beginDraw();
		backgroundTexture.noStroke();
		backgroundTexture.imageMode(PApplet.CORNER);
		backgroundTexture.image(image, 0, 0, width, height);
		backgroundTexture.endDraw();
		
		return backgroundTexture;
	}
	
	private static class BackgroundFactoryHolder {
		private final static EntityBackgroundFactory instance = new EntityBackgroundFactory(); 
	}
}
