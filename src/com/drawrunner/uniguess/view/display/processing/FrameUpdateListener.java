package com.drawrunner.uniguess.view.display.processing;

public interface FrameUpdateListener {
	public abstract void onFrameUpdated();
}
