package com.drawrunner.uniguess.view.display.processing;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;

import com.drawrunner.uniguess.view.animation.LinearAnimation;
import com.drawrunner.uniguess.view.animation.SquareInAnimation;
import com.drawrunner.uniguess.view.animation.SquareOutAnimation;
import com.drawrunner.uniguess.view.animation.ValueAnimation;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;
import processing.core.PVector;

public class LoadingScreen extends TrayEntity{
	private enum LoadingIconJumpStates { IDLE, UP, DOWN }
	private LoadingIconJumpStates loadingIconJumpState;
	
	private PImage headerLoadingIcon;
	private PImage innerLoadingIcon;
	
	private PFont loadingFont;
	
	private ValueAnimation loadingInnerIconRotationAnimation;
	
	private ValueAnimation loadingIconDownAnimation;
	private ValueAnimation loadingIconUpAnimation;
	
	private long lastLoadingInnerIconJumpTime;
	
	private int loadedMongoPlayersCount;
	private int totalMongoPlayersCount;
	
	private int loadedTwitchPlayersCount;
	private int totalTwitchPlayersCount;
	
	private int loadedMongoDistrictsCount;
	private int totalMongoDistrictsCount;
	
	private float progressRate;
	
	private ActionListener loadingListener;
	
	public LoadingScreen(Point size) {
		position = new PVector(0, 0);
		dimension = new PVector(size.x, size.y);
		
		loadedMongoPlayersCount = 0;
		totalMongoPlayersCount = -1;
		
		loadedTwitchPlayersCount = 0;
		totalTwitchPlayersCount = -1;
		
		loadedMongoDistrictsCount = 0;
		totalMongoDistrictsCount = -1;
		
		loadingInnerIconRotationAnimation = new LinearAnimation(700, TimeUnit.MILLISECONDS, 0, 360);
		
		loadingIconJumpState = LoadingIconJumpStates.IDLE;
		loadingIconUpAnimation = new SquareInAnimation(100, TimeUnit.MILLISECONDS);
		loadingIconDownAnimation = new SquareOutAnimation(100, TimeUnit.MILLISECONDS);
		lastLoadingInnerIconJumpTime = 0;
		
		progressRate = 0;
		
		loadingListener = null;
	}
	
	public void setIconsAndFonts(PImage headerLoadingIcon, PImage innerLoadingIcon, PFont loadingFont) {
		this.headerLoadingIcon = headerLoadingIcon;
		this.innerLoadingIcon = innerLoadingIcon;
		
		this.loadingFont = loadingFont;
	}
	
	@Override
	public void animate(PApplet p) {
		this.updateProgressRate();
	}

	public void updateProgressRate() {
		float loadedEntitiesCount = loadedMongoPlayersCount + loadedTwitchPlayersCount + loadedMongoDistrictsCount;
		float totalEntitiesCount = totalMongoPlayersCount + totalTwitchPlayersCount + totalMongoDistrictsCount;
		
		progressRate = loadedEntitiesCount / totalEntitiesCount;
	}
	
	@Override
	public void draw(PApplet p) {
		p.fill(48);
		p.rect(position.x, position.y, dimension.x, dimension.y);
		
		p.textFont(loadingFont, dimension.x * 0.045f);
		
		float headerIconRatio = headerLoadingIcon.width / (headerLoadingIcon.height * 1f);
		float headerIconWidth = dimension.x * 0.1f;
		float headerIconHeight = headerIconWidth / headerIconRatio;
		
		float headerRotatingIconRatio = innerLoadingIcon.width / (innerLoadingIcon.height * 1f);
		float headerRotatingIconWidth = headerIconWidth * 0.48f;
		float headerRotatingIconHeight = headerRotatingIconWidth / headerRotatingIconRatio;
		
		String loadingHeaderTitle = "Chargement des\ndonn�es du jeu"; 
		float headerTitleWidth = p.textWidth(loadingHeaderTitle);
		
		float headerInnerMarginX = dimension.x * 0.03f;
		
		float headerWidth = headerIconWidth + headerInnerMarginX + headerTitleWidth;
		float headerPosX = dimension.x * 0.5f - headerWidth * 0.5f;
		float headerPosY = dimension.y * 0.3f;
		
		float headerIconPosY = headerPosY - dimension.y * 0.036f;
		
		// Positionnement centr�
		float headerRotatingIconPosX = headerPosX + headerIconWidth * 0.5f + 1;
		float headerRotatingIconPosY = headerIconPosY + headerIconWidth * 0.5f;
		
		float headerIconsShiftY = this.drawLoadingIcon();
		
		p.tint(255, 240);
		
		this.drawHeaderLoadingIcon(p, headerIconWidth, headerIconHeight, headerPosX, headerIconPosY, headerIconsShiftY);
		this.drawLoadingIcons(p, headerRotatingIconWidth, headerRotatingIconHeight, headerRotatingIconPosX, headerRotatingIconPosY, headerIconsShiftY);
		this.drawHeaderTitle(p, headerIconWidth, loadingHeaderTitle, headerInnerMarginX, headerPosX, headerPosY);
		
		p.noTint();
		
		float progressBarWidth = dimension.x * 0.75f;
		float progressBarHeight = dimension.y * 0.05f;
		
		float progressBarPosY = this.drawProgressBar(p, headerPosY, headerIconPosY, progressBarWidth, progressBarHeight);
		
		this.drawSkipButton(p, progressBarHeight, progressBarPosY);
	}

	private float drawLoadingIcon() {
		float headerIconsShiftY = 0;
		
		switch (loadingIconJumpState) {
		case IDLE:
			if((System.nanoTime() - lastLoadingInnerIconJumpTime) / 1000000000f > 1.5f) {
				loadingIconUpAnimation.setStartValue(0);
				loadingIconUpAnimation.setEndValue(dimension.y * 0.1f);
				loadingIconUpAnimation.setDuration(120);
				loadingIconUpAnimation.start();
				
				loadingIconJumpState = LoadingIconJumpStates.UP;
			}
			
			break;
		case UP:
			if(!loadingIconUpAnimation.isAnimating()) {
				loadingIconDownAnimation.setStartValue(loadingIconUpAnimation.getEndValue());
				loadingIconDownAnimation.setEndValue(0);
				loadingIconDownAnimation.setDuration(loadingIconUpAnimation.getDuration());
				loadingIconDownAnimation.start();
				
				loadingIconJumpState = LoadingIconJumpStates.DOWN;
			}
			
			headerIconsShiftY = (float) loadingIconUpAnimation.nextValue();
			break;
		case DOWN:
			if(!loadingIconDownAnimation.isAnimating()) {
				if(loadingIconDownAnimation.getStartValue() > dimension.y * 0.02f) {
					loadingIconUpAnimation.setStartValue(0);
					loadingIconUpAnimation.setEndValue(loadingIconUpAnimation.getEndValue() * 0.5f);
					loadingIconUpAnimation.setDuration((long) (loadingIconDownAnimation.getDuration() * 0.8f));
					loadingIconUpAnimation.start();
					
					loadingIconJumpState = LoadingIconJumpStates.UP;
				} else {
					lastLoadingInnerIconJumpTime = System.nanoTime();
					loadingIconJumpState = LoadingIconJumpStates.IDLE;
				}
			}
			
			headerIconsShiftY = (float) loadingIconDownAnimation.nextValue();
			break;
		}
		return headerIconsShiftY;
	}
	
	private void drawHeaderLoadingIcon(PApplet p, float headerIconWidth, float headerIconHeight, float headerPosX,
			float headerIconPosY, float headerIconsShiftY) {
		p.fill(255, 240);
		
		p.imageMode(PApplet.CORNER);
		p.image(headerLoadingIcon, headerPosX, headerIconPosY - headerIconsShiftY, headerIconWidth, headerIconHeight);
	}
	
	private void drawLoadingIcons(PApplet p, float headerRotatingIconWidth, float headerRotatingIconHeight,
			float headerRotatingIconPosX, float headerRotatingIconPosY, float headerIconsShiftY) {
		if(!loadingInnerIconRotationAnimation.isAnimating()) {
			loadingInnerIconRotationAnimation.start();
		}
		
		float rotatingIconAngle = (float) loadingInnerIconRotationAnimation.nextValue();
		
		p.pushMatrix();
		p.translate(headerRotatingIconPosX, headerRotatingIconPosY - headerIconsShiftY);
		p.rotate(PApplet.radians(rotatingIconAngle));
		p.imageMode(PApplet.CENTER);
		
		p.colorMode(PApplet.HSB);
		
		p.tint(181, 56, 255);
		p.image(innerLoadingIcon, 0, 0, headerRotatingIconWidth, headerRotatingIconHeight);
		p.popMatrix();
		
		p.colorMode(PApplet.RGB);
	}
	
	private void drawHeaderTitle(PApplet p, float headerIconWidth, String loadingHeaderTitle,
			float headerInnerMarginX, float headerPosX, float headerPosY) {
		p.textAlign(PApplet.LEFT, PApplet.TOP);
		p.text(loadingHeaderTitle, headerPosX + headerIconWidth + headerInnerMarginX, headerPosY);
	}

	private float drawProgressBar(PApplet p, float headerPosY, float headerIconPosY, float progressBarWidth,
			float progressBarHeight) {
		float progressBarPosX = dimension.x * 0.5f - progressBarWidth * 0.5f;
		float progressBarPosY = headerPosY + headerIconPosY + 12;
		
		p.textFont(loadingFont, dimension.x * 0.024f);

		float progressBarMinWidth = p.textWidth("10%") * 1.2f;
		float progressBarSliderWidth = progressBarMinWidth;

		if(totalMongoPlayersCount >= 0 && totalTwitchPlayersCount >= 0 && totalMongoDistrictsCount >= 0) {
			progressBarSliderWidth = PApplet.map(progressRate, 0, 1, 0, progressBarWidth);
			progressBarSliderWidth = PApplet.max(progressBarSliderWidth, progressBarMinWidth);
			progressBarSliderWidth = PApplet.min(progressBarSliderWidth, progressBarWidth);
		}
		
		p.colorMode(PApplet.HSB);
		p.stroke(181, 64, 255, 255);
		p.strokeWeight(dimension.x * 0.0035f);
		p.noFill();
		p.rect(progressBarPosX, progressBarPosY, progressBarWidth, progressBarHeight, progressBarHeight * 0.5f);
		
		p.noStroke();
		p.fill(181, 64, 255, 255);
		p.rect(progressBarPosX, progressBarPosY, progressBarSliderWidth, progressBarHeight, progressBarHeight * 0.5f);
		
		p.colorMode(PApplet.RGB);
		
		String progressRateText = PApplet.floor(progressRate * 100) + "%";
		
		p.textAlign(PApplet.CENTER, PApplet.CENTER);
		p.fill(48);
		p.text(progressRateText, progressBarPosX + progressBarSliderWidth * 0.5f, progressBarPosY + progressBarHeight * 0.4f);
		return progressBarPosY;
	}

	private void drawSkipButton(PApplet p, float progressBarHeight, float progressBarPosY) {
		p.textFont(loadingFont, dimension.x * 0.036f);
		
		float skipButtonWidth = dimension.x * 0.5f;
		float skipButtonHeight = dimension.y * 0.12f;
		
		float skipButtonPosX = dimension.x * 0.5f - skipButtonWidth * 0.5f;
		float skipButtonPosY = progressBarPosY + progressBarHeight + 64;
		
		String skipButtonText = "Charger en arri�re-plan";
		
		if(loadedMongoPlayersCount == totalMongoPlayersCount && loadedMongoDistrictsCount == totalMongoDistrictsCount
		&& totalMongoPlayersCount > 0 && totalMongoDistrictsCount > 0) {
			p.strokeWeight(dimension.x * 0.003f);
			
			if(p.mouseX >= skipButtonPosX && p.mouseX < skipButtonPosX + skipButtonWidth
			&& p.mouseY >= skipButtonPosY && p.mouseY < skipButtonPosY + skipButtonHeight) {
				
				if(p.mousePressed) {
					p.fill(255, 240);
					p.stroke(255, 240);
					p.rect(skipButtonPosX, skipButtonPosY, skipButtonWidth, skipButtonHeight);
					
					p.fill(48);
					p.text(skipButtonText, skipButtonPosX + skipButtonWidth * 0.5f, skipButtonPosY + skipButtonHeight * 0.4f);
					
					if(loadingListener != null) {
						loadingListener.actionPerformed(new ActionEvent(this, 0, null));
					}
				} else {
					p.fill(255, 204);
					p.stroke(255, 204);
					p.rect(skipButtonPosX, skipButtonPosY, skipButtonWidth, skipButtonHeight);
					
					p.fill(48);
					p.text(skipButtonText, skipButtonPosX + skipButtonWidth * 0.5f, skipButtonPosY + skipButtonHeight * 0.4f);
				}
			} else {
				p.noFill();
				p.stroke(255, 240);
				p.rect(skipButtonPosX, skipButtonPosY, skipButtonWidth, skipButtonHeight);
				
				p.fill(255, 240);
				p.text(skipButtonText, skipButtonPosX + skipButtonWidth * 0.5f, skipButtonPosY + skipButtonHeight * 0.4f);
			}
		}
		
		p.noStroke();
	}
	
	public boolean hasLoadedAllData() {
		return progressRate == 1
			&& totalMongoPlayersCount >= 0
			&& totalTwitchPlayersCount >= 0
			&& totalMongoDistrictsCount >= 0;
	}
	
	public int getLoadedMongoPlayersCount() {
		return loadedMongoPlayersCount;
	}

	public void setLoadedMongoPlayersCount(int loadedMongoPlayersCount) {
		this.loadedMongoPlayersCount = loadedMongoPlayersCount;
	}

	public int getTotalMongoPlayersCount() {
		return totalMongoPlayersCount;
	}

	public void setTotalMongoPlayersCount(int totalMongoPlayersCount) {
		this.totalMongoPlayersCount = totalMongoPlayersCount;
	}

	public int getLoadedTwitchPlayersCount() {
		return loadedTwitchPlayersCount;
	}

	public void setLoadedTwitchPlayersCount(int loadedTwitchPlayersCount) {
		this.loadedTwitchPlayersCount = loadedTwitchPlayersCount;
	}

	public int getTotalTwitchPlayersCount() {
		return totalTwitchPlayersCount;
	}

	public void setTotalTwitchPlayersCount(int totalTwitchPlayersCount) {
		this.totalTwitchPlayersCount = totalTwitchPlayersCount;
	}

	public int getLoadedMongoDistrictsCount() {
		return loadedMongoDistrictsCount;
	}

	public void setLoadedMongoDistrictsCount(int loadedMongoDistrictsCount) {
		this.loadedMongoDistrictsCount = loadedMongoDistrictsCount;
	}

	public int getTotalMongoDistrictsCount() {
		return totalMongoDistrictsCount;
	}

	public void setTotalMongoDistrictsCount(int totalMongoDistrictsCount) {
		this.totalMongoDistrictsCount = totalMongoDistrictsCount;
	}

	public float getProgressRate() {
		return progressRate;
	}
	
	public void setLoadingListener(ActionListener loadingListener) {
		this.loadingListener = loadingListener;
	}
}
