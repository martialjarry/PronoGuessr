package com.drawrunner.uniguess.view.display.processing;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.drawrunner.uniguess.view.animation.EaseInAnimation;
import com.drawrunner.uniguess.view.animation.EaseOutAnimation;
import com.drawrunner.uniguess.view.animation.LinearAnimation;
import com.drawrunner.uniguess.view.animation.ValueAnimation;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;

public class PlayerToken extends TrayEntity {
	public final static int DEFAULT_SIZE = 64; 
	public final static long PRONO_ANIMATION_DURATION = 1700;
	public final static long PRONO_ANIMATION_DURATION_NETRACK = 3000;
	public final static long FADE_DURATION = 300;
	public final static PVector LABEL_MARGINS = new PVector(5, 3);
	
	private enum PlayerTokenStates { WAITING, INCOMING, OUTGOING }
	private PlayerTokenStates playerTokenState;
	
	public enum LogoStatus { NO_LOGO, GENERATED_LOGO, USER_LOGO }
	private LogoStatus logoStatus;
	
	private String playerName;
	private String playerDisplayName;
	
	private int opacity;
	
	private ValueAnimation posXAnimator;
	
	private ValueAnimation posYStartAnimator;
	private ValueAnimation posYEndAnimator;
	
	private ValueAnimation opacityAnimator;
	
	public PlayerToken(PGraphics playerLogo, PImage tokenShadow, String playerName, String playerDisplayName, PFont playerLabelFont) {
		super(playerLogo, tokenShadow, playerLabelFont);

		isVisible = false;
		
		playerTokenState = PlayerTokenStates.WAITING;
		
		logoStatus = LogoStatus.NO_LOGO; 
		
		if(playerName.toLowerCase().contains("netrak13")) {
			posXAnimator = new LinearAnimation(PRONO_ANIMATION_DURATION_NETRACK, TimeUnit.MILLISECONDS);
			
			posYStartAnimator = new EaseOutAnimation(PRONO_ANIMATION_DURATION_NETRACK / 2, TimeUnit.MILLISECONDS);
			posYEndAnimator = new EaseInAnimation(PRONO_ANIMATION_DURATION_NETRACK / 2, TimeUnit.MILLISECONDS);
		} else {
			posXAnimator = new LinearAnimation(PRONO_ANIMATION_DURATION, TimeUnit.MILLISECONDS);
			
			posYStartAnimator = new EaseOutAnimation(PRONO_ANIMATION_DURATION / 2, TimeUnit.MILLISECONDS);
			posYEndAnimator = new EaseInAnimation(PRONO_ANIMATION_DURATION / 2, TimeUnit.MILLISECONDS);
		}
		
		opacityAnimator = new LinearAnimation(FADE_DURATION, TimeUnit.MILLISECONDS);
		
		this.playerName = playerName;
		this.playerDisplayName = playerDisplayName;
		
		opacity = 0;
		
		position = new PVector(-DEFAULT_SIZE, -DEFAULT_SIZE);
		dimension = new PVector(DEFAULT_SIZE, DEFAULT_SIZE);
	}

	public List<CoinToken> makeProno(PApplet p, DistrictCard targetDistrictCard, int coinAmount) {
		double startPosX = Math.random() * (p.width / 2f - DEFAULT_SIZE);
		double endPosY = (p.height * 0.3f) + (Math.random() * (p.height * 0.1f));
		
		posXAnimator.setStartValue(startPosX);
		posXAnimator.setEndValue(startPosX + p.width / 2f);
		
		posYStartAnimator.setStartValue(-DEFAULT_SIZE);
		posYStartAnimator.setEndValue(endPosY);
		
		posYEndAnimator.setStartValue(posYStartAnimator.getEndValue());
		posYEndAnimator.setEndValue(posYStartAnimator.getStartValue());
		posYEndAnimator.setEndTime(posYEndAnimator.getEndTime());
		
		posXAnimator.start();
		posYStartAnimator.start();
		
		opacity = 0;
		
		opacityAnimator.setStartValue(opacity);
		opacityAnimator.setEndValue(255);
		opacityAnimator.start();
		
		List<CoinToken> coinTokens = this.transferCoins(p, targetDistrictCard, coinAmount);
		
		isVisible = true;
		playerTokenState = PlayerTokenStates.INCOMING;
		
		return coinTokens;
	}
	
	private List<CoinToken> transferCoins(PApplet p, DistrictCard targetDistrictCard, int coinAmount) {
		List<CoinToken> coinTokens = new ArrayList<CoinToken>();
		
		CoinTokenFactory coinTokenFactory = CoinTokenFactory.getInstance();
		
		int remainingTokens = coinAmount;
		
		while(remainingTokens > 0) {
			CoinToken newToken = null;
			
			if(remainingTokens - 25 >= 0) {
				newToken = coinTokenFactory.createCoinToken25(p);
				remainingTokens -= 25;
			} else if(remainingTokens - 15 >= 0) {
				newToken = coinTokenFactory.createCoinToken15(p);
				remainingTokens -= 15;
			} else if(remainingTokens - 10 >= 0) {
				newToken = coinTokenFactory.createCoinToken10(p);
				remainingTokens -= 10;
			} else if(remainingTokens - 5 >= 0) {
				newToken = coinTokenFactory.createCoinToken5(p);
				remainingTokens -= 5;
			} else if(remainingTokens - 3 >= 0) {
				newToken = coinTokenFactory.createCoinToken3(p);
				remainingTokens -= 3;
			} else if(remainingTokens - 1 >= 0) {
				newToken = coinTokenFactory.createCoinToken1(p);
				remainingTokens -= 1;
			}
			
			if(newToken != null) {
				coinTokens.add(newToken);
			}
		}
		
		float transitInterval = (float) Math.max(PRONO_ANIMATION_DURATION * 0.25f / (coinTokens.size() * 1f), 50);
		float transitStartTime = PRONO_ANIMATION_DURATION * 0.2f;
		
		for (int i = 0; i < coinTokens.size(); i++) {
			CoinToken coinToken = coinTokens.get(i);
			coinToken.transitToDistrict(this, targetDistrictCard, transitStartTime + i * transitInterval);
		}
		
		return coinTokens;
	}
	
	@Override
	public void animate(PApplet p) {
		float currentPosX = position.x;
		float currentPosY = position.y;
		
		switch (playerTokenState) {
		case WAITING:
			position = new PVector(-DEFAULT_SIZE, -DEFAULT_SIZE);
			break;
		case INCOMING:
			currentPosX = (float) posXAnimator.nextValue();
			currentPosY = (float) posYStartAnimator.nextValue();
			
			position = new PVector(currentPosX, currentPosY);
			
			opacity = (int) opacityAnimator.nextValue();
			
			if(posYStartAnimator.isFinished()) {
				posYEndAnimator.start();
				
				opacityAnimator.setStartValue(opacity);
				opacityAnimator.setEndValue(0);
				
				long fadeOutStartTime = posYEndAnimator.getEndTime() - FADE_DURATION * 1000000L;
				opacityAnimator.setStartTime(fadeOutStartTime);
				
				playerTokenState = PlayerTokenStates.OUTGOING;
			}
			
			break;
		case OUTGOING:
			currentPosX = (float) posXAnimator.nextValue();
			currentPosY = (float) posYEndAnimator.nextValue();
			
			position = new PVector(currentPosX, currentPosY);
			
			if(opacityAnimator.isAnimating()) {
				opacity = (int) opacityAnimator.nextValue();
			}

			if(!opacityAnimator.isAnimating() && System.nanoTime() > opacityAnimator.getStartTime()) {
				opacityAnimator.start();
			}
			
			if(posYEndAnimator.isFinished()) {
				isVisible = false;
				playerTokenState = PlayerTokenStates.WAITING;
			}
			
			break;
		}
	}
	
	@Override
	public void draw(PApplet p) {
		if(isVisible && this.hasLogo()) {
			this.drawShadow(p, opacity);
			
			float netrakOffset = this.isNetrak13() ? (1 - PApplet.abs(-posXAnimator.nextFraction() * 2 + 1)) : 0;
			
			float logoPosX = position.x + dimension.x * 0.5f;
			float logoPosY = position.y + dimension.y * 0.5f - netrakOffset * 12;
			
			float logoWidth = dimension.x * (1 + netrakOffset);
			float logoHeight = dimension.y * (1 + netrakOffset);
			
			p.imageMode(PApplet.CENTER);
			p.tint(255, opacity);
			p.image(background, logoPosX, logoPosY, logoWidth, logoHeight);
			p.noTint();
			
			p.textFont(labelFont);
			
			String name = playerDisplayName != null ? playerDisplayName : playerName;
			
			float labelWidth = p.textWidth(name) + LABEL_MARGINS.x * 2;
			float labelHeight = labelFont.getSize() * 1.85f + LABEL_MARGINS.y * 2;

			float labelPosX = position.x + dimension.x * 0.5f;
			float labelPosY = position.y + dimension.y + labelHeight * 0.5f + LABEL_MARGINS.y;
			
			p.rectMode(PApplet.CENTER);
			p.fill(0, 127 * (opacity / 255f));
			p.noStroke();
			p.rect(labelPosX, labelPosY, labelWidth, labelHeight, labelHeight);
			
			p.textAlign(PApplet.CENTER, PApplet.CENTER);
			p.fill(255, opacity);
			p.text(name, labelPosX, labelPosY + labelHeight * 0.12f);
		}
	}

	public boolean isNetrak13() {
		return playerName.toLowerCase().contains("netrak13");
	}
	
	@Override
	public boolean equals(Object object) {
		if(object != null && object instanceof PlayerToken) {
			PlayerToken otherPlayerToken = (PlayerToken) object;
			if(playerName != null && otherPlayerToken.getPlayerName() != null) {
				return playerName.equals(otherPlayerToken.getPlayerName());
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public void setLogoAsGenerated() {
		if(logoStatus == LogoStatus.NO_LOGO) {
			logoStatus = LogoStatus.GENERATED_LOGO;
		}
	}
	
	public void setLogoAsUser() {
		if(logoStatus == LogoStatus.NO_LOGO || logoStatus == LogoStatus.GENERATED_LOGO) {
			logoStatus = LogoStatus.USER_LOGO;
		}
	}
	
	public boolean hasNoLogo() {
		return logoStatus == LogoStatus.NO_LOGO || background == null;
	}
	
	public boolean hasGeneratedLogo() {
		return logoStatus == LogoStatus.GENERATED_LOGO && background != null;
	}
	
	public boolean hasUserLogo() {
		return logoStatus == LogoStatus.USER_LOGO && background != null;
	}
	
	public boolean hasLogo() {
		return (logoStatus == LogoStatus.GENERATED_LOGO || logoStatus == LogoStatus.USER_LOGO)
			&& background != null;
	}
	
	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	public String getPlayerDisplayName() {
		return playerDisplayName;
	}

	public void setPlayerDisplayName(String playerDisplayName) {
		this.playerDisplayName = playerDisplayName;
	}
}
