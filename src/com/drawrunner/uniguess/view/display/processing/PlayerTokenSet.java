package com.drawrunner.uniguess.view.display.processing;

import java.util.Iterator;

import processing.core.PApplet;
import processing.core.PVector;

public class PlayerTokenSet extends TrayEntityContainer {
	public PlayerTokenSet(PApplet p) {
		position = new PVector(0, 0);
		dimension = new PVector(p.width, p.height);
	}
	
	@Override
	public void animate(PApplet p) {
		Iterator<TrayEntity> trayEntityIterator = entities.iterator();
		
		for (; trayEntityIterator.hasNext();) {
			TrayEntity playerToken = trayEntityIterator.next();
			playerToken.animate(p);
		}
	}

	@Override
	public void draw(PApplet p) {
		Iterator<TrayEntity> trayEntityIterator = entities.iterator();
		
		for (; trayEntityIterator.hasNext();) {
			TrayEntity playerToken = trayEntityIterator.next();
			playerToken.draw(p);
		}
		
		trayEntityIterator = entities.iterator();
		
		boolean hasNetrackBeenDrawn = false;
		
		for (; trayEntityIterator.hasNext() && !hasNetrackBeenDrawn;) {
			TrayEntity trayEntity = trayEntityIterator.next();
			
			if (trayEntity instanceof PlayerToken) {
				PlayerToken playerToken = (PlayerToken) trayEntity;
				
				if(playerToken.isNetrak13()) {
					playerToken.draw(p);
					hasNetrackBeenDrawn = true;
				}
			}
		}
	}
	
	@Override
	public void addOrUpdateTrayEntity(PApplet p, TrayEntity trayEntity) {
		if (trayEntity instanceof PlayerToken) {
			PlayerToken playerToken = (PlayerToken) trayEntity;
			TrayEntity existingEntity = this.getTrayEntity(playerToken.getPlayerName());
			
			if(existingEntity == null) {
				entities.add(playerToken);
			} else {
				if(existingEntity instanceof PlayerToken) {
					PlayerToken existingPlayerToken = (PlayerToken) existingEntity;
					
					existingPlayerToken.setPlayerName(playerToken.getPlayerName());
					
					if(playerToken.getPlayerDisplayName() != null) {
						existingPlayerToken.setPlayerDisplayName(playerToken.getPlayerDisplayName());
					}
					
					if(this.isPlayerLogoUpdateNeeded(playerToken, existingPlayerToken)) {
						existingPlayerToken.setBackground(playerToken.getBackground());
					}
				}
			}
		}
	}

	private boolean isPlayerLogoUpdateNeeded(PlayerToken playerToken, PlayerToken existingPlayerToken) {
		return (existingPlayerToken.hasNoLogo() && (playerToken.hasGeneratedLogo() || playerToken.hasUserLogo()))
			|| existingPlayerToken.hasGeneratedLogo() && playerToken.hasUserLogo();
	}
	
	@Override
	public TrayEntity getTrayEntity(String playerName) {
		String lowerPlayerName = playerName.toLowerCase();
		
		PlayerToken playerToken = null;
		for (Iterator<TrayEntity> trayEntityIterator = entities.iterator(); trayEntityIterator.hasNext() && playerToken == null;) {
			TrayEntity trayEntity = trayEntityIterator.next();
			
			if(trayEntity instanceof PlayerToken) {
				PlayerToken currentPlayerToken = (PlayerToken) trayEntity;
				
				if(currentPlayerToken.getPlayerName().toLowerCase().equals(lowerPlayerName)) {
					playerToken = currentPlayerToken;
				}
			}
		}
		
		return playerToken;
	}
	
	public PlayerToken getRandomPlayerToken() {
		if(!entities.isEmpty()) {
			TrayEntity trayEntity = entities.get((int) (Math.random() * (entities.size() - 1)));
			
			if (trayEntity instanceof PlayerToken) {
				return (PlayerToken) trayEntity;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
}
