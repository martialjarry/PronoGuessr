package com.drawrunner.uniguess.view.display.processing;

import java.util.Observable;
import java.util.Observer;

import com.drawrunner.uniguess.view.display.DisplayResquestContents;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;
import processing.core.PVector;

public class PronoCounter extends TrayEntity implements Observer {
	private PFont countersFont;
	
	private int countCurrent;
	private int countMax;
	
	private float rateCurrent;
	private float rateMax;
	
	public PronoCounter(PVector position, PGraphics background, PFont regularTextsFont, PFont countersFont) {
		super(background, null, regularTextsFont);
		
		this.position = position;
		dimension = new PVector(background.width, background.height);
		
		this.countersFont = countersFont;
		
		countCurrent = 0;
		countMax = 0;
		
		rateCurrent = 0;
		rateMax = 0;
	}
	
	@Override
	public void animate(PApplet p) {
		
	}

	@Override
	public void draw(PApplet p) {
		if (isVisible) {
			PVector halfSize = new PVector(dimension.x * 0.5f, dimension.y * 0.5f);
			
			p.imageMode(PApplet.CORNER);
			p.image(background, position.x, position.y);
			p.textFont(labelFont, 7);
			p.fill(255, 225);
			
			float leftCounterPosX = position.x + halfSize.x - 16;
			float rightCounterPosX = position.x + halfSize.x + 6;
			
			p.textAlign(PApplet.RIGHT, PApplet.TOP);
			p.text("NB. PRONOS", leftCounterPosX, position.y + 10);
			
			p.textAlign(PApplet.LEFT, PApplet.TOP);
			p.text("PRONOS / SEC", rightCounterPosX, position.y + 10);
			
			p.textFont(countersFont, 44);
			
			String countText = PApplet.floor(countCurrent) + "";
			this.drawDigitCounter(p, countText, -1, position.x + halfSize.x - 16);
			
			String rateText = PApplet.floor(rateCurrent) + "";
//			if(rateCurrent < 100) {
//				rateText = PApplet.round(rateCurrent * 10) / 10f + "";
//			}
			
			this.drawDigitCounter(p, rateText, 1, position.x + halfSize.x + 24);
			
			p.noStroke();
			p.rectMode(PApplet.CORNER);
			p.fill(255, 108);
			p.rect(position.x + halfSize.x - 6, position.y + dimension.y * 0.07f, 2, dimension.y * 0.66f, 1);
			
			p.textFont(labelFont, 6);
			p.fill(255, 225);
			
			p.textAlign(PApplet.RIGHT, PApplet.BOTTOM);
			p.text(countMax, position.x + halfSize.x - 74, position.y + dimension.y - 13);
			
			p.textAlign(PApplet.LEFT, PApplet.BOTTOM);
			p.text(rateMax + "/s", position.x + halfSize.x + 60, position.y + dimension.y - 13);
			
			final float GAUGES_BEVEL_LENGTH = 2;
			
			float maxBarLeftPosX = leftCounterPosX;
			float maxBarLeftPosY = position.y + dimension.y - 19;
			float maxBarLeftWidth = 52;
			float maxBarLeftHeight = 3f;
			
			p.fill(255, 56);
			p.quad(maxBarLeftPosX, maxBarLeftPosY, maxBarLeftPosX - maxBarLeftWidth + GAUGES_BEVEL_LENGTH,
					maxBarLeftPosY, maxBarLeftPosX - maxBarLeftWidth, maxBarLeftPosY + maxBarLeftHeight, maxBarLeftPosX,
					maxBarLeftPosY + maxBarLeftHeight);
			
			float countRatio = countCurrent / (countMax * 1f);
			float rateBarLeftWidth = PApplet.max(maxBarLeftWidth * countRatio, GAUGES_BEVEL_LENGTH);
			
			p.fill(255, 255);
			p.quad(maxBarLeftPosX, maxBarLeftPosY, maxBarLeftPosX - rateBarLeftWidth + GAUGES_BEVEL_LENGTH,
					maxBarLeftPosY, maxBarLeftPosX - rateBarLeftWidth, maxBarLeftPosY + maxBarLeftHeight,
					maxBarLeftPosX, maxBarLeftPosY + maxBarLeftHeight);
			
			float maxBarRightPosX = rightCounterPosX;
			float maxBarRightPosY = position.y + dimension.y - 19;
			float maxBarRightWidth = 50;
			float maxBarRightHeight = 3f;
			
			p.fill(255, 56);
			p.quad(maxBarRightPosX, maxBarRightPosY, maxBarRightPosX + maxBarRightWidth - GAUGES_BEVEL_LENGTH,
					maxBarRightPosY, maxBarRightPosX + maxBarRightWidth, maxBarRightPosY + maxBarRightHeight,
					maxBarRightPosX, maxBarRightPosY + maxBarRightHeight);
			
			float rateRatio = rateCurrent / (rateMax * 1f);
			float rateBarRightWidth = PApplet.max(maxBarRightWidth * rateRatio, GAUGES_BEVEL_LENGTH);
			
			p.fill(255, 255);
			p.quad(maxBarRightPosX, maxBarRightPosY, maxBarRightPosX + rateBarRightWidth - GAUGES_BEVEL_LENGTH,
					maxBarRightPosY, maxBarRightPosX + rateBarRightWidth, maxBarRightPosY + maxBarRightHeight,
					maxBarRightPosX, maxBarRightPosY + maxBarRightHeight);
		}
	}

	private void drawDigitCounter(PApplet p, String countText, int direction, float startPosX) {		
		float counterPosY =  position.y + dimension.y * 0.5f;
		int charCount = countText.length();
		
		for (int i = 0; i < 4; i++) {
			char currentCountChar = Character.MIN_CODE_POINT;
			
			if(i < charCount) {
				currentCountChar = countText.charAt(direction > 0 ? i : charCount - (i + 1));
			}
			
			int slotOrder = i;
			
			this.drawDigitSlot(p, slotOrder, currentCountChar, direction, startPosX, counterPosY);
		}
	}
	
	private void drawDigitSlot(PApplet p, int order, char value, int directionRaw, float startPosX, float slotPosY) {
		float slotWidth = p.textWidth('8');
		float direction = Math.signum(directionRaw);
		
		float slotPosX = startPosX + (slotWidth + slotWidth * 0.05f) * order * direction;
		
		p.textAlign(PApplet.RIGHT, PApplet.CENTER);
		p.fill(255, 20);
		p.text("8", slotPosX, slotPosY);
		
		if(value != Character.MIN_CODE_POINT) {
			p.fill(255);
			p.text(value, slotPosX, slotPosY);
		}
	}
	
	@Override
	public void update(Observable observable, Object data) {
		if (data instanceof String[]) {
			String[] dataTokens = (String[]) data;
			
			if(dataTokens.length >= 5) {
				String displayResquestTypeRaw = dataTokens[0];
				
				try {
					DisplayResquestContents displayResquestContent = DisplayResquestContents.valueOf(displayResquestTypeRaw);
					
					if(displayResquestContent == DisplayResquestContents.PRONO_COUNT) {
						String countCurrentRaw = dataTokens[1];
						String countMaxRaw = dataTokens[2];
						
						String rateCurrentRaw = dataTokens[3];
						String rateMaxRaw = dataTokens[4];
						
						countCurrent = PApplet.floor(Float.parseFloat(countCurrentRaw));
						countMax = PApplet.floor(Float.parseFloat(countMaxRaw));
						
						rateCurrent = PApplet.floor(Float.parseFloat(rateCurrentRaw));
						rateMax = PApplet.floor(Float.parseFloat(rateMaxRaw));
					}
				} catch(IllegalArgumentException e) {
					System.err.println("Unknown display request type.");
					e.printStackTrace();
				}
			}
		}
	}
}
