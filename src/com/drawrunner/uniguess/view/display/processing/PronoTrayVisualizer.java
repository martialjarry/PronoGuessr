package com.drawrunner.uniguess.view.display.processing;

import java.awt.Point;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Queue;

import com.drawrunner.uniguess.model.communication.nodejs.CursedPaniacListener;
import com.drawrunner.uniguess.model.data.Configuration;
import com.drawrunner.uniguess.view.display.DisplayResquestContents;
import com.drawrunner.uniguess.view.display.processing.PlayerToken.LogoStatus;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;
import processing.event.KeyEvent;
import processing.opengl.PJOGL;

public class PronoTrayVisualizer extends PApplet implements Observer {
	private int fullscreenSetup;
	
	private int initialWidth;
	private int initialHeight;
		
	private PlayerTokenSet playerTokenSet;
	private DistrictCardGrid districtCardGrid;
	
	private PronoCounter pronoCounter;
	
	private Configuration configuration;
	
	private CursedPaniac cursedPaniac;
	
	private PImage trayBackground;
	private PImage playerTokensShadow;
	private PImage districtCardShadow;
	private PImage districtOddsShadow;
	
	private PFont playerTokensFont;
	private PFont districtCardsFont;
	
	private PFont defaultPlayersLogoFont;
	
	private LoadingScreen loadingScreen;
	
	private boolean isVisible;
	private boolean isReady;

	private boolean demoMode;

	private int spawnedPronoCount;
	private Queue<String[]> pendingPronos;
	
	private boolean firstMongoPlayersLoaded;
	private boolean firstTwitchPlayersLoaded;
	private boolean firstMongoDistictsLoaded;
	
	private FrameUpdateListener frameUpdateListener;
	
	private PVector cursedPaniacDirection;
	
	public PronoTrayVisualizer(int fullscreenSetup, Point initialSize) {
		this.fullscreenSetup = fullscreenSetup;
		
		initialWidth = initialSize.x;
		initialHeight = initialSize.y;
		
		isReady = false;
		
		pendingPronos = new ArrayDeque<String[]>();
		
		firstMongoPlayersLoaded = false;
		firstTwitchPlayersLoaded = false;
		firstMongoDistictsLoaded = false;
		
		loadingScreen = new LoadingScreen(initialSize);
		
		configuration = Configuration.getInstance();
		
		cursedPaniac = new CursedPaniac();
	}
	
	@Override
	public void settings() {
		this.size(initialWidth, initialHeight, P2D);
		
		if(fullscreenSetup > 0) {
			this.fullScreen(fullscreenSetup);
		}
		
		this.smooth(3);
		
		PJOGL.setIcon("media/fav_icon.png");
	}
	
	@Override
	public void setup() {
		Thread.currentThread().setName("Prono Tray Visualizer");
		
		trayBackground = this.loadImage("media/tray_background.png");
		
		this.buildPlayerTokenSet();
		this.buildDistrictCardGrid();
		this.buildCursedPaniac();
		
		this.buildPronoCounter();
		
		this.buildLoadingScreen();
		
		spawnedPronoCount = 0;
		demoMode = false;
		
		surface.setLocation(0, 0);
		surface.setTitle("Table des pronos");
		
		this.image(trayBackground, 0, 0, width, height);
		this.drawPauseOverlay();
		
		surface.setVisible(false);
		surface.pauseThread();
		
		isReady = true;
		
//		this.frameRate(1000);
	}

	private void buildPlayerTokenSet() {
		playerTokenSet = new PlayerTokenSet(this);
		playerTokensShadow = this.loadImage("media/shadows/player_token_shadow.png");
		playerTokensFont = this.createFont("fonts/players_label_font.otf", 9);
		defaultPlayersLogoFont = this.createFont("fonts/generated_logos_font.OTF", 24);
	}

	private void buildDistrictCardGrid() {
		districtCardGrid = new DistrictCardGrid(this);
		districtCardShadow = this.loadImage("media/shadows/district_flag_shadow.png");
		districtOddsShadow = this.loadImage("media/shadows/district_odds_shadow.png");
		districtCardsFont = this.createFont("fonts/district_labels_font.otf", 24);
	}

	private void buildPronoCounter() {
		final float PRONO_COUNTER_MARGIN = 16;
		PImage pronoCounterBackground = this.loadImage("media/counter_background.png");
		
		float counterBackgroundWidth = width - pronoCounterBackground.width - PRONO_COUNTER_MARGIN;
		float counterBackgroundHeight = PRONO_COUNTER_MARGIN;
		PVector pronoCounterPosition = new PVector(counterBackgroundWidth, counterBackgroundHeight);
		
		PFont pronoCounterRegularFont = this.createFont("fonts/counter_regular_font.otf", 56);
		PFont pronoCounterDigitFont = this.createFont("fonts/counter_digit_font.otf", 56);
		
		PGraphics pronoCounterBackgroundGraphic = this.createGraphics(pronoCounterBackground.width, pronoCounterBackground.height);
		pronoCounterBackgroundGraphic.beginDraw();
		pronoCounterBackgroundGraphic.background(pronoCounterBackground);
		pronoCounterBackgroundGraphic.endDraw();
		
		pronoCounter = new PronoCounter(pronoCounterPosition, pronoCounterBackgroundGraphic, pronoCounterRegularFont, pronoCounterDigitFont);
		
		this.togglePronoCounter(configuration.isDisplayCounter());
	}

	private void buildCursedPaniac() {
		PImage cursedPaniacSprite = this.loadImage("media/cursed_paniac/cursed_paniac.png");
		PGraphics cursedPaniacGraphic = this.createGraphics(cursedPaniacSprite.width, cursedPaniacSprite.height);
		cursedPaniacGraphic.beginDraw();
		cursedPaniacGraphic.background(cursedPaniacSprite);
		cursedPaniacGraphic.endDraw();
		
		if(configuration.isCursedPaniacEnabled()) {
			File cursedPaniacEatingDirectory = new File("media/cursed_paniac/cursed_paniac_chewing/");
			String[] cursedPaniacChewingFileNames = cursedPaniacEatingDirectory.list();
			Arrays.sort(cursedPaniacChewingFileNames);
				
			this.launchCursedPaniacLoading(cursedPaniacGraphic, cursedPaniacEatingDirectory, cursedPaniacChewingFileNames);
		}
	}
	
	private void launchCursedPaniacLoading(PGraphics cursedPaniacGraphic, File cursedPaniacEatingDirectory,
			String[] cursedPaniacChewingFileNames) {
		
		new Thread() {
			public void run() {
				LinkedList<PImage> cursedPaniacChewingSprites = new LinkedList<PImage>();
				
				for (String cursedPaniacChewingFileName : cursedPaniacChewingFileNames) {
					String cursedPaniacChewingFilePath = cursedPaniacEatingDirectory.getAbsolutePath() + File.separatorChar + cursedPaniacChewingFileName;
					PImage cursedPaniacChewingImage = PronoTrayVisualizer.this.loadImage(cursedPaniacChewingFilePath);
					cursedPaniacChewingSprites.add(cursedPaniacChewingImage);
				}
				
				cursedPaniac.setBackground(cursedPaniacGraphic);
				cursedPaniac.buildEatingSequence(cursedPaniacChewingSprites);
				cursedPaniacDirection = new PVector(0, 0);
			}
		}.start();
	}
	
	private void buildLoadingScreen() {
		PImage headerLoadingIcon = this.loadImage("media/loading/fav_icon_stroke.png");
		PImage innerLoadingIcon = this.loadImage("media/loading/inner_loading_icon.png");
		PFont loadingFont = this.createFont("Arial", displayHeight * 0.1f);
		loadingScreen.setIconsAndFonts(headerLoadingIcon, innerLoadingIcon, loadingFont);
	}

	@Override
	public void draw() {
		frameUpdateListener.onFrameUpdated();
		
		if(!isVisible) {
			this.drawPauseOverlay();
			
			this.noLoop();
			surface.setVisible(false);
			surface.pauseThread();
		}
	}

	public void updateLoadingScreen() {
		loadingScreen.animate(this);
		loadingScreen.draw(this);
	}
	
	public void updateRegularScreen() {
		if(isVisible) {
			this.imageMode(CORNER);
			this.image(trayBackground, 0, 0, width, height);
		}
		
		if(demoMode && Math.random() < 1f) {
			for(int i = 0; i < 10; i++) {
				this.makeRandomProno();
			}
		}
		
		try {
			if(isVisible) {
				districtCardGrid.animate(this);
				playerTokenSet.animate(this);
			
				pronoCounter.draw(this);
				
				if(configuration.isCursedPaniacEnabled() && cursedPaniac.isVisible()) {
					this.updateCursedPaniac();
					this.feedCursedPaniac();
				}
				
				districtCardGrid.draw(this);
				playerTokenSet.draw(this);
			}
		} catch(ConcurrentModificationException e) {
			e.printStackTrace();
		}
		
		this.spawnPendingPronos();
		
	}

	private void updateCursedPaniac() {
		cursedPaniac.animate(this);
		
		if(keyPressed) {
			float speed = CursedPaniac.SPEED / (frameRate / 60f);
			
			PVector normalizedDirection = new PVector(cursedPaniacDirection.x, cursedPaniacDirection.y);
			normalizedDirection.normalize();
			normalizedDirection = PVector.mult(normalizedDirection, speed);
			cursedPaniac.move(normalizedDirection);
		}
		
		cursedPaniac.draw(this);
	}
	
	private void feedCursedPaniac() {
		for(int i = 0; i < districtCardGrid.trayEntityCount(); i++) {
			TrayEntity districtGridEntity = districtCardGrid.getTrayEntity(i);
			
			if(districtGridEntity instanceof DistrictCardColumn) {
				DistrictCardColumn cardColumn = (DistrictCardColumn) districtGridEntity;
				
				for (int j = 0; j < cardColumn.trayEntityCount(); j++) {
					TrayEntity columnEntity = cardColumn.getTrayEntity(j);
					
					if (columnEntity instanceof DistrictCard) {
						DistrictCard districtCard = (DistrictCard) columnEntity;
						
						for (int k = 0; k < districtCard.coinTokenCount(); k++) {
							CoinToken coinToken = districtCard.getCoinToken(k);
							PVector coinCenter = PVector.add(coinToken.getPosition(), PVector.mult(coinToken.getDimension(), 0.5f));
							
							float coinDistance = PVector.dist(coinCenter, cursedPaniac.mouthAnchor());
							float absorbtionRadius = 130;
							
							if(coinDistance < absorbtionRadius
							&& !coinToken.isFeedingCursedPaniac()
							&& coinToken.getSpeed().mag() < 30
							&& coinToken.getPosition().y + coinToken.getDimension().y < districtCardGrid.getPosition().y
							&& cursedPaniac.mouthAnchor().y < districtCardGrid.getPosition().y) {
								coinToken.feedCursedPaniac(this, cursedPaniac);
							}
						}
					}
				}
			}
		}
	}
	
	private void drawPauseOverlay() {
		this.rectMode(CORNER);
		this.fill(0, 196);
		this.rect(0, 0, width, height);
		this.filter(GRAY);
	}
	
	@Override
	public void update(Observable observable, Object data) {
		if(data instanceof String[]) {
			String[] entityData = (String[]) data;
			
			if(entityData.length > 0) {
				String displayResquestTypeRaw = entityData[0];
				
				try {
					DisplayResquestContents displayResquestContent = DisplayResquestContents.valueOf(displayResquestTypeRaw);
					
					switch (displayResquestContent) {
					case PLAYER:
						this.processPlayerData(entityData);
						break;
					case PLAYER_PRONO:
						this.processPlayerPronoData(entityData);
						break;
					case MONGO_PLAYERS_LOADING_PROGRESS:
						this.processMongoPlayersLoadingProgress(entityData);
						break;
					case TWITCH_PLAYERS_LOADING_PROGRESS:
						this.processTwitchPlayersLoadingProgress(entityData);
						break;
					case MONGO_DISTRICTS_LOADING_PROGRESS:
						this.processMongoDistrictsLoadingProgress(entityData);
						break;
					case DISTRICT:
						this.processDistrictData(entityData);
						break;
					}
				} catch(IllegalArgumentException e) {
					System.err.println("Unknown display request type.");
					e.printStackTrace();
				}
			}
		}
	}
	
	private void processPlayerData(String[] playerData) {
		if(playerData.length >= 4) {
			String playerName = playerData[1];
			String playerDisplayName = playerData[2];
			String playerLogoUrl = playerData[3];
			
			PGraphics tokenBackground = null;
			PlayerToken.LogoStatus logoStatus = LogoStatus.NO_LOGO;
			
			EntityBackgroundFactory entityBackgroundFactory = EntityBackgroundFactory.getInstance();
			TrayEntity existingTrayEntity = playerTokenSet.getTrayEntity(playerName);
			
			if(existingTrayEntity == null) {
				if(playerLogoUrl == null) {
					tokenBackground = entityBackgroundFactory.createPlayerTokenGeneratedBackground(this, defaultPlayersLogoFont, playerName);
					logoStatus = LogoStatus.GENERATED_LOGO;
				} else {
					tokenBackground = this.loadPlayerLogo(playerName, playerLogoUrl);
					logoStatus = LogoStatus.USER_LOGO;
				}
			} else {
				if(existingTrayEntity instanceof PlayerToken) {
					PlayerToken existingPlayerToken = (PlayerToken) existingTrayEntity;
					
					if(playerLogoUrl != null && (existingPlayerToken.hasNoLogo() || existingPlayerToken.hasGeneratedLogo())) {
						tokenBackground = this.loadPlayerLogo(playerName, playerLogoUrl);
						logoStatus = LogoStatus.USER_LOGO;
					} else if(playerLogoUrl == null && existingPlayerToken.hasNoLogo()) {
						tokenBackground = entityBackgroundFactory.createPlayerTokenGeneratedBackground(this, defaultPlayersLogoFont, playerName);
						logoStatus = LogoStatus.GENERATED_LOGO;
					}
				}
			}
			
			PlayerToken playerToken = new PlayerToken(tokenBackground, playerTokensShadow, playerName, playerDisplayName, playerTokensFont);
			
			if(logoStatus == LogoStatus.GENERATED_LOGO) {
				playerToken.setLogoAsGenerated();
			} else if(logoStatus == LogoStatus.USER_LOGO) {
				if(playerLogoUrl != null) {
					playerToken.setLogoAsUser();
				}
			}
			
			playerTokenSet.addOrUpdateTrayEntity(this, playerToken);
		}
	}
	
	private PGraphics loadPlayerLogo(String playerName, String playerLogoUrl) {
		if(playerLogoUrl.endsWith(".")) {
			playerLogoUrl += "jpg";
		}
		
		try {
			PImage playerLogo = this.loadImage(playerLogoUrl);
			
			if(playerLogo != null) {
				EntityBackgroundFactory entityBackgroundFactory = EntityBackgroundFactory.getInstance();
				return entityBackgroundFactory.createPlayerTokenCustomBackground(this, playerLogo);
			} else {
				System.err.println("Unable to read image from the player \"" + playerName + "\".");
				return null;
			}
		} catch(Exception e) {
			System.err.println("Unable to read image from the player \"" + playerName + "\".");
			e.printStackTrace();
			
			return null;
		}
	}
	
	private void processPlayerPronoData(String[] playerPronoData) {		
		if(playerPronoData.length >= 3) {
			pendingPronos.add(playerPronoData);
		}
	}
	
	private void processMongoPlayersLoadingProgress(String[] playerData) {
		if(playerData.length >= 3) {
			try {
				if(!firstMongoPlayersLoaded) {
					int newLoadedMongoPlayersCount = Integer.parseInt(playerData[1]);
					
					if(newLoadedMongoPlayersCount > loadingScreen.getLoadedMongoPlayersCount()) {
						loadingScreen.setLoadedMongoPlayersCount(newLoadedMongoPlayersCount);
						
						if(loadingScreen.getTotalMongoPlayersCount() < 0) {
							loadingScreen.setTotalMongoPlayersCount(Integer.parseInt(playerData[2]));
						}
					}
				}
				
				if (loadingScreen.getLoadedMongoPlayersCount() == loadingScreen.getTotalMongoPlayersCount()) {
					firstMongoPlayersLoaded = true;
				}
			} catch (Exception e) {
				System.err.println("Impossible de lire le chargement des joueurs provenant de MongoDB.");
			}
		}
	}
	
	private void processTwitchPlayersLoadingProgress(String[] playerData) {
		if(playerData.length >= 3) {
			try {
				if(!firstTwitchPlayersLoaded) {
					int newLoadedTwitchPlayersCount = Integer.parseInt(playerData[1]);;
					
					if(newLoadedTwitchPlayersCount > loadingScreen.getLoadedTwitchPlayersCount()) {
						loadingScreen.setLoadedTwitchPlayersCount(newLoadedTwitchPlayersCount);
						
						if(loadingScreen.getTotalTwitchPlayersCount() < 0) {
							loadingScreen.setTotalTwitchPlayersCount(Integer.parseInt(playerData[2]));
						}
					}
				}
				
				if(loadingScreen.getLoadedTwitchPlayersCount() == loadingScreen.getTotalTwitchPlayersCount()) {
					firstTwitchPlayersLoaded = true;
				}
			} catch (NumberFormatException e) {
				System.err.println("Impossible de lire le chargement des joueurs provenant de Twitch.");
			}
		}
	}
	
	private void processMongoDistrictsLoadingProgress(String[] districtData) {
		if(districtData.length >= 3) {
			try	{
				if(!firstMongoDistictsLoaded) {
					int newLoadedMongoDistrictsCount = Integer.parseInt(districtData[1]);
					
					if(newLoadedMongoDistrictsCount > loadingScreen.getLoadedMongoDistrictsCount()) {
						loadingScreen.setLoadedMongoDistrictsCount(newLoadedMongoDistrictsCount);
						
						if(loadingScreen.getTotalMongoDistrictsCount() < 0) {
							loadingScreen.setTotalMongoDistrictsCount(Integer.parseInt(districtData[2]));
						}
					}
				}
				
				if(loadingScreen.getLoadedMongoDistrictsCount() == loadingScreen.getTotalMongoDistrictsCount()) {
					firstMongoDistictsLoaded = true;
				}
			} catch (NumberFormatException e) {
				System.err.println("Impossible de lire le chargement des districts provenant de MongoDB.");
			}
		}
	}
	
	private void spawnPendingPronos() {
		int spawnCount = Math.min(pendingPronos.size(), 5);
		
		for(int i = 0; i < spawnCount; i++) {
			String[] playerPronoData = pendingPronos.remove();
			
			String playerName = playerPronoData[1];
			
			try {
				String[] pronoTokens = playerPronoData[2].split(";");
				
				DistrictCard[] districtCards = new DistrictCard[pronoTokens.length / 2];
				int[] allOdds = new int[pronoTokens.length / 2];
				
				for (int j = 0; j < pronoTokens.length - 1; j += 2) {
					String predictedDistrictIdentifier = pronoTokens[j];
					int odds = (int) Float.parseFloat(pronoTokens[j + 1]);
					
					TrayEntity districtTrayEntitie = districtCardGrid.getTrayEntity(predictedDistrictIdentifier);
					
					if (districtTrayEntitie instanceof DistrictCard) {
						DistrictCard districtCard = (DistrictCard) districtTrayEntitie;
						districtCards[j / 2] = districtCard;
						allOdds[j / 2] = Math.floorDiv(odds, allOdds.length);
					}
				}
				
				TrayEntity playerTrayEntity = playerTokenSet.getTrayEntity(playerName);
				
				if(playerTrayEntity != null && districtCards.length > 0) {
					if(playerTrayEntity instanceof PlayerToken) {
						PlayerToken playerToken = (PlayerToken) playerTrayEntity;
						
						for (int j = 0; j < districtCards.length; j++) {
							DistrictCard districtCard = districtCards[j];
							int odds = (int) allOdds[j];
							
							if(districtCard != null) {
								List<CoinToken> newCoinTokens = playerToken.makeProno(this, districtCard, odds);
								
								for (CoinToken newCoinToken : newCoinTokens) {
									newCoinToken.setPronoIndex(spawnedPronoCount + 1);
									districtCard.addCoinToken(newCoinToken);
								}
								
								districtCardGrid.deployCard(districtCard);
								
								spawnedPronoCount++;
							}
						}
					}
				}
			} catch(NumberFormatException e) {
				System.err.println("Can't parse odds.");
				e.printStackTrace();
			}
			
		}
	}
	
	private void processDistrictData(String[] districtData) {
		if(districtData.length >= 5) {
			String districtIdentifier = districtData[1];
			String districtName = districtData[2];
			float districtOdds = Float.parseFloat(districtData[3]);
			String districtFlagUrl = districtData[4];
			
			PGraphics cardBackground = null;
			
			if(districtFlagUrl != null) {
				TrayEntity existingTrayEntity = districtCardGrid.getTrayEntity(districtIdentifier);
				
				if (existingTrayEntity == null) {
					PImage districtFlag = this.loadImage(districtFlagUrl);
					
					EntityBackgroundFactory entityBackgroundFactory = EntityBackgroundFactory.getInstance();
					cardBackground = entityBackgroundFactory.createDistrictCardBackground(this, districtFlag);
				} else {
					if(existingTrayEntity instanceof DistrictCard) {
						DistrictCard existingDistrictCard = (DistrictCard) existingTrayEntity;
						
						if(existingDistrictCard.getBackground() == null) {
							PImage districtFlag = this.loadImage(districtFlagUrl);
							
							EntityBackgroundFactory entityBackgroundFactory = EntityBackgroundFactory.getInstance();
							cardBackground = entityBackgroundFactory.createDistrictCardBackground(this, districtFlag);
						}
					}
				}
			}
			
			DistrictCard districtCard = new DistrictCard(cardBackground, districtCardShadow, districtIdentifier, districtName, districtOdds, districtCardsFont, districtOddsShadow);
			districtCardGrid.addOrUpdateTrayEntity(this, districtCard);
		}
	}
	
	@Override
	public void mouseClicked() {
//		demoMode = !demoMode;
	}
	
	@Override
	public void keyPressed(KeyEvent event) {
		if(configuration.isCursedPaniacEnabled() && cursedPaniac.isVisible()) {
			if(event.getKeyCode() == UP) {
				cursedPaniacDirection = PVector.add(cursedPaniacDirection, new PVector(0, -1));
			} else if(event.getKeyCode() == DOWN) {
				cursedPaniacDirection = PVector.add(cursedPaniacDirection, new PVector(0, 1));
			} else if(event.getKeyCode() == LEFT) {
				cursedPaniacDirection = PVector.add(cursedPaniacDirection, new PVector(-1, 0));
			} else if(event.getKeyCode() == RIGHT) {
				cursedPaniacDirection = PVector.add(cursedPaniacDirection, new PVector(1, 0));
			}
		}
		
		if(event.getKeyCode() == 'P') {
			this.makeRandomProno();
		}
	}
	
	@Override
	public void keyReleased(KeyEvent event) {
		if(configuration.isCursedPaniacEnabled() && cursedPaniac.isVisible()) {
			if(event.getKeyCode() == UP) {
				cursedPaniacDirection = PVector.add(cursedPaniacDirection, new PVector(0, 1));
			} else if(event.getKeyCode() == DOWN) {
				cursedPaniacDirection = PVector.add(cursedPaniacDirection, new PVector(0, -1));
			} else if(event.getKeyCode() == LEFT) {
				cursedPaniacDirection = PVector.add(cursedPaniacDirection, new PVector(1, 0));
			} else if(event.getKeyCode() == RIGHT) {
				cursedPaniacDirection = PVector.add(cursedPaniacDirection, new PVector(-1, 0));
			}
		}
	}
	
	public void exposeGridDistrictCards() {
		districtCardGrid.exposeDistrictCards(this);
	}
	
	public void disposeGridDistrictCards() {
		districtCardGrid.disposeDistrictCards(this);
	}
	
	public void togglePronoCounter(boolean display) {
		pronoCounter.setVisible(display);
	}
	
	public void spawnCursedPaniac(String invokatorName) {
		if(configuration.isCursedPaniacEnabled() && !cursedPaniac.isMovingAround()) {
			PVector cursedPaniacDimension = cursedPaniac.getDimension();
			
			float cursedPaniacStartPosX = -cursedPaniacDimension.x * 0.5f;
			float cursedPaniacStartPosY = height * 0.4f - cursedPaniacDimension.y * 0.5f;
			
			float cursedPaniacEndPosX = width * 0.5f -cursedPaniacDimension.x * 0.5f;
			float cursedPaniacEndPosY = cursedPaniacStartPosY - 30;
			
			PVector cursedPaniacStartPosition = new PVector(cursedPaniacStartPosX, cursedPaniacStartPosY);
			PVector cursedPaniacEndPosition = new PVector(cursedPaniacEndPosX, cursedPaniacEndPosY);
			
			cursedPaniac.spawn(invokatorName, cursedPaniacStartPosition, cursedPaniacEndPosition);
		}
	}	
	
	public void retireCursedPaniac() {
		if(configuration.isCursedPaniacEnabled() && cursedPaniac.isMovingAround()) {
			cursedPaniac.leave(height);
		}
	}
	
	private void makeRandomProno() {
		PlayerToken playerToken = playerTokenSet.getRandomPlayerToken();
		DistrictCard districtCard = districtCardGrid.getRandomDistrictCard();
		
		int odds = (int) this.random(4, 6);
		
		if(districtCard != null && playerToken != null) {
			List<CoinToken> newCoinTokens = playerToken.makeProno(this, districtCard, odds);
			
			for (CoinToken newCoinToken : newCoinTokens) {
				districtCard.addCoinToken(newCoinToken);
			}
			
			districtCardGrid.deployCard(districtCard);
		}
	}
	
	public void toggle(boolean state) {
		isVisible = state;
		
		if(state) {
			this.loop();
			surface.resumeThread();
			surface.setVisible(true);
		}
		
		// To visualize the disabling process, see the end of the draw() function
	}
	
	public boolean isVisible() {
		return isVisible;
	}
	
	public boolean isReady() {
		return isReady;
	}

	public void setReady(boolean isReady) {
		this.isReady = isReady;
	}
	
	public boolean hasLoadedAllData() {
		return loadingScreen.hasLoadedAllData();
	}
	
	public void assignLoadingListener(ActionListener loadingListener) {
		loadingScreen.setLoadingListener(loadingListener);
	}
	
	public void assignCursedPaniacListener(CursedPaniacListener cursedPaniacListener) {
		if(configuration.isCursedPaniacEnabled()) {
			cursedPaniac.setCursedPaniacListener(cursedPaniacListener);
		}
	}
	
	public void setFrameUpdateListener(FrameUpdateListener frameUpdateListener) {
		this.frameUpdateListener = frameUpdateListener;
	}

	public PronoCounter getPronoCounter() {
		return pronoCounter;
	}
}