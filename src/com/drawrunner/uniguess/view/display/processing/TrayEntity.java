package com.drawrunner.uniguess.view.display.processing;
import java.util.Observable;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;

public abstract class TrayEntity extends Observable {
	protected boolean isVisible;
	
	protected PVector position;
	protected PVector dimension;
	
	protected PGraphics background;
	protected PImage shadow;

	protected PFont labelFont;

	public TrayEntity() {
		isVisible = true;
		
		position = new PVector(0, 0);
		dimension = new PVector(0, 0);
		
		this.background = null;
		
		this.labelFont = null;
	}
	
	public TrayEntity(PGraphics background, PImage shadow, PFont labelFont) {
		isVisible = true;
		
		position = new PVector(0, 0);
		dimension = new PVector(0, 0);
		
		this.background = background;
		this.shadow = shadow;
		
		this.labelFont = labelFont;
	}
	
	public TrayEntity(PVector position, PVector dimension, PGraphics background, PImage shadow, PFont labelFont) {
		isVisible = true;
		
		this.position = position;
		this.dimension = dimension;
		
		this.background = background;
		this.shadow = shadow;
		
		this.labelFont = labelFont;
	}

	public abstract void animate(PApplet p);
	
	public abstract void draw(PApplet p);

	protected void drawShadow(PApplet p) {
		this.drawShadow(p, 255);
	}
	
	protected void drawShadow(PApplet p, float opacity) {
		float centerDeltaX = position.x + dimension.x * 0.5f - p.width * 0.5f;
		float centerDeltaY = position.y + dimension.y * 0.5f - p.height * 0.5f;
		float distanceFromCenter = PApplet.sqrt(PApplet.sq(centerDeltaX) + PApplet.sq(centerDeltaY));
		
		float shadowShiftX = centerDeltaX * 0.003f;
		float shadowShiftY = centerDeltaY * 0.003f;
		float shadowScale = 1 + distanceFromCenter * 0.00005f;
		
		float shadowWidth = dimension.x * 1.18f * shadowScale;
		float shadowHeight = dimension.y * 1.18f * shadowScale;
		
		float shadowPosX = position.x + dimension.x * 0.5f + shadowShiftX;
		float shadowPosY = position.y + dimension.y * 0.5f + shadowShiftY;
		
		p.imageMode(PApplet.CENTER);
		p.tint(255, opacity);
		p.image(shadow, shadowPosX, shadowPosY, shadowWidth, shadowHeight);
		p.noTint();
	}
	
	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}
	
	public PVector getPosition() {
		return position;
	}

	public void setPosition(PVector position) {
		this.position = position;
	}

	public PVector getDimension() {
		return dimension;
	}

	public void setDimension(PVector dimension) {
		this.dimension = dimension;
	}

	public PGraphics getBackground() {
		return background;
	}

	public void setBackground(PGraphics background) {
		this.background = background;
	}

	public PImage getShadow() {
		return shadow;
	}

	public void setShadow(PImage shadow) {
		this.shadow = shadow;
	}
	
	public PFont getLabelFont() {
		return labelFont;
	}

	public void setLabelFont(PFont labelFont) {
		this.labelFont = labelFont;
	}
}
