package com.drawrunner.uniguess.view.display.processing;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import processing.core.PApplet;

public abstract class TrayEntityContainer extends TrayEntity {
	protected List<TrayEntity> entities;
	
	public TrayEntityContainer() {
		entities = new CopyOnWriteArrayList<TrayEntity>();
	}
	
	public abstract void addOrUpdateTrayEntity(PApplet p, TrayEntity trayEntity);
	public abstract TrayEntity getTrayEntity(String entityId);
	
	public TrayEntity removeTrayEntity(int entityIndex) {
		if(entityIndex < entities.size()) {
			return entities.remove(entityIndex);
		} else {
			return null;
		}
	}
	
	public TrayEntity getTrayEntity(int entityIndex) {
		return entities.get(entityIndex);
	}

	public int trayEntityCount() {
		return entities.size();
	}
}
