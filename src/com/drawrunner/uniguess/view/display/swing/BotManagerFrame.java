package com.drawrunner.uniguess.view.display.swing;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.drawrunner.uniguess.control.BotManagerController;
import com.drawrunner.uniguess.control.PronoTrayController;
import com.drawrunner.uniguess.model.data.OutputManager;

/**
 * Permet de lancer PronoGuessr apr�s avoir r�gl� un multiplicateur de score et
 * �ventuellement une carte � charger. 
 * @author DrawRunner
 */
public class BotManagerFrame extends JFrame {
	private static final long serialVersionUID = -5564844311988576734L;
	
	private JLabel worldLabel;
	private JComboBox<String> worldComboBox;
	
	private JTextField multiplicatorTextField;
	
	private JButton launchPronoGuessrButton;
	
	private JButton goPronoButton;
	private JButton stopPronoButton;
	private JButton districtButton;
	private JButton districtAddButton;
	private JButton scoreDisplayAllButton;
	private JButton scoreAddButton;
	private JButton pronoResetButton;
	private JButton pronoDisplayAllButton;
	private JButton oddsDisplayButton;
	
	private JCheckBox togglePronoCounterCheckbox;
	private JButton disableCursedPaniacButton;
	
	public BotManagerFrame(BotManagerController botManagerController, PronoTrayController pronoTrayController) {
		// Panneau principal, contenant l'ensemble de l'interface
		Container contentPane = this.getContentPane();
		contentPane.setLayout(new BorderLayout());
		
		JPanel pronogessrStartPanel = new JPanel();
		pronogessrStartPanel.setLayout(new BoxLayout(pronogessrStartPanel, BoxLayout.PAGE_AXIS));
		pronogessrStartPanel.setBorder(new TitledBorder("Lancement de PronoGuessr"));
		
		JPanel commandsPanel = new JPanel(new FlowLayout());
		commandsPanel.setBorder(new TitledBorder("Commandes"));
		commandsPanel.setLayout(new BoxLayout(commandsPanel, BoxLayout.PAGE_AXIS));
		
		JPanel pronoTrayPanel = new JPanel(new FlowLayout());
		pronoTrayPanel.setBorder(new TitledBorder("Table Des Pronos"));
		
		JPanel preLaunchSettingsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		// Titre du champ de saisie du multiplicateur de score
		JLabel multiplicatorLabel = new JLabel("Multiplicateur : ");
		
		// Champ de saisie du multiplicateur de score (tout le texte y est
		// s�lectionn� par d�faut)
		multiplicatorTextField = new JTextField("1");
		multiplicatorTextField.setPreferredSize(new Dimension(35, 25));
		multiplicatorTextField.selectAll();
		
		// Case � cocher permettant d'activer, ou non, le chargement d'une
		// nouvelle carte
		JCheckBox worldCheckBox = new JCheckBox();
		worldCheckBox.setName("Enable world selection");
		
		// Titre du menu d�roulant de la carte � charger
		worldLabel = new JLabel("Charger un fichier de cotes : ");
		worldLabel.setEnabled(false);
		
		// Menu d�roulant de la carte � charger (d�sactiv�e par d�faut)
		worldComboBox = new JComboBox<String>();
		worldComboBox.setEnabled(false);
		worldComboBox.setPreferredSize(new Dimension(200, 30));
		this.buildWorldsComboBox(worldComboBox);
		
		// Bouton permettant de lancer PronoGuessr (peut �tre actionn� en
		// appyuant sur ENTR�E)
		launchPronoGuessrButton = new JButton("Lancer PronoGuessr");
		launchPronoGuessrButton.setName("Launch PronoGuessr");
		launchPronoGuessrButton.addActionListener(botManagerController);
		this.getRootPane().setDefaultButton(launchPronoGuessrButton);
		
		JPanel sessionCommandsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		goPronoButton = new JButton("!go_prono");
		goPronoButton.setName("Go Prono");
		goPronoButton.addActionListener(botManagerController);

		stopPronoButton = new JButton("!stop_prono");
		stopPronoButton.setName("Stop Prono");
		stopPronoButton.addActionListener(botManagerController);


		JPanel districtCommandsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		districtButton = new JButton("!pays");
		districtButton.setName("District");
		districtButton.addActionListener(botManagerController);
		
		districtAddButton = new JButton("!pays -ajouter");
		districtAddButton.setName("Add district");
		districtAddButton.addActionListener(botManagerController);
		
		
		JPanel scoreCommandsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		scoreDisplayAllButton = new JButton("!score -afficher_tous");
		scoreDisplayAllButton.setName("Display all scores");
		scoreDisplayAllButton.addActionListener(botManagerController);
		
		scoreAddButton = new JButton("!score -ajouter");
		scoreAddButton.setName("Add score");
		scoreAddButton.addActionListener(botManagerController);
		
		
		JPanel pronoCommandsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

		pronoResetButton = new JButton("!prono -reset");
		pronoResetButton.setName("Reset pronos");
		pronoResetButton.addActionListener(botManagerController);
		
		pronoDisplayAllButton = new JButton("!prono -afficher_tous");
		pronoDisplayAllButton.setName("Display all pronos");
		pronoDisplayAllButton.addActionListener(botManagerController);

		
		JPanel oddsCommandsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		oddsDisplayButton = new JButton("!cote");
		oddsDisplayButton.setName("Display district odds");
		oddsDisplayButton.addActionListener(botManagerController);
		
		
		JButton togglePronoTrayButton = new JButton("Activer la Table des Pronos");
		togglePronoTrayButton.setName("Toggle Prono Tray visibility");
		togglePronoTrayButton.addActionListener(pronoTrayController);

		togglePronoCounterCheckbox = new JCheckBox("Activer le compteur de prono");
		togglePronoCounterCheckbox.setName("Toggle Prono Counter");
		togglePronoCounterCheckbox.addActionListener(botManagerController);
		togglePronoCounterCheckbox.addItemListener(pronoTrayController);
		
		disableCursedPaniacButton = new JButton("D�sactiver Cursed Paniac");
		disableCursedPaniacButton.setName("Disable Cursed Paniac");
		disableCursedPaniacButton.setVisible(false);
		disableCursedPaniacButton.addActionListener(botManagerController);
		disableCursedPaniacButton.addActionListener(pronoTrayController);
		
		// Ajout d'une action sur l'�tat de la case � cocher afin que le menu
		// de s�lection de la carte s'active lorsque cette case est �coch�e et
		// vice-versa
		worldCheckBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				worldLabel.setEnabled(worldCheckBox.isSelected());
				worldComboBox.setEnabled(worldCheckBox.isSelected());
			}
		});

		// Construction de l'architecture de l'interface
		contentPane.add(pronogessrStartPanel, BorderLayout.NORTH);
		
		pronogessrStartPanel.add(preLaunchSettingsPanel);
		preLaunchSettingsPanel.add(multiplicatorLabel);
		preLaunchSettingsPanel.add(multiplicatorTextField);
		preLaunchSettingsPanel.add(worldCheckBox);
		preLaunchSettingsPanel.add(worldLabel);
		preLaunchSettingsPanel.add(worldComboBox);
		
		JPanel launchPronoguessrButtonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		pronogessrStartPanel.add(launchPronoguessrButtonPanel);
		launchPronoguessrButtonPanel.add(launchPronoGuessrButton);
		
		contentPane.add(commandsPanel, BorderLayout.CENTER);
		
		commandsPanel.add(sessionCommandsPanel);
		sessionCommandsPanel.add(goPronoButton);
		sessionCommandsPanel.add(stopPronoButton);
		
		commandsPanel.add(districtCommandsPanel);
		districtCommandsPanel.add(districtButton);
		districtCommandsPanel.add(districtAddButton);
		
		commandsPanel.add(scoreCommandsPanel);
		scoreCommandsPanel.add(scoreDisplayAllButton);
		scoreCommandsPanel.add(scoreAddButton);
		
		commandsPanel.add(pronoCommandsPanel);
		pronoCommandsPanel.add(pronoResetButton);
		pronoCommandsPanel.add(pronoDisplayAllButton);
		
		commandsPanel.add(oddsCommandsPanel);
		oddsCommandsPanel.add(oddsDisplayButton);
		
		contentPane.add(pronoTrayPanel, BorderLayout.SOUTH);
		pronoTrayPanel.add(togglePronoTrayButton);
		pronoTrayPanel.add(togglePronoCounterCheckbox);
		pronoTrayPanel.add(disableCursedPaniacButton);
		
		// Actualisation des dimensions de chaque �l�ment contenu dans la
		// fen�tre
		this.pack();
		
		this.locateWindow();
		
		// R�glage du titre et de la visibilit� de la fen�tre
		this.setTitle("UniGuess");
		
		// Ajout d'une lisaion entre le bouton Windows "Croix" de la fen�tre et
		// l'arr�t de l'application
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	/**
	 * Ajoute un item au menu d�roulant de s�lection de carte pour chaque
	 * fichier contenu dans le dossier "cotes" du projet.
	 * @param worldComboBox : Menu d�roulant de s�lection de carte.
	 */
	private void buildWorldsComboBox(JComboBox<String> worldComboBox) {
		OutputManager outputManager = OutputManager.getInstance();
		String oddsFolderPath = outputManager.getDataFolderPath() + File.separator + "/cotes";
		
		File oddsFolder = new File(oddsFolderPath);
		oddsFolder.mkdirs();
		
		// Si le dossier exite d�j� ou a bien �t� cr��, alors les items doivent
		// �tre ajout�s � partir des fichiers qui y sont contenus 
		if(oddsFolder.exists()) {
			File[] oddsFiles = oddsFolder.listFiles();
			
			for (File oddsFile : oddsFiles) {
				worldComboBox.addItem(oddsFile.getName());
			}
			
			if(oddsFiles.length > 0) {
				worldComboBox.setSelectedIndex(0);
			}
		}
	}
	
	public void toggleLaunchPronoguessrButton(boolean isEnabled) {
		launchPronoGuessrButton.setEnabled(isEnabled);
		
		if(isEnabled) {
			this.getRootPane().setDefaultButton(launchPronoGuessrButton);
		}
	}
	
	public void disableAllButtons() {
		pronoResetButton.setEnabled(false);
		pronoDisplayAllButton.setEnabled(false);
		goPronoButton.setEnabled(false);
		stopPronoButton.setEnabled(false);
		districtButton.setEnabled(false);
		districtAddButton.setEnabled(false);
		oddsDisplayButton.setEnabled(false);
		scoreDisplayAllButton.setEnabled(false);
		scoreAddButton.setEnabled(false);
	}
	
	public void displayAsWaitingPronosis() {
		pronoResetButton.setEnabled(false);
		pronoDisplayAllButton.setEnabled(false);
		stopPronoButton.setEnabled(false);
		districtButton.setEnabled(false);
		
		goPronoButton.setEnabled(true);
		districtAddButton.setEnabled(true);
		oddsDisplayButton.setEnabled(true);
		scoreDisplayAllButton.setEnabled(true);
		scoreAddButton.setEnabled(true);
		
		this.getRootPane().setDefaultButton(goPronoButton);
	}
	
	public void displayAsInPronosis() {
		goPronoButton.setEnabled(false);
		districtButton.setEnabled(false);
		
		pronoResetButton.setEnabled(true);
		pronoDisplayAllButton.setEnabled(true);
		stopPronoButton.setEnabled(true);
		districtAddButton.setEnabled(true);
		oddsDisplayButton.setEnabled(true);
		scoreDisplayAllButton.setEnabled(true);
		scoreAddButton.setEnabled(true);
		
		this.getRootPane().setDefaultButton(stopPronoButton);
	}
	
	public void displayAsWaitingDistrict() {
		stopPronoButton.setEnabled(false);
		goPronoButton.setEnabled(false);
		
		pronoResetButton.setEnabled(true);
		pronoDisplayAllButton.setEnabled(true);
		districtButton.setEnabled(true);
		districtAddButton.setEnabled(true);
		oddsDisplayButton.setEnabled(true);
		scoreDisplayAllButton.setEnabled(true);
		scoreAddButton.setEnabled(true);
		
		this.getRootPane().setDefaultButton(districtButton);
	}
	
	/**
	 * Positionne la fen�tre au centre de l'�cran principal.
	 */
	private void locateWindow() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int posX = (int) (screenSize.width / 2f - this.getSize().width / 2f);
		int posY = (int) (screenSize.height / 2f - this.getSize().height / 2f);
		this.setLocation(posX, posY);
	}
	
	public String chosenWorldPath() {
		String oddsFilePath = "null";
		
		if(worldComboBox.isEnabled()) {
			OutputManager outputManager = OutputManager.getInstance();
			String oddsFolderPath = outputManager.getDataFolderPath() + File.separator + "cotes";
			String uprocessedOddsFilePath = oddsFolderPath + File.separator + (String) worldComboBox.getSelectedItem();
			
			File oddsFile = new File(uprocessedOddsFilePath);
			oddsFilePath = oddsFile.toURI().toString();
		}
		
		return oddsFilePath;
	}
	
	public String multiplicator() {
		return multiplicatorTextField.getText();
	}
	
	public void toggleWorldCheckbox(boolean enableCheckbox) {
		worldLabel.setEnabled(enableCheckbox);
		worldComboBox.setEnabled(enableCheckbox);
	}
	
	public void togglePronoCounterCheckbox(boolean enableCheckbox) {
		togglePronoCounterCheckbox.setSelected(enableCheckbox);
	}
	
	public void toggleCursedPaniacDisablingButton(boolean setVisible) {
		disableCursedPaniacButton.setVisible(setVisible);
		this.pack();
	}
}
