package com.drawrunner.uniguess.view.display.swing;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.WindowEvent;
import java.util.Iterator;
import java.util.Map;
import java.util.function.BiConsumer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.drawrunner.uniguess.control.BotManagerController;
import com.drawrunner.uniguess.model.data.CommandTypes;

public class CommandArgumentsFrame extends JFrame {
	private static final long serialVersionUID = 8686517286857810298L;

	private CommandTypes commandType;
	private Map<String, JTextField> parameterInputs;
	
	public CommandArgumentsFrame(BotManagerController botManagerController, BotManagerFrame botManagerFrame, CommandTypes commandType, Map<String, JTextField> parameterInputs) {
		this.commandType = commandType;
		this.parameterInputs = parameterInputs;
		
		JPanel contentPane = (JPanel) this.getContentPane();
		contentPane.setLayout(new FlowLayout());
		
		parameterInputs.forEach(new BiConsumer<String, JTextField>() {
			@Override
			public void accept(String parameterName, JTextField parameterTextField) {
				JLabel parameterNameLabel = new JLabel(parameterName + " : ");
				parameterTextField.setPreferredSize(new Dimension(150, parameterTextField.getPreferredSize().height));
				
				contentPane.add(parameterNameLabel);
				contentPane.add(parameterTextField);
			}
		});
		
		JButton sendButton = new JButton("Envoyer");
		sendButton.setName("Send command");
		sendButton.addActionListener(botManagerController);
		this.getRootPane().setDefaultButton(sendButton);
		
		contentPane.add(sendButton);
		
		// Actualisation des dimensions de chaque �l�ment contenu dans la
		// fen�tre
		this.pack();
		
		// R�glage du titre et de la visibilit� de la fen�tre
		this.setTitle("Pr�ciser les param�tres de la commande");
		
		int posX = (int) (botManagerFrame.getLocation().x + botManagerFrame.getSize().width - this.getSize().width - 20);
		int posY = (int) (botManagerFrame.getLocation().y + (botManagerFrame.getSize().height - this.getSize().height) * 0.5f);
		this.setLocation(posX, posY);
		
		this.setVisible(true);
	}
	
	public boolean isReadyToSend() {
		boolean noneEmptyField = true;
		
		for (Iterator<JTextField> parameterTextFieldInterator = parameterInputs.values().iterator(); parameterTextFieldInterator.hasNext();) {
			JTextField parameterTextField = (JTextField) parameterTextFieldInterator.next();
			
			if(parameterTextField.getText().isEmpty()) {
				noneEmptyField = false;
			}
		}
		
		return noneEmptyField;
	}
	
	public String getArgument(String parameterName) {
		JTextField parameterInput = parameterInputs.get(parameterName);		
		if(parameterInput != null) {
			return parameterInput.getText();
		} else {
			return null;
		}
	}
	
	public void close() {
		this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}

	public CommandTypes getCommandType() {
		return commandType;
	}
}
