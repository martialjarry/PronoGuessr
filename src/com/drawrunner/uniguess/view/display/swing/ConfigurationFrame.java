package com.drawrunner.uniguess.view.display.swing;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

public abstract class ConfigurationFrame extends JFrame {
	private static final long serialVersionUID = 5361319092509661924L;

	protected JLabel buildTitle(String title) {
		JLabel titleLabel = new JLabel(title);
		titleLabel.setHorizontalAlignment(SwingConstants.CENTER);

		Font defaultFont = titleLabel.getFont();
		Font titleFont = new Font(defaultFont.getName(), Font.BOLD, 16);
		titleLabel.setFont(titleFont);
		
		Border titleBorder = BorderFactory.createEmptyBorder(0, 8, 8, 8);
		titleLabel.setBorder(titleBorder);
		
		return titleLabel;
	}
	
	/**
	 * Positionne la fen�tre au centre de l'�cran principal.
	 */
	protected void locateWindowAtCenter() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int posX = (int) (screenSize.width / 2f - this.getSize().width / 2f);
		int posY = (int) (screenSize.height / 2f - this.getSize().height / 2f);
		this.setLocation(posX, posY);
	}
}
