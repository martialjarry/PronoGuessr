package com.drawrunner.uniguess.view.display.swing;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import com.drawrunner.uniguess.control.ConfigurationController;
import com.drawrunner.uniguess.control.UniguessController;

/**
 * Permet de lancer PronoGuessr apr�s avoir r�gl� un multiplicateur de score et
 * �ventuellement une carte � charger. 
 * @author DrawRunner
 */
public class ConnectionCredentialsFrame extends ConfigurationFrame implements Observer {
	private static final long serialVersionUID = 1744382917826644111L;
	
	private JTextField targetChannelTextField;
	private JTextField databasePathTextField;
	private JTextField botUsernameTextField;
	private JTextField botPasswordTextField;
	
	public ConnectionCredentialsFrame(UniguessController uniguessController, ConfigurationController configurationController) {
		// Panneau principal, contenant l'ensemble de l'interface
		JPanel contentPanel = (JPanel) this.getContentPane();
		contentPanel.setLayout(new BorderLayout());
		
		Border contentBorder = BorderFactory.createEmptyBorder(8, 10, 4, 10);
		contentPanel.setBorder(contentBorder);
		
		JLabel titleLabel = this.buildTitle("J'ai besoin de ces identifiant de connexion pour fonctionner :");
		JPanel formPanel = this.buildFormPanel();
		JPanel buttonsPanel = this.buildButtonsPanel(uniguessController, configurationController);
		
		contentPanel.add(titleLabel, BorderLayout.NORTH);
		contentPanel.add(formPanel, BorderLayout.CENTER);
		contentPanel.add(buttonsPanel, BorderLayout.SOUTH);
		
		// Actualisation des dimensions de chaque �l�ment contenu dans la
		// fen�tre
		this.pack();
		
		this.locateWindowAtCenter();
		
		// R�glage du titre et de la visibilit� de la fen�tre
		this.setTitle("Configuration de PronoGuessr");
		
		// Ajout d'une lisaion entre le bouton Windows "Croix" de la fen�tre et
		// l'arr�t de l'application
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	private JPanel buildFormPanel() {
		GridBagLayout formLayout = new GridBagLayout();
		GridBagConstraints layoutConstraints = new GridBagConstraints();
		
		JPanel formPanel =  new JPanel(formLayout);
		
		targetChannelTextField = new JTextField();
		targetChannelTextField.setPreferredSize(new Dimension(550, 25));
		
		databasePathTextField = new JTextField();
		databasePathTextField.setPreferredSize(new Dimension(550, 25));
		
		botUsernameTextField = new JTextField();
		botUsernameTextField.setPreferredSize(new Dimension(550, 25));
		
		botPasswordTextField = new JTextField();
		botPasswordTextField.setPreferredSize(new Dimension(550, 25));
		
		JLabel targetChannelLabel = new JLabel("Cha�ne cible : ");
		targetChannelLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		
		JLabel databasePathLabel = new JLabel("Base de donn�es : "); 
		databasePathLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		
		JLabel botUsernameLabel = new JLabel("Nom de la cha�ne du bot : "); 
		botUsernameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		
		JLabel botPasswordTLabel = new JLabel("Mot de passe de la cha�ne du bot : "); 
		botPasswordTLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		
		layoutConstraints.fill = GridBagConstraints.HORIZONTAL;
		layoutConstraints.insets = new Insets(2, 0, 2, 0);
		
		layoutConstraints.gridx = 0;
		layoutConstraints.gridy = 0;
		formPanel.add(targetChannelLabel, layoutConstraints);
		
		layoutConstraints.gridx = 1;
		layoutConstraints.gridy = 0;
		formPanel.add(targetChannelTextField, layoutConstraints);
		
		layoutConstraints.gridx = 0;
		layoutConstraints.gridy = 1;
		formPanel.add(databasePathLabel, layoutConstraints);
		
		layoutConstraints.gridx = 1;
		layoutConstraints.gridy = 1;
		formPanel.add(databasePathTextField, layoutConstraints);
		
		layoutConstraints.gridx = 0;
		layoutConstraints.gridy = 2;
		formPanel.add(botUsernameLabel, layoutConstraints);
		
		layoutConstraints.gridx = 1;
		layoutConstraints.gridy = 2;
		formPanel.add(botUsernameTextField, layoutConstraints);
		
		layoutConstraints.gridx = 0;
		layoutConstraints.gridy = 3;
		formPanel.add(botPasswordTLabel, layoutConstraints);
		
		layoutConstraints.gridx = 1;
		layoutConstraints.gridy = 3;
		formPanel.add(botPasswordTextField, layoutConstraints);
		
		return formPanel;
	}

	private JPanel buildButtonsPanel(UniguessController uniguessController, ConfigurationController configurationController) {
		JPanel buttonsPanel = new JPanel(new FlowLayout());
		
		JButton validateButton = new JButton("Valider");
		validateButton.setName("Validate connection credentials");
		validateButton.addActionListener(uniguessController);
		validateButton.addActionListener(configurationController);
		this.getRootPane().setDefaultButton(validateButton);
		
		JButton closeButton = new JButton("Fermer");
		closeButton.setName("Close PronoGuessr");
		closeButton.addActionListener(uniguessController);
		
		buttonsPanel.add(validateButton);
		buttonsPanel.add(closeButton);
		
		return buttonsPanel;
	}
	
	public boolean isFormReady() {
		return !targetChannelTextField.getText().isEmpty()
			&& !databasePathTextField.getText().isEmpty()
			&& !botUsernameTextField.getText().isEmpty()
			&& !botPasswordTextField.getText().isEmpty();
	}
	
	@Override
	public void update(Observable observable, Object data) {
		if(data instanceof String[]) {
			String[] configurationData = (String[]) data;
			
			if(configurationData.length >= 4) {
				targetChannelTextField.setText(configurationData[0]);
				databasePathTextField.setText(configurationData[1]);
				botUsernameTextField.setText(configurationData[2]);  
				botPasswordTextField.setText(configurationData[3]); 
			}
		}
	}
	
	public String targetChannelValue() {
		return targetChannelTextField.getText();
	}
	
	public String databasePathValue() {
		return databasePathTextField.getText();
	}
	
	public String botUsernameValue() {
		return botUsernameTextField.getText();
	}
	
	public String botPasswordValue() {
		return botPasswordTextField.getText();
	}
}