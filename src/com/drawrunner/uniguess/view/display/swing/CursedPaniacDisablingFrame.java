package com.drawrunner.uniguess.view.display.swing;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;

import com.drawrunner.uniguess.control.BotManagerController;

public class CursedPaniacDisablingFrame extends JFrame {
	private static final long serialVersionUID = -6119710106794723022L;

	public CursedPaniacDisablingFrame(BotManagerController botManagerController) {
		Container contentPane = this.getContentPane();
		contentPane.setLayout(new FlowLayout());
		
		JButton disableOnceButton = new JButton("Juste pour cette fois");
		disableOnceButton.setName("Disable once");
		disableOnceButton.addActionListener(botManagerController);
		
		JButton disableForViewersButton = new JButton("Laisser pour les modos");
		disableForViewersButton.setName("Disable for viewers");
		disableForViewersButton.addActionListener(botManagerController);
		
		JButton disableForeverButton = new JButton("Pour toujours");
		disableForeverButton.setName("Disable forever");
		disableForeverButton.addActionListener(botManagerController);
		
		contentPane.add(disableOnceButton);
		contentPane.add(disableForViewersButton);
		contentPane.add(disableForeverButton);
		
		// Actualisation des dimensions de chaque �l�ment contenu dans la
		// fen�tre
		this.pack();
		
		this.locateWindow();
		
		// R�glage du titre et de la visibilit� de la fen�tre
		this.setTitle("D�sactiver Cursed Paniac...");
		
		// Ajout d'une lisaion entre le bouton Windows "Croix" de la fen�tre et
		// l'arr�t de l'application
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	private void locateWindow() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int posX = (int) (screenSize.width / 2f - this.getSize().width / 2f);
		int posY = (int) (screenSize.height / 2f - this.getSize().height / 2f);
		this.setLocation(posX, posY);
	}
}
