package com.drawrunner.uniguess.view.display.swing;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import com.drawrunner.uniguess.control.ConfigurationController;
import com.drawrunner.uniguess.control.UniguessController;

public class MongoUserCreationFrame extends ConfigurationFrame {
	private static final long serialVersionUID = -2024931871775769223L;
	
	private JRadioButton onLineDatabaseRadioButton;
	private JRadioButton offLineDatabaseRadioButton;
	
	private JLabel userNameLabel;
	private JTextField userNameTextField;
	
	private JLabel passwordLabel;
	private JPasswordField passwordTextField;
	
	private JLabel passwordConfirmLabel;
	private JPasswordField passwordConfirmTextField;
	
	public MongoUserCreationFrame(UniguessController uniguessController, ConfigurationController configurationController) {
		JPanel contentPanel = (JPanel) this.getContentPane();
		contentPanel.setLayout(new BorderLayout());
		
		Border contentBorder = BorderFactory.createEmptyBorder(8, 10, 4, 10);
		contentPanel.setBorder(contentBorder);
		
		JLabel titleLabel = this.buildTitle("J'ai besoin des identifiants d'acc�s � la base de donn�es :");
		
		JPanel formPanel = this.buildFormPanel(configurationController);
		JPanel buttonsPanel = this.buildButtonsPanel(uniguessController, configurationController);
		
		contentPanel.add(titleLabel, BorderLayout.NORTH);
		contentPanel.add(formPanel, BorderLayout.CENTER);
		contentPanel.add(buttonsPanel, BorderLayout.SOUTH);
		
		this.toggleUserCreationForm(false);
		
		// Actualisation des dimensions de chaque �l�ment contenu dans la
		// fen�tre
		this.pack();
		
		this.locateWindowAtCenter();
		
		// R�glage du titre et de la visibilit� de la fen�tre
		this.setTitle("Configuration de PronoGuessr");
		
		// Ajout d'une lisaion entre le bouton Windows "Croix" de la fen�tre et
		// l'arr�t de l'application
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	private JPanel buildFormPanel(ConfigurationController configurationController) {
		JPanel formPanel =  new JPanel(new BorderLayout());
		
		JPanel databaseModePanel = new JPanel(new BorderLayout());
		JPanel userCreationPanel = new JPanel(new GridLayout(3, 2));
		
		ButtonGroup connectionModeCheckboxGroup = new ButtonGroup();
		
		onLineDatabaseRadioButton = new JRadioButton("Base de donn�es en ligne (mLab par ex.)", true);
		onLineDatabaseRadioButton.setName("Online database");
		onLineDatabaseRadioButton.addActionListener(configurationController);
		
		offLineDatabaseRadioButton = new JRadioButton("Base de donn�es locale (h�berg�e sur le PC)", false);
		offLineDatabaseRadioButton.addActionListener(configurationController);
		offLineDatabaseRadioButton.setName("Offline database");

		connectionModeCheckboxGroup.add(onLineDatabaseRadioButton);
		connectionModeCheckboxGroup.add(offLineDatabaseRadioButton);
		
		userNameLabel = new JLabel("Nom d'utilisateur : ");
		userNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);

		userNameTextField = new JTextField("pronoguessr");
		
		passwordLabel = new JLabel("Mot de passe : ");
		passwordLabel.setHorizontalAlignment(SwingConstants.RIGHT);

		passwordTextField = new JPasswordField();
		
		passwordConfirmLabel = new JLabel("Confirmation du mot de passe : ");
		passwordConfirmLabel.setHorizontalAlignment(SwingConstants.RIGHT);

		passwordConfirmTextField = new JPasswordField();

		formPanel.add(databaseModePanel, BorderLayout.NORTH);
		
		databaseModePanel.add(onLineDatabaseRadioButton, BorderLayout.NORTH);
		databaseModePanel.add(offLineDatabaseRadioButton, BorderLayout.SOUTH);
		
		formPanel.add(userCreationPanel, BorderLayout.SOUTH);
		
		userCreationPanel.add(userNameLabel);
		userCreationPanel.add(userNameTextField);
		
		userCreationPanel.add(passwordLabel);
		userCreationPanel.add(passwordTextField);
		
		userCreationPanel.add(passwordConfirmLabel);
		userCreationPanel.add(passwordConfirmTextField);
		
		return formPanel;
	}
	
	private JPanel buildButtonsPanel(UniguessController uniguessController, ConfigurationController configurationController) {
		JPanel buttonsPanel = new JPanel(new FlowLayout());
		
		JButton validateButton = new JButton("Valider");
		validateButton.setName("Validate mongo user creation");
		validateButton.addActionListener(uniguessController);
		validateButton.addActionListener(configurationController);
		this.getRootPane().setDefaultButton(validateButton);
		
		JButton closeButton = new JButton("Fermer");
		closeButton.setName("Close PronoGuessr");
		closeButton.addActionListener(uniguessController);
		
		buttonsPanel.add(validateButton);
		buttonsPanel.add(closeButton);
		
		return buttonsPanel;
	}
	
	public void toggleUserCreationForm(boolean state) {
		userNameLabel.setEnabled(state);
		userNameTextField.setEnabled(state); 
		
		passwordLabel.setEnabled(state);
		passwordTextField.setEnabled(state);
		
		passwordConfirmLabel.setEnabled(state);
		passwordConfirmTextField.setEnabled(state);
	}
	
	public boolean isFormReady() {
		if(onLineDatabaseRadioButton.isSelected()) {
			return true;
		} else if(offLineDatabaseRadioButton.isSelected()) {
			return !userNameTextField.getText().isEmpty()
				&& passwordTextField.getPassword().length > 0
				&& passwordConfirmTextField.getPassword().length > 0
				&& this.passwordMatch();
		} else {
			return false;
		}
	}
	
	public boolean passwordMatch() {
		char[] password = passwordTextField.getPassword();
		char[] passwordConfirm = passwordConfirmTextField.getPassword();
		
		if(password.length == passwordConfirm.length) {
			boolean passwordMatch = true;
			
			for (int i = 0; i < password.length && passwordMatch; i++) {
				if(password[i] != passwordConfirm[i]) {
					passwordMatch = false;
				}
			}
			
			return passwordMatch;
		} else {
			return false;
		}
	}
	
	public String userNameValue() {
		return userNameTextField.getText();
	}
	
	public String passwordValue() {
		if(this.passwordMatch()) {
			return String.valueOf(passwordTextField.getPassword());
		} else {
			return null;
		}
	}
	
	public boolean isInOnlineMode() {
		return onLineDatabaseRadioButton.isSelected();
	}
	
	public boolean isInOfflineMode() {
		return offLineDatabaseRadioButton.isSelected();
	}
}